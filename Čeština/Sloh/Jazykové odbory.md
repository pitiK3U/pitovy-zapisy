---
title: Jazykové odbory
---
fonetika a fonologie
====================

-   zvuková stránka

lexikologie
===========

-   slovní zásoba

3.  gramatika/mluvnice
    ==================

    a.  morfologie (skloňování, časování)
        ---------------------------------

    b.  syntax
        ------

4.  stylistika (sloh)
    =================

-   zabývá se pravidla pro tvorbu textů s ohledem na komunikační situaci
