---
title: Aktuální větně členění
---
Aktuální větně členění

1.  Věta oznamovací

-   sice máme volný slovosled, ale není úplně libovolný.

-   větu oznamovací můžeme dělit na 2 části

    -   Téma - východisko (o něčem mluvím)

    -   Réma - nová hlavní informace; to, co chceme o tématu sdělit; je
        > na konci oznamovací věty

Ten král měl tři dcery.

Král = Téma

tři dcery = Réma

K orbě se dodnes používá primitivních nástrojů v těchto zemí.

→ V těchto zemích se dodnes používá k orbě primitivních nástrojů.

-   Réma lze dát na začátek věty - musí se signalizovat - !

Tři dcery měl ten král!

Nepiju pivo. → (vytýkání) Pivo, to já nepiju.

2.  otázka

    a.  zjišťovací

        -   Byl jsi včera doma? - (viz věta oznamovací) réma na
            > **konci**

    b.  doplňovací

-   Kde jsi včera byl? - réma na **začátku**
