---
title: Souvětí
---
---
title: Souvětí
---
Souvětí

-   2 přísudky (v určitém slovesném tvaru) několikanás

-   → jednoduchá

> X

-   polovětná konstrukce

    -   slovesné přídavné jméno (těsný)

    -   přechodník

    -   příčestí trpné

podřadné

HV - VV -VV

Souřadné

HV - HV

VV VV

Vedlejší věta

-   nahrazuje větný člen (VV ⇔ větný člen)

-   ptáme se otázkou

-   typy

    -   přísudková VV (přísudek → VV; pouze slovesně jmenný se sponou)

        -   Obloha byla, **jako by** (kdyby) ji vymaloval.

Viděl jsem **ho**, jak vchází. / vcházet - **doplňková**

Viděl jsem, jak vchází. - **předmětná**

Každý, kdo přišel, obdivoval se té kráse. → příchozí / návštěvník -
**podmětná**

Kdo chce vidět Idiota, ať se dostaví do ředitelny. - **podmětná**

Proč děláš, že mě neznáš? - **předmětná**

Přišel, aniž by pozdravil. - **PÚZ**

Jelikož dlouho pršelo, řeka se vylila z břehů. - **PÚP** (už něco bylo →
)

Pavel prosadil své řešení bez ohledu na to, že bylo zdlouhavé. -
**Přípustková** (přestože bylo zdlouhavé)

Studuje na to, aby se mohl v budoucnu věnoval učitelské profesi. - **PÚ
účelová** (teprve bude)

Souřadné spojení

-   hlavní věty

-   i věty vedlejší které jsou závislé na větě nadřazené

-   větné členy

-   Poměr:

    -   slučovací + (a, i, nebo, 1x ani → nepíšeme , )

    -   odporovací x (ale, avšak)

    -   stupňovací /\\ (ba, dokonce)

    -   vylučovací (nebo → píšeme , )

    -   důvodový (**neboť** )

    -   důsledkový (a proto, a tak, a tudíž)

Řekni mi, co čteš, a já ti řeknu, kdo jsi.
