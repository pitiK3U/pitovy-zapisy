﻿---
title: Syntax
---
---
title: Syntax
---
-   odbor zabývající se tvorbou vět

-   věta = myšlenka a je ohraničená (jak v podobě mluvené tak i psané)

-   **Věta musí mít přísudek! **

-   Přísudek je sloveso v určitém slovesném tvaru.

-   **Výpověď** = existuje v reálné komunikaci, 
    --------------------------------------------

    -   ### věta

    -   ### větný ekvivalent

    -   ### **Elipsa** (výpustka přísudka)

        -   často v poznámkách (učební styl), titulky v publicistickém,
            > (Karel císařem)

-   1 přísudek ⇒ věta **jednoduchá**

-   více přísudků + větné členy ⇒ **souvětí**

Podmět Po
---------

-   ### **nevyjádřený** - Šel ven. ([on]{.underline})

-   ### **neexistuje = bezpodmětové/jednočlenné** - Prší. Je pět hodin. Bolí mě v krku.

Přísudek Ps \~
--------------

-   **slovesný**

-   **slovesně jmenný** (jmenný se sponou) - Je hezký. Je hezky. Je šest
    hodin.

<!-- -->

-   **jednoduchý** -

    -   Byl jsem doma.

-   **složený** -

    -   složený jenom tehdy když máme sloveso způsobové (muset,) nebo
        > fázové()

    -   Musel jsem být doma.

    -   Učitel by měl být spravedlivý.

-   Podmět + přísudek → základní skladební dvojice

 

Rozvíjející větné členy
-----------------------

-   ### Přívlastky Pk \...

    -   Přívlastek rozvíjí podstatné jméno

        -   Shodné - (nový klíč; skloňuje se s podst. jm.; nejčastěji
            > vyjádřen příd. jm.; **město Brno** - vyjimka)

        -   Neshodné - neshodné s podstatným jménem (Kunderův román
            > Nesmrtelnost)

-   ### Předmět Pm \-\--

    -   závisí na **slovese** (přídavné jméno)

    -   ptáme se na něho pádovou otázkou

Učím se hrát na klavír.

-   ### Příslovečné určení Pu - . - . - .

    -   závisí na slovese, příd. jméně, příslovci

    -   můžeme se zeptat jinak než pádovou otázkou

    -   dělí se:

        -   místa

        -   času

        -   způsob

        -   míry

        -   příčiny

        -   účelu

<!-- -->

-   ### Doplněk D xxx

    -   závisí na jménu a současně slovesu

    -   vyjádřen nějakým jménem

Vrátil jsem se ze školy **unavený**. (Po - já)
