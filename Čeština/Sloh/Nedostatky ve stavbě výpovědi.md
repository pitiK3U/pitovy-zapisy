---
title: Nedostatky ve stavbě výpovědi
---
1.  KONTAMINACE

-   záměna (slovesné vazby, použijeme jinou vazbu ke slovesu; \* něco co
    je špatně)

\*Osočil se ~~na~~ mě.

2.  ZEUGMA \[zeugma; zeugmatu\]

-   zanedbání dvojí vazby -

\*Upomínat a naléhat na vládu. → Upomínat na vládu a naléhat na ni.

3.  ANAKOLUT

-   vyšinutí z vazby, vykolejení, souvětí někam začneme a končíme jinak

\*Chladným a deštivým počasím \... nás nepříjemně zaskočilo. \--

4.  ATRAKCE

-   nevhodné přispůsobení pádu (u podst. jm.)

\*Vzhledem k šířce vozovce. → šířce vozovky.

\*Před sluncem západem → před slunce západem

\*Pokud se týká o výkony naše týmu... - KONTAMINACE (/\...týka
výkonu\...)

\*V tanečních se poprvé setkal a oslovil svou příští přítelkyni. -
ZEUGMA

\*Tyto negativní jevy nás nepoložily na kolena. - KONTAMINACE (nesrazily
na kolena/nepoložily na lopatky)

\*Režisér nechtěl, aby jeho film oslavoval a mluvil o hrdinství. -
ZEUGMA

\*Rovnoprávností ženy, když je zaměstnaná sotva lze zajistit. - ANAKOLUT
(zajistit rovnoprávnost/ rovnoprávnosti dosáhnout)

\*Japonky prohrály výborně hrajícím Američankám. - KONTAMINACE
(proti/podlehly)
