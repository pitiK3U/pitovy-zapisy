---
title: vztahy mezi větnými členy
---
vztahy mezi větnými členy

I.  formální

-   týkají se tvaru slov - morfologie

    A.  souřadnost - **parataxe** (mezi několika větnými členy)

<!-- -->

-   Hraju fotbal a tenis. \-- **ve stejném pádě**

    B.  shoda - **kongruence**

<!-- -->

-   **Po + Ps** mezi Podmětem a Přísudkem

-   **Pks + podst** mezi Přívlastek shodným a podstatené jméno

    C.  řízenost - **rekce**

<!-- -->

-   Předmět - je to dáno

-   Přívlastek neshodný ve 2. pádu

    D.  přimykání - **adjunkce**

<!-- -->

-   Příslovečné určení

-   Přívlastek neshodný

II. významové

    E.  mezi podmětem a přísudkem je přisuzování - **predikace**

    F.  mezi nadřízeným a podřízeným je **determinace** (podst. jm. -
        > přívlastek nesh.; **v případě adjunkce a rekce**)

    G.  několikanásobné větné členy - **koordinace**

    H.  přistavování - **apozice** (lev, král zvířat)
