---
title: Česká meziválečná próza
---
[[Česká poezie v meziválečném
období]{.underline}](https://drive.google.com/open?id=1ORMTO_jtpDFpoq6AfrP1h_i880r0mkXgDzNp0tMLazM)
Předchozí \| Další [[České divadlo v meziválečném
období]{.underline}](https://docs.google.com/document/d/1I9w6A5DpPs65G3pYvst0YSnkkvGu5DBz9lhf-WRv5xs)

ČSR 1918 - 1938 (39)
====================

MNOHO PROUDŮ, rozdělená literatura :

1.  literatura , která reagovala na [1.sv. válka](./1.sv. válka.html) 

    a.  legionářská (Medek, Kopta, Langer)

    b.  protiválečná - [[J.
        > **Hašek**]{.underline}](https://drive.google.com/open?id=1iggVUUoIKn-hLtgHgjV7hzPB-hOeY0re1Nm1XTuNh9s), 
        > ***Švejk***

    c.  lit. pod vlivem
        > [**[expresionismu]{.underline}**](https://drive.google.com/open?id=1tcoDI5FuBfUTX4riYOAgFP5wKvrPfYFM_ea287QRU7g) - 
        > Literární skupina - Lev Blatný - Richard Weiner

2.  [**demokratický**](#demokratický-proud), Lidové noviny

    d.  K. **Čapek**, Josef, Karel Poláček, **Eduard Bass**

3.  levicově,
    [**[komunisticky]{.underline}**](#levicový-proud---komunistickýsocialistický)
    orientovaní autoři

    e.  soc. realismus - **Ivan Olbracht**, Marie Majerová, Marie
        > Pujmanová

4.  [**[imaginativní]{.underline}**](#imaginativní-proud) literatura

    f.  **Vladislav Vančura**

5.  [**[psychologicky]{.underline}**](#psychologická-próza) orientovaná

    g.  Jarmila Glazarová, Jaroslav Havlíček

6.  [**[Katolická]{.underline}**](#katolická-próza) lit.

    h.  **Jaroslav Durych**

7.  **Ruralisté - ,,venkov"**

    i.  **agrární** strana, **konzervativní** pravicové názory, snaha
        > držet se tradic, ovlivněni **náboženstvím**, snaží se
        > idealizovat venkov - Josef Knap, František Křelina → perzekuce
        > a vězněni

Demokratický proud
==================

Karel Čapek (1890 - 1938)
-------------------------

-   překladatel, spisovatel, dramatik a žurnalista

-   pocházel z Malých Svatoňovic u Trutnova v Podkrkonoší

-   Velký vliv otce, který byl venkovský lékař

-   sourozenci: bratr Josef a sestra Helena → ***Moji milí bratři***

-   studoval filozofii v Praze, v Berlíně a v Paříži, (na gymnázii v
    Hradci Králové, pak na Jarosce)

-   režisérem Divadla na Vinohradech

-   u zrodu spisovatelské organizace Penklub

-   → **Francouzská poezie nové doby**

-   manželka - herečka Olga Scheinpflugová

-   T.G.M. → pátečníci - scházeli se v pátek → ***Hovory s T.G.M.***

-   cestoval → cestopisy - Anglie, Itálie, Španělsko,

-   záliba v zahradničení - ***Zahradníkův rok***

-   ***Dášenka*** -

-   kritizoval symbolismus a dekadenci

-   **Umělecký měsíčník** → poté psal do **Lidových novin**

-   byl proti zmechanizování života, chtěl návrat k lidství, svobodnému
    bratrství, prostý život a hledání boha

### Prozaické

-   pragmatismus - lidská praxe, pravda je relativní

-   ***Povídky z jedné a druhé kapsy***

    -   detektivní zápletka - odlišné názory,

-   povídka ***Jasnovidec*** - s. 250, jak se projevuje čapkovská
    relativita pravdy a rozmanitost názorů; budeme charakterizovat hl.
    postavu (státní zástupce); proč je jazyk K. Čapka stále živý (něco
    co tlačí text k dnešku)

-   noetická trilogie - 3 romány

-   ***Hordubal*** - Podkarpatská rus, přišel z Ameriky, zjistil, že je
    > mu manželka nevěrná, je zabit a vyšetřuje se jeho vražda, 2 různé
    > pohledy

-   ***Povětroň*** - o letci, který havaruje, nemůže hovořit, 3 osoby se
    
> snaží rekonstruovat, co se mohlo stát
    
-   ***Obyčejný život*** -

utopická díla - sci-fi zápletka, jde o **etické** otázky, zda lidstvo
zvládne novou techniku, Je-li lidstvo schopno zvládnout tu techniku

- ***Krakatit***

  -   výbušnina, velké síly, je to podle sopky Krakatoa

  -   vynálezce ing. Prokop v horečce prozradí výrobu výbušniny,
      
      > dostane se do špatných rukou a Prokop se sna

-   [***[Válka s
    Mloky]{.underline}***](https://drive.google.com/open?id=1tGtpU4ssVYH0_oM1vYA2R6j2O2fTJpMMQOv2grhLu28)

    -   povinná četba

    -   román proti nacismu a fašismu a válce, dobývání území

    -   psáno výrazně publicistickým stylem, připomíná články z novin

    -   parodie odborného a administrativního stylu

### 2. Drama + J

-   ***Ze života hmyzu***

    -   alegorie, personifikace

    -   kritická hra

    -   motýli - povrchní mezilidské vztahy, povrchní láska

    -   chrobáci - hamižnost, chamtivost

    -   mravenci - poslužní, organizovaní, ochotni poslouchat bez odporu

- \+ J : ***Adam Stvořitel***

  -   narážka na Bibli

  -   nelíbí se mu svět, Bůh ho zničí a Adam začne svět stvořit

  -   stvoří ho horší - svět není dokonalý, ale nechají ho tak -
      
      > kritika komunismu

- ***R.U.R.***

  -   vynález robotů - sci-fi, uměle vytvoření, nemají city, jsou
      
  > zneužívání a vzpouří se a vyhubí lidstvo
      
  -   přežije pouze Alquist, při vzpouře Helena zničí recept na tvorbu
      
  > robotů,
      
  -   ze 2 robotů se vstávají lidé, protože k sobě pocítí lásku

-   ***Věc Makropulos***

    -   dlouhověkost, nesmrtelnost,

    -   dcera vynálezce vypije lektvar, E.M. - Emilia Martí

    -   je to prokletí - zničí se recept

    -   zhudebnil Janáček

-   30\. léta - reakce na fašismus

    - ***Bílá nemoc***

      -   neléčitelná nemoc, lékář, který na to má lék a léčí pouze
          
  > chudé,
          
      -   diktátor taky onemocní a chce lék, ale Galén mu dá lék pouze
      
      > pokud nevyhlásí válku,
      
      -   Marshall válku vyhlásí, protože to chtějí zfanatizovaní lidé

      -   Galén je pacifista

      -   Galéna rozdupe zfanatizovaný

    -   ***Matka***

        -   reaguje na ohrožení ČSR ze strany Německa

        -   ohlasy šp. obč. války
    
    -   matka má 5 synů a manžela a 4 syna už ztratila ve válce,
            
> nechce ho pustit do války
            
    -   bombardují školu a zabijí děti
        
        -   → matka nakonec pošle syna do války

 Karel Poláček
-------------

-   Rychnov nad Kněžnou?, V Čech

-   sbíral židovské anekdoty

-   redaktorem Lidových novin

-   soudničky - příběhy ze soudní síně

-   2\. světová- odvezen do Terezína \--\> Osvětim, zahynul v plynové komoře

-   humoristické a satirické dílo

-   dílo:

-   ***Muži v ofsajdu*** - metaforicky, dílo o 2 fotbalových fanoušcích,
    Viktoria Žižkov X Slavia

-   satirické dílo: - měla být pentalogie, ale je tetralogie, 5. se
    ztratil

    -   ***Okresní město*** - zobrazuje poměry na malém městě, humor,
        
    > který kritizuje - kritika mentality maloměšťáků - malý rozhled
    
-   ***Bylo nás pět*** - román z obd. 2. světové války, vzpomíná na své
    dětství, 5 chlapců, kteří prožívají různá dobrodružství na malém
    městě, hl hrdina - Péťa Bajza - vypravěč, vypráví z pozice chlapce,
    humorné situace, využívá jazykové komiky - snaží se vyjadřovat
    spisovně, ale nedodržuje všechny.., spojuje knižní a nespisovné
    prostředky,

Eduard Bass - Schmitt
---------------------

-   pocházel z bohaté rodiny, jeho otec byl továrník

-   z rodiny utekl, zajímalo ho lidové umění

-   utekl ke kabaretu Červená sedma - podle Jiřího Červeného (tanec,
    písně, scénky, zábavný podnik)

-   pracoval v lidových novinách

-   také psal soudničky

-   **rozhlásky** - veršované komentáře k aktuálnímu politickému dění

    -   ***Klapzubova jedenáctka*** - pan klapzuba má jedenáct synů
        
> \--\> udělá z nich družstvo, veliký úspěch, s nadsázkou
        
    -   *Cirkus Humberto* - román, hl. hrdina Vašek Karas udělal kariéru
        
        > v cirkusu, byl to Čech

3. Levicový proud - komunistický/socialistický
==============================================

Ivan Olbracht
-------------

-   vlast. jménem - Kamil Zeman

-   otec - Antonín Zeman - realistický spisovatel - ,,Antal Stašek" - S
    Čechy

-   matka židovka

-   redaktor, sociální demokracie → po rozpadu se přidá k levici -
    Komunista

-   po válce významné politické funkce

-   odešel 29 od komun. strany → pak se vrátil

-   dílo:

1)  soc. realismus

-   *Anna Proletářka* - ( \--Anna Sekretářka)

    -   mladá Anna přichází do Prahy sloužit v bohaté rodině, seznámí se
        
        > s mladým dělníkem Toníkem, který ji přivede ke komunismu

2)  psychologická - proniknout do psychologie postav

-   Gorkij - inspirace \_\_\_ - povídky *O zlých samotářích* - ras =
    živili se mrtvými zvířaty

-   *Žalář nejtemnější* - o bývalém policejním důstojníkovi, který
    oslepne, začne podezírat svoji manželku, protože nemá nad ní
    kontrolu, žárlí

- zajímala ho Podkarpatská Rus - ***Nikola Šuhaj Loupežník*** -

  -   slavný román, loupežník, původně voják, po světové válce se
      
  > stává loupežníkem,
      
  -   líčen jako hrdina, který bohatým bere a chudým dává,

  -   legenda mu připisuje legendární vlastnosti, silná lyrizace-

  -   muzikál Balada pro banditu - divadlo Husa na provázku

  -   baladické rysy

  -   motiv zrady - jeho milá Eržika - má poměr s českým četníkem,
      
  > Nikola ho zabije, zradí a zabijí ho kamarádi
      
  -   židé negativně zobrazení - bohatí

-   *Golet v údolí* - povídky z PR

    -   židé jsou zde zobrazeni pozitivně

    -   Golet = židovské osídlení

    -   3 povídky - ***O smutných očích Hany Karadžičové***

        -   zfilmováno - Hanele

        -   Prostředí ortodoxních židů,

        -   Hana odchází do Ostravy, protože chce vydělat peníze, aby
            
> mohli jet do Palestiny
            
-   sionismus - chudí židé věřili že se jim bude žít lépe
        
-   zamiluje se do ateisty - odpadlý žid
        
        -   Rodina X ateista - rodina ji zavrhne

Marie Majerová
--------------

-   anarchistka - znala Neumanna - *Nový kult*

-   proletářská próza

-   dílo:

-   román ***Siréna***

    -   osudy rodiny Hudců

    -   píše o hutnících na **Kladně** - žila zde od 12 let - nářečí

-   novela ***Havířská balada***

    -   obraz hornické bídy

    -   próza na princip Villonské balady

    -   hl. hrdina propuštěn za stávku → propadá do hluboké bídy

Marie Pujmanová
---------------

-   měla domácího učitele

-   manžel **Ferdinand Pujman** - operní režisér a hudení teoretik

-   dílo:

-   trilogie ***Lidé na křižovatce*** - *Hra s ohněm - Život proti
    smrti*

    -   zobrazuje českou společnost od 20.let přes hospodářskou krizi,
        
> okupaci republiky, zahraniční i domácí odboj až po osvobození
        
    -   inspirací k postavě podnikatele byl **T. Baťa** - popisuje
    
    > negativně
        
    -   dělnický hrdina

imaginativní proud
==================

Vladislav Vančura
-----------------

-   narodil se na **Opavsku** (v Háji)

-   spojen s městečkem Zbraslav - žil zde a provozoval **lékařskou**
    praxi

-   strýc **Antonín Vančura = Jiří Mahen**

-   odešel z komunistické strany - na protest proti Gottwaldovi

-   účastnil se schůzek **pátečníků**

-   zúčastnil se odboje → popraven v době nacismu

-   byl **režisérem**, silně ovlivněn filmovým uměním

-   představivost - jeho próza je silně ovlivněna poezií

-   **Devětsil** - básnické směry

-   těžké výrazy - archaismy i z jeho doby, lyrizovaný jazyk

-   proletářská poezie

    -   ***Pekař Jan Marhoul***

        -   pekař má dobré srdce a prodává i na dluh

        -   → sám na to doplatí, zkrachuje

        -   projevuje se **Vančurův jazyk**

        -   **biblické motivy**

- poetismus

  -   novela (povídka) ***Rozmarné léto***

      -   laděno **humoristicky** - poetismus

      -   **fiktivní** městečko - Krokovy Vary

      -   scházejí se 3 muži na plovárně - **Antonín Důra** - majitel
          
  > plovárny
          
      -   přijede cirkus, přijede **kouzelník** s asistentkou
      
      > artistkou Annou
          
  -   → všichni 3 muži se snaží navázat milostný vztah a nedaří se
          
      > jim to
      
      -   manželka Důry naváže milostný vztah s kouzelníkem aji s ním
      
      > chvíli žije v maringotce
      
      -   cirkus odjel a město se vrátí do normálu
      
      -   zfilmováno - **Jiří Menzel**
      
      -   **jazyková komika** - vznešená, těžká čeština

-   30\. léta

-   **slavné dílo:**

    -   ***Markéta Lazarová***

        -   historický román ze středověku, vyjadřuje se podobně jako
            
> film, **filmová řeč**
            
-   2 znepřátelené rody loupežníci/rytíři - Kozlík X Lazar
        
-   Lazar má dceru Markétu, Kozlík syna Mikoláše
        
-   válka mezi rody, Mikoláš zajme Markétu a líbí se mu
        
-   → znásilní ji (několikrát)
        
        -   byla zaslíbena Bohu - měla se stát jeptiškou, ale i přesto
    
        > se Markéta do Mikoláše zamiluje
            
-   další milostná dvojice - Alexandra (z rodu Kozlíků) a
            
        > Kristián (německý šlechtic), Kristiána zajmou
            
-   Alexandra je vášnivá, ale prudké povahy, pro Kristiána
            > přijede otec a odveze ho - Alexandra to bere za zradu a
            > zabije ho

        -   loupežníci pochytáni a zavražděni, ale ženy jsou těhotné a
            
        > rod pokračuje
    
        -   postavy jsou velmi vášnivé, tvrdé a prudké - Vančura k nim
            > vyjadřuje sympatie - žili plnohodnotným životem - (jeho
            > předkové byli loupežníci a rytíři)
        
-   používá svůj **složitý jazyk** - není používán humoristicky,
            > archaismy z té doby, **složitá souvětí**, složité metafory
            > přirovnání, oslovuje čtenáře, přerušuje ho, různé úvahy,
            > kamerové záběry
        
        -   zfilmováno - **František Vláčil**, slavný, těžký film

<!-- -->

-   *Konec starých časů*

    -   zfilmováno s Abrahamem v hl. roli

    -   hl. hrdina = ruský emigrant, dobrodruh

    -   různě si vymýšlí příběhy

    -   získá si dověrů zbohatlíků

<!-- -->

-   v době okupace

    -   *Obrazy z dějin národa českého*

        -   navazuje na Palackého, kapitola o **Kosmovi** - jak na to
            
> přišel, kde
            
-   inspirace kronik
        
-   snaha podpořit vědomí
        
        -   nedokončené, zatčen a popraven

5. Psychologická próza
======================

Jaroslav Havlíček
-----------------

-   pocházel z Podkrkonoší, **Jilemnice** - malé město, vliv na tvorbu

-   pracoval jako **bankovní úředník**

-   do lit. vstupuje pozdě (30. léta - kolem 40 let)

-   pracoval po nocích, horečnatě, nebavila ho práce

-   zemřel přirozeně v průběhu války

-   syn Zbyněk - poválečný surrealismus, básník

-   Charakteristika

    -   zobrazuje psychické nemoci a vliv na člověka

-   dílo:

- ***Petrolejové lampy***

  -   **román**, původní název - ,,*vyprahlé touhy*"

  -   film - odlišuje se od předlohy !!

  -   hlavní hrdinka - Štěpánka Kiliánová - schopná, není krásná =
      
  > nemůže se dlouho provdat
      
  -   emancipovaná na svoji dobu, umíněná

  -   vezme si svého bratrance Pavla Malinu = bývalý důstojník armády,
      
  > pohledný, mnoho ctitelek, Pavlovi jde i o peníze
      
  -   po svatbě se zjistí že Pavel má **Syfilis** - psychický rozpad,

  -   musí se vyrovnávat s rozpadem osobnosti

  -   metonymie - Svatá Anna, to byl pro Štěpku den pouti do městečka,
      
  > k židli matky Kiliánové, obložené peřinami.
      
  -   polopřímá řeč, vnitřní monolog

- ***Neviditelný***

  -   **ich forma,**

  -   **zfilmováno**

  -   hl. hrdina - Petr - inženýr chemie, ambiciózní, negativní hrdina

  -   chce nahoru - přižení se do továrnické rodiny - zjistí že jeho
      
  > manželka je psychicky postižená → zabije ji
      
  -   jediný chlapeček je psychicky postižený

  -   neviditelný = v rodině je strýc kterého rodina schovává,

-   ***Helimadoe***

    -   o vesnickém lékaři, má 5 dcer, He\|li\|ma\|do\|e - zkratky jeho
        
> dcer
        
-   dcery se střídají v domácích prací
    
    -   lékař je autoritářský a dcery do určité míry tyranizuje

Jarmila Glazarová
-----------------

-   vl. jm. Podivínská, pocházela z Čech, ale provdala se na sev. Moravu
    u Slezska

-   vlaza si lékaře, kulturní pracovník

-   vstoupila do literatury pomocí soutěži - napsala knihu ohledně
    soužití se svým manželem ***Roky v kruhu***

-   dílo:

    -   ***Roky v kruhu***

-   romány

    - ***Vlčí jáma***

      -   Ve Slezsku, hl hrdinka - mladá dívka Jana, je sirotek,
          
  > vychovává starší manželská dvojice - Robert a Klára
          
  -   Robert - ušlechtilý,
    
  -   Klára - zlá, mstivá, tyranské sklony
    
  -   Jana se do Roberta zamiluje
    
  -   → psychicky vyhrocená situace
    
  -   → končí smrtí jejich adoptivních rodičů → Jana se osvobozuje
    
-   ***Advent***
    
    -   Valašsko
    
        -   hl hrdinka - chudá žena Františka, má nemanželské dítě (muž
        
    > zahyne), odsuzována morálně
            
        -   vezme si bohatého sedláka, ale ten bohatý sedlák začne být
        > agresivní a zlý vůči jí a chlapečkovi, strašně
        > **pokrytecký** - dělá že je věřící
        
        -   Metod - chlapečkovi se to nelíbí a uteče z domu, Františka
        > **čeká** (jako Panna Maria) v noci v prosinci, vybavuje se
        > **retrospektivně,** co vše prožila
        
        -   chlapeček se vrátí, dojde k požáru, kde sedlák i s milenkou
            
            > zahynou → osvobození

6. Katolická próza
==================

Jaroslav Durych
---------------

-   studoval na **kněze**, vojenský lol**vojenský lékař**

-   **mistr kontrastu** - inspirován barokem

-   zajímal se o období 30. leté války - **Albrecht z Valdštejna** - že
    kdyby byl českým králem

-   Prozaik, básník, dramatik, publicista

-   Durych šel vždy proti proudu

-   vyhrocené a útočné postoje

-   katolík - negativní vztah k 1. republice - spor s **K. Čapkem** -
    obč. válka ve Španělsku - Čapek s Republikány × Durych obdivoval
    \_\_\_

-   dílo:

    - milostná mystická povídka ***Sedmikráska***

      -   2 hrdinové, nemají jména - on a ona, 7x se potkávají →
          
  > sedmikráska, zamilují se, rozejdou se, znovu se potkají
          
  -   symbolická povídka
    
  -   novela o lásce, nacházení, nenacházíme hned, mystika, krása
    
  -   ona - krásná, ctnostná
    
-   ***Bloudění***
    
    -   tzv. **Velká valdštejnská trilogie**
    
    -   krutý příběh lásky
    
    -   český protestant - Jindřich
    
    -   Andělka - katolička španělka
    
    -   v době 30. leté války
    
-   ***Rekviem***
    
    -   tzv. **Menší valdštejnská trilogie**- 3 povídky
    
    -   osud **Albrechta z Valdštejna**
    
    - ***Kurýr***
    
          -   kurýr veze dopis od A z V a je mu věrný v situace kdy je
          
          > vyzrazen
    
    -   *Valdice*
    
        -   švédská armáda zneuctí Valdštejnův hrob
        
            -   klášter → vězení
