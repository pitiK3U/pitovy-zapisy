---
title: Václav Hrabě básník, 60
---
Václav Hrabě básník, 60. léta, ve své době velmi populární \--\> stává
se mluvčím generace studoval učitelský obor (čeština a dějepis) jazzový
hudebník verše rozptýleně/časopisecky roptýleny \--\> později sbírky
hodně byly jeho verše recitovány vliv beatníků \--\> spojen s návštěvou
Ginsberga v československu zemřel v 25 letech na otravu plynem \--\>
,,Mácha v 60. let\" témata: láska, mírný odpor vůči společnosti = spor
generací = stáří × mládí, revolta dílo: sbírka Blues pro bláznivou holku
Stop-time obsah otázka lásky, generační spor, formální chybí interpunkce
volný verš není rým personifikace metafora přirovnání

Jaroslav Seifert
----------------

-   v komunistické straně → kvůli Gottwaldovi vystoupil →

-   po válce vstupuje do literatury jako významný a uznávaný autor

-   v 50. letech je jeho literatura kritizována oficiální → nepodporuje
    režim

-   v 60. letech se vrací do poezie → proměna jeho **poetiky** = způsob
    jakým píše

-   68 - odsoudil okupaci → zakázán

-   v 77 podepsal **chartu** = kritika porušování lidských práv → úplně
    přestal vydávat

-   → režim se tvářil, že vydává pouze v meziválečné době

-   v **1984** získal **Nobelovu cenu** - také ocenění jeho občanských
    názorů

-   v 89 pochází poslední jeho sbírky

-   dílo:

    -   báseňská skladba ***Píseň o viktorce***

        -   Viktorka z *Babičky*

        -   Seifert počítá s tím, že čtenář o příběhu ví

        -   **oslava lásky**, pochopení milenců

        -   propojuje s osudem **B. Němcové** - nešťastná láska

-   50\. léta - kritika

-   básnická sbírka ***Maminka***

    -   kvůli kritice uniká do svého dětství

-   60\. léta

-   sbírka ***Koncert na ostrově***

    -   přelom v jeho poetice

    -   návrat k 2. sv. válce

-   70+80. léta (ze 3 stačí umět 2)

-   sbírka ***Morový sloup***

    -   ***A sbohem*** - chtěl se rozloučit, ale ještě žil

-   sbírka ***Vetešník z Piccadilly***

-   sbírka ***Býti básníkem***

-   dříve

    -   pravidelnost

    -   písňovitost

    -   refrén = opakování

×

-   později

    -   nepravidelný

        -   rytmus

        -   rým

        -   → prozaizace

-   **jednoduchými slovy umí vyjádřit hluboké myšlenky**

-   motivy

    -   bilancování života

    -   vzpomínky i na přátele, jiné umělce, ...

    -   Praha a její krása

    -   láska, erotické motivy, krása dívek

Jan Skácel
----------

-   Moravský básník

-   pochází se Strážnicka (Jihovýchodní morava)

-   spojen s **Brnem** - redaktor časopisu **Host do domu** (podle
    sbírky Walkera)

-   autorem **rozhlasových fejetonů** - ***Jedenáctý bílý kůň***

    -   Kde je moravská hymna? → pauza mezi českou a slovenskou hymnou

-   celoživotní přátelství s **Oldřichem Mikuláškem** (manželka →
    později manželka Skácela)

-   po r. 68 **zakázán** → částečně uvolněn v 80. letech

-   zemřel brzy před 89 - StB na jeho pohřbě

-   dílo:

-   60\. léta

    -   sbírka ***Smuténka*** - **autorský neologismus**

    -   sbírka ***Metličky***

-   70\. léta

    -   sbírka ***Dávné proso*** - lidová poezie, zdramatizováno
        > divadlem Na provázku

-   80\. léta

    -   sbírka ***Kdo pije potmě víno***

-   charakteristika

    -   méně srozumitelná, poezie filosofující, meditativní

    -   **moravské motivy** - památky a krajina, silný motiv vína

    -   motiv **času** - zamýšlí se nad plynutím času, pomíjivost ×
        > věčnost

    -   motiv **dětství** = symbol čistoty, nevinnosti, štěstí

    -   **miluje stručnost**

    -   **nedořečenost** = zdá se nám že něco chybí

    -   motiv **ticha**

    -   ovlivněn východní poezií (Haiku)

    -   pravidelný i nepravidelný verš - spíš nepravidelný

    -   rým ano i ne, spíš ne

    -   uprostřed verše mezera a následuje velké písmeno
