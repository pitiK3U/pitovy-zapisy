---
title: Májovci
---
> Předchozí \| Další
> [[Ruchovci]{.underline}](https://drive.google.com/open?id=1_rW1y0Om0SUkJzNODf2km90Wr8I8gWNQA0EVAiCF9eM)

V roce 1858 vydání almanachu **Máj**

Přijímali Máchovu romantiku, kvůli tomu, že byl světově známý

\\Vítězslav Hálek

Jan Neruda

Adolf Heyduk

Rudolf Mayer

V Šolc

K Světlá

J Arbes

Starší autoři, kteří přispěli do ...:

> Němcová
>
> Erben
>
> Karel Sabina - revoluční romantici
>
> Josef Václav Fryč - revoluční romantici

Hálek

-   Lyrický básník, prozaik, novinář

-   Ze středních čech

-   Rodiče chtěli aby studoval teologii

-   Dobře se oženil - šťastné manželství

-   Ve své době více ceněný než Neruda

-   Psal do **Národních listů**

-   Poezie je **optimisticky laděná**

-   **Lyrika**

    -   **Večerní písně** - přírodní lyrika

    -   **V přírodě** - přírodní lyrika

-   Epika

    -   Pohádky z naší vesnice - kritika

-   Povídky - vesnický realismus

-   problémy: sobectví (nesmí být bohatí a chudí), vejminek - neúcta ke
    > starším, odnusouvali sem starší

    -   Muzikantská Liduška

        -   Příběh nešťastné dívky, která si nemohla vzít koho si
            > chtěla - muzikanta

    -   Na vejminku

        -   Příběh mezi starým otcem a jeho sobeckými dětmi

    -   Poldík rumař

        -   Příběh o podskalském pískaři, jenž se obětavě stará o
            > chlapce ženy, kterou kdysi nešťastně miloval

Jan Neruda 1834 - 1891

-   Básník, prozaik, dramatik, žurnalista

-   **Národní listy**

-   Narodil se v Praze v rodině vojenského vysloužilce

-   Později se přestěhovali do Ostruhové ulice (dnes Nerudova)

-   Chodil do německé školy

-   Neoženil se ale byl v nešťastné lásce

-   První láska **Anna Holinová -** rodiče nechtěli vdát protože byl
    > chudý

-   Další **Karolína Světlá** - byla vdaná

-   Anička Tichá

-   Poezie:

    -   ***Hřbitovní kvítí***

        -   První básnická sbírka

        -   Byla kritizována - byla pesimistická -\> přestal na 10 let
            > básnicky vytvářet

        -   **Motiv hřbitova**, **smrti**

        -   Sociální témata

    -   ***Knihy veršů***

        -   *Kniha veršů výpravných*

        -   *Kniha veršů lyrických a smíšených*

        -   *Kniha veršů časových a příležitostných*

        -   Básně věnované - matce, otci, Aničce

        -   **Sociálně laděná balada**

        -   *Před frontou milosrdných*

            -   Dělali chudé nemocnice, ústavy

            -   Vyčerpaný dědeček nemá ani sílu zazvonit

            -   **Rodina se nemohla starat o staré, -\> odkázání na
                > ústavy**

        -   *Dědova mísa*

            -   Děda rozbije mísu

            -   Dostane koryto od syna -\> ponižující

            -   **Pointa -** mladý vnouček si taky dělá koryto pro
                > otce - uvědomí si jak se chová

        -   *Vším jsem byl rád*

            -   Neruda začne uvažovat pozitivně

    -   ***Písně kosmické ***

        -   Druhé období básnické tvorby Nerudy

        -   **Pozitivně laděné**

        -   Moderní vědy a dění vesmíru

        -   Humor

    -   ***Balady a romance***

        -   Humor a optimismus, ironie

        -   Neruda

        -   **Balady neodpovídají definici balady = ironie**

        -   *Balada o polce*

        -   ***Balada helgolandská***

            -   Je jiná než ostatní = tragický konec

            -   Na ostrově ztroskotá loď a muž ji jde z chamtivosti
                > vykrást a zjistí že tam umřel jeho zeť

        -   ***Romance o Karlu IV.***

            -   Je šťastná

            -   Karlovi nechutná nejdříve české víno ale později jo

            -   Metafora vztahu Karla IV. k českému národu

    -   ***Prosté motivy***

        -   O běhu lidského života

        -   Neruda stárne - vyrovnává se se svým věkem

        -   Prosté motivy

    -   ***Zpěvy páteční***

        -   Vydal Jaroslav Vrchlický

        -   Obavy o osudu národa (nacionalismus)

        -   **Náboženské motivy -** Velký pátek, vzkříšení

        -   ***Jen dál***

<!-- -->

-   **Fejetony:**

    -   V novinách a knižně

    -   Krátké vtipné

    -   ***Kam s ním?***

        -   Potřebuje se zbavit starého slamníku a nejde mu to

    -   ***1.máj 1890***

        -   První oslava dělnické práce, spíše reportáž

-   Prozaické sbírky

    -   **Arabesky**

        -   Zachycují různé zajímavé lidi

        -   ***Byl darebákem***

            -   *Psal o sobě*

    -   ***Povídky malostranské***

        -   **Kritizuje pověry, maloměšťanství**

        -   ***Hastrman***

            -   Starší pán sbírá kameny a myslí že jsou vzácné

            -   ale zjistí že jsou bezcenné

            -   myslí že ho rodina nebude mít ráda

            -   Ale rodina ho má ráda = happyend

        -   **Přivedla žebráka na mizina**

            -   **Téma pověry -\>** mohou vést ke zločinu

            -   Vypráví v ich-formě, nemusí být postavou aby byla ich
                > forma

            -   Popisy = realismus, kontrast mezi šťastným začátkem a
                > smutným koncem

Karolina Světlá

-   Představitelka **venkovské i městské prózy**

-   Narodila se v bohaté německé obchodnické rodině Rottových (původní
    > jméno Johanna Rottová)

-   S vlasteneckou společností přišla až ve 14 letech

-   Její domácí učitel byl **Petr Mužák** a seznámil jí s **českým
    > prostředím** a **českou literaturou**

-   Později se vdala za jejího učitele, nešťastně se do ní zamiloval
    > Neruda

-   Inspirovala se **George Sandovou** a **Boženou Němcovou**

-   Její pseudonym **Karolina Světla** přijala podle rodiště svého
    > muže - podještědské vesnice, kam jezdívala na letní pobyt

-   Znaky:

    -   Ženy jsou hrdinky

        -   Ženy vzdělané

        -   Morální, mravná - může muže změnit k lepšímu, často řeší
            > otázku věrnosti

-   Dílo:

    -   ***Černý Petříček*** - povídka z pražského prostředí ze života
        > obyčejných lidí

    -   ***Kříž u Potoka***

        -   Evička je hlavní postavou románu

        -   Mlynářka jí vypráví příběh o zakletí rodiny Potockých

    -   *Vesnický román*

    -   *Kantůrčice*

Vávlav Šolc a Rudolf Mayer - almanach Máj, umřeli mladí

Adolf Heyduk

-   Žil dlouho, učitel na univerzitě, cestoval - Slovensko

-   Téměř 60 sbírek básní

-   ***Cimbál a husle***

Jakub Arbes

-   Osobitý prozaik a publicista

-   Pocházel ze Smíchova - sympatizoval s dělníky

-   Za své názory vězněm

-   Sociální romány - zastaralé

-   **Romaneta**

    -   **Novela** (velký spád děje)

    -   Na začátku stojí romantická záhada

    -   Záhada se odhaluje racionálním postupem

    -   Podobné jako sci-fi

-   ***Ukřižovaná***

-   ***Newtonův mozek*** - 2 motivy - získává mozek Newtona, stroj času

-   ***Svatý Xaverius***

    -   **ich-forma**

    -   Obraz sv. Xaveria v malostranském chrámu sv. Mikuláše(malíř
        > Balka),

    -   mladík Xaverius se snaží obraz rozluštit a najít poklad

    -   Omylem založí oheň a ocitne se ve vězení

    -   Příběh vypráví novinář, který se s hledačem setkává ve vězení a
        > tam hledač umírá
