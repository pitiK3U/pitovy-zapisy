---
title: Baroko v české literatuře
---
Baroko v české literatuře

-   hl. vliv politický vývoj - bitva Na Bílé hoře → konec náboženské
    svobody (protestanté museli odejít)

    -   katolický - česky nebo latinsky, později německy, domácí proud,
        > náboženská lit., legendy, životopisy svatých, náboženská
        > dramata, duchovní písně - **kancionály**, **postila** -
        > kázání, **vliv jezuitského řádu**

    -   protestantský - psán v exilu (exulanský)

Katolický - domácí

Bedřich Bridel 1618-1680

-   jezuita, řídil tiskárnu, psal česky

-   ***Co bůh? Člověk?*** - básně, sbírka, duchovní téma

Bohuslav Balbín 1621-1688

-   historik, básník, dramatik, psal latinsky

-   ***Rozmanitosti z historie království českého*** - geografické
    > encyklopedické dílo

-   ***Rozprava na obranu jazyku slovanského zvláště pak českého*** -
    > obrana národního jazyka

Felix Kadlinský 1613-1675

-   ***Zdoroslavíček*** - básnická sbírka duchovní poezie

Antonín koniáš

-   pálil protestantské knihy

-   ***Koniášův klíč*** - soubor zakázaných knih

Adam Michna z Otradovic

-   hudební skladatel a varhaník, autor mnoha duchovních písní

-   ***Chtíc aby spal***

Exulanský

Jan Amos Komenský 1592 1670

-   narozen na Moravě

-   vzdělání v bratrských školách, Heidelberg, Herborn

-   1614 se vrací na Moravu, knězem, učitelem v Přerově, knězem ve
    > Fulneku, po r. 1621 se skrývá u Karla staršího ze Žerotína,

-   při útěcích přišel o knihovnu, potom kvůli moru přišel o ženu a 2
    > děti →

-   série ***Útěšných spisů*** - ***Labyrint světa a ráj srdce*** →
    > věnoval Karlu s. ze Ž., ***Truchlivý*** - spis, ***Hlubina
    > bezpečnosti***;

-   1628 - prchá z vlasti do polského Lešna, učil na Gymnáziu,

-   ***Brána jazyků otevřená***

-   1641 - Londýn, pozvání anglického parlamentu, 1642 - Švédsko za
    > přítomnosti královny se sešel s **René DesCartes**

-   1648 - Konec války - vestfálský mír → panovník určoval náboženství →
    > Komenský se nemůže vrátit, vrací se ze Švédska do Lešna, umírá 2.
    > žena

-   jednota bratrská se rozpadá - ***Kšaft umírající matky Jednoty
    > bratrské*** - (duchovní) závěť, matka věřících, J. A. K. poslední
    > biskup

-   1651-54 v Maďarsku v Sáros potoku - pozván kvůli reformě školství,
    > škola \_\_\_ na 7 let - neúspěch vrací se do Lešna, požár lešna →
    > ztráta domova a knih, které nebyly vydány

-   Slovník ***Poklad jazyka českého***, šel do Amsterdamu, Rumbert -
    > malíř, dr. Tulp,

-   ***Opera didaktika omnia*** - veškeré spisy didaktické (43)

-   ***Vše náprava*** - velké dílo, ***Jedno Potřebné*** - teologická
    > myšlenka, myšlenková závěť lidstva, snažil se napsat co shořelo,
    > 1670 umírá, pohřben v naardenu

<!-- -->

-   ***Labyrint světa a ráj srdce***

    -   satirická skladba, poutník putuje městem, hledá životní
        > povolání, Mámení a Všudybud, poutník nachází pravé štěstí v
        > mystickém přimkmění k Bohu

-   ***Kšaft umírající matky Jednoty bratrské***

    -   matka Jednoty bratrské odkazuje svým dcerám v cizině své
        > duchovní vlastnictví, odkazuje touhu po vyspělosti a pravdě,
        > důraz na vzdělání, láska k vlasti, láska k nateřtině

Didaktická díla:

-   ***Informatorium školy mateřské*** - děti do 6 let s matkou,
    > příručka jak vychovávat do 6 let

-   ***Brána jazyků otevřená*** - učebnice latiny, encyklopedie, v té
    > době revoluční, popisovala \_\_\_

-   ***Velká didaktika*** - latinsky, 1. systematické dílo o výchově a
    > vzdělání, položil základ pedagogiky jako vědy, myšlenky; právo na
    > vzdělání pro každého, vzdělání bezplatné, má se odvíjet od žáků,
    > definoval školní rok → prázdniny

-   ***Orbis pictus*** - ,,svět v obrazech", učebnice jazyků s
    > encyklopedií, bohatě ilustrovaná, několika jazyčná

spisy pansofické:

-   ***Všeobecná náprava o věcí lidských*** - pro člověka je dosažitelné
    > všechno poznání a vzdělání pro ukončení válek a všeho zlého
