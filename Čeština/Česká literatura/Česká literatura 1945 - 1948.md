---
title: Česká literatura 1945 - 1948
---
[[Absurdní
drama]{.underline}](https://drive.google.com/open?id=17sPxm0st8a7AkdeFgTzMulN6PErJMsSfFHj0yqg101g)
Předchozí \| Další [[Česká literatura
1948-1956]{.underline}](https://docs.google.com/document/d/13rv_CIX0MNXF3VEzyOQxjspx1xHhGk8cSe_4W5eYIxE)

Česká literatura 1945 - 1948

-   1945 - osvobození

-   1948 - komunistický převrat

-   zážitek války, osvobození

-   doba **nadšení**, **optimismu**

-   doba demokratického systému (přestože je omezen)

Poezie
======

-   oslavují **osvobození**, **SSSR**

-   Vladimír **Holan**

×

-   Jan **Zahradníček** - dívá se **pesimisticky**

-   skupiny:

-   **Ohnice**

    -   navazuje na Ortena

    -   pesimisticky orientovaná

Skupina 42
----------

-   zajímá je život ve městě, život okolo básníka

-   **Jindřich Chalupecký**

### Jiří Kolář

-   skupina **42**

-   autodidakt = **samouk**, neměl vyšší vzdělání, sám se vzdělává

-   manuální profese, stal se spisovatelem

-   **výtvarník** - dělal **koláže**, **muchláže** - zmuchlali papír

-   po 48 byl autor nežádoucí → dokonce byl **uvězněn**, na základě
    textů, které si připravoval, na udání

-   byl propuštěn → emigroval → žil ve Francii

-   po 89 se vrací do Československa

-   charakteristika

    -   **deníkový charakter**- zajímá ho plynutí času

-   dílo:

    -   ***Dny v roce***

    -   ***Roky ve dnech***

    -   ***Prométheova játra***

        -   byl za to uvězněn

        -   básnická sbírka + prozaické texty, úvahy, recenze

        -   Prometheus byl potrestán, že ukradl bohům oheň, pták by jedl
            > játra == alegorie

        -   vzniká po komunistickém převratu → reflektuje po únorové
            > období

        -   charakteristika

            -   \...

### **Ivan Blatný** - spojen s Brnem

-   syn [[Lva
    Blatného]{.underline}](https://drive.google.com/open?id=1okMWHMJtKLyuVb1ZqCKviuk8T10-mjRe_fIuEm66LD0)
    → Brno, lit. skupina expresionismus

-   → Ivan brzy osiřel → velký vliv, brzy vstupuju do literatury

-   sbírky vychází v období poválečném

-   po únoru 48 → emigroval do Británie

-   stesk a jiné problémy → psychické choroby → v psychiatrické léčebně→
    zdravotní sestra sbírala a poslala **Škvoreckému** do Kanady

-   dílo:

    -   sbírka ***Tento večer***

    -   sbírka ***Melancholické procházky***

        -   procházky po Brně, různé dojmy z poválečného Brno

-   z psychiatrické léčebny

    -   sbírka ***Stará bydliště***

    -   sbírka ***Pomocná škola v Bixley***

### Josef Kainar

-   autor strašně známé básně = ***Stříhali dohola malého chlapečka***

-   nesmírně talentovaný

-   patřil ke skupině 42

    -   ***Nové mýty*** (mythy)

        -   poválečná sbírka

        -   zabývá se typickými věcmi skupiny 42

        -   zároveň ho zajímají mikro příběhy

        -   ***Stříhali dohola malého chlapečka***

            -   symbol = **začínají mu různé povinnosti** **,začíná
                > pomalu, ale jistě dospívat**

            -   zhudebnil Vladimír Mišík

-   v 50. letech **zůstává v čs**, **podřizuje tvorbu komunistickému
    režimu**

    -   ***Český sen***

        -   básnická sbírka

        -   vrací se k zážitkům z války

        -   ***Taková láska***

            -   o dvou starých lidech

            -   starý pán kupuje kytičku staré paní

            -   měli 2 děti, které zahynuli v osvětimi

-   60\. léta = uvolnění; **inspirace** **jazzem**

    -   ***Moje blues***

        -   básnická sbírka

        -   blues = pomalá, smutná jazzová hudba

        -   mění se jeho poetika = **používá nerýmovaný a volný verš**

        -   objevuje se **obecná čeština**, psáno spisovně, ale hodně
            > obecná čeština

-   zhudebnil Mišík

-   ***Starý pán kupuje kytičku***

František Hrubín
----------------

-   básník, dramatik

-   po roce 45 vychází jeho nejvýznamnější díla, ale tvořil již před
    válkou

-   pocházel z Prahy, ale jeho rodina pocházela z Lešan - Posázaví →
    krajina kolem Lešan výrazně ovlivnila jeho tvorbu

-   30\. léta - tvoří pod vlivem **spirituální poezie**

-   40\. léta - reaguje na **osvobození** - píše kvalitní díla

-   50\. léta - působí na něj tlak komunistického režimu → uniká k **dětské
    poezii**

-   **1956** - sjezd spisovatelů - uvolnění režimu po smrti Stalina → F.
    Hrubín s J. Seifertem se **zastávali pronásledovaných a nevydávaných
    autorů**

-   60\. léta - **drama**

-   na konci života neúspěšně bojoval s alkoholismem

-   dílo:

-   30\. léta ovlivněna křesťanstvím = **františkánská doba** = jednoduchost,
    pokora, skromnost

    -   bás. sbírka ***Země sudička***

    -   bás. sbírka ***Včelí plást***

-   40\. léta

    -   bás. skladba ***Jobova noc***

        -   osvobození

        -   měla být recitována v D46

        -   biblický motiv = **Job** → přesmyčka slova **boj**

        -   ukázka pro lidi, jestli mají bojovat proti nacismu nebo se
            > schovat

        -   ústřední motiv **Země**

        -   používá lidovou píseň - **Ach Čechy krásné, Čechy mé**

        -   **refrén**, **variace**, opakování

        -   působivá, citová

-   bás. skladba ***Hirošima***

    -   varuje před atomovou bombou

-   50\. léta - **pohádky, které jsou veršované**

    -   ***Špalíček veršů a pohádek***

        -   veršované pohádky, velmi známé

        -   ,, princeznička na bále poztrácela korále "

    -   ***Romance pro křídlovku***

        -   zfilmováno = lyricko-epická básnická skladba

-   drama - pod vlivem slavného ruského dramatika **Čechova** - silná
    lyrizace, mezilidské vztahy

    -   ***Srpnová neděle***

        -   odehrává se v J. Čechách, na venkov přijížděli z města Prahy
            > na ,,letní byt"

        -   **Šrámkovské** motivy

        -   paní Mixová tráví léto na vesnici, **nešťastné manželství**

        -   milenec Morák za paní Mixovou přijede na návštěvu

        -   → má pocit, že jí chce něco říct

        -   → **řekne, že se musí vzít někoho jiné protože má dítě**

        -   → snaží se flirtovat s ostatními muži

        -   → paní Mixová skočí do rybníka s **úmyslem se zabít**

        -   → je tam bahno - volá ,,Je tu bahno, je tu bahno" - symbol

        -   Morák = neschopný, cynický, neupřímý, nemorální

        -   **mezilidské vztahy**

    -   ***Křišťálová noc***

### 

Próza
=====

Julius Fučík
------------

-   komunistický novinář

-   literární vědec, kritik - zabýval se Boženou Němcovou

-   reportáže do Sovětského svazu

-   byl vězněn nacisty → popraven

-   ve vězení psal tajně deník

-   ***Reportáž psaná na oprátce***

    -   nadsázka, metonymie

    -   psal na malé papírky = **motáky** → pašoval je z vězení ven

    -   po válce reportáž je sestavena

    -   mnohé nejasnosti

    -   text velmi pravděpodobně není autentický- byl upravován

    -   nejvydávanější knihou

-   po válce se z něj stal **hrdina**

Jan Drda
--------

-   prozaik už před válkou

-   charakteristika

    -   zobrazuje postavy jako **hrdiny**

-   autor pohádek

-   divadelní hra ***Hrátky s Čertem***

    -   chápána alegoricky

    -   nebe = sověti

    -   peklo = nacisti

-   ***Němá barikáda***

    -   soubor povídek

    -   konec okupace, osvobození, povstání

    -   ***Vyšší princip***

        -   odehrává se v době **Heydrichiády**

        -   hl. hrdina je středoškolský profesor - opakuje frázy = **z
            > vyššího principu mravního**

        -   směšná .. - vyšší princip

        -   po smrti Heydricha zesměšní jeho fotku v novinách

        -   jsou udáni → hrozí jim smrt

        -   profesor se snaží za studenty přimluvit

        -   později se také přihlásí, že souhlasí s atentátem před
            > studenty, použije z vyššího principu mravního

        -   → hrdinský čin, protože ve třídě musí být udavač

    -   ***Pancéřová pěst***

        -   protitanková střela

        -   jde o boje v době Pražského povstání proti nacistům

        -   hl. hrdina 15 let kluk použije zbraň proti tanku

Divadlo
=======

-   Voskovec + Werich → vrací se z emigrace z USA

-   Werich zůstává -

    -   hraje a vystupuje v této době,

    -   působí v muzikálu ***Divotvorný hrnec***,

    -   hraje je filmech ***Císařův pekař, Pekařův císař***

    -   sympatizuje s režimem po 45

-   Voskovec

    -   zůstal v USA

    -   oženil se tam

    -   \- spekulace jestli odešel z důvodů politických

    -   → také to tak bylo

-   **D34** → **D46**

    -   **Emil František Burian**
