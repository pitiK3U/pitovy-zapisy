---
title: Česká literatura 1969-1989
---
[[Česká literatura
1956-1968]{.underline}](https://drive.google.com/open?id=1N4oYBIN7U86l0bDpIBg_FYVLG7_2TrVm4rxdmtLtBmw)
Předchozí \| Další
[[???]{.underline}](https://docs.google.com/document/d/1ArEeMMiI1oco-y6Dv7nPu-XucGrQA83Ez_zMBP6ccRI/)

Česká literatura 1969-1989
==========================

-   období **normalizace**

-   od potlační

-   až do pádu komunistického režimu

-   režim mnohem tužší než v 60.letech

-   po r 68, 69→ **2. vlna emigrace**, je silnější

    -   Kundera

    -   Škvorecký

-   → zakázání

    -   **Václav Havel** -

    -   **Bohumil Hrabal** - vycházela díla, ale s cenzurními zásahy

Literatura

Oficiálně vydávaná
==================

-   autoři, kteří režim podporovali

-   autoři, kteří sebe kritizovali → cenzura

-   -   

Ineditní
========

-   nevydávána u nás

 exilová
-------

-   68' Publishers Toronto

    -   založili Škvorecký a Z. Salivarová

doma = SAMIZDAT
---------------

-   přepisovali na stroji

-   byli za to tresty - za pašování/přepisování

Poezie
======

oficiální
---------

### Jiří Žáček

-   lehká poezie, plná humoru, hravá, zábavná, často psal pro děti,
    > bajky (*Bajky a nebajky*)

-   aféra = k čemu jsou holky na světě, aby byly maminky

neoficiální (zakázána)
----------------------

### J. Seifert

-   režim se tvářil, že po revoluci nevydával

-   problém když dostal Nc

### Jan Skácel

-   na konci 80 let povolen, chvilku

### O. Mikulášek

Underground
-----------

-   ze západu

-   spojen s **rock**em

-   dlouhé vlasy = **máničky** → pronásledováni

-   nesměli se nosit jeansy do školy

-   **odpor vůči společnosti, společenským normám, režimu,**

-   něco jako beatnici

-   rocková skupina, byla souzena = **Plastic People of the Universe**

    -   **Magor** - Ivan Martin Jirous - byl vězněn

        -   výtvarný kritik, hudební kritik

        -   ***Magorovy labutí písně***

            -   **nespisovný jazyk** - **obecná čeština**

            -   **vulgarismy** spojeny s **křesťanskými motivy**

            -   **kritika režimu**

-   **Egon Bondy** = Zdeněk Fišer

    -   filozof - východní myšlení, buddhismus

    -   **marxista** - kritizoval komunismus zleva, že nevládne
        > pracující vrstva

    -   kamarád s Hrabalem → ***Něžný barbar*** (vystupuje zde)

    -   zhudebnili Plactici

    -   zesměšňuje oficiální hesla režimu

    -   prozaik

    -   ***Invalidní sourozenci***

Písničkáři
----------

-   spojeni s folkovou hudbu - sólista na kytaru, důležitý je text =
    jsou zárověn básníci

### Karel Kryl

-   60\. leta

-   ovlivnen okupací z roku 68

-   báseň Bratříčku, zavírej vrátka

-   po okupaci emigroval \--\> do Mnichova

-   působil jako redaktor rádia Svobodná Evropa - zakázána

-   vrátil se po r. 89

-   v té době byl velmi slavný

-   velmi kriticky se vyjadřoval o poměrech po revoluci

### Karel Plíhal

### Jaroslav Hutka

-   emigrace v nizozemsku

-   píseň Náměšť - hymnou revoluce

### Jaromír Nohavica

-   vychází z poetiky Kryla a 2 ruských disidentů Vladimir Vysockij a
    Bulat Okudžava, také je překládal

-   měl problémy - kritizoval komunistické poměry - ?spolupráce STB?

-   spojen s Ostravskem,

-   písně

-   *Pane prezidente*

-   velmi talentovaným básníkem = hodně aluzí

-   ostravský dialekt

Próza
=====

Oficiální - nejsou s režimem, ale jsou vydáváni
-----------------------------------------------

### Ota Pavel

-   hlavní díla byla cenzurovaná

-   *Smrt krásných srnců*

### Vladimír Páral

Ineditní
--------

i\) emigrace

Kundera

Škvorecký

ii\) dizidenti

(Hrabal)

### Pavel Kohout

### Ludvík Vaculík

-   angažoval se v 60. letech na sjezdu spisovatelů

-   autorem dokumentu ***Dva tisíce slov*** - chtěl větší demokracii

    -   kdo podepsal dokument byl pronásledován

-   novinář, spisovatel

-   dílo

-   román ***Sekyra***

    -   valašsko, autobiografický - hl. hrdina je otec novináře

    -   otec byl naivním zapáleným komunistou, musí se vyrovnávat s
        > deziluzí = minulost je jiná než si myslel

    -   konfrontace názorů syna × otce

-   román (novela) ***Morčata***

    -   svoboda × nesvoboda

    -   hl. hrd. je bankovní úředník, který se musí podrobovat
        > **kontrolám** \-- alegorie

    -   doma má morčata → nesvobodu v práci si vylévá doma na morčatech

-   ***Český snář***

    -   dokumentární povaha

    -   osudy dizidentů - Vaculík, postavy nejsou přejmenovány

    -   otevřená

Divadlo
=======

-   kromě oficiálního divadla i tajné divadlo → **bytové divadlo** -
    Vlasta Chramostová (herečka)

-   povolená divadla

    -   **Semafor**

-   **Husa na provázku** → **Na provázku**

    -   založeno v Brně

    -   název je podle díla **Jiřího Mahena** () - brněnský spisovatel,
        > ředitel knihovny, napsal Husa na provázku = dadaistické dílo

    -   **Polívka**, **Donutil**

    -   Milan Uhde - ***Balada pro banditu*** = slavný muzikál,
        > nejznámější dílo, zfilmováno, na motivy Ivana Olbrachta -
        > Nikola Šuhaj Loupežník

-   divadlo **Járy Cimrmana**

    -   **Zdeněk Svěrák**, **Ladislav Smoljak**

    -   některé hry zfilmovány

    -   ***Vyšetřování ztráty třídní knihy***

    -   ***Hospoda na mýtince*** → zfilmováno

    -   Jára Cimrman

        -   **fiktivní postava** → **mystifikace** (oblbují diváka ←
            > přesvědčují, že žil)

        -   čech, který se narodil ve Vídni,

        -   geniální člověk,

        -   všestranný

        -   neuznaný, nedoceněný

        -   oni ho teď objevují

        -   **humor**, **ironie**, **satira**, **slovní humor**

        -   → dělají si legraci z českého nacionalismu

        -   reálie - odehrává se v **době Rakouska-Uherska**

        -   vymyslel telefon, žárovku (Járovka)

-   slavní herci

    -   Jan Tříska, Jiří Kodet, Jiřina Bohdalová, Jiřina Jirásková,
        > Vladimír **Menšík**, Kopecký

-   zakázaní dramatici

Václav Havel
------------

Topol
-----

Kohout
------
