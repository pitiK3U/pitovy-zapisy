---
title: Přelom 19. a 20. století
---
> Předchozí \| Další [[Česká literatura na přelomu 19. a 20.
> století]{.underline}](https://drive.google.com/open?id=1ijs0PvnsxAKVEPjeV8C6SdpyEN4M-tivObDQl1KyaoU)

Světové události
================

-   Mezinárodní napětí

-   Politické problémy

-   → první světová válka

-   Ve společnosti snaha o zrovnoprávnění žen a židů

-   poslední univerzální směr **secese **

    -   snaha aby architektury vypadaly přirozeně

-   → pesimismus

-   fin de siécle - ,,**konec století**"

-   vznik socialistických hnutí

Nové filozofické koncepce
-------------------------

### Arthur Schopenhauer

-   nejdůležitější, vliv východní filozofie, důležitá je naše představa

### Friedrich Nietzche

-   filozof, teoretik, koncept nadčlověka

-   zdůrazňuje moc

-   rozděluje umění na

    -   apollónské - zdůrazňuje řád (klasicismus)

    -   dionýsovské - zdůrazňuje city, vášně

Směry
=====

Symbolismus
-----------

-   skutečnost naznačována na základě **symbolů** (zastupují skutečnost)

-   mnohoznačnost

-   utíká do vlastního světa autora

-   podle symbolistů umění nemá přímo vyjadřovat a zasahovat do
    > společenských problémů - **umění pro umění** (

Impresionismus
--------------

-   spontánnost

-   snaha zachytit nějakou chvilku, prchavý okamžik -

-   smyslové vjemy

    -   Malíř - Claude Monet, Auguste Renoir

    -   hudba - Claude Debussy

Dekadence
---------

-   opovrhování společenství

-   opovrhování morálkou

-   **hledání krásy v ošklivosti**

-   vypjatý individualismus a obdiv aristokracie

-   vyhledávání tabuizovaných témat: morbidita, erotika, kult smrti, zla
    > (i satana)

Představitelé: 
===============

### Impresionismus - 

-   Prokletí básníci;

-   Antonín Sova,

-   F. Šrámek,

-   Vilém Mrštík

### Symbolismus - 

-   Stefan Mallarmé - *Faunovo odpoledne* (zhudebnil C. Debussy)

-   Maurice Maeterlinck - symbolistická divadelní hra = *Modrý pták*

-   Otokar Březina

###  Dekadence

-   Oscar Wilde

-   prokletí básníci

-   Karel Hlaváček

-   časopis Moderní revue

Prokletí básníci
----------------

-   provokovali, revoltovali, odklon od společnosti, obohatili
    > literaturu

### Charles Baudelaire \[šárl bódlér\]

-   stál u zrodu **symbolismu**, **dekadence** i **volného verše **

-   z bohaté rodiny, žil z dědictví - hodně utrácel, bohémský život

-   spojen s mnoho skandály

    -   obarvil si vlasy na zeleno

    -   chodil na procházku s krabem

    -   milenku, které byla mulatka - ,,Moje černá Venuše"

-   psal kritiku na estetické normy

-   navazoval na **Edgara Allana Poea**

-   dílo:

    -   ***Květy zla***

        -   jediná básnická sbírka

        -   mění estetické normy - mění to co je krásné

        -   cynismus

        -   syrová otevřenost díla měšťáky pobuřovala

        -   ***Zdechlina***

            -   nejznámější báseň

            -   **krása ošklivosti**

            -   sugestivní zvukomalebnost

            -   motivy smrti, hniloba, rozklad

### Jean Arthur Rimbaud \[rembó (da)\]

-   problémy s matkou → útěky z domova

-   básnická tvorba v 15 až 19 letech

-   seznámil se s **Verlainem**

-   → stal se tulákem, vojákem, obchodníkem se zbraněmi

-   Dílo:

    -   ***Opilý koráb ***

        -   **alexandrín** - jambický, 12-13 slabik; uprostřed se
            > **shoduje začátek stopy** a **začátek slova**, doprostřed
            > krátké slovo

        -   dekadentní rysy

        -   v metafoře korábu bez vesel a kormidla zmítaném vichřicí
            > zachycuje sebe, vzpurného bohéma

    -   ***Iluminace***

        -   **,,básně v próze"** - lyrika v próze

    -   ***Samohlásky***

        -   **synestézie** - spojování různých vjemů

    -   *Spáč v úvalu*

        -   sonet

        -   sugestivní obraz spícího vojáka

        -   prudký kontrast v závěrečném ? věřil ??

-   *Večerní modlitba*

    -   je o tom jak večer močí

### Paul Verlaine

-   vězněn → na konci života se snažil obrátit v katolismus - nepomohlo

-   → alkoholismus

-   typické = **hlásková instrumentace** - dosahuje se k hudebnosti
    > verše, záměrné opakování samohlásek, **mistrné zachycení nálad **

-   dílo:

    -   *Saturnské básně*

    -   *Galantní slavnosti*

### Tristan Corbiere

-   Dílo

    -   *Žluté lásky* - básnická sbírka

Walt Whitman
------------

-   americký básník

-   **volný verš** - **nemá rytmus**, většinou není rým ale může být

-   měl ódu sám na sebe (jako správný američan)

-   učitel,

-   hlavní představitel **civilismu**

    -   zobrazení technických vymožeností

    -   pokrok civilizace, zobrazení městského života

    -   demokracie

-   Dílo:

    -   *Stébla trávy*

        -   malá sbírka → rozrostla se

        -   optimistický tón

Oscar Wilde
-----------

-   anglický spisovatel, dramatik a básník (Ir 🇨🇮 )

-   autor symbolických próz a moderních pohádek

-   propagoval dekadenci a nezávislost umělce i jeho díla ve vztahu k
    > realitě

-   vyznačoval se lehkostí, smyslem pro komiku situace

-   z bohaté rodiny

-   studoval filologii v Dublinu a v Oxfordu → začal psát

-   ženatý**, ale homosexuální vztah**

-   Dílo:

**Moderní symbolistické pohádky:** (alegorické a špatný konec)

-   ***Šťastný princ***

-   ***Slavík a růže***

**Povídky:**

-   ***Strašidlo cantervillské*** - hororový román

-   ***Zločin lorda Arthura Savilla***

**Konverzační komedie**

-   ***Bezvýznamná žena***

-   ***Ideální manžel ***

-   ***Jak důležité míti Filipa***

    -   **frazém -** míti filipa = být chytrý

    -   **konverzační komedie**

    -   ,,The importance of being Earnest"

***Obraz Doriana Graye***

-   román

-   příběh o Dorianovi, malíř se do něj zamiluje a maluje proto jeho
    > krásu

-   vyřkne přání, že chce být stále mladý, ale aby stárl místo něho
    > obraz

-   fantastický motiv - obraz = symbol duše

-   Dorian pohrdá morálkou ale zůstane stále krásný a obraz stárne

-   dekadence = smrt milenky, zavraždění malíře portrétu

Anton Pavlovič Čechov
---------------------

-   Ruský prozaik, dramatik

-   zanechal lékařské praxe

-   spolupracoval s mnoha časopisy

-   za epidemie cholery a hladu pomáhal postiženým

-   **MCHAT** (Moskevské umělecké divadlo)

-   zemřel na tuberkulózu

-   typické znaky:

    -   málo děje a pomalý

    -   podstatná psychologie založená na rozhovorech

    -   spíše **monology** - mluví spolu ale každý o svém

    -   **symboly**

    -   **filozofické myšlenky**

    -   **impresionistické** motivy se symboly

-   asi 1000 povídek

    -   nejznámější ***Dáma s psíčkem*** - psychologická milostná

-   dílo

    -   ***Racek***

        -   Treplev nešťastně zamilovaný do dívky Niny, která chce být
            > herečkou, Treplev je neúspěšný spisovatel, má konkurenta
            > staršího úspěšného spisovatele, do kterého se Nina
            > zamiluje → Nakonec špatně skončí, ale Nina si stejně
            > nenajde cestu k Treplevu

        -   Racek se stane symbolem Niny

    -   *Strýček Váňa*

    -   *Tři sestry*

    -   *Višnový sad*

        -   hodně zadlužená šlechtická rodina → hádají se jestli mají
            > prodat višňový sad, aby se dostali z dluhů, hraběnka to
            > nechce prodat kvůli sentimentální hodnotě, nakonec sad
            > koupí bezohledný obchodník a skácí sad kvůli penězům
