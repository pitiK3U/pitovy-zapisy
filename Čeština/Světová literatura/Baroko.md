---
title: Baroko
---
Baroko
======

-   poslední společný styl pro celou Evropu

-   od pol. 16. do 18.stol.

-   úzce spjatá s duchovním vývojem → reformace → rozštěpení církve na
    protestanty (méně posvátných knih, přijímají podobojí) a katolíky

-   ### Tridentský koncil

    -   pol 16. stol., **pokus o vyřešení církevních rozporů**,

    -   **transsubstanciace** - přeměna Ježíše v podobě podobojí,

    -   ekumena - snaha sblížit křesťanské církve

    -   ad fontes - k pramenům

    -   index librorum prohibitorum - list zakázaných knih

-   ### 17. stol.

    -   30\. válka

    -   **návrat morové epidemie** → ⅓ půdy neobdělávaná → **hladomor**

    -   vpád Turků do Evropu

    -   příliv uprchlíku z Polska (židé), Běloruska

    -   ďábel = reálné existence → upalování čarodějnic

    -   **špatná situace → lidé se obrací k víře**

    -   zámořské objevy → zničení věcí ve které se do té doby věřilo

    -   touha po absolutní pravdě

    -   roste bohatství Evropy (bohatí ￪, chudí ￬)

Historické souvislosti
----------------------

-   1540 - založení jezuitského řádu

-   1545 - 1563 - tridentský koncil

17.stol. - rozjitřená doba

-   1618-1620 - stavovské povstání

-   1620- bitva na Bílé hoře

-   1627 - obnovené zřízení zemské

-   1618-1648- třicetiletá válka

-   1648 - vestfálský mír

-   vpád Turků do Evropy

-   morová epidemie

Charakteristika baroka
----------------------

-   reakce na rozklad životních jistot (renesance zpochybnila
    náboženství, historické události)

-   odtud touha po absolutních jistotách, pravdě a lásce, tj. po Bohu

-   ponoření do vlastního nitra, mysticismus

    -   citlivost převládá nad racionalitou

-   snaha ohromit , působit na smysly, expresivnost, dynamičnost,
    dramatičnost

-   vypjaté kontrasty

-   člověk je nicotný, pomíjivý

Poetika Baroka
--------------

1.  rozpornost, konflikt, antiteze, ostré kontrasty

2.  barokní hrdina: světec, mučedník, dobyvatel, dobrodruh

3.  výrazové prostředky: \_\_\_, uchvacující, ohromující, útok na lidské
    smysly a představivost

Hans Grimmelshausen

-   němec, 1621-1676

-   ***Dobrodružný Simplicius Simplicissimus*** - pikaresní román,
    příběh hl. hrdiny, psáno s humorem ,,prosťáček nejprostší"

Španělská mysticismus

Tereza z Avily 1515-1592

-   mystička, zakládala kláštery, reformovala Karmelitánský řád

-   ***Milostná lyrika Ježíšovská*** - alegorické, milostné

Jan od Kříže 1545-1591

-   psal milostné verše, erotickou láskou

Itálie

Torquato Tasso 1544-1595

-   projevy extrémní emocionality (citlivosti)

-   ***Osvobozený Jeruzalém*** - dílo předělával kvůli své nejasnosti,
    > hrdinská epopej o 1. křížové výpravě, milostná zápletka, snaží se
    > vyrovnay antickým dílům, náboženské motivy, zápas o lidskou duši

John Donne 1571-1631

-   duchovní náměty, paradoxy, dramatizované \_\_\_\_, antiteze, citově
    > rozjitřené verše, škola metafyzických básníků, básně,

-   autorem verše ***Komu zvoní hrana*** - když umře člověk umře část
    > nás

John Milton 1608 - 1674

-   protestantské baroko, básník, toužil vytvořit národní eposy
    > (Odyssea, Aeneis), angličan

-   ***Ztracený ráj*** - duchovní příběh, Adam a Eva, Bůh × Peklo, Dobro
    > × zlo, alegorie
