---
title: Světová poválečná literatura
---
[[Česká literatura za
okupace]{.underline}](https://drive.google.com/open?id=1iFvUqfe2EXrFZ1DmQA97zv584pw3Z-2V-6KZrIqoLSE)
Předchozí \| Další?? [[Absurdní
drama]{.underline}](https://drive.google.com/open?id=17sPxm0st8a7AkdeFgTzMulN6PErJMsSfFHj0yqg101g)

Světová poválečná literatura po r. 1945
=======================================

USA
---

### 2. sv. válka

#### Norman Mailer

-   účastnil se 2. sv. války, bojoval v tichomoří × Japoncům

-   tvorba vychází z osobních zážitků

-   později psal romány, životopisy (**Marilyn Monroe**)

-   dílo:

    -   ***Nazí a mrtví***

        -   jde o osudy vojenské roty/mužstva, které bojuje proti
            > Japoncům v tichomoří

        -   ukazuje jejich hrdinství

#### Joseph Heller

-   žid

-   působí ve středomoří

-   dílo:

    -   ***Hlava XXII***

        -   mezi letci

        -   román ze středomoří

        -   obraz armády negativní - kritizován za nevlastenecký pohled

        -   absolutní pravidla, hloupost důstojníků, byrokracie → armáda
            > nepřítelem vojáka

        -   název → odkazuje na vojenské předpisy

        -   hl. hrdina **Yossarian** - je antihrdina, fláká se, nechce
            > bojovat, chce **utéct** z armády - podobnost se Švejkem

        -   založeno na nadsázce = hyperbola

#### William Styron \[stajron\]

-   zažil válku velmi málo, nepíše z vlastní zkušenosti

-   jeho tvorba spadá až do 70. let, druhá vlna zájmu o 2.sv.v.

-   pocházel z amerického jihu, pod vlivem jižanského autora W.
    Faulknera

-   problém na jihu - rasismus - židovská otázka, Osvětim, holocaust

    -   ***Sophiina volba***

        -   zfilmován, hl role - herečka

        -   Stingo - začínající spisovatel, v poválečném \_ se seznámí s
            > dvojicí, Sophie a Nathan, Nathan je žid, chová se k Sophii
            > hrubě a ta si to nechává líbit

        -   Stingo se Sophií začíná sbližovat a zjišťuje co v Evropě
            > zažila,

        -   Sophie byla v odboji, antisemitská rodina, dostala se do
            > Osvětimi se svými 2 dětmi, jeden opilý ss ji nechal vybrat
            > jedno dítě pošle do plynu a jedno přežije nebo pošle obě
            > děti→ pošle holčičku, ale chlapečka už nevidí → bere si to
            > vše na sebe a proto si to nechá líbit od Nathana

        -   mezi Stingem a Sophií dojde k milostnému vztahu → ani toto
            > ji nezachrání → vrací se k Nathanovi a spáchají sebevraždu

        -   retrospektivní, Sophie neříká pravdu o minulosti a opravuje
            > se,

        -   různí vypravěči

        -   míchání jazyků - **makaronismus**

        -   sofiina volba - nemožné, volba mezi těžkým a těžkým

### Beatnici (,,Beat generation")

-   význam názvu není jasný

-   skupina spisovatelů formuje se v 50. letech → získává vliv v 60.
    letech

-   generace bouřliváků

-   ideálem je **hipster** - volný člověk, nosili dlouhé vousy → dnes se
    zmocnil komerce

-   rysy:

1.  revolta, vzpoura (- není důvod, hospodářská prosperita; )

-   generační vzpoura

-   bouří se proti politické situace (hrozila 3. sv.; antikomunismus)

-   chtějí revoltu → porušování běžných pravidel

2.  motiv tuláctví

-   ,,tulák" → cesty autem, jezdí po USA, ,,on the road"

3.  alkohol, drogy, večírky, nevázaný sex, experimentují s
    > homosexualitou

4.  spojení s jazzovou hudbou → rock

5.  experimentovali s zen-buddhismu - meditace

-   navazují na ně Hippies - ,,květinové děti", proti válce ve Vietnamu,
    nechtěli narukovat, ,,Make love not war"

#### Allen Ginsberg

-   předky z Evropy, matka byla **židovka**

-   slavný básník

-   dává najevo homosexualitu

-   byl v 60. letech v čsr na **Majálesu** → vládě se nelíbily jeho
    ,,moresy"

-   ***Kvílení***

    -   básnická skladba

    -   zachycení soudenní Ameriky

    -   kriticky, snaží se bojovat proti **Molochu** (obr z bible,
        > symbol nesvobody, to, co mu přišlo špatné - státní moc,
        > reklama, moc firem,)

    -   dlouhý verš

    -   ovlivněn jazzovou rytmikou

    -   vliv Whitmana

-   ***Kadiš***

    -   báseň, kterou napsal pod dojmem smrti jeho matky

    -   Kadiš židovská modlitba za mrtvé

#### Jack Kerouac

-   předky francouzské

-   zemřel relativně mladý

-   věnoval se **tuláctví**

-   zajímá se o **buddhismus** - darmovi tuláci

-   **prozaik**

-   ,,***On the road"** - **Na cestě***

    -   próza - nemá strukturu, nečlenil text (vydavatel musel rozdělit
        > na kapitoly; Hrabal má taky nekonečný proud textu)

    -   hl. hrdina - **Sal** (Paradise), obdivuje zkušenějšího beatníka
        > **Deana** (Moriarty - ze Sherlocka)

    -   podnikají cestu po USA

Opožděná lit. částečně navazující na Beatníky

**Charles Bukowski**

-   román ***Hollywood***

    -   vypráví o režisérovi **Chinaski**, točí film

    -   provokativní próza - kritika Hollywoodu

    -   ironie, satira, odmítnutí reklamy, Hollywoodu

    -   ,,O čem je váš film" - ,,O ničem"

    -   ,,Kdo se na něj bude dívat" - ,,Doufám, že nikdo"

Volně k beatníkům přiřadit

**Ken Kesey** \[kízi\]

-   román ***Vyhoďme ho z kola ven***

    -   zfilmováno Forman - Přelet nad kukaččím hnízdem

    -   Forman - dal do filmu svoji zkušenost ze svého života za
        > komunismu

    -   boj za svobodu proti nějaké instituci - psychiatrické léčebně -
        > Vyšší sestře

### drama

#### Tennessee Williams

-   vl. jménem Tomas Williams,

-   Tennessee - přezdívka protože pocházel z amerického jihu, posměšná
    přezdívka

-   potýkal se se 2 problémy - **alkoholem**, byl **homosexuál**

-   **hry pojednávají o mezilidských vztazích, hrdinky**

-   ***Tramvaj do stanice Touha***

    -   divadelní hra

    -   hrdinka **- Blanche**, nešťastné manželství řeší alkoholem

    -   přijede navštívit její sestru, začne se jí líbit švagr a že by s
        > ním mohla něco mít

    -   švagr - ukáže se jako hrubý člověk → znásilní ji

    -   psychicky se rozpadne → zblázní se

    -   **psychické problémy lidí**

-   ***Kočka na rozpálené plechové střeše***

    -   div\. hra

    -   hrdinkou je **Maggie**

    -   řeší 2 problémy - její manžel o ni nejeví zájem - popíjí a mluví
        > o svém kamarádovi (homosexuální motiv)

    -   Maggie bojuje o svého manžela

    -   oba bojují o přízeň otce jejího manžela - Velký táta - bohatý

    -   Velký táta má druhého syna, který má manželku a děti -
        > bezproblémová rodina

    -   na konci Maggie zalže, že čeká dítě - na konci otevřená možnost
        > že najdou cestu k sobě

    -   zfilmována - Liss Taylorová

    -   název metafora

#### Edward Albee \[ólbi\]

-   dramatik, režisér, dramaturg

-   působil v **New Yorku** - **Broadway**, ale i v Evropě

-   vliv mělo **absurdní drama**, bývá řazen, ale sám ne nepřiřazoval

-   **Pulitzerova cena** - dává se novinářům, ale i spisovatelům,
    americká nobelovka

-   dílo:

    -   ***Kdo se bojí Virginie Woolfové***

        -   v čsr v 60. letech ***Kdopak by se Kafky bál***

        -   pitvali se v lidském nitru, negativní

        -   2 manželské páry - Jiří a Marta pozvou k sobě Nicka a
            > Drahounku

        -   muži jsou kolegové z univerzity

        -   ukazuje se jak špatný je vztah Jiřího a Marty, kteří se
            > nejdříve netváří špatně

        -   urážky, výčitky, osobní zraňování, ukazuje se že Marta trpí
            > mnoho problémy - alkoholismus, starší než Jiří, je
            > neplodná

        -   oba páry jsou neplodní

        -   Jiří a Marta si vymyslí svého syna, bojují synem proti sobě

        -   Jiří nechá vymyšleného syna umřít → Marta se zhroutí →
            > otevřený konec,

        -   sonda do odlidštěných vztahů, kritický pohled na manželství

        -   Nick začne s Martou flirtovat

        -   pouze na dialozích

Britská literatura
------------------

1.  **fantasy, sci-fi** -

    -   John Ronald Tolkien,

    -   Rowlingová,

    -   Arthur Clarke

    -   uč. - 19.10

2.  vychází z sci-fi, kritická = varuje před nebezpečím → **černá utopie
    = antiutopie = dystopie**

    -   není určený čas, ani prostor, většinou budoucnost, varuje před
        > nebezpečím

    -   Orwell

    -   William Golding

3.  **rozhněvaní mladí muži**

**2.**

### William Golding

-   sloužil u námořnictva

-   zajímá ho otázka **morálky** - je ovlivněn křesťanstvím

-   kladl si otázku - morálky, křesťanství, civilizace = jestli jsme
    schopni udržet morálku v zátěžové situaci

-   ukazuje dobu kdy se upustí od morálních hodnot

-   žil v době kdy hrozila 3. světová

-   **Nobelova cena**

-   dílo:

    -   ***Pán much***

        -   **román**, **kolektivní robinzonáda -** ztroskotá kolektiv
            > lidí

        -   odehrává se v nepříliš vzdálené budoucnosti, probíhá válka

        -   skupina hochů z anglické elitní školy ztroskotá na opuštěném
            > ostrově

        -   **Ralph** - je rozumný, racionální, snaží se rozumem
            > překonávat různé překážky a snaží se dodržovat pravidla

        -   ×

        -   **Jack** - začne vést skupinu lovců, má velkou náchylnost k
            > agresivitě, začne zneužívat moc, využívá strachu

        -   dochází ke konfliktu mezi Jackem a Ralphem

        -   chlapci pod vlivem Jacka se začnou barbarizovat, několik
            > chlapců umře

        -   chtějí zabít Ralpha a honí ho

        -   objeví se dospělí a zarazí hon na Ralpha = **deus ex
            > machina**

        -   mimo naši civilizaci se z nás mohou stát barbaři

        -   tzv. obluda = mrtvý pilot

### George Orwell

-   vl. jm. = Eric Arthur Blair

-   Orwell podle anglické řeky

-   narodil se mimo Anglii, otec koloniální úředník

-   **novinář**, pracoval pro **BBC**

-   účastnil se **španělské občanské války**

-   → z války vychází dílo ***Hold Katalánsku*** (bránili se proti
    Frankovi, → pronásledoval je; Barcelona je v Katalánsku),

-   poznal zde praktiky tajné sovětské policie

-   → poznal stalinistický režim, byl levicového smýšlení

-   → zklamaný z toho jak byli tyto myšlenky realizovány

-   zemřel v mladém věku (před 50) brzo po 2. sv. v.

-   dílo:

    -   ***Farma zvířat*** (Zvířecí statek)

        -   **novela**, **alegorie** - na totalitní režim, **bajka** -
            > zvířata mají lidské vlastnosti, reprezentují určité typy
            > lidí

        -   vyženou správce farmy a začnou si vládnou sama

        -   ve skutečnosti je zde těžká diktatura

        -   vládnou **prasata** (nejdůležitější = prase Napoleon)

        -   psi = tajná policie

        -   ovce = hloupé, důvěřivé,

        -   kůň = maká, maká a nic z toho nemá, prodají ho na jatka

        -   **pointa** = vládnoucí vrstva se začne podobat lidem

        -   výroky a tajná hesla

            -   ?

        -   nemělo vyjít za války - sovětský svaz protestoval, až po
            > válce

    -   ***1984***

        -   román, sci-fi = velký bratr, sledovací systémy

        -   odehrává se v budoucnosti, napsáno v **1948** = přesmyčka

        -   hl. hrdina **Winston**

        -   pracuje na ministerstvu pravdy, přijde na to že se
            > skutečnost falšuje

        -   myslí si že se přidá

        -   nejen on ale i jeho milá Julie

        -   dystopie - musí skončit špatně

        -   rozdělení společnosti = říká že je rovnost, ale není rovnost

        -   **tajná kniha** - říká proč to tak je

        -   odpadlík Goldstein

        -   **ironie**

        -   **newspeak**

### 3. rozhněvaní mladí muži

-   mírnější než Beatníci

-   vadil jim **konzervatismus**

-   také jim vadilo třídní/kastovní **rozdělení obyvatelstva** -
    nemožnost kariéry, nemožnost sociálního vzestupu

-   název podle div. hry ***Ohlédni se v hněvu*** - John Osborne

-   **Kingsley Amis** - syna Martina, který je taky spisovatel;

    -   autor žánru **Univerzitního románu**

    -   kritizuje poměry na univerzitě

    -   ***Šťastný Jim*** (Lucky Jim)

        -   univerzitní román

        -   balancuje na hraně **humoristické** a **satirické**
            > literatury

Francie
-------

-   rozvíjela se vysoce intelektuální literatura,

-   experimentální lit. → **nový román** = snaží se rozbourat základy
    epiky

    -   děj - snaží se rozbourat, není jednotný

    -   postavy - snaží se rozbourat, nejsou jasné - on/ona

    -   experiment se neujal

-   filosofický směr - **Existencionalismus** (Existencialismus)

    -   Kafka = předchůdce, neznali ho

    -   existence člověka je vnímaná jako smutná, je sám ve zlém světě,
        > vystaven nepřátelskému světu, nemá oporu, nemá vztah k
        > ostatním lidem, izolovaný, jediné co nám může zachránit je
        > víra v Boha

    -   francouzští spisovatelé jsou **ateisté** - takže nelze najít
        > útěchu ani v náboženství = **není žádná pomoc**

    -   **Albert Camus** \[albér kami (/se)\]

    -   **Jean Paul Sartre** žán pol sártr

### Albert Camus

-   narodil se v kolonii Francie - **Alžírsku**

-   nositel **Nc**

-   kritizoval **marxismus** a komunistických zemí × Sartre

-   zemřel při autonehodě - nejasné okolnosti

-   odboje

-   dílo:

    -   ***Mýtus o Sisyfovi***

        -   filosofický esej, antický mýtus

        -   Sisyfos - dělá zbytečnou/marnou práci, je odsouzen bohy,
            > tlačí kámen, kámen spadne → musí znovu

        -   esej pojednává o marnosti, zbytečnosti

    -   ***Mor***

        -   alegorický román - nacismus

        -   v městečku se nezadržitelně šíří mor → problém jak a zda lze
            > ten mor zastavit,

        -   nacismus mohl lidi nakazit ideologií

    -   ***Caligula***

        -   drama

        -   zvrhlý římský císař, který zneužíval moc

        -   ve hře se řeší otázky moci

    -   ***Cizinec***

        -   krátký román,

        -   hl hrdina **Meursault** \[mersó (lta)\] - plytký člověk,
            > povrchní, bez hlubších citů, netečný, lhostejný

        -   Meursault v Alžírsku zastřelí araba v sebeobraně

        -   → odsouzen za vraždu → jeho postoj se nezmění

        -   když přijde k němu kněz před popravou odmítne

        -   psáno v **ich-formě**,

        -   velmi **jednoduchý, primitivní jazyk** = doporučeno
            > studentům

### Jean Paul Sartre

-   velmi známý představitel franz. existencialismu

-   60\. léta - velmi populární

-   v r. 1964 dostal **Nc** - odmítl

-   pod vlivem **marxismu** - smysl má revoluce, pouze využívá ke
    **kritice** stávající společnosti, ale nesouhlasí s revolucí

-   zabýval se otázkou **svobody**, vnitřní svobodu - jsme vždycky
    vnitřně svobodní → **odsouzeni ke svobodě** = vždy máme možnost
    volby, často není nic příjemného/hezkého

-   jeho životní družka - **Simon de Beauvoire** \[bóvoár\] - jedna z
    hlavních zakladatelek/myslitelek **feminismu**

    -   ***Druhé pohlaví***

-   filosof, prozaik

1.  próza

-   ***Hnus*** (/ ***Nevolnost***)

    -   hlavní hrdina, intelektuál, nepochopený ostatní společností

    -   je mu z ostatních lidí nevolno

    -   žije sám

-   ***Zeď***

    -   sbírka povídek a novel

    -   ***Zeď***

        -   nejznámější novela

        -   hl. hrdina Pablo bojuje ve španělské občanské válce → zajat
            > Frankisty

        -   → chtějí aby vyzradil úkryt svého kamaráda

        -   → **volba má prozradit a zradit / neprozradit a nechat se
            > zastřelit**

        -   → vymyslí si úkryt na hřbitově, kde si myslí, že tam nebude

        -   **paradox**, motiv nešťastné náhody = najdou ho na hřbitově

1.  dramata

-   ***Mouchy***

    -   adaptace slavného antického dramatu ***Oresteia*** - hl. hrdina
        > **Orestes** napsal **Aischylos**

    -   zabil jeho matku, matka zabila jeho otce, protože během trojské
        > války měla milence → mstil se, matkovrahem, morální otázky

    -   → začnou ho pronásledovat výčitky svědomí

Italský neorealismus
--------------------

-   začíná hned těsně na konci 2. sv. války - konec fašistického režimu

-   silně spojen s **filmem** ← originální filmová díla \> literatura
    (je sekundární

-   **věrné zachycení skutečnosti, **

-   **kritika sociálních problémů** - boj proti fašismu, válka → přináší
    běžným, chudým, obyčejným lidem → nejvíce **trpí ženy a děti**

-   filmy

    -   točí černobílé filmy záměrně - kontrasty černé a bílé, už byly
        > barevné filmy

    -   v jejich filmech hrají neherci, běžní lidé, hrají sami sebe,
        > zemědělec = zemědělec, ...

    -   režiséři

        -   de Sica, Visconti (, **F. Fellini** - vychází, ale nepatří k
            > neorealismu)

        -   **Pier Paolo Pasolini** - režisér i spisovatel, známější
            > jako režisér

            -   ***Darmošlapové***

                -   román o lidech na okraji společnosti, povaleči a
                    > kriminálníci

            -   Boccaccio - Dekameron \-- zdůrazňuje erotično

            -   Chaucer - Povídky canterburské

            -   zfilmoval evangelium sv. Matouše - zdůrazňuje sociální
                > otázky, levicový ateista, točil v chudém italském
                > městečku → Ježíš nevstane z mrtvých = není věřící

            -   **Saló** aneb 120 dní Sodomy - volná inspirace markýzem
                > de Sade, sadismus, na konci války fašisté pochytají
                > mladé chlapce a dívky a zavřou je na zámek a tam se
                > stávají oběti ..., → alegorie fašismu

### Alberto Moravia

-   Pinchese \[pinkese\] - **židovského původu**

-   nebyli pronásledování tak jako v německu → až po té

-   intelektuál a odpůrce fašismu

-   zajímají ho osudy žen

-   dílo:

    -   ***Římanka***

        -   krásná chudá dívka Adriana, zklamaná v lásce → stane se z ní
            > luxusní prostitutka → hodně peněz

        -   → matka jí říká, že dělá dobře, že mají co jíst a že si v
            > budoucnu někoho najde, když má peníze

    -   ***Horalka***

        -   Cesira \[čezira\] je vdova a má krásnou mladou dospívající
            > dceru

        -   → utečou před válkou do hor a snaží se dceru po dobu války
            > chránit

        -   → celou dobu se jí to daří

        -   → až přijde osvobození mladou dívku znásilní

Německy psaná literatura
------------------------

1.  Německo

    a.  západní (SRN) -

-   vyrovnávali se s válkou a vinou německa, nacismus a hrůzy války

-   ale žili v demokratickém prostředí = svoboda slova

-   vzniká **skupina 47** (rok)

    -   **Günter Grass**,

    -   **Heinrich Bōll**

    -   kriticky nahlížel na postoj Němců na 2. sv., ale i poválečnou
        > německou společnost

    -   křesťanské motivy

    -   získal **Nc**

    -   ***Kdes byl, Adame?***

        -   odpovědnost za válku; biblický motiv = Adam

    -   ***Biliár o půl desáté***

        -   líčí osudy rodiny významného německého architekta

        -   retrospektivní - na narozeninách vzpomínají na válku

    -   ***Klaunovy názory***

        -   klaun vypráví své názory - všichni se mu smějí × na první
            > pohled vypadají směšné, avšak skrývaj v sobě kritiku

    b.  východní NDR - komunistický stát

<!-- -->

-   nebyla svoboda slova, museli podporovat režim

-   **Anna Seghers** (ová)

    -   ***Sedmý kříž***

        -   koncentrační tábor, uteče 7 vězňů

        -   → 6 jich pochytají a popraví - ukřižují

        -   7\. vězeň jim stále utíká a 7. kříž je stále prázdný

        -   potřebuje se dostat za hranice

        -   zkoumá jak jsou obyčejní němcí ochotní pomoct uprchlíkovi

-   **Christa Wolf** (ová)

    -   zabývá se otázkou rozdělení německa = berlínská zeď

    -   ***Rozdělené nebe***

        -   rozdělení německa

        -   o milostném páru, který rozdělí, že muž emigruje a žena
            > zůstane v NDR

        -   muselo být ukázáno, že on je špatný

2.  Rakousko

-   **Elfriede Jelinek** (ová)

3.  Švýcarsko

-   Friedrich Dürrenmatt

### Günter Grass

-   Gdaňsk - Danzig (německy) - rozvoj nacismu, Hitler využil

-   po válce se stává členem **skupiny 47** - antifašisticky
    orientovaný, kritizující

-   ve stáří se přiznal, že byl v SS → celý život se s tím snaží
    vyrovnat

-   byl básníkem, kritizoval Izreal za politiku vůči palestincům

-   získal **Nc**

-   dílo:

-   próza

    -   tzv. gdaňská trilogie

        -   na sebe navazují jen volně

        -   román ***Plechový bubínek***

            -   zfilmováno, západo-německý film

            -   **tragikomika** + **hyperbola** (nadsázka) =
                > **groteska**

            -   hl. hrdina Oskar Matzerath

            -   Oskar ve 3. letech se rozhodne, že přestane růst

            -   je inteligentní od narození, sexuálně vyspělý

            -   má zvláštní schopnosti - když řve praská sklo

            -   závislý/fixovaný na plechový bubínek

            -   fantastické motivy - záměrně

            -   je až cynický, bez morálních zábran

            -   → dívá se na svět z ,,**žabí perspektiva"**

            -   poválečná doba, kritika poměrů

            -   retrospektivní → skončí v blázinci

        -   román ***Psí roky*** (leta)

            -   zatoulá se nejvýznamnější pes 3. říše, hitlerův vlčák

            -   → celá německá armádo ho hledá

        -   novela ***Kočka a myš***

            -   řeší otázky dospívání → jako symbol si vzal ohryzek
                > (hlas)

### **Elfriede Jelinek** (ová)

-   je vnímána kontroverzně - dodnes žije

-   provokatérka, potížistka - příliš kritická k rakouské společnosti -
    nelíbí se jí vyrovnání s válkou = nedostatečně se vyrovnali s
    válkou - ,,**oběti nebo pachatelé"**

-   silná feministka

-   dostala **Nc**

-   dramata, ale zejména prozaička

-   dílo:

    -   román ***Pianistka***

        -   Erika Kohutová - jm. podle českého zvířete (Jelinková)

        -   talentovana hudebnice, týraná svoji matkou na které je
            > závislá

        -   svůj život si kompenzuje do perverzních sadomasochistických
            > úchylek

### Friedrich Dürrenmatt

-   švýcar, německy hovořící

-   evangelický pastor

-   hledá morální hodnoty a etické otázky

-   podporoval české disidenty v době kom. režimu

-   dramatik a prozaik

-   film **Stalo se za bílého dne** - první film, který poukazuje na
    sexuální nasilí na dětech

-   dílo:

    -   ***Návštěva staré dámy***

        -   odehrává se v chudém městečku

        -   do městečka přijde bohatá žena Claire, která kdysi žila →
            > musela odejít a žila s hanbou

        -   nešťastná láska s Alfredem, nemanželské dítě

        -   vrátí se a chce pomstu \-- nabízí velké peníze za smrt
            > Alfreda

        -   zabijí ho všichni = kolektivní vina

    -   ***Fyzikové***

        -   2 bloky, vyvíjí zbraně

        -   zodpovědnost vědců za lidi, za svoji výzkumy

        -   odehrává se v soukromém blázinci, kam se ukryl významný
            > fyzik Mobius

        -   našel ničivou zbraň a ukryje se

        -   mají zájem 2 velmoci a vyšlou agenty - **Einstein** a
            > **Newton** - **příznaková jména**

        -   nakonec to agenti vzdají

        -   → receptu se zmocní majitelka blázince - nevíme co se stane
            > je podivná

    -   novela ***Soudce a jeho kat***

        -   porušena zásada detektivního žánru

        -   detektiv = pachatel

        -   komisař a inspektor vyšetřují vraždu policisty

        -   inspektor zabil policistu a svedl stopy na mafiána, kterého
            > taky zabil

        -   komisař na to přijde a začne si hrát na boha

        -   → inspektor si to uvědomí a spáchá sebevraždu\\

Ruská - od Martina H. 
----------------------

\- SSSR, komunismus

\- Čerpal z toho, že \"osvobodili Evropu\"

\- Dělení

1\) Oficiální - podporovaná, z doby 2. svět

\- **Boris Polevoj**

§ ***Příběh opravdového člověka***

□ Postřelený letec Alexej Meresjev se doplazí se zpět

□ Musí mu amputovat nohy, ale dostane protézu

□ Znovu se naučí létat a bojuje znovu

2\) Kritika

\- **Boris Pasternak**

\- **Alexandr Solženicyn**

3\) Emigrantská

\- **Vladimír Nobokov**

\- Kritika

\- **Boris Pasternak**

§ Básník, prozaik

§ Žil v 1. pol. 20. stol.

§ Díla:

□ ***Doktor Živago***

® Osudy ruska v době revoluce

® Román

® Vzbudil na západě velký ohlas a získal **NC**

® Hlavní hrdina lékař

◊ Humanista

◊ Snaží se pomáhat lidem

◊ Je v rozporu s tvrdými podmínkami

◊ Má vztah s mladou Larou

\- **Alexandr Solženicym**

§ Studoval fyziku

§ Zmínil se kriticky o Stalinovy v osobní korespondenci

§ Odsouzen a poslán na Sibiř do gulagu

§ Byl tímto poznamenán

§ Pozděj vykázán ze sovětského svazu a žil v USA

§ Pozděj se vrací do Ruska

§ Na konci života se stal podporovatelem Putina

§ Nositel **NC**

§ Díla:

□ povídka ***Jeden den Ivana Děnisoviče***

® To s čím se Ivan musí denně potýkat v gulagu

® Byl tam poslán, protože byl zajat, ale vrátil se k sovětům,

a proto si mysleli, že je to špeh pro nepřátele

® Ukazuje korupci - práskání - vězňů

® Je rád, že přežil den

® **Dokumentární popis**

□***Souostroví gulag***

® Na pomezí administrativního stylu

® Posbíral články o gulazích

□ ***Případ na stanice Krečetovka***

® Na železniční stanici a MC je přednosta

® MC je hodný, ale ovládán ideologií

® Přijde člověk, co říká, že je voják

® Postupně začne cizince podezírat

® A nakonec ho udá a cizinec je zatčen

\- **Vladimír Nobokov**

§ 3.Etapa

§ Z bohaté šlechtické rodiny, která utekla

§ Vyrůstal a žil v zahraničí

§ Studoval na různých univerzitách

§ Byl velice vzdělaný

§ Hodně žil v USA (místo, kde se odehrává Lolita)

§ Psal i anglicky

§ Byl i v Praze

§ Entomolog

§ Předchůdce postmodernismu

§ Hodně **naráží na jiná díla** = **aluze** - rys postmodernismu

§ Díla:

□ ***Lolita***

® Vztah dospělého muže a 12-ti leté holčiny

® Prozaické, epické dílo

® Psáno anglicky

® Vydáno ve francii ale na hodně místech zakázáno

® Dvakrát zfilmováno

® Psáno retrospektivně a jako životopis

® Vypravěč je MC který vypráví z vězení

® MC - 35 letý muž z Francie

® Bojuje se svou touhou po mladých dívkách

® Ale nechá se svést

® Říká jim nymfičky

® Podplácí právě tu lolitu a využívá ji k... :)))

® Ale ve skutečnosti právě lolita manipuluje MC

® Je zkušená a není to první člověk, kterým manipulovala

® 96 kapitol

® Začátek je ve Francii s manželkou, se kterou se rozvede a jede do USA

® Kde mu Šarlota (matka Lolity) poskytuje bydlení

® Později se snaží zabít Šarlotu, aby měl Dolores (lolita) pro sebe

® Šarlota umírá a on má Dolores pro sebe

® Potom cestují po státech... Dále číst samy

® **Hodně franc. vět**

® Mnoho narážek (**Aluze**) na jiná lit. Díla

® Řadí se do postmodernismu právě kvůli aluzím

® Psáno v ich-formě
