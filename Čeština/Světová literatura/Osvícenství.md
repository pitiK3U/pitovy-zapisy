---
title: Osvícenství
---
[[Klasicismus]{.underline}](https://drive.google.com/open?id=1EERoljVxyK45YSqiz6kU47-8IOR3OE29dTp7P-tFJL0)
Předchozí \| Další
[[Preromantismus]{.underline}](https://docs.google.com/document/d/1Bq8dAs6oL6o1vbIxCGa_xplEnws8BlHHPbBFqt082Po)

Osvícenství
===========

-   **tzv. ,,filosofie rozumu a pravdy"**

-   evropské myšlenkové hnutí **18. stol.**

-   z Anglie a Francie

-   podpora ,,osvícenských" absolutistických vladařů (1. fáze - vláda
    rozumu

Politické myšlenky
------------------

-   proti absolutistickému systému, církvi, konzervatismu

-   všichni občané mají být svobodní a rovnoprávní

-   zesílení demokratického a revolučního charakteru (inspiruje
    francouzskou a americkou revoluci)

Filosofické myšlenky
--------------------

-   boj proti tmářství, nevědomosti a pověrám

-   kult rozumu

-   příčinou veškerého zla je nevědomost

Ch. Perrault
------------

-   odklonění od Antiky, napodobování přírody

-   vláda rozumu:

    -   **sbírání vědomostí**

    -   **rozvoj myšlení**

    -   **,,nastavovat zrcadlo"**

-   rozešla se **věda a umění**

Fáze
----

-   ### První fáze

    -   pod vlivem racionalismu klasicistního (Voltaire, Diderot)

-   ### Druhá fáze

    -   vrací se k akcentování lidského citu, vyzvédá jeho
        > individuálnost

        -   zejm. Rousseau - souvisí s nastupujícím preromantismem

### ***Encyklopedie aneb Racionální slovník věd, umění a řemesel*** (1751-1772)

-   28 svazků

-   monumentální dílo vzniklo pod vedením [**[D.
    Diderota]{.underline}**](#denis-diderot---18.stol.) a matematika
    **Jeana d'Alemberta**

-   podílelo se na něm 178 vědců

<!-- -->

-   poskytla myšlenkové impulzy Francouzské revoluci

-   vyzývá čtenáře k vlastnímu uvažování a hledání pravdy ve vědě a
    historii, nikoli v náboženství

-   vyvolala ostře negativní reakce ze strany jezuitů

-   Encyklopedisté prohlášeni za nebezpečnou sektu, projekt byl úředně
    zastaven

-   vydávání pokračovalo tajně

V Německu:

-   G. E. Lessing

-   J. G. Herder

U nás mělo vliv na první fázi národní obrození:

-   Josef Dobrovský a další

Lit. žánry: naučný filosofický román, literární dopisy, eseje, rozpravy
a pojednání

### Montesquieu \[Monteskije\]

-   dílo: - ***Perské listy*** - satira

-   ,,lepší hlava dobrá než plná" - dobře umět myslet

### Voltaire - 18.stol. 

-   spisovatel, dramatik, filozof, historik, encyklopedista

-   často ironizoval → mnoho nepřátel → chvíli byl uvězněn

-   proti náboženství, ale věřil, že existuje bůh

-   dílo:

    -   ***Zaira*** - drama

    -   ***Panna Orleánská*** - drama, vysmívá se kultu Johanky z Arku

    -   ***Candide*** - ,,optimismus", filozofický román, cestopis s
        > četnými odbočkami, hl. hrdina žije u Panglose (má být
        > Leibniz - tento náš svět je nejlepší z možných světů - dělá si
        > z něj legraci) - hl. hrdina vyhnán ze zámku, putuje světem,
        > potkává samé špatné věci (války, \... ), dobrý konec

### Denis Diderot - 18.stol.

-   vůdce encyklopedistů

-   filozof, estetik, teolog

-   filozofická díla:

    -   ***Encyklopedie aneb Racionální slovník věd, umění a řemesel***

    -   ***List o slepcích k ponaučení vidoucích***

    -   ***Myšlenky o výkladu přírody***

-   próza:

    -   ***Jakub fatalista***

        -   (nevěří že může něco ovlivnit - vše je osud) - řeší svobodu
            > vůle, ovlivňování osudu × fatalismus, → náhoda řídí život

    -   ***synovec Rameauův*** - románový dialog, jehož hrdinou je
        > životní ztroskotanec

    -   ***Jeptiška*** - břitká kritika klášterního života

### Anglický román - ⅓ 18.stol.

-   rysy klasicistní a osvícenské, později sentimentalistické

-   #### Daniel Defoe

    -   ***Moll Flandersová*** - hrdinka je žena ze zplodiny

    -   ***Robinson Crusoe*** - víra v činorodost člověka, ve
        > společenský pokrok člověka, 28 let na ostrově, zachránil se
        > pomocí své racionality

### Anglie

-   #### Jonathan Swift - 17. / 18. stol.

    -   angličan, žil v Irsku

    -   ***Gulliverovy cesty***

        -   námořník Gulliver, navštívil 4 země - liliputů, obrů,
            > létající země, hvajninimů - mimořádně ušlechtilé bytosti,
            > neznají faleš, chovají lidi jako odporná stvoření; 4 dílný
            > alegorický satirický parodický a utopický

Thomas More- popraven jindřichem VII., **spis *Utopie***

-   #### Henry Fielding - ½ 18. stol.

    -   ***Tom Jones*** - sleduje vývoj hl. hrdiny, dětství -podstrčený
        > do šlechtické rodiny

-   #### Samuel Richardson - konec 17. / ½ 18. stol.

    -   ***Pamela (aneb odměněná cnost)*** - sentimentalistický román

    -   ***Clarissa*** - vrchol sentimentalistického románu

-   #### Laurence Sterne - ½ 18. stol.

    -   ***Tristem Shandy***

        -   román s antihrdinou

        -   typografické triky

        -   mnoho odboček

        -   soustředění na myšlenky (ne na příběh)
