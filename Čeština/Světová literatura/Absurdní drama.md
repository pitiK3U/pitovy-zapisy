---
title: Absurdní drama
---
[[Světová poválečná
literatura]{.underline}](https://drive.google.com/open?id=1ScdHTTrdW4HPbm-rPLPFgjKMp1AvI1IuIe4cPijLcpo)
Předchozí \| Další [[Česká literatura 1945 -
1948]{.underline}](https://drive.google.com/open?id=1IPq0qEm72yyeUTNzJg0Aw1Q9LmnP-oWZSymNdbfcLao)

Absurdní drama

-   vzniká po 2. sv. válce

-   50\. léta Kafka

1.  **nesmyslnosti, odlidštění**

-   úzkost = nová technologie

-   hrozba zneužití techniky → 3. světová

-   **byrokracie**

2.  **není jednotný děj**, je rozbitý, potlačený

3.  **mluví nesmyslně** = nemožnost komunikace → rozbíjí se jazyk =
    ztrácí sdělnou funkci → nesmyslné fráze

4.  motivy = **symboly**, **nadsázky** (hyperboly)

Předchůdce - 19./20. stol. **Alfred Jarry** \[žary\]

-   skutečně velmi zvláštní → vymyslel si vlastní obor patafyzika

-   jeden z prvních cyklistů

-   dílo:

    -   ***Král Ubu***

        -   hl. hrdina podle učitele fyziky, kterého neměl rád

        -   Ubu se v důsledku převratu dostane na trůn

        -   → neskrývá své záměry → není ve vládě kvůli lidem

        -   → chce to rozkrást, nažrat se,

        -   povstání proti němu → odejdou s manželkou Matkou Ubu jinam

        -   → chtějí to zkusit znovu

Samuel Beckett
--------------

-   zakladatel abs. dramatu, **minimalismus**

-   původem **Ir**, ale žil ve **Francii** → začal psát Francouzsky

-   zapojil se do fr. odboje za 2. sv.

-   znal se s **V. Havlem**

-   dostal **Nc**

-   dílo:

    -   ***Čekání na Godota***

        -   čekání na něco co nikdy (pravděpodobně) nebude

        -   2 tuláci - Estragon a Vladimír - čekají na Godota

        -   Godot nepřijde, ale místo něj přijde **Pozzo** a **Lucky**

        -   Pozzo vede Luckyho na vodítku a Lucky mluví **nesmyslně**

        -   odejdou a následujícího dne čekají zase na Godota

        -   další den přijde zase Pozzo s Luckym, ale Pozzo je **slepý**
            > a Lucky je **němý**

        -   Estragon - franc.

        -   Vladimír - slovanské

        -   Pozzo - italské, → **moc vás zaslepý**

        -   Lucky - ang. - ironické příznakové

        -   Godot = starý pán s dlouhými vousy → God = **Bůh**

        -   **motiv moci** → může převrátit - **oslepit**

    -   ***Konec hry***

        -   vliv obavy z **3. sv. války** a **konce lidstva**

        -   postavy **zmrzačené**, **nepohyblivé**

        -   hl. hrdina **Hamm** se nemůže hýbat, připoutaný k židli

        -   rodiče se nemůžou hýbat a jsou v popelnici

        -   jediný sluha **Clov** se může hýbat

        -   ukazané vztahy **=** jsou špatné

    -   ***Dech***

        -   rozhlasová hra, minimalismus

        -   celá hra je výdech a nádech

Eugene Ionesco \[evžen jonesko\]
--------------------------------

-   rumun

-   žije ve Francii, píše francouzsky

-   dílo:

    -   ***Plešatá zpěvačka***

        -   nejde o žádnou zpěvačku

        -   2 manželské páry **Smithovi** a **Martinovi** se spolu
            > sejdou a vedou rozhovor

        -   říkají nesmyslné fráze, nejsou přiměřené

        -   neposlouchají se a nereagují → začnou se přeřvávat

        -   přijde hasič, zjistí že nehoří → odejde

        -   nastane střih → vymění si místa

        -   → začíná to od znova

        -   → rozpad jazyka

    -   ***Židle***

        -   Starecek a stařenka před smrtí

        -   Vedou rozhovory, kteří nejsou na scéně = pouze s prázdnými
            > židlemi

        -   Umírají - - přijde řečník, má zhodnotit jejich život, je
            > němý nemůže nic říct

        -   Jejich život neznamenal vůbec nic

        -   Symbol smysluplného života

    -   ***Nosorožec***

        -   Lidé se začnou z nevysvětlitelnych důvodu měnit v nosorožce

        -   Začne se šířit jako nemoc

        -   Nosorožec je jeden jako druhý X lidi jsou různí

        -   Varování před státností, stávají se masou, stádem

-   Rozdíl mezi beckettem a Ionescem

    -   U B končí tichem

    -   U Ionesca končí hlukem

V. Havel - obohatil o zkušenost absurdity s komunistickým režimem

Magický realismus
=================

-   směr postmoderní doby z karibské oblasti

-   zakladatelé Guatemalec M. A. Asturias a Kubánec A. Carpentier

-   Inspirován kulturou mimoevropských národů

-   Obsahuje mytické vesmírno a mísí ho s každodenním životem

-   Drží se zásad tradičního života

Gabriel Garciá Márquez
----------------------

-   prozaik, novinář, filmový kritik, scénarista

-   jeden z nejvýznamnějších představitelů magického realismu

-   dostal **Nc**

-   dílo:

    -   román ***Sto roků samoty***

        -   **nekonečná změť příběhů**, **množství postav**

        -   popisuje lidské vášně, lásku, občanské války, přírodní
            > katastrofy

        -   mísí se tam fantastické a nadpřirozené motivy a prostý život

        -   základem jsou **dějiny Kolumbie**

        -   technické novinky jsou brány jako zázraky ale nadpřirozené
            > motivy jsou normální

        -   dějiny rody Buendía ve městečku Macondo

        -   rod je stížen kletbou že má vymřít když se narodí dítě s
            > prasečím ocáskem

        -   končí incestem (Teta x Synovec) po kterém se narodí takové
            > dítě

        -   postavy zůstávají neustále samy a nemohou najít pořádné
            > vztahy

    -   román ***Láska za časů cholery***

        -   dvě vrstvy - pro intelektuála a člověka, co moc nečte

        -   téma = láska

        -   zfilmováno

    -   novela ***Kronika ohlášené smrti***

        -   Jde o kruté morální normy

        -   Dívka Angela \[Anchela\]

        -   Je vrácena ženichem protože prý není panna

        -   ⇒ obrovská ostuda

        -   Bratři zpovídají Angelu a ona jim řekne že to byl Sandiago
            > (nevíme jestli to je pravda)

        -   Bratři zabijí Sandiaga, ale obec, která to celá ví, s tím
            > nic neudělá (tvrdé normy)

        -   Retrospektivní kompozice

 Čingiz Torekulovič Ajtmatov
---------------------------

-   Řadí se do ruské literatury, ale původně byl z Kirgizstánu

-   Bylo mu hodně dovoleno, protože byl z rozvíjející se země

-   zajímají ho zkušenosti z **2. sv. války** a **stalinismus**

-   sleduje osudy lidí

-   používá prvky z lidové ústní slovesnosti

-   kritizuje sociální poměry a ekologické problémy

-   Dílo:

    -   ***Džamila***

        -   Milostná novela

        -   Žena podvede muže a utíká z vesnice

        -   Příběh se odehrává v době 2. svět

        -   Utíká s přistěhovalcem zatímco její muž je ve válce

        -   Mohli si toto dovolit protože to vyšlo v 60. letech + byl z
            > Kirgizstánu

    -   ***Stanice Bouřná***

        -   \"Den delší než století\" - podtitul

        -   MC - je železniční dělník, který chce pohřbít svého přítele
            > podle zvyklostí

        -   Když tam jede, na pohřeb, tak vzpomíná na život a co se svým
            > přítelem prožil

        -   Další část příběhu se zabývá základnami pro letadla a lety
            > do vesmíru

        -   Kontrast nejnovější technologie a starých zvyků

    -   ***Popraviště***

        -   Ekologická tematika

        -   Jde o popravování zvířat - jak jsou zabíjena a jak moc

        -   MC - Vlčice která přichází o mláďata

        -   Ukazuje jak máme k přírodě blíž než si myslíme

Postmoderna
===========

-   Literární ale i filozofický směr

-   Postmoderna - pojem chápán jako současná kultura

-   Počátky v 70. letech (filosofické)

-   V umění se projevuje od 80. let

-   Začal v architektuře (mísení dosavadních stylů)

-   Později se projevuje v lit. a jinde

-   Filozofie

    -   Už nejde věřit velkým ideologiím protože se diskreditovaly
        > (konec velkých vyprávění)

    -   Nejde věřit levicovým a komunistickým idejím

    -   Chtějí se na vše dívat kriticky (Na velké pravdy)

    -   Dívání se na věci z různých úhlů (viděli jsme např.: Čapek)

    -   Říkají, že všechno už tu bylo - snaží se pospojovat to, co už
        > je, místo vyrábění nových věcí

    -   Kritika Eurocentrismu

    -   Filozofové

        -   J. F. Lyotard

        -   J. Derrida

            -   Dekonstrukce

                -   Každý pojem lze rozložit zpětně a ukázat že je umělý
                    > (společenské pojmy)

                -   Např.: Národ, Právo ( fungují protože jim lidé věří)

-   Literatura

    -   Snaha o relativizaci - ironie

    -   Mísí různé žánry a funkční styly

    -   Dvouúrovňová

        -   Jde o uspokojení čtenáře příběhem

        -   Hlubší rovina většinou se jedná o Aluze

    -   Používání určitých motivů

        -   **Kniha**

            -   Čteme a poznáváme svět

            -   Každý si ji interpretuje jinak

        -   **Labyrint** (bludiště)

            -   Svět není jednoznačný

        -   **Zrcadlo**

            -   Vidíme svět různě

        -   **Muzeum**

            -   Vše už se stalo (viz filosofie)

    -   Hrají si se čtenáři

        -   Mělo svá pozitiva, ale i negativa, proto už dnes směr není

Umberto Eco
-----------

-   Italský autor

-   Byl akademik (Jak napsat diplomovou práci)

-   Zabýval se středověkem a jeho estetikou

-   Zabýval se i sémiotikou

    -   Teorie o znacích

    -   Co každý znak znamená

-   Později (80. letech) se stává velice slavným

-   Díla:

    -   *Dějiny krásy a Dějiny ošklivosti*

        -   Jeho odborná díla

    -   ***Jméno růže***

        -   Zfilmováno (ale špatně)

        -   Významné základní dílo postmoderny

        -   Odehrává se ve středověku v klášteře benediktinů

        -   Přichází Vilém z Baskervillu - MC (narážka na Sherlocka)

        -   A jeho pomocník Aco - vypravěčem

        -   Dojde k záhadným úmrtím

        -   Vilém je dotázán aby je vysvětlil

        -   Velké témata jsou sexualita a kniha

        -   Za vraždami stojí slepý mnich Korche

        -   Stojí o to aby lidé nečetli 2. knihu poetiky od Aristotela

        -   Nechce aby ji četli protože pojednává o smíchu v pozitivním
            > smyslu

        -   Bojí se, že by bylo zpochybněno křesťanství a obecně všechny
            > pravdy

        -   Motiv postmoderny - ve světě je chaos, zakázaná kniha,
            > labyrint

        -   Žánr - historický, detektivní a filosofický román

        -   Různé části psány různými způsoby

        -   Hodně aluzí

        -   Děj se odehrává za sedmi dní a je rozdělen podle denního
            > režimu mnichů

    -   ***Foucaultovo kyvadlo***

        -   Název podle myslitele a fyzikálního přístroje (nemají nic s
            > dějem společného)

        -   O konspiračních teoriích

        -   Vychází z řádu templářů

        -   Řád byl zrušen, ale dílo je o tom, že přežil a ovládají svět
            > ze stínů

        -   Kritický pohled na tuto teorii (dělá si z ní srandu)
