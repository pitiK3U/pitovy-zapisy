---
title: Světová próza a drama v meziválečné době
---
[[Poezie v předválečném, válečném a meziválečné
období]{.underline}](https://drive.google.com/open?id=1tcoDI5FuBfUTX4riYOAgFP5wKvrPfYFM_ea287QRU7g)
Předchozí \| Další [[Česká poezie v meziválečném
období]{.underline}](https://docs.google.com/document/d/1ORMTO_jtpDFpoq6AfrP1h_i880r0mkXgDzNp0tMLazM/edit)

Světová próza a drama v meziválečné době

Americká literatura 🇺🇸
======================

Ernest Hemingway (1889-1961)
----------------------------

-   americký prozaik

-   nositel Nobelovy ceny za rok 1954

-   vyrůstal v rodině vzdělanců

-   do 1. sv. války šel dobrovolně → stal se odpůrcem

-   od r. 1921 žil v Paříži jako zahraniční dopisovatel

-   po 2. sv. žil na Kubě

-   zemřel v USA - alkohol → pravděpodobně sebevražda

-   výrazný a osobitý styl

-   psal řadu povídek - těžký návrat vojáků do normálního života

-   **metoda ledovce** - Hemingwayův postup psaní

    -   psaní za využití podtextu - podstatné není řečeno(banální věci →
        > podtext)

    -   *,,Zjistěte, co vám říkají city, co vás uvedlo do varu, pak to
        > napište tak, aby to čtenář také viděl."*

    -   doporučoval hospodařit se silnými slovesy a krátkymi větami

    -   ovlivnil další literární vývoj

-   je řazen mezi literáty tzv. **ztracené generace**

    -   autoři poznamenáni válkou

    -   ovlivněni Evropou

    -   základní témata

        -   zklamání

        -   skepse

        -   rozklad lidských a společenských hodnot

    -   Gertruda Steinová

-   dílo:

    -   *Sbohem, armádo (!)* -

        -   román, vlastní zkušenosti z 1. sv. v.

        -   hl. hrdina američan - dobrovolně do 1. sv. v. -
            > **autobiografický motiv**

        -   milostný vztah s ošetřovatelkou→ z armády prchají do
            > Švýcarska

        -   → neštěstí je pronásleduje → umře mladé dítě i žena

    -   *Stařec a moře* -

        -   novela, za niž obdržel Nobelovu cenu (povídky, kratší próza)

        -   Starý rybář, nedaří se mu chytit rybu, ale věří že se mu
            > podaří

        -   Kuba→ vypluje hluboko do moře → snaha ulovit velkou rybu

        -   než dopluje na břeh, žraloci mu rybu sežerou→ kostra

    -   *Komu zvoní hrana* (umíráček)

        -   román z období španělské občanské války

        -   bestseller

        -   **autobiografie** - hl hrdina američan za republikány (x
            > fašisté)

        -   milostný motiv - láska ke španělské dívce

        -   **obětuje se pro ostatní** (=solidarita, sounáležitost X
            > první román)

        -   název z díla anglického barokního básníka Johna Donne
            > (Neptej komu zvoní hrana, zvoní tobě)

    -   *Zelené pahorky africké*

        -   román a cestopis

        -   motiv **safari**

        -   zachycuje Hemingwaye jako lovce

Francis Scott Fitzgerald
------------------------

-   účastnil se první světové války

-   ztracená generace

-   chtěl na válku zapomenout

-   → píše o poválečné době → lidé si chtějí užívat

-   zbohatl díky prvnímu románu ***Na prahu ráje** →* úspěch + bohatství
    → patřil do bohaté společnosti

-   oženil se s bohatou dívkou Zeldou, manželka, výstřední, cestovali -
    Evropa

    -   ukázalo se že trpí duševní chorobou

    -   → manželství se rozpadlo

    -   → alkoholismus

    -   scénáře pro Hollywood

-   v této době nový styl hudby - **Jazz** → nová móda - ženy krátké
    vlasy, mohly kouřit, automobilismus

-   motivy

    -   **Jazz** - ***Povídky jazzového věku***

-   dílo:

    -   ***Velký Gatsby*** \[gecby\]

        -   tragický román, zfilmováno 2x, DiCaprio

        -   Gatsby - hl hrdina má záhadnou minulost, zamilovaný do
            > Daisy, když byl vojákem, kvůli válce se nemohli vzít,
            > Daisy si vzala Toma,Gatsby zbohatl, aby si mohl vzít
            > Diasy, Tom X Gatsby o Daisy, Tom má milenku, manželku
            > automechanika, Daisy omylem přejede mladou ženu (milenku
            > Toma), Gatsby to vezme na sebe, Tom poštve manžela mrtvé
            > ženy proti Gatsbymu, automechanik zabije Gatsbyho

        -   hlavní myšlenka = všechno věnuje lásce k Daisy ale je mu to
            > k ničemu, končí tragicky

        -   motiv:

            -   párty - všichni ho měli rádi

            -   na pohřbu - skoro nikdo nepřijde

        -   vyprávěno je v ich-formě, přítel Nick

    -   ***Něžná je noc***

        -   román, velmi silně autobiografický (Tender is the night)

        -   hl hrdina - mladý lékař, psychiatr, Dick Diver, zamiluje se
            > do své mladé pacienty Nikol → vezme si ji → manželství se
            > rozpadá → Dick zaznamenává úpadek (Diver = potapěč)

        -   **příznaková jména** - jméno postavy odpovídá vlastnostem

        -   název vypůjčen od anglického romantika Johna Keatse

William Faulkner
----------------

-   autor ovlivněn válkou - účastnil se

-   nepíše o válce

-   žil na Jihu Spojených států - Mississippi

-   vztahy mezi černochy a bělochy, rasismus

-   dostal Nobelovu cenu

-   jazykově těžké - složité dlouhé věty, odbočky, vsuvky (oceňován)

-   dílo:

    -   ***Absolone, Absolone! ***

        -   líčí vzestup jedné Jižanské rodiny (bílé),

        -   také se dotýká otázky rasismu

    -   ***Divoké palmy***

        -   postaven na paralelní kompozici - 2 příběhy

        -   1\. příběh - milenecké dvojice

            -   **existenční problémy**

            -   problém konverzatismu

            -   žena je těhotná → milenec provede **potrat** → zdravotní
                > problémy → zničí to vztah

            -   smutný příběh

        -   2\. příběh

            -   o trestanci

            -   v době povodně uteče z vězení

            -   zachrání dítě při povodni

            -   → dobrovolně se vrátí do vězení

            -   → protože utekl, zvýší mu trest

        -   kontrast

            -   konec života X záchrana

            -   zmar lásky, neštěstí X láska k člověku

        -   **morální otázky**

John Steinbeck
--------------

-   pocházel z Jihu Spojených státu - Kalifornie → tvorba spojena s
    kalifornií

-   autor, studoval biologii → motivy přírody

-   → život farmářů, zemědělských dělníků

-   → navazuje na realismus → **nářečí**

-   **→ biblické motivy**

-   Nobelova cena za literaturu

-   dílo:

    -   ***Hrozny hněvu***

        -   doba velké hospodářské krize - 30. léta (X Fitzgerald)

        -   farmářská rodina z Oklahomy přijde o farmu→ přestěhují se do
            > Kalifornie

        -   chudoba a problémy

        -   **solidarita**, pomoc, která je důležitá

        -   název z **Bible,** píseň z doby americké obč. války

        -   Růži umře dítě → potkají člověka, který umírá hladem → má
            > nepotřebné mléko → nakojí ho

    -   ***O myších a lidech***

        -   novela - kratší, pointa - závěr = tragický příběh

        -   zfilmována

        -   odehrává se na farmě v Kalifornii

        -   hl hrdinové 2 bratři (George a Lennie)- zemědělští dělníci

    -   *Na východ od ráje*

        -   hrdinou - farmářská rodina

        -   biblické motivy **Kain a Ábel**

    -   *Na plechárně*

        -   z prostředí (mexických) přistěhovalců, chudší vrstvy

Britská literatura 🇬🇧 (a irská 🇮🇪)
==================================

-   rozdělení:

    -   Experimentální

        -   **proud vědomí - asociace**, soustředí se na **detailní
            > pocity** a **myšlenky** hrdinů

        -   čas příběhu nerovná se **≠** času textu

        -   J. Joyce, V. Woolfová

    -   Klasická

        -   David H. Lawrence \[lórence\], John Galsworthy \[gasvorty\]
            > ()

-   G. B. Shaw 🇮🇪 - dramatik

James Joyce
-----------

-   pocházel z Dublinu, žil v Evropě 🇪🇺

-   učitel jazyků

-   bojoval s chudobou ← díla nevynášela

-   těžká oční choroba

-   ve své době nedocenněný

-   dílo:

    -   ***Odysseus***

        -   experimentální román

        -   hl hrdina - Leopold Bloom - chudý novinář, židovského původu

        -   chodí po Dublinu

        -   paralela (ironie) - se starověkou bájí o Odysseovi

        -   všechno se odehrává **1 den**

        -   detailní proud vědomí - **asociace**

        -   na Blooma čeká jeho žena (paralela na O. čeká Penelopé)

        -   je psán **různými styly**

        -   různé **jazykové prostředky**

        -   na některých místech **zrušena interpunkce**

        -   každá kapitola má **symbol**

 Virginia Woolfová
-----------------

-   angličanka 🇬🇧 , ve své době provokativní - feministické názory

-   manžel = opora, psychické problémy

-   homosexuální sklony

-   spáchala sebevraždu

-   zajímala se o malířství

-   dílo:

    -   ***K majáku***

        -   rodina Ramseyových plánují výlet k majáku

        -   maják - symbol naděje

        -   přítelkyně Lily - malířka, maluje obraz - symbol

        -   román se skládá ze 3 částí nerovnoměrné

1.  pouze 1 den - plánují jeden den, dlouhá část

2.  stručne 10 let, krátké

3.  1 den - výlet uskuteční, dlouhá část

    -   *Paní Dallowayová*

        -   líčen den ve kterém organizuje oslavu pro jejího manžela

        -   dílem inspirovaný film - *Hodiny*, 3 ženy, paralela, M.
            > Cunningham

    -   *Orlando*

        -   fantastický motiv - hl. postava žije několik století

        -   postava se mění z muže (sukničkáře) na ženu (emancipovanou)

John Galsworthy
---------------

-   patří ke tradiční linii literatury

-   realismus, zobrazení společnosti

-   konzervativní názory

-   zobrazuje vyšší vrstvy

-   předseda mezinárodní organizace spisovatelů PEN klub

-   nositel **Nc**

-   dílo:

    -   *Sága rodu Forsithtů* \[forsajtů\]

        -   románová trilogie

        -   sága - rozsáhlé vypravování, stará germánská literatura

        -   vyšší vrstvy z angl. společnosti - bohatá rodina (podobný
            > motiv v germanské lit.)

        -   několik generací

David Herbert Lawrence
----------------------

-   snažil se provokovat britskou společnost

-   zabýval se problematikou sexuality, pudovou stránku člověka

-   postaral se o skandál

-   pocházel z nižší vrstev - důležitý

-   skandál

-   dílo:

    -   *Milenec lady Chatterleyové*

        -   zakázan jako pornografie → nesměl vyjít do 60. let

        -   musel vydat v Itálii, kde i žil

        -   skandální

        -   děj:

            -   Connie, hlavní hrdinka pochází z vyšších vrstev

            -   její manžel byl zraněn ve válce → impotentní → je
                > neuspokojená i psychicky, nemůžou mít děti

            -   Connie se seznámí s hajným z nízké vrstvy, přitahuje je
                > ji,

            -   otěhotní s ním

            -   popisuje její pocity, touhy

            -   motiv přírody, sexuality,

            -   zfilmováno - český film

            -   **jazyková charakteristika postav** (odlišení)

George Bernard Shaw
-------------------

-   Ir 🇮🇪, žil v Anglii, v Londýně

-   dožil se strašně vysokého věku - 90 let

-   nositel **Nc**

-   dramatik

-   typické

    -   ironický až sakarstický tón

    -   vysmívá se špatným lidským vlastnostem - pokrytectví

-   dílo:

    -   *Svatá Jana*

        -   Johanka z Arku, bojovala ve 100 leté válce Fr. X VB

        -   upálena jako čarodějnice - zrazena Vládcem

        -   **kritika mocných, politiků, církve**

    -   ***Pygmalion***

        -   →antický sochař, zamiloval se do své sochy→socha ožila -
            > paralela

        -   dílo je známé jako muzikál ***My fair lady***

        -   zfilmováno, adaptace My fair lady ze zelňáku

        -   prof fonetiky anglické **Higgins** hlavní hrdina

        -   za ním přijde Líza, prostá květinářka

        -   ona chce, aby ji naučil mluvit správně anglicky

        -   Higgins se vsadí se svým přítelem, že z ní udělá dámu

        -   podaří se mu to, je to **komedie**, ze začátku jí to nejde

        -   později se Líza do něj zamiluje i profesoru se líbí

        -   mnoho konců

        -   → Líza si nevezme Higginse, ale chlapce ze své vrstvy

        -   homurné - **jazyková komedie, jazyková charakteristika
            > postav**

        -   kritizováno - **povýšenost, pohrdání**

Francouzská literatura 🇫🇷
=========================

-   **experimentální proud**

    -   Marcel Proust \[průst\]

-   **klasický proud**

    -   **Antoine de Saint Exupéry \[exipéri\]**

Marcel Proust
-------------

-   z bohaté rodiny

-   cítil se nemocný (hypochondr) → nevycházel z

-   dílo:

    -   ***Hledání ztraceného času***

        -   7\. dílný experimentální román

        -   podrobný popis dojmů, pocitů, nálad (,,encyklopedie
            > psychologie")

        -   asociace

        -   potlačení dějové linie

        -   zachycuje život vyšších vrstev

Antoine de Saint Exupéry
------------------------

-   **pilot** (havaroval), hlavně vozil poštu přes Saharu

-   účastnil se 2. světové války jako letec proti nacistům

-   nevíme co se s ním stalo - letadlo se ztratilo

-   hlásil se k humanismu

-   ovlivněn křesťanskému

-   odpůrce moderní literatury

-   hledal pozitivní motivy - ,, V každém dítěti je ukrytý Mozart jen ho
    v něm musíme probudit"

-   dílo: (souvisí těsně se životem)

-   prózy (střídá úvahy a jeho zážitky jako letce; nelze zařadit)

    -   *Noční let*

    -   *Země lidí*

<!-- -->

-   *Citadela*

    -   filozoficky zaměřená

<!-- -->

-   ***Malý princ***

    -   pro děti i pro dospělé

    -   vychází z jeho zážitků

    -   pohádku sám ilustroval

    -   ich-forma

    -   hlavní postava havaruje na poušti, → pilot potká **malého
        > prince** → seznamují se → malý princ z jiné planety, kde má
        > **růži** → vydal se na cesty → nerozumí světu dospělých → malý
        > princ najde přítele **lišku** **→** začne se mu stýskat →
        > nechá se uštknout hadem (symbol zla,smrti)

    -   děti mluví vždy pravdu a vidí věci co dospělý nevidí

    -   retrospektivní kompozice

    -   motivy

        -   **pohled očima dítěte** - kritika světa dospělých

        -   **přátelství** - malý princ a liška

        -   **láska** - **růže**

        -   **smrt** - není jasné, uštkne had

        -   **jednoduchý jazyk**

    -   **uvozovky - přímá řeč**

    -   **ich forma - odpověděl jsem**

    -   **neznačená (nevlastní) řeč - vnitřní monolog**(ve 20.století)
        > **-** Kdybych já měl padesát tři minuty nazbyt, pomyslel si
        > malý princ

Německá literatura 🇩🇪
=====================

-   nejvíce ovlivnila **1.sv. válka**

-   **nacismus**

Thomas Mann
-----------

-   z bohaté rodiny, Lübeck

-   → později žil v Mnichově

-   bratr Heinrich byl významným spisovatelem

-   nositel **Nc**

-   **liberál** → odpůrce nacismu → **Čsr** mu udělila azyl → v Praze
    gymn. Thomase Manna → USA, Švýcarsko

-   dílo:

    -   ##### ***Buddenbrookové*** \[budenbrokové\]

        -   autobiografické rysy

        -   bohatá rodina

-   Novela

    -   ##### ***Mario a kouzelník***

        -   odehrává se v Itálii v době fašismu

        -   **manipulace** - základní téma

        -   můžeme se proti manipulátorům obrátit

        -   vypráví z pozice svědka, který je v itálii na dovolené

        -   vystoupení slavného kouzelníka, který je manipulátor

        -   vybere si jednoho chlapce Maria a zmanipuluje ho že si myslí
            > že kouzelník je jeho dívka a políbí kouzelníka

        -   potom si to uvědomí a cítí se ponížený a kouzelníka zastřelí

-   Román

    -   ##### ***Kouzelný vrch***

        -   ve švýcarském sanatoriu, kde se schází různá společnost
            > (**symbolicky odráží tehdejší společnost**)

        -   homosexuální motivy

    -   ##### ***Jozef a bratři jeho***

        -   antisemitismus

        -   Jozef ze starého zákona, starozákonní biblická látka

        -   nenáviděn bratry, kteří se ho snažili zabít → potom prodají
            > do otroctví

        -   Jozef se dostane do Egypta → stane se rádcem faraona

        -   bratři se dostanou do egypta → Jozef jim odpustí

    -   ##### ***Doktor Faustus***

        -   inspirace slavnou pověstí o Faustovi

        -   hl hrdina není faust, ale hudební skladatel

        -   taky se mu zdá že jeho hudba není dostatečně známá

        -   a upíše se ďáblovi a začne skládat

        -   a daří se mu

        -   metafora nacismu (německý národ se ,,upsal ďáblu)

Lion Feuchtwanger
-----------------

-   pocházel z židovské továrnické rodiny

-   historické romány → pozitivní úloha židů v dějinách evropy

-   velmi čtený a známý a oblíbený autor

-   byl v emigraci - USA

-   dílo

-   román

    -   ##### ***Ošklivá vévodkyně***

        -   Markéta Pyskaté - vzala si Jana Jindřicha

        -   Tyrolsko

        -   nevzhledná ale skvělá panovnice

        -   manželské problémy

        -   převrat proti svému manželovi → když byl Jan Jindřich na
            > lovu uzavřela všechny hrady a nepustili ho

        -   Jan Jindřich se vrátil na moravu

    -   ##### ***Žid Süss***

        -   "žid sladký", hlavní hrdina židovský finančník - brán
            > pozitivně

        -   nacisté se inspirovali románem a natočili **antisemitský
            > film propagační**

    -   ##### ***Bláznova moudrost***

        -   **oxymorón**

        -   pojednává o životě filozofa J. J. Rousseau

    -   ##### ***Židovka z Toleda***

        -   ve středověkém španělsku

        -   žili zde 3 náboženství - **křesťanství**, **islám** a
            > **židovství**

        -   do slavné a bohaté židovky Rachel se zamiluje křesťanský
            > král

        -   žije s ní otevřeně jako s milenkou → pobuřuje lid a královnu

        -   válka mezi muslimy a křesťany

        -   královna poštve lid proti židovce a jsou zabiti (otec a
            > židovky)

        -   láska X nenávist

        -   tolerance X fundamentalismu

Herrmann Hesse
--------------

-   z rodiny evangelického pastora

-   zajímala ho východní spiritualita → Budhismus, Indiusmus

-   podnikl také cestu do Indie

-   odpůrce války, militarismu, nacionalismu

-   přestěhoval se do Švýcarska - **Bodamské jezero**, na břehu → získal
    švýcarské občanství

-   **Nc** po 2. sv.

-   jeho díla byla více čtená v 60. léta

-   dílo:

    -   ##### ***Siddhárta***

        -   povídka

        -   inspirovaná volně životopisem buddhou

        -   **lyrizovaná próza, rytmizovaná**

        -   psáno vysokým stylem

    -   ##### ***Stepní vlk***

        -   Harry Haller → inicály HH - autobiografický znak

        -   hlavní hrdina si případá jako **stepní vlk**

        -   připadá si rozpolčený - částečně člověk a částečně vlkem -
            > samotářský, sklony k agresivitě

        -   o této próze se říká že je jedna z nejlépe zobrazení
            > schizofrenie

Stefan Zweig
------------

-   Rakouský autor

-   zajímal se o psychologii

-   vliv **Freuda** - zkoumá lidské pudy, vášně

-   ,,psychologické novely" - krátké prózy, povídky

-   zajímal se o životopisy významných osobností - beletrizované
    životopisy

-   humanista - odpůrce fašismu → musel emigrovat → utekl do Brazílie →
    spáchal sebevraždu

-   dílo:

-   životopisy:

    -   ##### ***Balzac***

    -   ##### ***Rolland***

    -   ##### ***Marie Stuartovna***

-   novely:

    -   ##### ***Šachová novela***

    -   ##### ***Amok***

        -   příběh lékaře, ke kterému přijde dobře postavená těhotná
            > žena, není těhotná s manželem, lékař se do ní zamiluje,
            > ona podstoupí potrat ve špatných podmínkách a zemře →
            > lékař si to vyčítá → lékař to neunese a spáchá sebevraždu

        -   **ich-forma** - člověk, kterému to lékař vypráví - **nevíme
            > jména**

        -   **novela**

    -   ##### ***Zmatení citů***

        -   **povídka**

        -   téma homosexuality - ženatý profesor se zamiluje do svého
            > studenta, ale student nemá zájem

Bertolt Brecht
--------------

-   zejména dramatik, začíná jako básník

-   expresionismus

-   povoláním lékař

-   působil v Berlíně - dramaturg, režisér

-   komunista - odpůrce nacismu a fašismu

-   uprchl do USA → po válce se vrátil do Berlína, komunistická část
    Berlína - NDR

-   → povstání - povstal proti vládě

-   dílo:

-   dramatické:

-   **epické divadlo** - experiment, mělo by se blížit próze, porušoval
    jednotu děje, času a místa- děj rozkládá na jednotlivé epizody,
    **chce zdůrazňovat, že hra není skutečná**, komentáře, nápisy -
    rozbíjel

-   ve hrách se objevovaly **songy** - písně, jednoduché pouliční písně,
    primitivní jazyk

    -   ##### ***Třígrošová opera***

        -   námět je podle anglického dramatika Johna Gay z 18. století

        -   odehrává se v Londýně, kde spolu soupeří 2 kriminálnické
            > gangy, představitel jednoho gangu Mackie si má vzít dcera
            > vůdce druhého gangu - na usmíření gangů

        -   Mackie je sukničkář - budoucí manželka je tím uražena

        -   **intriky, zrady** → Mackie má být popraven → královna mu
            > udělí milost

        -   námět použil český dramatik Václav Havel - ředitel londýnské
            > policii je spojen se zloději, korupce

    -   ##### ***Matka Kuráž a její děti***

        -   protiválečné drama

        -   období 30. leté války

        -   reaguje na Hitlera - kritika

        -   ztratí ve válce všechny tři své děti (němá Katrin se snaží
            > varovat před vojáky, a tak bubnuje, ale vojáci ji
            > zastřelí)

        -   matka Kuráž - drobná obchodnice - markytánka (prodávající
            > vojákům potřebné věci → finančně závislá na válce)

    -   ##### ***Kavkazský křídový kruh***

        -   odehrává se v Gruzii (pohoří Kavkaz)

        -   adaptace biblické látky (Starý zákon) - spor dvou žen o
            > dítě, rozsoudí je Šalamoun (ten v tom díle není, ale spor
            > je) → rodička se raději vzdá, ale druhá klidně dítě
            > rozpůlí

    -   ##### ***Život Galileův***

        -   odvolal své učení a tím si zachránil život

    -   ##### ***Zadržitelný vzestup Artura Uie***

        -   karikatura Adolfa Hitlera

Pražská německá literatura
==========================

Gustav Meyrink (1868-1932)
--------------------------

-   pocházel z Vídně, do Prahy se dostal díky matce, která byla herečka
    ( → Praha ho inspirovala )

-   jako spisovatel se proslavil se po odchodu z Prahy

-   množství povídek, které ukazují Prahu jako magické město

    -   ##### *Golem*

        -   démonický román

        -   prý je schovaný na půdě Staronové synagogy

Egon Erwin Kisch - ,,Zuřivý reportér"
-------------------------------------

-   tvůrce moderní reportáže, vzor literárního žurnalismu, procestoval 5
    kontinentů

-   pomezí literatury faktu a umělecké literatury

-   ##### ***Pražský pitaval***

    -   soubor kriminalistických studií

Franz Werfel
------------

-   básník, dramatik, prozaik

-   pocházel z Prahy, ale žil ve Vídni ()

-   z bohaté židovské rodiny, ale velký vliv křesťanství

-   → jako žid a antifašista musel emigrovat → USA

-   zemřel v USA

-   známý a čtivý

-   dílo: (romány)

    -   ##### ***Čtyřicet dnů***

        -   román líčí boj Arménům proti Turkům (povstání tvrdě
            > potlačeno - ,,genocida" Armédů Turky)

    -   ##### ***Píseň o Bernadettě***

        -   Bernadetta - dívka, která podle legendy, byla svědkem
            > zjevením Panny Marie,

        -   Lourdy - Fr. místo, kde se zjevení stalo

        -   malá dívenka přesvědčí všechny, kdo nevěří, že zázrak viděla

Franz Kafka
-----------

-   žil v Praze ve Zlaté uličce, Malá strana

-   z bohaté židovské rodině

-   studoval práva, pracoval v pojišťovně

-   vztah k rodině - s otcem si nerozuměli

-   problematický vztah se ženami

-   zemřel na tuberkulózu

-   díla nevycházela za jeho života - nespokojen s díly, přál si je
    zničit → Max Brod je zachoval

-   Charakteristika Kafkovy tvorby

    -   **absurdita**

    -   **byrokracie**

    -   **odlidštěnost** - velké problémy v mezilidských vztazích

    -   **pesimismus**

    -   předchůdce **EXISTENCIALISMU** - naše existence na tomto světě,
        > bez pomoci, opory, vystaven sám do tohoto světa

    -   **neschopnost komunikace** - nejsme schopni se domluvit

    -   člověk má v sobě jakousi **vinu,** za kterou trpí

-   dílo:

    -   ##### [***[Proměna]{.underline}***](https://drive.google.com/open?id=1HqOh4DM5gy5gm2oafhdfslkWrf6VZ0x4SkbSjVN5WJ0)

        -   povídka - jedno z děl která vyšla

    -   ##### ***Proces***

        -   román, který Kafka nedokončil

        -   Max Brod- žid, vydával díla Kafky

        -   hlavní postava **Josef K.** - **odlidštění**,
            > **autobiografie**

        -   → je zatčen a souzen a neví proč

        -   → nakonec je brutálně popraven (propíchnout nožem)

        -   slavné = předznamenával totalitní režimy - předstihl

    -   ##### *Amerika*

    -   ##### ***Zámek***

        -   nesmyslná zápletka

        -   hlavní hrdina je zeměměřič - geodet

        -   chce se dostat do zámku, kde má pracovat, objednali si ho

        -   ale přes různé byrokratické přednášky se nemůže dostat

Ruská literatura 🇷🇺
===================

-   1917 říjnová bolševická revoluce → komunisté → SSSR → sovětský (=v
    souladu s režimem)

-   rozděluje se do tří proudů

1.  oficiální sovětská literatura (povolená, podporovaná, v souladu
    sovětského režimu; **socialistický realismus**)

-   Maxim Gorkij, M. Šolochov

2.  emigrantská literatura (Francie, čsr)

-   Ivan Bunin

3.  domácí lit. - kritická, nesměla být vydávána

-   Michail Bulgakov

Oficiální 
----------

-   realismus - zobrazit skutečnost jak je

-   **socialistický realismus** (v didaktickém testu!)- kapitalismus je
    kriticky zobrazován(období před revolucí), jediné řešení co vyplývá
    je revoluce, po revoluci je hrdiné a úspěšné budování nového lepšího
    světa

-   u nás se objevuje už v období první republiky u komunisticky
    orientovaných spisovatelů a v 50. letech byl tvrdě vnucován

### Maxim Gorkij

-   → **Hořký**

-   Gorkij je pseudonym, původním jménem Alexej Peškov

-   v mládí musel pracovat a zažil velkou bídu, těžký život → hořký →
    kriticky zobrazuje před revoluční společnost

-   typické - patří k socialistickému realismu, kriticky zobrazuje
    kapitalismus, ale vyhne se druhé části = nepíše po revoluci

-   dlouho byl po vzniku v zahraničí na léčení a vrátil se do komunismu
    ve 30.letech (v době Stalina), v té době byl oslavován a velmi
    významným spisovatel (Gorkij)

-   → není jasné zda nebyl na Stalinův příkaz odstraněn, smrt za
    nejasných okolností

-   dílo: - próza i drama

    -   ##### ***Bosácké povídky***

        -   bosák - ten kdo chodí bosý - neměli na boty

        -   lidé z nejnižších vrstev - žebráci, tuláci, cikáni

        -   Cikáni jdou do nebe - film

        -   inspirace Ivan Olbracht

-   romány - zachycuje buržoazii, podnikatele

    -   *Podnik Artamonových*

    -   *Městečko Okurov*

    -   ##### ***Matka***

        -   revolucionář, který je odsouzen za svou činnost, ale matka
            > pokračuje v činnosti syna → stane se z ní revolucionářka

### Michail Šolochov

-   Don - Ukrajina, v historii zde šili ruská skupina carské armády =
    **Kozáci** (obývají ruští separatisté)

-   zajímá ho:

    -   reakce Kozáků na revoluci

    -   v pozdější době ho zajímá **kolektivizace**

    -   **období 2 sv. války**

-   Nositel **Nc**

-   dílo:

-   román

    -   ##### Tichý Don

        -   rozsáhlý román **řeka**

        -   období kolem ruské revoluce

        -   problém Kozáků

    -   ##### Osud člověka

        -   novela (**povídka**) - krátká próza

        -   2\. světová válka

        -   Hlavní hrdina - **Andrej Sokolov**

        -   Sokolov vypráví svůj život autorovi před válkou a po válce

        -   měl rodinu a dobrý život ale musel narukovat → dostal se do
            > zajetí

        -   příšerné podmínky v táboře, čelí smrti, když se jim něco
            > nelíbí

        -   dostane se do dolů a vozí důstojníka a přejede na druhou
            > stranu - vlastenecký hrdina - zajme německého důstojníka

        -   (zajatci byli pronásledováni - Šocholov rehabilitoval
            > zajatce

        -   když se dostane do ruské armády zjistí že celé rodina až na
            > syna zemřela po vymborbadování

        -   při dobývání Berlína padne jeho syn, po válce se přestěhuje
            > ke kamarádovi a **ujme se mladého chlapce a vychovává ho
            > jako syna** - **humanismus** - má slabé srdce

        -   téma - osud ve válce, vyrovnání se s **traumaty**, motiv
            > **války**, **věrný** války, motiv **hrdosti**
            > (**vlastenectví** - ideální)

        -   kompozice retrospektivní - vyprávění sokolova

        -   vyprávěno **ich-formou** (vypravěč a sokolov - ich-forma)

        -   spisovný jazyk, vojenský slang

Emigrantská
-----------

### Ivan Bunin

-   ze šlechtické rodiny - velmi vzdělaný

-   emigroval do Paříže

-   Nc - **mistr povídek a novel** (krátká próza)

-   téma

    -   protikomunistické názory

    -   mez lidské vztahy (psychologicky založená) - **láska**

    -   vysoká jazyková úroveň

    -   dílo:

        -   ##### Míťova láska

            -   tragická novela

            -   hlavní hrdina = student Miťa

            -   zamilován do Káťy, té se líbí umělecký život, (tíhne k
                > bohémskému životu), má ráda divadla a

            -   Míťa na Káťu žárlí - **žárlivost**

            -   odjíždí na venkov → čeká na dopis od Káťy

            -   Míťa se trápí kvůli čekání na dopis

            -   Míťa je nevěrný Káti s nějakou dívkou z venkova

            -   Káťa napsala že se s ním rozejde a Míťa spáchá
                > sebevraždu

            -   **přírodní motivy** - jak se mění příroda koresponduje k
                > prožívání hlavního hrdiny

Domácí
------

-   žil v rusku, ale kritický postoj ke komunistickému režimu, názory
    > nemohli podporovat, ,,psali do šuplíku", dílo vydáváno až několik
    > let po

### Michail Bulgakov

-   narodil se ve středních vrstvách, vzdělaná rodina

-   působil jako novinář

-   dramaturg v divadle **MCHAT**
    ([[Čechov]{.underline}](https://drive.google.com/open?id=1ijs0PvnsxAKVEPjeV8C6SdpyEN4M-tivObDQl1KyaoU))

<!-- -->

-   dílo:

-   sci-fi novela

    -   ##### Psí srdce

        -   morální otázky

        -   hl. hrdina profesor, lékař - antikomunista

        -   provede šílený experiment - psovi transplantuje část mozku
            > člověka

        -   → vznikne **zrůda** → člověk nemorální, hrubý

        -   nový člověk = symbol experimentů, které sověti prováděli

        -   nakonec to vrátí zpátky

        -   dokud je pes je o něm psáno pozitivně, sympatický

        -   **dlouhý vnitřní monolog**

    -   ##### román ***Mistr a Markétka***

        -   Faustovská látka

        -   má 2 dějové linie - těžký román

            -   Mistr - historik, který píše o Pilátovi (měl soudit
                > Ježíše) -zlo

            -   sekta - spiritisticky orientovaná, vyvolává duchy - zlo

        -   mnoho složitých symbolů symbolů
