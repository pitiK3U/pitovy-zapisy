---
title: Útvary českého jayzka
---
nespisovná

-   obecná čeština

    -   původem dialekt středních čech

    -   → rozšíření celých čech

    -   → Morava

    -   dlouhý → dlouh**ej**

-   dialekty = nářečí

    -   Východomoravské nářečí

        -   dl**ú**h**ý**

    -   středomoravské = **hanácké**

        -   dl**ó**h**é**

-   sociolekty = **slangy**

    -   definován sociální skupinou

    -   slang mládeže, studentů (zemáček, dějáček), ...

Spisovná

-   **Hovorová čeština**

    -   morfologie: studuj**u**, pracuj**u**

    -   lexikologie: **řidičák, motorka**

-   **Neutrální**

-   **Knižní**

    -   morfologie: **mohu,**

-   **Archaická**

    -   nepoužívají se už dnes,

    -   mohou být nesrozumitelná

    -   přechodník minulý: **udělav**

    -   **oř**

mohu - knižní, můžu - hovorové a nic mezi
