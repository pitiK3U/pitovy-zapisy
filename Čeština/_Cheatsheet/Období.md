---
title: Období
---
---
title: Období
---
**Romantismus**
===============

-   **Touha po něčem co je nedosažitelné**, který vyplývá z rozporu mezi
    > sen, ideálem a realitou

    -   **Touha po ideálním společenském řád**u

    -   **Touha bez cíle**

-   **Vypjatý individualismus** (nelíbí se mi svět nemusím žít jak se po
    > mně chce)

    -   **touha po lásce**

-   Zobrazování jevů v jejich rozpornosti

    -   Rozpornost v postavách

    -   Rozpornost ve výstavbě díla

-   Intenzivní citovost

    -   Obrazování lidských citů, zejména lásky

    -   Zalíbení v tajemném

<!-- -->

-   Romantické literární druhy, formy a žánry:

    -   Próza - povídky, novely, romány

    -   Poezie - lyrickoepická

        -   Byronská básnická povídka - POÉMA (lyricko.epická báseň
            > většího rozsahu s věším obsahem)

        -   Lyrická - intimní, reflexivní(uvažování)

**Realismus**

-   Vypracovaná charakteristika postav

-   Důkladný popis prostředí

-   Reálné zobrazení bez iluzí

-   Typizace - typický představitel nějaké společnosti

-   Popisují kriticky, odsuzování problémů (-\> kritický realismus)

-   Rozvoj prózy

    -   epika

        -   Román

        -   Povídka

    -   Drama

        -   Realistické hry
