---
title: _Karel Čapek - R.U.R_
---
+-----------------------------------+-----------------------------------+
| 1\. literární druh a žánr         |                                   |
+===================================+===================================+
| 2\. téma a motiv                  |                                   |
+-----------------------------------+-----------------------------------+
| 3\. zasazení výňatku do kontextu  | co se odehrálo před výňatkem a co |
| díla                              | po něm -- velmi stručně           |
+-----------------------------------+-----------------------------------+
| 4\. časoprostor                   | kdy a kde se dílo děje            |
+-----------------------------------+-----------------------------------+
| 5\. kompoziční výstavba           | chronologická/retrospektivní/řetě |
|                                   | zová/rámcová                      |
|                                   |                                   |
|                                   | gradace, kontrast, paralelismus,  |
|                                   | pointa, in medias res             |
|                                   |                                   |
|                                   | struktura díla -- kapitoly, části |
|                                   | apod.                             |
|                                   |                                   |
|                                   | případně rozbor názvu díla        |
|                                   |                                   |
|                                   | U dramatu: expozice -- kolize --  |
|                                   | krize -- peripetie - drama        |
+-----------------------------------+-----------------------------------+
| 1\. vypravěč/ lyrický subjekt     | vševědoucí,                       |
|                                   |                                   |
|                                   | osobní - personální (vidí ho      |
|                                   | hlavy jen některým osobám),       |
|                                   |                                   |
|                                   | přímý -- ich forma                |
|                                   |                                   |
|                                   | autorský -- komentuje text z      |
|                                   | pozice autora (např. Markéta      |
|                                   | Lazarová)                         |
+-----------------------------------+-----------------------------------+
| 2\. vyprávěcí způsoby             | > er forma/ich forma              |
|                                   | >                                 |
|                                   | > dialog, monolog, vnitřní        |
|                                   | > monolog                         |
+-----------------------------------+-----------------------------------+
| 3\. typy promluv                  | přímá řeč, nepřímá řeč,           |
|                                   |                                   |
|                                   | polopřímá řeč (jen u er-formy),   |
|                                   | neznačená přímá řeč,              |
+-----------------------------------+-----------------------------------+
| 4\. veršová výstavba              | volný / vázaný verš,              |
|                                   |                                   |
|                                   | rýmového schématu (rým sdružený,  |
|                                   | střídavý, obkročný, přerývaný)    |
|                                   |                                   |
|                                   | metrum (trochej -U, daktyl -UU,   |
|                                   | jamb U-) (U=nepřízvučná, - =      |
|                                   | přízvučná),                       |
|                                   |                                   |
|                                   | ne/pravidelnost v počtu slabik    |
|                                   |                                   |
|                                   | strofy (kolik mají veršů atd.)    |
+-----------------------------------+-----------------------------------+
| 5\. postava                       | charakteristika postav a vztahy   |
|                                   | mezi nimi                         |
+-----------------------------------+-----------------------------------+
| 1\. jazykové prostředky a jejich  | slovní zásoba (spisovná,          |
| funkce ve výňatku                 | nespisovná, citově zabarvená      |
|                                   | slova, zdrobněliny, archaismy,    |
|                                   | historismy, poetismy, vulgarismy, |
|                                   | slang, argot, různé typy frazémů, |
|                                   | přísloví atd.)                    |
|                                   |                                   |
|                                   | nějaké slovotvorné zvláštnosti    |
|                                   | (např. v 1984 slova jako Zamini,  |
|                                   | nebo zkratková slova apod.)       |
|                                   |                                   |
|                                   | stavba vět, druhy vět (zvolací    |
|                                   | aj.), charakter textu             |
+-----------------------------------+-----------------------------------+
| 2\. textu tropy a figury a        | Tropy (obrazná pojmenování):      |
| jejich funkce ve výňatku          | přirovnání, metafora,             |
|                                   | personifikace, metonymie,         |
|                                   | synekdocha, alegorie, symbol,     |
|                                   | apostrofa, eufemismus,            |
|                                   | dysfemismus, ironie, oxymóron,    |
|                                   | synestézie, řečnická otázka,      |
|                                   | gradace, hyperbola, epiteton      |
|                                   |                                   |
|                                   | Figury (zvl. uspořádání slov):    |
|                                   | aliterace, anafora, epifora,      |
|                                   | epizeuxis, paralelismus, elipsa,  |
|                                   | inverze, enumerace -- výčet,      |
|                                   | antiteze                          |
+-----------------------------------+-----------------------------------+
| 1\. kontext autorovy tvorby.      | Kdy žil, do jakého um. směru      |
|                                   | patřil                            |
|                                   |                                   |
|                                   | jakými obory tvorby se zabýval    |
|                                   | (prozaik, spisovatel,             |
|                                   | romanopisec, dramatik, básník,    |
|                                   | novinář, esejista apod),          |
|                                   |                                   |
|                                   | další jeho díla                   |
+-----------------------------------+-----------------------------------+
| 2\. literární / obecně kulturní   | Další autoři, kteř s ním          |
| kontext                           | souvisejí (stejná doba, stejný    |
|                                   | um. směr, na koho navazoval, kdo  |
|                                   | navazoval na něj, dílo, které     |
|                                   | rozebírané dílo inspirovalo a     |
|                                   | kterým bylo inspirováno, atd.     |
|                                   | stejné motivy nebo témata)        |
+-----------------------------------+-----------------------------------+

STRUKTURA ÚSTNÍ ZKOUŠKY

kritérium ověřované vědomosti a dovednosti

ANALÝZA

UMĚLECKÉHO

TEXTU

I. část

• zasazení výňatku do kontextu díla

• téma a motiv

• časoprostor

• kompoziční výstavba

• literární druh a žánr

II\. část

• vypravěč / lyrický subjekt

• postava

• vyprávěcí způsoby

• typy promluv

• veršová výstavba

III\. část

• jazykové prostředky a jejich funkce ve výňatku

• tropy a figury a jejich funkce ve výňatku

LITERÁRNĚHISTORICKÝ

KONTEXT

• kontext autorovy tvorby

• literární / obecně kulturní kontext

ANALÝZA

NEUMĚLECKÉHO

TEXTU

I. část

• souvislost mezi výňatky

• hlavní myšlenka textu

• podstatné a nepodstatné informace

• různé možné způsoby čtení a interpretace textu

• domněnky a fakta

• komunikační situace (např. účel, adresát)

II\. část

• funkční styl

• slohový postup

• slohový útvar

• kompoziční výstavba výňatku

• jazykové prostředky a jejich funkce ve výňatku

1.  Žánr

2.  Děj (fabule) - stručně = podstatné hlavní zápletka

3.  Postavy a jejich charakteristika

4.  Motivy

5.  Téma = obsahuje hlavní myšlenku

6.  Kompozice

    -   **Kompoziční postup =** chronologický

    -   Kompoziční principy = **kontrast, gradace, paralela**

7.  Vypravování

    -   **Vypravěč**

    -   **Ich, ...**

    -   **Pásmo vypravěče / pásmo postav**

8.  Jazykové prostředky

    -   **Spisovný, knižní jazyk, ... (příklady)**
