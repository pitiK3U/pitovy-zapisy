---
title: Romain Rolland - Petr a Lucie
---
1.  Žánr = **epika**, **novela** (středně dlouhý příběh, málo postav,
    **poeta**) **protiválečná**

2.  Děj (fabule) - stručně = podstatné hlavní zápletka

    -   Petr v metru spatří dívku do které se ihned zamiluje

    -   seznámí se spolu a tráví spolu čas

    -   Petr má jít za chvíli na vojnu → nechtějí se opustit

    -   umřou oba dva

3.  Postavy a jejich charakteristika

    -   **Petr** - mladý syn z bohatší měšťanské rodiny, miluje od
        > začátku Lucii, má jít brzy na vojnu, idealizuje si svět,
        > základní problém = brzy narukuje do války

    -   **Lucie** - dívka z chudší rodiny, žije s matkou, která pracuje
        > v továrně na zbraně, přivydělává si malováním obrazů, krásná,
        > realistka

    -   **→ protagonisté**

    -   bratr Filip - vojna ho promění z dobrého na sebestředného,
        > soucit a lítost když uvidí ty dva zamilované

4.  Motivy

    -   Láska, válka, chudoba, nesmyslnost, život, smrt, přeměna válkou,
        > kontrast,

    -   **baladické rysy**

    -   náboženské motivy - berou **Ježíše jako přítele** (Ježíš miloval
        > a oni taky milují), **Velký Pátek** (umřel Ježíš a umřou i
        > oni)

5.  Téma = obsahuje hlavní myšlenku

    -   Láska X válka

6.  Kompozice

    -   Paříž, 1918

    -   chronologický postup

    -   Kompoziční principy = **kontrast, gradace, paralela**

7.  Vypravování

    -   **Er-forma**

    -   **vypravěč vševědoucí**

    -   **Polopřímá řeč** (pásmo vypravěče je zařazeno mezi pásmo
        > postav)

    -   neznačená/nevlastní přímá řeč (Ale vítr je odnáší, Jen ať
        > nezavane znovu a ať se sobě již navěky neztratíme!)

    -   nepřímá

8.  Jazykové prostředky

    -   **Spisovný, knižní jazyk, ... (příklady)**

    -   **filozofické otázky**

    -   **dialogy a přímá řeč**

    -   **bohatá slovní zásoba**

    -   **jednoduché věty**

    -   **popis pocitů**
