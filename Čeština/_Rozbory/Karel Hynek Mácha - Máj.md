---
title: Karel Hynek Mácha - Máj
---
+-----------------------------------+-----------------------------------+
| 1\. literární druh a žánr         | Lyrickoepická báseň               |
+===================================+===================================+
| 2\. téma a motiv                  | Čas, osud, myšlenky smrti --      |
|                                   | pomíjivost života, ztotožnění     |
|                                   | životního pocitu s básní,         |
|                                   | zklamání, smutek, sebelítost      |
|                                   |                                   |
|                                   | Láska, žárlivost, strach ze       |
|                                   | smrti, vina a trest, májová       |
|                                   | příroda, věčnost a nekonečno,     |
|                                   | myšlenky lepší budoucnosti,       |
|                                   | Nešťastný životní osud Viléma a   |
|                                   | Jarmily a zobrazení májové        |
|                                   | přírody                           |
+-----------------------------------+-----------------------------------+
| 3\. zasazení výňatku do kontextu  |                                   |
| díla                              |                                   |
+-----------------------------------+-----------------------------------+
| 4\. časoprostor                   | Není podstatný, jarní příroda,    |
|                                   | noc, brzy ráno -- poprava,        |
|                                   | silvestr                          |
+-----------------------------------+-----------------------------------+
| 5\. kompoziční výstavba           | Retrospektivní,                   |
|                                   |                                   |
|                                   | Gradace                           |
|                                   |                                   |
|                                   | Oxymóron -- umřelé hvězdy svit    |
|                                   |                                   |
|                                   | Kontrast -- život x smrt          |
|                                   |                                   |
|                                   | Paralelismus -- temná noc a       |
|                                   | myšlenky Viléma na smrt, některé  |
|                                   | verše se změní pouze slovo,       |
|                                   |                                   |
|                                   | 4 zpěvy a 2 intermezza -- 1.zpěv  |
|                                   | -- zpráva od plavce pro Jarmilu,  |
|                                   | 2.zpěv Vilém ve vězení,           |
|                                   | intermezzo-příroda se připravuje  |
|                                   | na pohřeb Viléma, 3.zpěv --       |
|                                   | Poprava Viléma, Vilém se loučí s  |
|                                   | rodnou zemí, intermezzo -- nářek  |
|                                   | Vilémových druhů, 4.zpěv --       |
|                                   | poutník Hynek, zamýšlí se nad     |
|                                   | tragédií lidského osudu           |
|                                   |                                   |
|                                   | Náznak 5-ti stupňového tématu     |
+-----------------------------------+-----------------------------------+
| 1\. vypravěč/ lyrický subjekt     | vševědoucí                        |
|                                   |                                   |
|                                   | personální                        |
|                                   |                                   |
|                                   | přímý -- ich forma -- 4.zpěv      |
+-----------------------------------+-----------------------------------+
| 2\. vyprávěcí způsoby             | er forma později ich forma        |
|                                   |                                   |
|                                   | dialog, monolog                   |
+-----------------------------------+-----------------------------------+
| 3\. typy promluv                  | přímá řeč, nepřímá řeč,           |
|                                   |                                   |
|                                   | polopřímá řeč (jen u er-formy),   |
|                                   | neznačená přímá řeč,              |
+-----------------------------------+-----------------------------------+
| 4\. veršová výstavba              | vázaný verš,                      |
|                                   |                                   |
|                                   | rým sdružené, střídavé, obkročné, |
|                                   |                                   |
|                                   | opakovaní počáteční sloky,        |
|                                   |                                   |
|                                   | pravidelnost v počtu slabik       |
|                                   |                                   |
|                                   | jambický verš -- začíná           |
|                                   | nepřízvučnou dobou(jednoslabičná  |
|                                   | slova)                            |
+-----------------------------------+-----------------------------------+
| 5\. postava                       | Vilém -- loupežník, zamilovaný do |
|                                   | Jarmily, žárlivý, pomstychtivý,   |
|                                   | nešťastný, smutný, otcovrah       |
|                                   |                                   |
|                                   | Jarmila -- krásná, nešťastná,     |
|                                   | zamilována do Viléma, svedena     |
|                                   | otcem Viléma                      |
|                                   |                                   |
|                                   | Poutník Hynek -- vystupuje ve     |
|                                   | 4.zpěvu, sám autor, zamyšlený,    |
|                                   | vrací se k Vilémově popravišti    |
|                                   |                                   |
|                                   | Otec Viléma -- nečestný, vyhnal   |
|                                   | Viléma, svůdce Jarmily            |
|                                   |                                   |
|                                   | Plavec                            |
|                                   |                                   |
|                                   | Příroda                           |
+-----------------------------------+-----------------------------------+
| 1\. kontext autorovy tvorby.      | Český básník, představitel        |
|                                   | českého romantismu, zakladatel    |
|                                   | moderní české poezie              |
|                                   |                                   |
|                                   | dílo -- Obrazy mého života        |
+-----------------------------------+-----------------------------------+
| 2\. literární / obecně kulturní   | romantismus -- rozvoj v           |
| kontext                           | 30.letech, v Čechách se nikdy     |
|                                   | pořádně nerozvinul, jediným       |
|                                   | pravým romantickým hrdinou byl    |
|                                   | Mácha, ostatní autoři mají        |
|                                   | romantické tendence, ale i prvky  |
|                                   | z ostatních směrů                 |
|                                   |                                   |
|                                   | Karel Jaromír Erben -- Kytice     |
|                                   |                                   |
|                                   | Josef Kajetán Tyl -- Rozervanec,  |
|                                   | Fidlovačka aneb Žádný hněv a      |
|                                   | žádná rvačka                      |
|                                   |                                   |
|                                   | Karel Sabina -- Úvod povahopisný  |
|                                   | -- obhajoba Máchy, Hrobník        |
+-----------------------------------+-----------------------------------+
