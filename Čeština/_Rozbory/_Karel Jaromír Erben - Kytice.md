---
title: _Karel Jaromír Erben - Kytice
---
I. část

**zasazení výňatku do kontextu díla**

-   **Kytice** a **Věštkyně** - dávají naději a víru v lepší budoucnost
    vlastencům

-   **Polednice** a **Vodník** - záporná role nadpřirozených postav

-   **Poklad** a **Dceřina kletba** - narušení vztahu mezi matkou a
    dítětem

-   **Svatební košile** a **Vrba** - přeměna člověka (oživlá mrtvola
    milence a žena převtělená ve vrbu)

-   **Zlatý kolovrat** (pohádka) a **Záhořovo lože** (legenda) - vina,
    pokání, konečné vykoupení

-   **Štědrý den** a **Holoubek** - kontrasty, jako je štěstí se smutkem
    nebo láska se smrtí

-   **Lilie** - 13. báseň, nikam se nehodí

**téma a motiv**

-   nadpřirozeno

-   **mezilidské vztahy → vina, trest (osud trestá)**

    -   matka ⇔ dítě

    -   muž ⇔ žena

**časoprostor**

-   neurčitý, nepodstatný - mýty

**kompoziční výstavba**

-   **13 témat** - dvojice mají stejné téma zrcadlově

-   **refrény**

-   dialogy

**literární druh a žánr**

-   poezie

-   **balady** - rychlý spád → elipsy

II\. část

**vypravěč / lyrický subjekt**

-   vševědoucí,

-   osobní - personální (vidí ho hlavy jen některým osobám),

-   přímý -- ich forma

-   autorský -- komentuje text z pozice autora (např. Markéta Lazarová)

**postava**

-   charakteristika postav a vztahy mezi nimi

**vyprávěcí způsoby**

-   er forma/ich forma

-   dialog, monolog, vnitřní monolog

**typy promluv**

-   přímá řeč, nepřímá řeč,

-   polopřímá řeč (jen u er-formy), neznačená přímá řeč,

**veršová výstavba**

-   rýmová schéma

-   4 veršová strofa

-   **sylabotónický systém** - 6 slabik ve verši

-   převážně trochej (-U)

III\. část

**jazykové prostředky a jejich funkce ve výňatku**

-   **elipsy** - balady, rychlý spád, stručnost

-   **citově zabarvená slova** - prokletí, nešťastnice

-   **zdrobněliny** - holoubek, běloučký

-   **krátké věty**, **zvolací věty** = !, **řečnické otázky**

-   **gradace**, **kontrasty** = dramatičnost

**tropy a figury a jejich funkce ve výňatku**

-   **personifikace** - ,,běží časy, běží"; ,,pevně stojí vina"

-   **epizeuxis** - ,,běží časy, běží"

-   **paralelismus** - ,,běží časy, běží"

-   **přirovnání** - ,,rok jako hodina"

-   **hyperbola** - ,,všecko s sebou mění" ?

-   **apostrofa** - ,,Nekoukej, nežaluj!"

-   **epiteton** - ,,běloučký holoubek!

LITERÁRNĚHISTORICKÝ KONTEXT

**kontext autorovy tvorby**

-   Kdy žil, do jakého um. směru patřil

-   jakými obory tvorby se zabýval (prozaik, spisovatel, romanopisec,
    dramatik, básník, novinář, esejista apod),

-   další jeho díla

**literární / obecně kulturní kontext**

-   Další autoři, kteř s ním souvisejí (stejná doba, stejný um. směr, na
    koho navazoval, kdo navazoval na něj, dílo, které rozebírané dílo
    inspirovalo a kterým bylo inspirováno, atd. stejné motivy nebo
    témata)

**Děj**(fabule)

**Kytice** a **Věštkyně** - upomínají na lidovou tvorbu a poezii (první
a poslední)

-   *Kytice* (*Mateřídouška*)

    -   přirovnání ke sbírce

    -   Matka zemře, děti pláčou na hrobě, tak se vtělí do květiny -
        > mateřídoušky. Matka představuje vlast, děti národ.

-   *Věštkyně*

    -   Skládá se z úryvků šesti lidových pověstí o české zemi.

    -   O Libuši, jež pošle pro Přemysla Oráče a na zem dopadne hlad,
        > dokud on nedoorá své pole. O Libuši, která věští založení
        > Prahy a položí svého prvorozeného syna do vody, než nastanou
        > dobré časy.

    -   O Karlu IV. a kolébce. O kostelu se zlatým zvonem, který se
        > zaryje do země, dokud se neobnoví ctnosti, láska, víra a
        > naděje. O tom, že by se Češi měli sjednotit k jednomu
        > náboženství. O půlce sochy, která by potřebovala hlavu (rozum)
        > a srdce, aby bylo dobře.- věštila z vody, že přijdou zlé doby
        > → zlatá kolébka → Karel IV.

**Poklad** a **Dceřina kletba** - o porušení vztahu matka-dítě (druhý a
předposlední)

-   *Poklad*

    -   matka a dítě, jeskyně s penězi

    -   **lidé mají raději bohatství než své milované**

-   *Dceřina kletba*

    -   dcera zabila své dítě - narodilo se v hanbě = bez otce

    -   dcera si chce vzít život, nadává na toho kluka

    -   nakonec prokleje matku, že ji **zvůli** dávala

    -   → jde se očistit (oběsit)

-   *Holoubek*

    -   vdova otrávila svého manžela

    -   → za 3 dny se oženila znovu

    -   → holoubek zpívá pravdu a donutí nevěstu se zabít

-   *Svatební košile*

    -   rouhání

    -   letí krajinou, odhození náboženských věcí → **zrychlení**

-   *Polednice*

    -   tragický konec

    -   nadsázka

    -   zlá nadpřirozená bytost

-   *Zlatý kolovrat*

    -   balada - matka a dcera

    -   pohádka - Dorka

    -   **závist**

-   *Štědrý den*

    -   nesnažit se vidět do budoucnosti

    -   Dívky Marie a Hana v adventu předou len a jdou se podívat na
        > jezero. Když vysekají díru, má se jim zjevit ženich. Haně se
        > zjeví myslivec Václav, Marii však jen kostel, svíce, rakev. Do
        > roka se Hana vdá a Marie zemře. O dalším adventu předoucí
        > dívky radši nechtějí znát, co je čeká.

-   *Záhořovo lože*

    -   = loupežník + lidojed

    -   poutník jde do pekla pro svou duši - upsal ji jeho otec

    -   mučí ďábla kvůli smlouvě

    -   1\. pekelná koupel → 2. pekelná děva - objetí → 3. záhořovo lože

    -   chce odpuštění → světský život

    -   zasadí kyj → strom se zlatými jablky

    -   vrátí se po 90 letech a je jim odpuštěno

-   *Vodník*

    -   matka a dcera

    -   dcera neposlechne → spadne do rybníka → svatba + dítě → chce za
        > matkou → matka ji nechce pustit zpět → dítě umře

    -   mezilidské vztahy

-   *Vrba*

    -   manželka se přes noc promění ve vrbu

    -   muž vrbu pokácí → zabije ji → dítě brečí → píšťalka

    -   mezilidské vztahy

-   *Lilie*

    -   umřela mladá dáma, nechtěla být pohřbena na hřbtivoě, ale v lese

    -   ne hrobě vyrostla překrásná lilie

    -   objevil ji Velmož a vzal domů → zjevila se dáma

    -   nemohla být na slunci, chtěla stín → Volmož se o ni postará

    -   → vzali se → dítě

    -   velmož musel do války - o lilii se stará matka velmože

    -   → schválně ubližovala lilii a nechala ji spálit

    -   → dáma i dítě umřeli

    -   → velmož se vrátil a proklel matku, aby se jí stalo to stejné
