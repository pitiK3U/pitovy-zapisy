---
title: Franz Kafka - Proměna
---
Proměna - Franz Kafka

1.  Žánr

    -   literární druh: **epika**, próza

    -   literární žánr: **povídka** (novela)

2.  Děj (fabule) - stručně = podstatné hlavní zápletka

    -   Řehoř se jednoho dne promění v brouka a nemůže vstát do práce.
        > Rodina se ptá jestli je v pořádku a přesvědčuje ostatní že má
        > vše pod kontrolou přestože nemůže vylézt z postele.

    -   Rodina je z Řehořovi proměny vyděšená. Řehoř rodině rozumí, ale
        > rodině jemu ne.

    -   Martin se utopí v pudinku.

3.  Postavy a jejich charakteristika

    -   Řehoř

        -   hlavní postava, obchodní cestující

        -   zodpovědný, živí rodinu, svědomitý, pečlivý

        -   úzkosti a snížená sebedůvěra

        -   promění se v brouka → trápí ho, že udělal rodině problém

        -   přemýšlí jak pomůže rodině i v podobě brouka

    -   setra Markéta

        -   obětavá, snaží se Řehořovi pomáhat, hraje na housle, najde
            > si práci v obchodě

    -   Řehořův otec

        -   praktický, přísný, tvrdý

        -   po proměně musí začít pracovat

    -   Řehořova matka

        -   starostlivá

        -   přemáhá v sobě odpor k proměněnému Řehořovi

4.  Motivy

    -   **izolovanost** - Řehoř nemůže s ostatními komunikovat

    -   **bezmocnost** - Řehoř nemůže se svým osudem nijak bojovat

    -   **odcizení** - Řehoř se víc cítí jako brouk a rodina přestává
        > věřit že je to Řehoř

    -   **pesimismus** - situace se nezlepšuje

    -   **absubrita** - proměna je nereálná

5.  Téma = odcizení mezi lidmi, nemožnost komunikace

6.  Kompozice

    -   Dílo je rozděleno do 3 kapitol:

        1.  Rodina se vyrovnává s Řehořovou proměnou

        2.  Snaha žít s proměnným Řehořem

        3.  Smrt brouka rodinu osvobodí

    -   **Kompoziční postup =** chronologický

    -   Kompoziční principy = **kontrast, gradace, paralela**

        -   **gradace** - vztah mezi rodinou a proměněným Řehořem

        -   nejdřív se Řehoř zamyká zevnitř, po proměně ho rodina zamyká
            > zvenku

7.  Vypravování

    -   **Vypravěč** - strohý, bez emocí, věcný

    -   **er-forma**

    -   vnitřní monolog

    -   přímá řeč

    -   **Pásmo vypravěče / pásmo postav**

8.  Jazykové prostředky

    -   **spisovný jazyk**, prostý jazyk

    -   bez archaismů, slangových či hovorových výrazů

    -   vnitřní monolog
