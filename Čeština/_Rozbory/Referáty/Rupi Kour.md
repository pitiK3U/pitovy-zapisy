---
title: Rupi Kour
---
Rupi Kour
=========

-   indická autorka, ilustrátorka, básnířka

-   ve 4 letech emigrovala do Kanady

-   na střední psala krátké básně

-   psala na Tumblr

-   později k básním přidávala ilustrace na Instagram

-   dílo:

    -   ***Mléko a med*** \-- ***Milk and honey***

        -   sbírka básní

        -   popisuje své životní zkušenosti, emoce a pocity

        -   kontroverzní témata

        -   básně mají věnování

        -   jednoduché ilustrace

        -   pro autorku neexistují tabu

        -   texty nezačínající velkým písmenem

        -   první část - **zraňovaná** - násilí

        -   druhá - **milující** - vášeň, láska

        -   třetí

        -   poslední - **odpouštějící**, sebeláska

    -   ***Květy slunce***

        -   doplňuje předchozí

        -   cesta růstu a zahojení

        -   oslavuje lásku mateřství a ženství
