---
title: Krysař - Viktor Dyk
---
Krysař - Viktor Dyk
===================

**žánr** - **novela** - epické dílo (soustředí se na děj, redukují se
popisy)

**hl. postava:** Krysař

> Agnes - zamiluje se do ní krysař, milenec Christian
>
> Sepp - rybář, hloupější, duševně zaostalý
>
> konšelé = hradní

původní německá legenda - upravuje

**děj**: Konšelé pozvou krysaře do města Hammeln, přijde vyhánět krysy,
konšelé mu nezaplatí, zklamaný v lásce k Agnes - dítě s Christianem,
znechucen, vrátí se kvůli Agnes - spáchala sebevřaždu, Krysař píšťalou
všechny odvede do propasti - věří že propast je zaslíbená, Sepp přežije
(nevinný a nenechá se nalákat) a najde nemluvně (symbol budoucnosti)

**motivy**: lásko, hamižnost, motiv ďábla - socha ďábla, , kritika
maloměšťáctví, kritika předsudků, zklamání

Chronologická, člení se na kapitoly, dialogy, monology

knižní výrazy

**personifikace**

symbolismus - dítě na konci = nevinnost, **sedmihradsko** - ráj, píšŤala
- symbol moc, **krysy** - symbol hříšnosti

**Anarchismus** = krysař - nerozumí si se společností

**Gradace**

**Pointa**
