---
title: Lolita - Nabokov Vladimír
---
Lolita - Nabokov Vladimír

-   prozaické epické dílo

-   psychologický román

-   téma = sexuální vztah z nezletilou dívkou

-   anglicky psaný, vydáno ve Francii 1925 - v mnoha zemích zakázáno

-   retrospektivní, životopis vězně Humbert

-   fiktivní rozhovor se soudní porotou a čtenářem = musíme se zamyslet
    sami

-   muži je 35 let

-   se svojí touhou bojoval, snažil se ní vyléčit, boj prohrává → nemůže
    odolat

-   mladým dívkám říka = nymfičky

-   s Lolitou manipuluje - zneužívájí, podplácí ji → uvědomí si že ji
    zničil

-   **Dolores** Hazeová - Lolita (Dolor = bolest)

-   Charlata Hazeová - matka Dolores

-   Lolita - chce být herečkou, ona s ním manipuluje, drzá, zkažená na
    svých 12 let

-   děj: - 2 části a 69 kapitol

    -   ze začátku se dozvídáme o jeho touze, dozvídáme se jak zjistil o
        > své touze

    -   první divka Annabel

    -   odehrává se ve Francii, má manželku, která ho začne podvádět a
        > rozvedou se a dostane se do Ameriky

    -   bydlí u Charloty a dostává se do styku s Dolores

    -   původně nechtěl bydlet u Charloty, ale když uvidí Dolores, tak
        > si to rozmyslí

    -   vezme si Charlotu a stane se adoptivním rodičem Dolores

    -   → Charlota umře v automobilové nehodě

-   jazyk:

    -   těžký

    -   mnoho francouzských slov = **makaronismus**

    -   narážky na jiné literární díla = **aluze** → **typický rys
        > postmodernismu**

    -   narážka na Poa

    -   SPZ automobilu - roky, kdy se narodil a zemřel Shakespeare

    -   psáno v **ich-formě**

-   snaha udržet 2 roviny - 1 čtivá a 2. rovina - pro ty co jsou
    vzdělaní
