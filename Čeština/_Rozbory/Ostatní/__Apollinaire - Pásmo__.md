---
title: __Apollinaire - Pásmo__
---
1.  Žánr

    -   lyrika, báseň

    -   kubufuturismus - slavná báseň ze sbírky alkoholy, Apollinare

2.  Děj (fabule) - není

3.  Postavy a jejich charakteristika

4.  Motivy - vyjadřuje pocity a dojmy

5.  Téma (= vnitřní monolog, )

    -   římská a řecká mytologie

    -   křesťanství

    -   moderní doba - stroje, technika, autobusy, letadla

    -   alkohol - oslava života

    -   cestování - zmiňuje = Prahy,

    -   láska

6.  Kompozice

    -   **historie** , křesťanství a moderní doba = **Kontrast**

    -   **láska**

    -   **cestování**

    -   **zatracení** - alkohol, jeho neštěstí

    -   časoprostor - +- Paříž, současnost autora

7.  Vypravování

    -   **lyrický subjekt**

    -   ve **2. osobě** = říká sobě X říká čtenáři

    -   veršová výstavba

        -   **chybí interpunkce** - typické pro **futurismus**

        -   sloky nestejně dlouhé

        -   rým nepravidelný = **Asonace**

            -   **shodují se samohlásky**

            -   **neshodují se souhlásky**

        -   tendence sdruženého rýmu

        -   **volný verš**

8.  Jazykové prostředky

    -   **chybí interpunkce**

    -   **asociace = volný proud myšlenek**

    -   **vnitřní monolog**

    -   Tropy

        -   apostrofa

        -   inverze

        -   personifikace

        -   oxymorón

        -   přirovnání

        -   metafora

    -   Figury

        -   anafory

    -   **jazykový útvar/vrstva/varieta**

        -   knižní výrazy - znaven

        -   **neologismy, archaismy -** poesie, bandaskami, míšenka
            > přesličná, avion = letadlo, hangár

        -   **hovorovost** - Eiffelko

Sloka = strofa
