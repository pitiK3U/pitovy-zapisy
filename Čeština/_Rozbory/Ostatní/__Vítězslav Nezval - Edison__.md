---
title: __Vítězslav Nezval - Edison__
---
1.  Žánr - **lyricko-epická báseň**, (pásmo/poema)

    -   rým = sdružený (aabb)

    -   verš = 11-12 slabičný trochej

    -   poetismus / surrealismus

2.  -   1\. zpěv - hazardní hráč a motiv sebevraždy, na konci motiv Edisona

    -   2\. zpěv - o edisonovi, jeho mládí a porovnává se s ním

    -   3\. zpěv - technika a vynálezy, pokrok

    -   4\. zpěv - smysl života, cestování, oslava

    -   5\. zpěv - autor se navrací do reality ze snění, motiv lásky, stíny -
        > překonává

3.  Postavy a jejich charakteristika

    -   lyrický subjekt - autor

    -   Edison - symbol

4.  Motivy

    -   Smysl života - beznaděj a naděje

    -   Pokrok techniky

    -   motiv sebevraždy, lásky

    -   žárovka - symbol naděje X stíny - symbol beznaděje

    -   pozitivní vyznění

5.  Téma - Smysl **moderního** života

6.  Kompozice

    -   rozděleno do **5 zpěvů**

    -   rámcová kompozice

    -   pásmo,

    -   **refrén** - pozměňuje se

    -   Kompoziční principy = **kontrast, gradace, paralela**

    -   **časoprostor:** V Praze, Americe, čas za života Edisona

    -   **Lyrickým subjektem je autor**

7.  Veršová výstavba

    -   sdružený rým (aabb)

    -   rytmus - trochej

8.  Jazykové prostředky

    -   spisovný až knižní jazyk

    -   **termíny**

    -   **chybí interpunkce,**

    -   **anafora**

        -   jako Bůh jenž stvořil růži noc a blín

        -   jako Bůh jenž touží stvořit nová slova

        -   jako Bůh jenž musí tvořit vždycky znova

    -   epizuexis

    -   zvukomalba

    -   hlásková instrumentace (kako/)

    -   apostrofa - Edison, ,,\... jste \..."

    -   personifikace

        -   Už je zase duše smutná po slavnosti

    -   metafory

    -   metonymie

    -   synestezie

    -   přirovnání

        -   vaše ruce bledé jako křída

    -   aliterace
