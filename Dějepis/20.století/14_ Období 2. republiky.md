---
title: 14_ Období 2. republiky
---
Období 2. republiky 30. 9. 1938 -- 15. 3. 1939
==============================================

Situace po Mnichovu
-------------------

-   prezident E. Beneš abdikoval (5.10.1938 → odletěl do anglie, začal
    tvořit ? vládu )

-   vláda Jana Syrového rekonstruována

    -   snaha navázat kontakt s Německem

-   Strana národní jednoty (R. Beran)

    -   koncepce italského korporativního státu (fašizace)

-   Národní strana práce (soc. dem. + nár. soc.)

-   KSČ ilegální (konec r. 1938; → ti co byli ohroženi emigrovali, mohli
    si připravit v mírovém ovzduší ,,síť")

Slovensko
---------

-   6\. 10. 1938 HSĽS = jediná polit strana

    -   sjednoceny slovenské občanské strany

    -   KSS zakázána

-   **Žilinská dohoda** -- požadavek autonomie, zástupci Hlinkovi strany

-   7.10. autonomní vláda -- Jozef Tiso (kněz, nečekali na česko)

-   11.10. autonomní vláda Podkarpatské Rusi

**Vídeňská arbitráž**
---------------------

-   2\. 11. 1938 -- **ztráta území jižního Slovenska** ve prospěch Maďarska

    -   Itálie a Německo - domluvili že Maďarsko získá jižní část
        > Slovenska

Česko - Slovenská republika
---------------------------

-   19.11. 1938 NS -- zákon o autonomii Slovenska a Podkarpatské Rusi

-   30.11. prezidentem zvolen dr. Emil Hácha (nechtěl být prezidentem,
    bral jako službu národu; tragická postava)

-   1.12. nová vláda -- Rudolf Beran (krajně pravicový politik)

-   min. zahraničí Fr. Chvalovský snaha o orientaci na Německo (doufal
    že se dá čs zachránit)

Březen 1939
-----------

-   krize vztahů mezi vládou čs. a slovenskou

-   výjimečný stav, armáda

-   záminka pro Hitlera

-   13.-14.3. 1939 Tiso a Ďurčanský v Berlíně - jednání s Hitlerem

-   14.3. 1939 vyhlášen Slovenský štát - slovenský sněm, neprojednáno s
    vládou čs

Okupace
-------

-   14.3. Hácha a Chvalovský v Berlíně - Hácha nechtěl podepsat

-   15.3. 1939 ve 4 hod. podepsán protokol svěřující osud českého národa
    do rukou Hitlera (vyhrožování bombardací Prahy; vojsko už přešlo
    hranici - např na Ostravsku)

Protektorát Čechy a Morava
--------------------------

-   **15.3. 1939** obsazení německými vojsky (porušení Mnichovské
    smlouvy)

-   16\. 3. 1939 Hitler v Praze (Hitler přijel do Prahy dříve jak Hácha)

-   výnos o Protektorátu

    -   prezidentem Hácha (65 let, starší **nemocný** muž)

    -   protektorátní vlády:

        -   R. Beran - agrárník

        -   ing. Alois Eliáš - gen. čs armády (zapojil se do odboje -
            > popraven ze příchodu Heinricha)

        -   dr. Jar. Krejčí - kolaborant

        -   Richard Bienert

-   říšský protektor Konstantin von Neurath

-   státní tajemník Karl Hermann Frank
