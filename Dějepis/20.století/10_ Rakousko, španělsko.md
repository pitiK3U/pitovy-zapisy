---
title: 10_ Rakousko, španělsko
---
Šíření fašismu Rakousko, Španělsko
==================================

Příklad Itálie a Německa
------------------------

-   1932:

    -   diktatura v Portugalsku

    -   Španělsku (Primo de Rivera)

-   1934:

    -   v Estonsku a Lotyšsku

-   30\. léta:

    -   Bulharsko, Maďarsko, Polsko, Rumunsko

Rakousko
========

-   krize → volání po „vládě pevné ruky" (vůdce, který by je vyvedl z
    krize)

-   kancléř Engelbert Dollfuss (křesťansko sociální strana)

-   7.3. 1933 převrat

-   odstraňování demokracie:

    -   zrušena ústava

    -   omezena činnost odborů

    -   zatýkání sociálních demokratů -

-   V rakousko mnoho zástupců hitlera

Rakousko stavovským státem
--------------------------

-   zákaz nacionálně socialistické strany (NSDAP; Hitler chtěl Velké
    Německo)

-   orientace na Itálii

-   Dollfuss založil **Vlasteneckou frontu** (květen 1933)

-   11.-16. 2. 1934 -- povstání dělníků v Linci a Vídni

    -   krvavé boje

    -   1200-1500 mrtvých (Vláda použila zbraně i letadla proti
        > povstalcům)

Další fašizace
--------------

-   únor 1934 -- zákaz soc. dem. strany

-   březen --Vlastenecká fronta je jediná legální

-   30.4. **1934 korporativní ústava**

    -   klerofašistický režim (fašismus podporovaný církví)

-   25.7. 1934 pokus o puč rakouských nacistů

    -   Dollfuss zavražděn

    -   převrat se nepodařil

-   nový kancléř dr. Kurt Schuschnigg → člen vlastenecké fronty, odpůrce
    nacismu

-   rakouští nacisté připravují připojení k Německu

-   Schuschnigg -- bál se že nedopadne dobře → pokus odvrátit referendum

-   Hitler jej přinutil odstoupit

-   jmenován Seyess-Inquart -- povolal německé jednotky

-   **11. -- 13.3. 1938 anšlus Rakouska**

    -   Rakousko **Východní markou** (Rakousko zaniklo)

-   pronásledování odpůrců

Španělsko
=========

-   1933 založena El Falanga Españole - nacionalistická strana →
    ,,Falangisté"

-   leden 1936 vznik Lidové fronty - před volbami se domluvila na
    koalici

-   16.2. **1936** volby

    -   **vláda Lidové fronty**

    -   reformy - nebyli tak radikální jako ve Francii

-   Falanga se spojila s konzervativními generály (po vzoru Německa
    získali mocenskou sílu)

-   generál Francisco Franco (caudillo - něco jako vůdce)

-   plánování převratu pomocí povstání

Fašistické povstání
-------------------

-   18.7.1936 počátek povstání proti republice

-   povstání republikány téměř potlačeno

-   demokratické státy: politika nevměšování (ať si to španělé vyřeší
    jak chtějí)

Pomoc fašistům
--------------

-   maurské oddíly - ze španělského Maroka

-   Itálie: materiál, vojenští instruktoři, 60 000 vojáků

-   Německo: 16 000 vojáků, letecká legie Condor

    -   26\. 4. 1937 Guernica - zkouška bombardování

Občanská válka
--------------

-   sbírky na pomoc Španělsku - demokratické státy, zbraně, lék.
    materiál,

-   interbrigadisté - lidé z jiných zemí bojovali za republiku (z
    ČSR 2200) (např. Ernest Hemingway)

-   30.7.1936 - Burgos: fašistická opoziční vláda (gen. Franco) :
    frankisté

Příčiny porážky republikánů
---------------------------

-   rozpory v táboře republikánů, komunističtí bojovníci zlí na
    obyvatelstvo

-   zahraniční pomoc fašistům

-   25\. 10. 1936 - Římské protokoly (Berlín, Hitler + Ciano - zeť
    Mussoliniho), koordinace postupu na Balkáně s pomocí francouzů Osa
    Berlín - Řím

-   25\. 11. **1936**: N+Jap (+It 6.11.1937) **Pakt proti Kominterně** (proti
    SSSR)

-   27\. 9. **1940** Pakt tří -- Berlín: N+It+Jap = **Osa Berlín -- Řím -
    Tokio**

Španělsko fašistickou diktaturou
--------------------------------

-   únor 1939 -- frankisté u francouzských hranic vlády Fr a VB uznaly
    vládu gen. Franca

-   Madrid -- nová vláda plk. Cassady = 5. kolona (vnitřní zrádce)

-   5.3.1939 převrat

-   **duben 1939** ovládnuto **celé Španělsko**

Důsledky občanské války
-----------------------

-   za 3 roky zahynulo 500 000 lidí

-   1 mil. osob v koncentračních táborech

-   demokracie až za 40 let
