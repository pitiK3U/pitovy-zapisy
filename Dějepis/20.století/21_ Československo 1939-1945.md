---
title: 21_ Československo 1939-1945
---
Březen 1939
-----------

-   14.3. vyhlášen slovenský štát

-   15.3. okupace - protektorát

-   16.3. Maďarsko okupovalo Podkarpatskou Rus

-   protektorát = koloniální diktatura

-   cíl:

    -   vyhlazení českého národa

    -   rasově vhodní -- germanizace

    -   Češi měli být využiti jako pracovní síla (600 000 mužů na nucené
        > práce)

Projevy odporu
--------------

-   (29.9 1939 bojkot tramvajové dopravy)

-   ilegální tiskoviny

-   **28.10. 1939** demonstrace

-   15.11 pohřeb Jana Opletala (Němci zakázali, Češi nerespektovali;
    záminka pro zbavení se české inteligence)

Akce 17. listopad
-----------------

-   uzavřeny české vysoké školy

-   9 studentů popraveno

-   1200 - Sachsenhausen

Kolaborace
----------

-   malé procento - odpovídá procent u jiných národů

-   těžké poznat hranice mezi kolaborací a odbojem

-   gen. ing. Alois Eliáš - předseda 2. protektorátní vlády; zapojen do
    **odboje**, po nástupu H. zabit

> x

-   Emanuel Moravec - za 1. sv. ruský legionář, kolaborant

Odbojové organizace 
--------------------

-   **Obrana národa** (ON) - v případě protiněmecké akce se dají do
    zbraně a postaví se okupantům

-   **Petiční výbor Věrni zůstaneme** (PVVZ)

↓

-   **Ústřední vedení odboje domácího** (ÚVOD)

<!-- -->

-   **Komunistický odboj** od jara 1941

    -   ilegální od 15.3.1939 - čas získat kontakty

    -   izolace (smlouva SSSR s Německem - chtěli bojovat s fašisty)

    -   září 1941 kontakt se Západním odbojem (čs exilovou vládou)

Centra odboje v zahraničí
-------------------------

-   Londýn (Beneš, Jan Masaryk - radio, J. Stránský)

-   Moskva - komunisté

-   formy odboje:

    -   sabotáže

    -   diverze

    -   zpravodajská činnost

    -   **ilegální tisk**

    -   partyzánské skupiny

Změna říšského protektora
-------------------------

-   září 1941 odvolán K. von Neurath (nacista, ale neztotožňoval se s
    praktiky SS a SA)

-   28.9. 1941 **Reinhard Heydrich** zastupující říšský protektor

    -   stanné právo - ,, buzerace občanů ", bezpečností složka si mohla
        > dělat co chtěla

        -   486 popraveno - gen Alois Eliáš

        -   2242 zatčeno

    -   říjen 1941 1. transport židů do Lodže

    -   listopad 1941 do Terezína

    -   příprava na „konečné řešení české otázky" - politika biče a
        > marcipánu, vyvraždění českého lidu a nechání pouze dětí
        > vhodných na přeučení

Atentát
-------

-   prosinec 1941 výsadky z V.B.

    -   operace Anthropoid (138 výsadková peruť RAF)

    -   Silver A - parašutistická skupina

    -   Silver B

-   **27.5. 1942 atentát na Heydricha**

    -   Jozef Gabčík, Jan Kubiš

    -   Josef Valčík

    -   4.6. Heydrich zemřel

Heydrichiáda (po 4.6. 1942)
---------------------------

-   nové stanné právo

-   popraveno 3188 osob

-   10.6. 1942 -- Lidice (vrátilo se 16 dětí)

-   24.6. 1942 Ležáky u Chrudimi (z 11 dětí se vrátili 2)

-   18\. 6. 1942 parašutisté spáchali sebevraždu (kostel sv. K. Boromejského)

Odboj po atentátu
-----------------

-   **Úvod** -- zlikvidován

-   **Rada tří (R3)** -- gen. Vojtěch Luža, Josef Grňa, Josef Císař --
    působila do konce války

-   **Tři králové** (pplk. Josef Balabán, pplk. Josef Mašín, škpt. V.
    Morávek)

-   partyzánské jednotky

-   **KSČ** -- ilegální ÚV

    -   léto 1943 rozbit 3. ÚV

-   **Předvoj** -- mládež (levicově orientovaná)

Zahraniční odboj
----------------

Západní: Francie, V.B., Blízký východ + Afrika

-   30.4. 1939 vojenská jednotka -- **Krakov**, Katovice

    -   velitelem v Krakově pplk. **L. Svoboda**

-   22.5 1939 → Francie -- cizinecká legie (1200 mužů; když Francie
    začne válku proti Německu, tak češi budou bojovat proti Německu)

-   po 1.9. 1939 boje na straně Poláků

-   18.9. 1939 asi 100 mužů + pplk. Svoboda zajati RA

-   Z Polska přes Rumunsko → Francie → Británie → Střední východ

Francie
-------

-   výcvikové středisko Agde

    -   \+ krajané (celk 8000 mužů)

    -   leden 1940 1. čs. pěší divize

    -   gen. Rudolf Viest, gen. Jaroslav Čihák

-   celkem 11400 mužů

-   červen 1940 boje o Francii

-   asi 4000 evakuováno do V.B.

-   ústup přes Gibraltar do Británie (23.6. 1940)

Blízký východ
-------------

-   **Bejrút**; 29.6. 1940 asi 200 osob

-   únor 1941 **Alexandrie**

-   červen 1941 760 mužů

-   1\. boje v Sýrii

-   21.10. 1941 11. východní prapor → **Tobruk**

-   gen. Karel Klapálek

-   květen 1942 -- obrana Haify

-   poč. 1943 obrana Tobruku

-   **5.7. 1943 1326 mužů → Anglie**

Velká Británie
--------------

-   červenec 1940 -- výcvikové střed. **Chomondely Park** \[čomly\]
    (Chester); 5000 mužů

-   říjen 1940 čs. brigáda -- 3.300 mužů

-   1942/43 obrana pobřeží Anglie

-   \+ 1326 mužů z Afriky

-   gen. Bedřich Neumann

-   1.9. 1943 čs. smíšená obrněná brigáda (4500 mužů)

-   gen. Alois Liška

-   → podíl na invazi do Normandie; dobyli město Dunkerque

Čs. letecké jednotky
--------------------

-   z Polska → Francie (470 mužů)

-   červenec 1940 → V. Británie (nemůže být cizí armáda podel zákona)

-   gen. Karel Janoušek

-   říjen 1940 přijati do RAF

    -   bitva o Anglii:

        -   310\. peruť stíhací

        -   312\. peruť stíhací

        -   313\. peruť stíhací

        -   311\. peruť - ofenzivní let nad Německo

-   Celkem přes 2000 letců

-   od r. 1939 - 1945 na Západě 14 000 mužů

-   přes 1200 zahynulo

Východní odboj
--------------

-   **18.7. 1941 dohoda o** spolupráci v boji proti Německu

    -   exilová vláda + SSSR

        -   souhlas s vytvořením čs. jednoty v SSSR

-   **Buzuluk** -- únor 1942

    -   gen. Heliodor Píka, **pplk. Ludvík Svoboda**

    -   1\. čs. samost. prapor

-   **8.3. 1943 bitva u Sokolova** (nadp. O. Jaroš - padl, po smrti
    povýšen na kapitána)

    -   1\. čs. samost. brigáda (léto 1943)

-   **říjen, list. 1943 boje o Kyjev**

-   **4.1. 1944 Bílá Cerkev**

-   1\. čs. armádní sbor (duben 1944; 16 000 mužů)

-   postup s 38. armádou I. ukr. frontu

Změna čs. zahraniční orientace
------------------------------

-   12.12. 1943 Smlouva o přátelství, vzájemné pomoci a poválečné
    spolupráci

    -   E. Beneš - mění se zahraniční orientace čs., vydírání ze strany
        > Stalina

Slovensko 1939 - 1944
=====================

-   stát závislý na Německu

    -   němečtí poradci

    -   Hlinkova garda (Hlinka už nežil)

    -   Úřad propagandy

    -   Ústředna státní bezpečnosti

-   prezident Jozef Tiso

-   před. vlády Vojtěch Tuka

-   slovenský sněm

-   ideologie: klerikalismus (Tiso - kněz), protičeský a protimaďarský
    nacionalismus a fašismus

-   arizace židovského majetku

-   většina národa souhlasí - neměli nikdy svůj vlastní stát, chtěli
    nezávislost

-   odboj antifašistický

-   KS (komunisté), evangelíci ve spojení s Londýnem

Slovensko ve válce
------------------

-   září 1939 vyhlášena válka Polsku

-   23.6. 1941 vypovězena válka SSSR

    -   válečné hospodářství -- nespokojenost obyvatel, vše do německa

    -   60 000 mužů posláno na frontu

    -   nechtěli bojovat proti SSSR a za stranu německa

    -   → dezerce i

        -   květen 1942 kpt. Ján Nálepka

        -   říjen 1943 Melitopoľ -- 2750 mužů

Odboj
-----

-   perzekuce do povstání mírná

    -   vězněni členové ÚV KSS

-   odboj

    -   komunistický (1.-5. ÚV KSS): K. Šmidke, G. Husák

    -   občanský blok: J. Ursíny, J. Lettrich, V. Šrobár

-   září 1943 jednání

-   **21.12. 1943 Vánoční dohoda:**

1.  SNR (slovenská národní rada)

2.  společný boj proti německému i domácímu fašismu

3.  obnovení ČSR

Slovenské národní povstání
--------------------------

-   jaro 1944 -- Vojenské ústředí

    -   pplk. Ján Golian

-   29.6. 1944 porada SNR a VÚ

    -   příprava posvtání s posunem RA

    -   4.8. 1944 do Moskvy K. Šmidke a Mik. Ferjenčík

Partyzáni
---------

-   jaro 1944 -- 26 desantů ze SSSR

    -   konec srpna -- asi 15000 mužů

    -   Jegorov, Veličko, Ušiak, Lad. Kalina aj.

-   12.8. Tiso příkaz slovenské armádě zlikvidovat „bandity" - partyzány
    → většina armády se k nim přidala

-   24.8 německý velvyslanec u Tisy

-   partyzáni obsazují města: Martin, Lipt. Mikuláš, Brezno, aj.

Počátek povstání
----------------

-   29.8. 1944 něm. oddíly překročily slovenské hranice u Čadce a
    Púchova; u Bratislavy

-   **29.8.** J. Golian vyzval slov. armádu k odporu

-   povstalecké území 20000 km2

-   Banská Bystrica

-   mobilizace

-   2 divize slov. armády na vých Slovensku x gen. Höffle (40 000 mužů)

Průběh povstání
---------------

-   **1.9. 1944 Deklarace slovenského národa**

1.  SNR pro obnovení ČSR

2.  přebírá moc

3.  výzva k boji → teror - němci vypalovali vesnice

-   Strečno (mezinárodní povstání - mnoho cizích partyzánů), Telgárt,
    Martin, Ružomberok

-   Tri duby - letiště

Karpatsko-dukelská operace
--------------------------

-   8.9. -- 27.11. 1944

    -   6.10. -- 1. čs. armádní sbor

-   ztráty:

    -   RA 80000; 21000

    -   čs. arm.sbor. 6500; 1844

Porážka povstání
----------------

-   27.10. 1944 Němci obsadili B. Bystrici

-   27./28.10. gen. R. Viest z Donoval rozkaz k přechodu na partyzánský
    boj

    -   Prašivá, Ďumbier

    -   3.11. 1944 zajati R. Viest aj. Golian

        -   popraveni

-   mezinárodní charakter povstání

-   masový teror Němců a Hlinkových gard

    -   hromadné hroby

    -   Tokajík, Sklabini, Kunerad, Uhrovce

Osvobození Československa
-------------------------

-   ohlas SNP

-   vyhláška K.H. Franka

-   partyzáni na Moravě: Beskydy, Českomoravská vrchovina

-   britské a sovětské desanty

-   Σ 90 skupin s 8000 příslušníky

Směry osvobození
----------------

-   pražský směr:

    -   1\. UF (Ukrajinský front) -- maršál Koněv

    -   4\. UF -- gen. Petrov

-   vídeňský směr:

    -   2\. UF + rumunské divize -- maršál Rodion Malinovský

-   padlo 140000 vojáků RA

-   západní Spojenci

    -   3\. armáda; gen. Bradley, gen. Patton

    -   5.5. 1945 frontální postup 6 divizí (150 000 vojáků)

-   **linie České Budějovice -- Plzeň -- Karlovy Vary**

-   Σ 127 padlých

Moskevská jednání
-----------------

-   22.-29.3. 1945

-   prezident Beneš s KSČ (Komunisté výhoda)

    -   návrh vládního programu

    -   složení 1. čs. vlády

    -   ustavena NF (národní fronta)

-   4.4. 1945 vláda v Košicích

    -   5.4. 1945 Košický vládní program

Vrchol českého odboje
---------------------

-   stř. Čechy -- 1 mil. mužů arm. skupiny Mitte

-   maršál Schörner

-   90 000 SS a německé policie

-   teror:

    -   Ploština, Javoříčko, Dolní Životice

    -   poprava 52 odbojářů v Terezíně

-   29./30.4. 1945 ustavena **ČNR** (česká národní rada)

    -   prof. Albert Pražák

Pražské povstání
----------------

-   začátek Května povstání v přerově, Nymburku, Poděbradech, Chlumci,
    na Železnobrodsku, v Semilech, Jilemnici, jičíně

-   **5.5 Praha** -- boj o rozhlas

    -   1400 ČNR převzala moc

-   5./6.5. 1600 barikád; 30 000-50 000 bojovníků

-   6.5. ofenzíva SS

    -   1\. UF postup k Praze

    -   vlasovci - důležitá pomoc

-   7.5. bombardování Prahy

    -   represe proti obyvatelům

-   8.5. -- 3. jednání ČNR s Němci Protokol o provedení formy kapitulace

    -   SS kapitulaci neuznaly

-   **9.5. příjezd RA**

-   boje i 10. 5.

Ztráty ČSR
----------

-   300 000 obyvatel = 25 občanů z 1000

-   ČSR patří k 5 nejvíce postiženým v Evropě (SSSR, Jugoslávie)

-   pronásledována inteligence: Josef Čapek, Emil Filla, Ferdinand
    Peroutka, Vladislav Vančura, Vojtěch Preissig, Karel Poláček, aj.

Výsledky 2. světové války
-------------------------

-   změna politické mapy světa

-   2 supervelmoci

-   SSSR -- velké ztráty

-   sílí pozice KS

-   levicové tendence v západní Evropě

-   USA -- nejsilnější ekonomicky i politicky

-   rozpad kolonií

-   Čína -- konec občanské války (1949)

-   válečné škody

    -   Německo -- 6mil. obyv.

        -   vysídlení z ČSR a Polska
