---
title: 9_ Německo - vznik fašistické diktatury
---
Dopad hospodářské krize
-----------------------

-   pokles prům. výroby na 53,3% (1932)

-   zahraniční obchod o 2/3

-   konec zahraničních investic

-   růst nezaměstnanosti:

    -   1929 1,6 mil.

    -   1931 4,3 mil.

-   poč. 1933 6 mil.

-   snižování mezd

-   **proletarizace** středních vrstev - drobní podnikatelé museli
    zavřít své podniky, dostali se na úroveň nezaměstnaného dělníka \--
    využil Hitler

Politické důsledky krize
------------------------

-   14.9. 1930 volby do Říšského sněmu

    -   NSDAP z 12 na 107 mandátů

-   prezident Hindenburg -- koaliční vlády

-   stupňování sociální demagogie NSDAP (obviňovali z krize židy,
    bolševiky, ...; sliboval že bude odčiněna Versailleská smlouva)

-   11.10. 1931 Harzburg

    -   NSDAP + Stahlhelm (vysloužilci z 1. sv. války)

-   oporou Hitlera Kirdorf, Thyssen (zbrojařské průmysly - viděli
    budoucí zisk)

1932
----

-   červen -- novým kancléřem Franz von Papen (strana Centrum; neměl
    podporu)

-   **31.7. 1932 volby do ŘS**

    -   NSDAP 37,8% (měla právo určit předsedy říšského sněmu; \> ½
        > nezaměstnaných)

    -   SPD 22%

    -   Centrum 16%

    -   KPD 15%

    -   ostatní 9%

→ Hermann Göring předsedou Říšského sněmu

Pokračování politické krize
---------------------------

-   NSDAP v ŘS -- nedůvěra vládě Papena

-   12.9. 1932 ŘS rozpuštěn

-   narůstal pouliční teror SA (terorizovali kohokoliv -
    kontraproduktivní, většina lidí chce klid (já taky :( ); *černý
    obelisk* - Remarque)

-   první známky **oživení výroby**

-   6.11. 1932 volby do ŘS

    -   NSDAP ztratila asi 4 mil. hlasů

-   novým kancléřem Kurt von Schleicher - generál vermachtu, vkládali se
    do něj naděje že oslabí NSDAP

Cíl gen. K. von Schleichera
---------------------------

-   rozšířit pracovní místa

-   zapojit odbory

-   zapojit SPD

-   plebejské křídlo NSDAP - věděl že reformy myslí vážně

-   rozštěpit NSDAP

-   konec 1932 -- komunální volby

    -   další pokles NSDAP - Hitler potřeboval udělat kroky aby získali
        > moc, ,,nálepkování" - i když není pravda a očistíte se nikdy
        > se toho nezbavíte

Hitlerův převrat
----------------

-   obavy z „rudého generála" - Kurt von Schleicher

-   28.1. 1933 Schleicher přinucen k demisi

-   **30.1. 1933 Hitler říšským kancléřem **

→ Německo = fašistickou diktaturou

-   1.2. rozpuštěn ŘS

-   postupná likvidace demokracie

Zastrašování před volbami
-------------------------

-   diskreditace politických nepřátel - nemá šanci se očistit před
    volbami, na Komunisty a soc. dem.

-   **26./27.2.1933 požár Říšského sněmu**

-   zatýkání členů SPD a KPD - utekli před

-   zatčeno 300 000 osob

-   zavražděno 4 200 osob

-   lipský proces (září -- prosinec 1933 \--\>)

    -   G. Dimitrov, Popov, Tanev → Dimitrov obhájil je

    -   M. van der Lubbe - popraven mladík

Volby do ŘS 5.3. 1933
---------------------

-   NSDAP 43,9% = 288 mandátů

-   německá nacionální strana 8%

-   SPD 120 mandátů

-   KPD 81 mandátů

-   Centrum 74 mandátů

Kroky k diktatuře
-----------------

-   zmocňovací zákon (23.3. 1933)

-   zákaz KPD (14.3.)

-   zákaz SPD (10.5.)

-   od 14. 7. 1933 NSDAP = jedinou politickou stranou

-   koncentrační tábory

-   tažení proti židům → 1.4 1933 bojkot židovských obchodů

-   3.5.1933 Geheime Statspolizei -- gestapo

-   12.11. 1933 volby do ŘS s jedinou kandidátkou

-   NSDAP 92% hlasů

Spor o „druhou revoluci"
------------------------

-   vedení SA

-   30.6. 1934 Noc dlouhých nožů (Röhmův puč)

-   Bad Wiessee

-   zavražděno asi 100 předáků SA

-   G. Strasser, E. Röhm, K. von Schleicher

-   asi 1000 osob

-   SS = mocenská síla „Třetí říše"

Charakteristika Německa
-----------------------

-   klasický totalitní stát

-   policejní režim

-   státní centralismus (zrušeny zemské parlamenty)

-   většina obyvatel souhlasí s režimem

-   snížení nezaměstnanosti (1937 ½ mil.)

-   zahraničně politické úspěchy

-   od srpna 1934 Hitler „vůdce a říšský kancléř"

Ideologická převýchova
----------------------

-   zneužita filosofie F. Nietzscheho

-   nenávist k demokracii

-   kult vůdce, síly, války

-   nacionalismus, rasismus -- nordická rasa, Übermench

-   Lebensraum

-   pohrdání „méněcennými" národy a rasami: Židy, Romy, Slovany,
    barevnými, i Francouzi

Pronásledování Židů
-------------------

-   15.9. 1935 zákon o ochraně německé krve a cti

-   součástí Norimberských zákonů

-   Židé zbaveni občanských práv

-   diskriminace v dopravě, školství, povolání, veřejných prostorách
    (viz učebnice str. 56)

-   9./10.11. 1938 „Křišťálová noc"

Hospodářská politika
--------------------

-   řízené hospodářství

    -   přídělový systém

    -   4leté plány

    -   vojenské výdaje

    -   strategické zásoby

    -   podpora zemědělství

    -   omezen volný pohyb pracovních sil

    -   rozpuštěny odbory -- Německá fronta práce

-   zákon o dědičných statcích (29.8.1933)

Příprava na válku
-----------------

-   19.10. 1933 vystoupilo ze SN

-   1935-1937 -- průmyslová výroba -- zbrojení

-   krize r. 1937 se v Německu neprojevila

-   podobný systém chtěli nacisté i v ovládnuté Evropě
    (Grossraumwirtschafft)

Kulturní politika
-----------------

-   Josef Goebbels - šéfem propagandy, velmi inteligentní

-   → pálení zakázaných knih - pocit sounáležitosti

-   cenzura

-   zneužívání rádia, tisku, sportu

-   1937 -- Dům německého umění - předměty které měli symbolizovat
    nacismus z germánských časů

-   muzeum „degenerovaného umění"

-   emigrace: H. a T. Mannové , E. M. Remarque, A. Zweig, L.
    Feuchtwanger; M. **Dietrichová** - herečka a šansoniérka, nenáviděla
    nacismus
