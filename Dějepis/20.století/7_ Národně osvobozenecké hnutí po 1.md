---
title: 7_ Národně osvobozenecké hnutí po 1
---
Charakteristika
===============

-   růst ekonomického významu kolonií

-   nacionalismus

-   ,,politické národy"

-   demokratizace se neuplatnila

-   oslabení mateřských zemí

Čína
====

-   od 1911 republikou

-   ale ústřední vláda neměla skoro žádný vliv

-   militaristé (v těch oblastech se chovali jako diktátoři)

-   hnutí 4. hvězda v Šan-tungu (Japonsko ho chtělo ovládnout; proti
    japonské hnutí)

<!-- -->

-   Sun Jat-sen

-   zásady Kuomintangu ( zal. 1912)

    -   nacionalismus

    -   demokracie

    -   blahobyt lidu

Změny v Kuomintangu
-------------------

-   1925 Sun Jat-sen zemřel → gen. Čankajšek

    -   Národně revoluční armáda

-   duben 1927 státní převrat

    -   vyloučena KS (Mao Ce-tung)

> ↓

-   občanská válka

<!-- -->

-   říjen 1928 Nanking -- Čankajškova vláda

Indie
=====

-   největší kolonie britů

-   patřil k nim i Pákistán a Bangladéš

-   demonstrace v **Amritsar** (13.1. 1919) (potlačeno velmi tvrdě,
    hodně obětí)

-   Indický národní kongres

-   Mahatma Gandhi

-   Jawaharlal Nehrú

-   **program nenásilí**

-   hnutí spolupráce - prosinec 1920

-   únor 1922 pokojná demonstrace ve městě Čauri-Čaura - zase vojensky
    zasáhnuto

↓

-   kampaň odvolána

-   Gandhi 6 let vězněn

Turecko
=======

-   1920-21 válka s Řeckem

-   Mladoturecká strana

-   březen **1923** Turecká republika

-   prezident **Mustafa Kemal paša -- Atatürk** (- tak si říkali turci;
    totalitní režim; ,,otec Turků")

-   politický systém (země jediné politické strany - Mladé turecko;
    přijato právní zřízení v Evropě: zrušeno mnohoženství, vzdělání pro
    ženy, zavedena evropská móda, modernizace obecně)

Afghánistán
===========

-   od 1890 britský protektorát (předtím byl samostatný)

-   1919 samostatnost (nikdy se nepodařilo afghanistan ovládnout)

-   modernizace x islámští fundamentalisté

-   → vyhrávají fundamentalisté

Írán (Persie)
=============

-   1919 britský protektorát

-   hnutí důstojníků a buržoasie

-   Réza chán Pahlaví - stát v čele (dělat modernizaci všeho možnýho,
    umožnění vzdělání ženám)

-   1925 zvolen doživotním panovníkem

-   ekonomické reformy

Arabské země
============

-   konec války zklamání -- nejsou samostatné

-   Turecko(?) z nich vytvořilo mandátní území (podobné jako
    protektorát)

-   Saúdská Arábie

-   1926-32 Ibn Saud

-   Irak

-   1920 samostatnost (Britové)

-   Zajordánsko (dnešní Jordánsko)

-   1928 vyhlášena nezávislost

Palestina (dnešní Izrael)
=========================

-   britská mandátní území

-   sionismus

-   Theodor Herzl - židovského původu, zajímal se o politiku, přišel s
    myšlenkou, že židé mají právo na svůj stát - to popsal ve svém díle
    Judenstaat

-   významné osobnosti podporovali sionusmus

→ přistěhovalectví Židů

-   od začátku věků konflikty s Palestinci

Egypt
=====

-   formálně nezávislý od 1922

Sýrie + Libanon
===============

-   francouzské mandátní území

-   1925-27 povstání Drúzú

-   republika

Latinská Amerika
================

-   posílení válkou

-   užší kontakty s USA vliv na politiku

-   panamerické konference

-   **1923 Panamerická smlouva**

-   nestabilita, střídání vlád, sklon k totalitním režimům

-   někde reformy: Argentina, Brazílie, Chile, Mexiko

-   vzpoury: Bolívie, Brazílie, Venezuela, Kuba
