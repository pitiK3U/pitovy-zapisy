---
title: 23_ Studená válka
---
Studená válka OBDOBÍ KONFRONTACE 
=================================

\- vše kromě vojenské války

Střední a jihovýchodní Evropa
-----------------------------

-   obsazeno RA (krom Řecka, Albánie)

    -   konec 1945 -- odchod RA z ČSR a Jugoslávie

    -   zůstala -- Rumunsko, Maďarsko, Německo, Rakousko (i armáda
        > západních spojenců)

-   nátlak

    -   model 1: využívání domácí KS

    -   model 2: ozbrojený zásah

-   1946 -- 1948 lidově demokratické státy (lidová vláda lidu)

Informační byro KS (září 1949 -- 1956)
--------------------------------------

-   Andrej Alex. Ždanov**: rozdělení světa do dvou táborů -- třídní
    podklad** (kde vládne dělnická třída jsou ,,hodné" X kapitalistické)

-   červenec 1947 G. F. Kennan

-   Západ -- příklon k levici; válka je zklamala ve víře pravicím =
    neochránila

-   Harry Truman

-   W. Churchill -- Fulton (15.3.1946)

-   učebnice str. 103

-   Winston Churchill at Fulton (You Tube)

Trumanova doktrina --12.3. 1947
-------------------------------

-   doktrina = projev

-   Kongres

    -   G. Marshall, G. Kennan

-   politika zadržování komunismu - 400 mil \$

    -   Turecko, Řecko

-   cíl = stabilizace v zemích ohrožených sovětskou expanzí

Marshallův plán (5.6. 1947)
---------------------------

-   hosp. obnova

-   nenávratná půjčka

-   dodávky spotřebního zboží, surovin, strojů

-   obnova Německa

-   postoj SSSR → ČSR, Polska

-   červen, červenec 1947 -- Paříž

    -   konference min. zahr. věcí

    -   V. Molotov

-   realizace 1948-1952

-   modernizace infrastruktury

-   pozitivní v obl. sociální a politické

Německo
-------

-   sovětská zóna:

    -   KS, SED

    -   1946 volby -- SED

    -   radikální ekonomická opatření

-   květen 1948 - USA, VB, Fr + Belg., Ni, Luc.

-   3.6.1948 dohoda

    -   o vytvoření něm, státu (3 zóny)

    -   ústavě

    -   měnové reformě

Vznik SRN
---------

-   západní zóny:

    -   CDU; (CSU) - křěsťané?

    -   FDP - svobodní demokraté

    -   SPD - sociální demokraté

    -   KPD - komunisté

-   zemské volby

-   USA + VB → Bizonie (1.1. 1947)

-   \+ Fr → Trizonie (17.4. 1948) - není samostatný stát

Berlínská krize
---------------

-   26.7.1948 - 12.5.1949

-   blokáda přístupových cest do záp. Berlína

-   mezinárodní napětí - hrozila 3. sv. v.

-   letecký most - 250 tun zboží

<!-- -->

-   1.9. 1948 Bonn; prozatímní parlament

    -   Konrad Adenauer - nepošpinil se nacismem

-   12.5. 1949 schválena 3 mocnostmi ústava

-   srpen 1949 volby do Spolkového sněmu - CDU

    -   Theodor Heuss prezident

    -   K. Adenauer spolkový kancléř

-   **7.9. 1949 vyhlášena SRN**

-   Západní Berlín

Sovětský blok
-------------

-   7.10. 1949 vyhlášena NDR

    -   Wilhelm Pieck

    -   Sovětská kontrolní komise

    -   RA

-   Albánie, Bulharsko, Československo, Maďarsko, NDR, Polsko, Rumunsko

-   Jugoslávie

    -   J. Broz-Tito x Stalin

    -   1948 -- nebezpečí konfliktu

    -   1955 schůzka Tita s Chruščovem ukončení sporů

NATO
----

-   4.4. 1949

-   (USA, VB, Kanada, Francie, Belgie, Nizozemí, Itálie, Lucembursko,
    Norsko, Dánsko, Island, Portugalsko)

-   1952 Řecko, Turecko

-   1955 SRN

-   12.3. 1999 ČR

-   Anders Fogh Rasmussen (od 2009)

USA v AMERICE

-   mezinárodní konference

-   Rio de Janeiro 1947

-   Bogota 1948

-   Charta **Organizovaných amerických států**

Korejský poloostrov

-   Korea osvobozena USA + SSSR

-   38\. rovnoběžka

-   **Severní Korea**: vliv SSSR

    -   Kim Ir-Sen

    -   9.9. 1948 KLDR

-   **Jižní Korea**: 1948 volby do NS

    -   Li Syn Man

    -   Korejská republika

Korejská válka (1950-1953)

-   25.6. 1950 útok severní Koreje

-   1\. fáze: (25.6. -- září 1950)

    -   28.6. - Soul

-   USA -- Rada bezpečnosti OSN

    -   vojenská pomoc (16 států, 90% USA)

    -   gen. Douglas Mac Arthur

-   2\. fáze: (září -- 1.10. 1950)

    -   výsadek USA v týlu S korej. armády

    -   ústup

    -   osvobozen Soul

    -   1.10. USA překročily 38. rovnoběžku -- čínské hranice

-   3\. fáze: říjen 1950 -- 27.7. 1953)

    -   čínští dobrovolníci (1mil.)

    -   vojska OSN na 38. rovnoběžku

Jednání o příměří

-   červenec 1951 -- 27.7. 1953

-   Pchanmundžom

    -   demarkační linie

    -   demilitarizované pásmo

Sjednocování Západu

-   vyhrocení napětí Východ x Západ

-   **Pařížská smlouva** (27.5. 1952)

    -   Evropské obranné společenství (EOS)

    -   USA, Francie, SRN, Benelux

-   24.10. 1954 **pařížské dohody**

    -   plná suverenita SRN

    -   květen 1955 přijata do NATO

Vznik Varšavské smlouvy

-   14.5. 1955

-   politická a vojenská spolupráce

-   potvrzení dvou nepřátelských bloků

-   SSSR -- atomová a vodíková zbraň
