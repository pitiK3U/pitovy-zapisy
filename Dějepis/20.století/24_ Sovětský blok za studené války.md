---
title: 24_ Sovětský blok za studené války
---
Sovětský blok za studené války Ideologická a ekonomická unifikace
=================================================================

Ekonomická integrace

-   1949 RVHP

    -   extenzivní socialistická industrializace

    -   těžba surovin

    -   těžký průmysl

    -   industrializace zaostalých států

    -   nedostatek spotřebního zboží

    -   centrální plánování

    -   likvidace soukromého vlastnictví

    -   kolektivizace zemědělství

    -   věda, kultura

Policejní režimy

-   perzekuce

-   represe i proti členům KS

-   cíl: zastrašování (Jugoslávie)

-   politické procesy: popravy

-   László Rajk

-   Trajčo Kostov

-   Rudolf Slánský, Vladimír Clementis

Stalinova diktatura

-   kult osobnosti

-   nekonaly se sjezdy KSSS

-   řídil zahraniční politiku

-   zásahy do vědy: jazykověda, ekonomika apod.

-   kybernetika

-   XIX\. sjezd KSSS (1952): lži o hosp. úrovni

-   propagace gigantických staveb

-   protižidovská kampaň

-   leden 1953 zatčení kremelských lékařů

-   L. P. Berija

-   5.3. 1953 Stalin zemřel
