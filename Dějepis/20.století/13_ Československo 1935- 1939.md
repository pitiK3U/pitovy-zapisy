---
title: 13_ Československo 1935- 1939
---
Československo 1935- 1939
=========================

1934
----

-   doznívání krize

-   politické problémy:

    -   posilování KSČ

    -   sílí krajní pravice

        -   pravice silnější i ve vládě

-   český fašismus = okrajový:

    -   část Národně demokratická strana

    -   Národní liga

    -   Národní fronta

> ↓

-   Národní sjednocení

    -   K. Kramář, F. X. Hodáč

    -   program

Nacionalismus v německém prostředí
----------------------------------

-   4.10. 1933 zakázána DNSAP a DNP

-   2.10. 1933 založena Sudetendeutsche Heimatsfront (SHF)

    -   Konrád Henlein, K. H. Frank

    -   kontakt s NSDAP (program zatím nezveřejnili)

Agrární strana (republikánská)
------------------------------

-   proti smlouvám z r. 1935

-   pravicový proud: R. Beran (, Josef Zadina)

    -   kontakty s Henleinem

Mezinárodní situace
-------------------

-   spojenectví s Francií

-   E. Beneš

    -   koncepce kolektivní bezpečnosti

    -   přehodnocení vztahu k SSSR -- uznán de iure (1934; jako
        > suverénní stát)

-   **1934 -- německo-polská smlouva o neútočení** (nebezpečné pro čsr)

> ↓

-   smlouva s SSSR -- Francie (2.5. 1935)

-   ČSR -- SSSR (16.5. 1935)

Situace před volbami 1935
-------------------------

-   sílí agresivita SHF

-   agrárníci chtějí jednat s Henleinem

-   demokratické strany -- žádost zákazu SHF

↓

-   změna názvu SdP (30.4. 1935; změnili název → do voleb se nedalo
    rozhodnout o zakázání)

Volby do NS 19.5.1935
---------------------

1.  SdP - volili sudetní němci

2.  agrární str.

3.  soc. dem.

4.  KSČ

5.  nár.

6.  lid. str.

nejvíce mandátů agr. (45)

SDP (44)

-   Nár. sjednocení 5,6%

-   NOF 2,0%

-   HSLS 6,9%

    -   (na Slovensku 50%)

Vláda agrárníků
---------------

-   Jan Malypetr

-   odvolán R. Beranem

-   → dr. Milan Hodža (od 5. 11. 1935; první Slovák, stal se předsedou v
    nešťastné době)

Prezidentské volby
------------------

-   21.11. 1935 oznámil **T.G. Masaryk** příští abdikaci (ohlásil
    dopředu)

-   14\. 12. **abdikoval**

-   kandidáti:

    -   dr. Edvard Beneš

    -   dr. Bohumil Němec (agrárníci; těsně před volbami ukončil
        > kandidaturu)

-   18.12. **1935** zvolen **E. Beneš** (340 hlasů z 440; nemohl
    vykonávat fce ministra zahraničí)

    -   24 pro Němce -- neplatné

    -   76 prázdných - nesouhlasili s kandidaturou Beneše

<!-- -->

-   Ministr zahraničních věcí -- dr. **Kamil Krofta**

-   ČSR = ostrov demokracie

-   útočiště emigrantů

-   Heinrich a Thomas Mannové

-   účast dobrovolníků ve Španělsku

Agitace K. Henleina
-------------------

> • podporován v Anglii a Rakousku
>
> • 1936 „Anglie je ochotna pomoci sudetským Němcům v SN"
>
> • Henlein v Anglii přednášel
>
> • srpen 1936 -- jedná s Hitlerem =\> spolupráce H. a SdP

Program SdP
-----------

1.  teritoriální autonomie -- na území ČSR (žili zde ale i češi)

2.  uznání za 2. státní národ

3.  právo hlásit se k německému národu jako jeho součást → připojení k
    německu

4.  vypsání nových voleb -- pokus o zisk větší moci

Iniciativa M. Hodži
-------------------

> • plán pro Podunají
>
> • únor 1937 -- jednání se o zlepšení postavení německé menšiny
>
> • dohoda o národnostním vyrovnání -- SdP odmítla
>
> • podzim 1937 -- jednání Hodži s Henleinem -- ztroskotala
>
> • T.G. M. = morální oporou
>
> • 14.9. 1937 zemřel

Rok 1938
--------

> • 1.1. článek R. Berana -- přijetí Henleina a Hlinky do vlády aby se
> zlepšily vztahy s Německem
>
> • odpor veřejnosti
>
> • 11.--13. 3. anšlus Rakouska
>
> -- změna mezinárodního postavení ČSR
>
> -- hranice s Německem 2100 km
>
> -- SdP stupňovala nátlak
>
> -- provokace v pohraničí

Osm karlovarských bodů
----------------------

> • březen -- jednání Henleina s Hitlerem: autonomie \... odtržení
>
> • 1.4. Hodža zahájil jednání s SdP
>
> • 23.-24.4. 1938 sjezd SdP v Karlových Varech
>
> -- úplná autonomie Sudet
>
> -- samospráva -- německém sídelním území
>
> -- svobodné přiznání k něm. národu
>
> -- Němci -- 2. státní národ
>
> -- A mnoho dalších nepravdivých výmyslů a požadavků
>
> • SdP ve spojení s NSDAP
>
> • 21.4. 1938 plán Fall Grün (Hitler + Keitl podepsali)

x

> • 15.5. manifest *Věrni zůstaneme* (pozděj vydán jako petice a
> podepsán milióny)
>
> • nepokoje v pohraničí - pokračovali
>
> • přesuny německého vojska
>
> • pohotovost SA a SS (oddíly)
>
> ↓
>
> • 21.5.1938 částečná mobilizace -- první, druhý a speciální ročník
> vojáků

Postoj Francie a Velké Británie
-------------------------------

> • E. Daladier:
>
> -- ČSR Francie nemůže pomoci
>
> -- ústupky Němcům
>
> -- jednání s Henleinem
>
> • 3.8. -16.9. mise lorda W. Runcimana
>
> -- Když přijel němci si stěžovali („Němky si trhaly vlasy")
>
> -- Podal zkreslenou zprávu
>
> • SdP odmítla 3. a 4. plán E. Beneše

 Vrchol útoků na ČSR
-------------------

-   26.8. K. H. Frank v Berlíně

-   2.9. Henlein v Berlíně

-   **12.9. sjezd NSDAP v Norimberku**

    -   Hitler veřejně žádal připojení pohraničí (je nárokem Německa
        > vzít si čs pohraničí)

-   **12.9. pokus o puč v pohraničí** (henleinovci; přepadali policejní
    stanice, pošty; řada lidí zraněna)

-   stanné právo

-   13.9. puč potlačen

    -   vůdcové SdP uprchli do Německa

    -   16.9. zákaz SdP

    -   17.9. založeny Freikorps

Cesta k Mnichovu
----------------

-   15.9. schůzka Chamberlaina s Hitlerem (v Berchtesgadenu - Hilter si
    ho pozval k sobě a nejel do Anglie)

-   19.9. předloženo čs. vládě jako **ultimátum** (území s více jak 50%
    němců;)

-   20.9. vláda odmítla → zrušení smluv

-   20./21.9. angl. a fr. velvyslanci u prezidenta Beneše

    -   ultimátum přijato vládou - doufali že bude jednání o území

-   **22.9. generální stávka na obranu republiky**

-   22.9. odstoupila Hodžova vláda → aby další vláda nemusela ultimátum
    dodržet

-   úřednická vláda gen. Jana Syrového

-   **23.9. všeobecná mobilizace** (řopík - některé pevnosti na
    pohraničí, ne všechno dobudováno)

-   22.-23. 9. jednání Chamberleina s Hitlerem v Bad Godesbergu

    -   rozsah a realizace německých požadavků

 Mnichovská konference **29.9.1938**
-----------------------------------

-   A. Hitler

-   B. Mussolini

-   N. Chamberlain

-   E. Daladier

-   bez zástupce ČSR (,,o nás bez nás")

-   Hitler získal opevnění včetně zbraní

Neplatnost mnichovské dohody
----------------------------

-   Velká Británie a Francie r. 1942

-   Itálie 1944

-   NDR 1950

-   1973 dohoda mezi ČSSR a SRN o anulování MD (jako kdyby žádná nebyla)
    s konečnou platností

Důsledky mnichova
-----------------

-   ztráta

-   41 098 km2

-   4 879 000 obyv. (odsun českých obyvatel z pohraničí do vnitrozemí,
    vzali si sebou co unesli)

-   přišli o průmyslové oblasti

-   porušena dopravní síť

-   pohraniční opevnění
