---
title: 6_ Totalitní režimy
---
Totalitní režimy

Shodné rysy
===========

-   odstranění demokracie

-   systém jediné politické strany

-   stát = nástrojem stranické politiky

-   jediný světový názor (podle jedné pol. strany)

-   proti liberalismu -- řízené hospodářství (chtějí ovládat
    hospodářství, aby vyhovovalo straně)

-   vůdcovský princip - v čele muži co jsou považovány za ideál, mají
    vždy pravdu a povedou lid

-   propaganda - ministerstvo

-   ideologizace školství - věděli že musí na děti působit už v mladém
    věku

-   zločiny a genocida ve jménu ideologie - vyvražďovali miliony lidí

-   potlačení lidských práv

Rozdíly
=======

-   ideologické zdůvodnění

    -   internacionalismus x národ

    -   Komunismus - nadnárodní

    -   fašismus

-   jiný historický vývoj

    -   komunismus - marxismus v pol. 19. stol. vydělení ze soc. stran

    -   

-   odlišné konečné cíle

    -   sociální spravedlnost x rasismus

    -   fašisté - stát pro čistou rasu

    -   komunismu - sociálně spravedlivý stát pro všechny

-   třídní boj x sjednocení tříd

    -   všichni lidé jsou členem jedné

    -   kom. - stále jsou třídy, které jsou protikladné

-   kolektivní vlastnictví x soukromé

    -   faš

    -   kom - znárodňování

-   humánní hodnoty x rasa je víc než jednotlivec

    -   faš - rasa je nade vše

    -   kom. - hodnoty které nedodržují

SSSR
====

-   vznik osobní diktatury Stalina (po r. 1924; ,,diktatura
    proletariátu", lenin nechtěl aby Stalin nastoupil)

-   teorie o zostření „třídního boje" (bál se o svoji pozici, špioni
    kteří chtěli poškodit ruský systém)

-   fyzická likvidace „nepřátelských tříd" - kdokoliv mohl být označen

-   1919 Komunistická 3. internacionála

    -   bolševizace stran

-   jednotné hnutí

Itálie
======

-   Benito Mussolini - tvůrce fašismu, jevil se jako \[šovinista\]

-   1919 -- Fasci di Combattimento (*bojové svazy*; bojůvky; frustrovaní
    italové, kteří nedostali co chtěli, mladí agresivní lidé)

-   → 1921 nacionální fašistická strana

-   růst členské základny (asi 1000 členů → 250 tis. členů; zabití asi
    200 lidí

-   fašisté ukradli socialistům program → aby je lidi volili, neplatilo,
    možná

-   program:

    -   pozemková reforma - přerozdělení pro drobné dělníky

    -   zábor církevní půdy

    -   sociální zabezpečení

    -   mzdové minimum

    -   připojení Rijeky, Dalmácie, Albánie (obnovení Imperia Romana)

    -   jednotná národní pospolitost -- korporace, celek je důležitý

    -   Duce (vůdce/vévoda)

<!-- -->

-   hlavní polit. nepřítel = socialistická strana

-   1921 -- volby jen 7% hlasů

-   ↓

-   říjen **1922 pochod** fašistických milicí **na Řím** (zmocní se Říma
    násilím)

-   ?? chtěl aby král podepsal mimořádný zákon aby se proti bojůvkám
    vojstvo - nepodepsal

Cesta k fašistické diktatuře
----------------------------

-   30.10. 1922 Mussolini jmenován předsedou vlády

-   1924 -- parlament- kritika Giacoma Matteottiho (socialista;
    kritizoval postupy fašistické) → zavražděn

-   prosinec 1925 schválen nový ústavní zákon

    -   výkonná moc: král + vláda

    -   zákonodárná moc: parlament + **souhlas Velké fašistické rady**

<!-- -->

-   říjen 1926 výjimečné zákony (zákaz jiných stran)

-   **1928** Velká fašistická rada: zrušen parlament → **fašistická
    diktatura**

    -   formálně konstituční monarchie

    -   cenzura

    -   tajná policie

    -   státní kontrola hospodářství

    -   zákaz nefašistických stran

    -   zákaz stávek a odborů

Šíření fašismu
--------------

-   diktatury:

    -   Portugalsko -- A. Carmona, A. Salazar

    -   Španělsko -- Vojenská diktatura; 1923-30 (Primo de Rivera )

    -   Polsko - (Józef Piłsudski (1926-35))

    -   jižní Amerika: (Ekvádor, Brazílie, Argentina, Bolívie,
        > Dominikánská rep.)

-   režimy s fašistickými rysy: (sympatizovali s fašismem)

    -   Rumunsko, Bulharsko, Řecko, Albánie. Estonsko, Lotyšsko, Litva,
        > Maďarsko (království, včele admirál), Turecko

Fašistické strany v demokratických státech
------------------------------------------

-   Francie

    -   Cagoulardi

    -   Ohnivé kříže - důstojníci fr. armády - **nebezpečné**

    -   Vlastenecká mládež

-   Velká Británie

    -   fašistický svaz Sira Oswalda Mosleye - navazovali kontakty s
        > němci, sympatizoval se stranou král, který se vzdal kvůli \...

-   ČSR

    -   [[DNSAP]{.underline}](https://drive.google.com/open?id=1cufOh_UmqTTnZZhQ1ezS6lKe8wuHLndWJ--XFjOLd9Q)
        > → SdHf - sudetoněmecká strana

    -   DNP

    -   Národní obec fašistická

    -   Národní sjednocení

Německo
=======

-   nacionální socialismus (nacismus)

-   1919 DAP (dělnická strana) v Mnichově →1920 NSDAP (nacionálně
    socialistická strana německa)

-   Adolf Hitler (nepatřil mezi zakladatele)

-   Gregor a Otto Strasser -- *plebejské křídlo* (věřili socialistickému
    programu - socialistické reformy)

-   růst členské základny -- příčiny:

-   program

    -   sociální a ekonomické reformy

    -   boj x velkokapitálu (hájit zájmy velkostatkářů X monopolů;
        > Hitler považoval za lákadlo)

    -   podíl dělnictva na zisku

    -   pozemková reforma - pro venkovské obyv.

    -   odmítání versailleského systému

    -   demagogie - obviňovali židy ze všeho a když jim seberou majetek
        > vyřeší tím všechny problémy

    -   Velké Německo -- Drang nach Osten

Hitler (otec bil matku a děti, hitler na matce lpěl, nebyl přijat za
1.sv. do armády - označen za ? vojenky neschopného?)

-   8.11. 1923 „**pivní puč**" v Mnichově

    -   gen. Erich Ludendorf

    -   puč potlačen

-   NSDAP zakázána

-   Hitler uvězněn (9 měsíců)

    -   *Mein Kampf* (můj boj)

    -   odmítal kapitalismus

-   Alfred Rosenberg - ideolog nacistické strany, popraven

    -   *Mýtus 20. století* (F. Nietzsche - nadčlověk, rasa pánu X rasa
        > otroků, nebyl antisemita, zemřel 1900 - jeho sestra upravila
        > Nietzcheho spisy vhodné por nacisty)

Organizace NSDAP
----------------

-   vůdcovský princip -- Führer

-   demagogie - hledání jednoduchých řešení tam kde je to složité

-   přísný centralismus

-   ospravedlnili právo vraždit ostatní národy

-   SA (Sturmabteilung - útočný oddíl)

-   SS (Schutzstaffel - ochranný oddíl)

-   Hitlerjugend - propaganda dětí

-   děvčata - měli rodit mladé válečníky
