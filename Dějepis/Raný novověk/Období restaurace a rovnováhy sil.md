---
title: Období restaurace a rovnováhy sil
---
-   snaha vrátit vše do stavu před napoleonskými válkami

-   hlavní rysy:

    -   konsolidace tradičních monarchií

    -   neúspěšná povstání, spiknutí

    -   národní a národně osvobozenecká hnutí

    -   kapitalistická industrializace

    -   moderní státní správa - Francie vzorem

    -   pravidelně kongresy velmocí (první r. 1818 v Cáchách) - důležitý
        > pro Francii, odvoláno vojsko velmocí

**Národní hnutí a revoluce ve 20s 19. st.**\
Německo\
snaha sjednotit Německo\
Nenáviděli Napoleona\
mládež, profesoři, studenti začali zakládat ↓\
1815 vlastenecké spolky (buršenšafty)\
1817 setkání spolků na Wartburgu\
300 let výročí zveřejnění Lutherových tezí na hradě, kde se ukrýval\
Na nějaký čas revoluční nálada potlačena\
1819 karlovarská usnesení\
potrestání účastníků setkání na Wartburgu\
učitelé nemohli učit\
studenti nemohli studovat\
zakázány vlastenecké spolky\
Itálie\
• tajné organizace -- karbonáři, Silvio Pellico (byli kvůli vrácení
původních dynastií)\
• 1820 povstání v Neapolsku\
○ chtěli po novém panovníkovi Ferdinandu I. Bourbonském ústavu\
○ byl zaskočen\
○ → vyhověl\
• převrat v sardinském království (změnu ve vládě)\
• 1821 rakouská intervence\
• nepovedlo se, ale začala myšlenka sjednocení u intelektuálů\
Španělsko\
• leden 1820 -- revoluce\
• vliv na latinsko-americké kolonie\
○ Simon Bolivar (Bolivie), José San Martin (Martinik)\
○ nezávislost (v průběhu 40. let)\
○ Mexiko, Argentina, Peru, Chile, Kolumbie, Bolívie\
○ Brazílie - portugalská\
• 1823 francouzská intervence ve Španělsku

Rusko\
• spolky intelektuálů a vojáků, kteří nebyli spokojeni s absolutistickou
vládou (samoděržaví)\
• spolky o sobě nevěděli - nemohli zaznamenávat o schůzkách\
• 1821 Jižní spolek\
○ radikální\
○ Rusko republikou\
○ zrušení nevolnictví\
○ Část šlechtické půdy rozdělit dělníkům\
• 1822 Severní spolek\
○ Rusko konstituční monarchií\
○ zrušení nevolnictví\
• Společnost sjednocených Slovanů\
○ prvně osvobodit Rusko od samoděržaví (absolutismus)\
○ pak pomoci ostatním státům od Habsburků\
○ pak vytvořit společný federativní stát\
• 1825 zemřel car Alexandr I. → bezvládí\
• Mikuláš I.\
• členové spolků se domnívali, že jak nastoupí nový car, bude nejlepší
příležitost ho k něčemu přemluvit\
• 26. 12. 1825 povstání děkábristů - Senátní náměstí v Petrohradě\
○ Sešlo se 3000 důstojníků\
○ žádali na carovi, aby přistoupil na jejich požadavky\
○ spolky se o sobě dozvěděli až na místě → zmatky\
○ doufali, že se k nim cestou přidá lid, to se nestalo\
○ povstání tvrdě potlačeno armádou\
• období ještě tvrdší perzekuce

Národnostně osvobozenecké hnutí
-------------------------------

#### Srbsko

-   1804--1813 --- 1. protiturecké povstání

-   obklíčen Bělehrad

-   představitel ĐorĐe Petrovič KaraĐorĐe \[džordže karadžordže\]

-   sultán vyhlásil džihád - svatá válka

-   Srbové dobyli Bělehrad, Kalemegdan, Šabac

-   podpora Ruska (tylžský mír)

-   1812 rusko-turecký mír (zisk Besarábie, Moldávie) - srbové osamocení

-   1813 Turci táhnou na Bělehrad - Turky bylo zabito asi xx srbů

-   Miloš Obrenovič

-   1815--1817 --- nové povstání (nějak to moc nedopadlo, Turci
    > potlačili)

-   1827--1829 --- rusko-turecká válka

    -   připojila se Francie a Velká Británie

    -   Srbové a Rumuni si vydobyli autonomii

-   1829 drinopolský mír (Rusko-Turecko)

KaraĐorĐe Petrovič

-   Dorde (Jiří) Petrovič

-   kara = černý

-   vysoký 190cm - Turci se ho báli

-   strážce ovcí

-   Manželka Jelena Jovanovič

-   odešel do Rakouska -\> voják Rakouska

-   člověk, kterému se vyhýbají kulky

-   Turci zavraždili 76 vážených srbů

-   → 1804 vůdce I. srbského povstání proti Osmanské říši

-   zabíjel zrádce a dezertery

-   zakládali školy

-   snaha Turků o podplacení

-   1815 2.srbské povstání - Miloš Obrenovič

-   příkaz zabít Karadordeho: Nikola Novakovič

-   hlava poslána do Instanbulu

-   dynastie Karadordevičů X

-   národní hrdina

-   pomník v Bělehradě

-   pohřben v rodinné hrobce

Výsledky

-   Srbové získali autonomii

-   M. Obrenovič dědičným knížetem

-   svoboda vyznání

-   školy, tiskárny, pošta

-   půda = soukromý majetek

#### Řecko

-   Řekové v Turecku potřební

-   pravoslavná víra

-   kontakty s Evropou

-   1821 povstání v Moldavsku

-   často prchali do Ruska (třeba Alexandr Ypsilanti)

-   protiřecký teror (ostrov Chios; 1822)

-   filhelénské hnutí (dobrovolníci odjinud na podporu Řeků, zbraně,
    > potraviny, např. G. Byron)

-   1826 podpora Ruska → 1827--1829 --- rusko-turecká válka

-   1827 útok loďstva (VB, F, R - nechtěli aby si Rusko vydobylo vše na
    > Balkáně)

-   1829 drinopolský mír (Rusko-Turecko)

-   1830 Londýn: hranice; dědičná monarchie

-   1832 králem Otto Wittelsbach (Bavorsko)

#### Rumuni

-   knížectví Válachie, Moldávie

-   1831--1832 autonomie (v osmanské říši)

-   do 1851 obsazena ruským a tureckým vojskem

-   1861 sjednocena -- Rumunsko (Alexandr Ion Cuza byl zvolen v obojích
    > knížectví)

#### Bulhaři

-   národně osvobozenecké hnutí až ve 30. a 40. letech

**Revoluce ve 30. letech 19. st.**
----------------------------------

### Francie

#### 20. léta

-   králem Ludvík XVIII. (1814--1824)

-   poté Karel X. (1824--1830)

    -   chtěl obnovit absolutismus

    -   liberálové v poslanecké sněmovně

    -   svoboda tisku

    -   legální opozice: liberálové

    -   nelegální opozice

        -   demokraté

        -   republikáni

        -   bonapartisté

#### 1830

-   volby -- vítězí liberálové

-   král se s tím nechtěl smířit a nechal rozpustit sněmovnu

-   bylo vidět, že král chce zavést absolutistický režim

-   červenec -- Ordonance

    -   omezení politických práv

    -   rozpuštění poslanecké sněmovny

    -   velký krok k absolutismu

    -   nevešlo v platnost

-   26\. 7. barikády (*červencová revoluce*)

-   přidali se vojáci

-   Karel X. se s tím smířil, abdikoval a uprchl

-   Prozatímní vláda

-   Ludvík Filip Orleánský

#### Červencová monarchie 1830 - 1848

-   nový politický systém

    -   rozšířeny občanské svobody

    -   podpora podnikatelů

    -   snížen census (více lidí mohlo volit)

-   30\. léta -- hospodářská prosperita

-   po r. 1839 hospodářská krize

    -   korupční aféry

###  

### Belgie

-   vítězství francouzské revoluce vyvolalo revoluci v Belgii

-   Nizozemské království

    -   Vilém I. Oranžský (jinej; Nizozemec)

    -   centralismus

    -   holandština × francouzština

    -   kalvinismus × katolicismus

    -   persekuce belgických politiků

    -   velký státní dluh, který vytvořilo především Nizozemí, ale měla
        > ho platit Belgie

    -   vliv Francie

-   Belgičané čekali na příležitost, jak se proti režimu postavit

-   25\. 8. 1830 -- povstání v Bruselu

-   září 1830 -- královská armáda (Nizozemci) poražena

-   24\. 9. 1830 -- prozatímní vláda v Belgii

-   samostatnost Belgie

    -   *průlom do vídeňského kongresu*

    -   uznaly to i velmoci (ale až 1839)

-   1831 ústava (na tu dobu nejliberálnější)

-   Leopold I. Sasko-koburský

-   vyhlášena věčná neutralita

### Německo

-   liberální hnutí *Mladé Německo*

    -   součástí celoevropského hnutí *Mladá Evropa*

    -   členové mladí intelektuálové

    -   cíl

        -   odstranění absolutismu

        -   sjednocení Německa

    -   nic moc úspěch

    -   Říššká rada je začala persekuovat (1834) → emigrace

### Polsko

-   ruští caři Alexandr I. a Mikuláš I. se nechali korunovat za polské
    > krále

-   Království polské

    -   ohlas revoluce ve Francii a Belgii

    -   tajné organizace proti Mikuláši I.

-   konzervativci: Adam Czartoryovský

    -   domnívali se, že když představí požadavky carovi, tak jim
        > ustoupí

    -   moc se jim to nedařilo

-   radikálové: Joachim Lelewel

    -   Rusku nevěřili

-   30\. 11. 1830 -- povstání ve Varšavě

-   svolán sejm

    -   Mikuláš sesazen

    -   pak už se ale nedokázali dohodnout

-   dali šanci Rusku, aby zformovalo armádu

-   květen 1831 -- útok ruského vojska

-   povstalci poraženi → persekuce, emigrace (A. Mickiewicz, F.
    > Chopin, J. Lelewel)

-   výsledky a význam

    -   zrušen statut autonomního království

    -   zrušeno polské vojsko

    -   zrušena univerzita + střední školy

    -   ochromen kulturní život

    -   *rusifikace* (porušťování)

    -   vliv na české a slovenské obrození

### Velká Británie

-   30\. a 40. léta -- politické a sociální otřesy

-   hnutí za volební reformu

-   Charta lidu (1838)

    -   všeobecné volební právo

    -   zrušení majetkového procesu

    -   úprava velikosti volebních obvodů

    -   získala mnoho podpisů

    -   parlament ji odmítl

-   40\. léta -- nová petice s 3. mil. podpisy (také odmítnuta)

-   chartismus postupně upadal

    -   pro lidi bylo důležitější, aby měli co jíst, než aby měli
        > volební právo

-   první odborové organizace

    -   angl. *Trade Unions*

    -   starají se o správu hospodářské činnosti

#### Irové

-   konec 20. let -- zákon o zrovnoprávnění katolíků s protestanty

-   boj za autonomii

-   Daniel O\'Connel

-   hnutí bylo ukončeno hladomorem ve 40. letech

-   1848 pokus o povstání (*Mladé Irsko*)

### Východní otázka

-   *kdo bude mít vliv na Balkáně?*

-   Rusko -- zájem o kontrolu Černého moře, Bosporu a Dardanel

    -   „ochrana pravoslavných"

-   Francie, Velká Británie

-   Rakousko -- zájem o Podunají

-   rozpory mezi velmocemi

 

Vznik kapitalistické společnosti
================================

**Politické směry**
-------------------

### Liberalismus

-   svobodný občan, svoboda slova, podnikání

    -   svoboda svědomí (vyznání)

-   rovná práva

-   neomezován státem - pokud neporušuje zákony státu

-   ochrana majetku

-   politická moc pro ty, kteří platí daně -- census

-   *svoboda končí tam, kde začíná svoboda druhých*

#### Ekonomický liberalismus

-   *nechte běžet* (nezasahování do trhu) - *laissez-faire,
    > laissez-passer*

-   zásada volné soutěže

-   svobodný trh

-   stát nezasahuje do ekonomiky ani do vztahu
    > „zaměstnanec--zaměstnavatel" (různá pracovní doba 12,13,14 hodin
    > denně)

### Demokratické hnutí

-   oddělilo se od liberalismu

-   vychází z J. J. Rouseaua

-   *každý občan bez rozdílu majetku má mít podíl na politickém
    > rozhodování*

-   → všeobecné volební právo

#### Radikální demokraté

-   sociální reformy - chtěli prosadit co nejrychleji i s pomocí násilí

-   demokracie má vyřešit sociální problémy

-   samospráva

-   podpora řemeslníků a dělníků (nebyli legální takže neměli podporu od
    > dělníků a řemeslníků)

### Utopický socialismus

-   politika sociálně spravedlivá

-   Claude Henri de Saint-Simon

    -   *všichni lidé mají pracovat podle svých možností*

    -   *majetek by měl patřit celé společnosti*

-   Charles Fourier

    -   obchodník, viděl do zákulisí obchodu

    -   proti obchodníkům

    -   Falangy →

    -   *společnost by se měla skládat z malých jednotek, jejíž
        > příslušníci by spolu žili, pracovali a dělili se o výdělek*

    -   velkoprůmysl = nehumální

-   Robert Owen

    -   industrializace = nutná, nelze zastavit

    -   *člověk výtvorem společenského prostředí* → školy, čítárny

    -   odstranil práci dětí

    -   zaměstnával (+ poskytoval sociální zabezpečení) v prádelnách

    -   zkrachoval - zničila ho konkurence, protože nechtěli zavést to
        > co on

-   František Matouš Klácel

    -   augustiniánský mnich

    -   domníval se na základě křesťanství že mají lidé žít rovně

    -   odešel do Ameriky a tam založil osadu s přáteli

-   nic moc nevyšlo

#### Význam

-   analýza příčin sociální nerovnosti

-   liberalismus sám nemůže vytvořit spravedlivou společnost

-   navázaly další programy

### Francouzští socialisté

-   Pierre Joseph Proudhon

    -   každý, kdo má větší vlastnictví, ho nakradl

    -   drobní výrobci

    -   jejich sdružování

    -   stát je zbytečný

    -   *Proudonisté*

-   August Blanqui

    -   *společnost má kontrolovat a rozdělovat výrobu*

    -   *nutná revoluce v čele s intelektuály*

    -   nelegální myšlenky - většinu života strávil ve vězení

### Vědecký socialismus

-   Karel Marx

    -   syn bohatého advokáta

    -   studoval na univerzitě

    -   zapojil se do hnutí Mladé Německo

    -   uprchl do Francie

    -   poznal se s Engelsem

-   Fridrich Engels

    -   továrníkův syn

    -   kritik společenských poměrů

-   zdroje

    -   utopický socialismus

    -   anglická politická ekonomie

    -   G. W. F. Hegel

-   otázka nadhodnoty

-   hybné síly dějin: buržoazie × proletariát (dělníci)

-   *revoluce není spiknutí*

-   společenské vlastnictví výrobních prostředků

-   r.1848 program pro Svaz komunistů v Londýně→Manifest komunistické
    > strany

-   po neúspěšné revoluci 1848/1849 umírněnější názory

-   vliv mezinárodní organizace dělníků

    -   *musíme se domluvit*

    -   1864 -- I. internacionála v Londýně

        -   marxistická a anarchistická

        -   nedokázali se domluvit

    -   1889 -- II. internacionála v Paříži

        -   dohodli se na zajištění sociálních výhod pro dělníky

    -   1919 -- III. internacionála v Moskvě

        -   Bolševikové chtěli vnucovat všem komunistickým stranám svůj
            > program

    -   1951 -- socialistická internacionála

        -   existuje dodnes a sdružuje sociálně-demokratické strany

### Anarchisté

-   Michail Bakunin

-   odmítali stát

-   zničení státu → osvobození člověka

-   sociální spravedlnost

-   sdružování do malých obcí -- dobrovolné federace -- komuny

-   krajní křídlo -- politické atentáty

### Nový názor na národ

-   pospolitost rovných občanů

-   společná minulost

-   národní zájmy

-   každý vlastenec slouží národu

-   Johann Gottfried Herder

    -   jazyk, lidová kultura

    -   duch národa

-   národní hnutí: Čechy, Morava, Slovensko, Uhersko, Norsko, Chorvatsko

### Průmyslová revoluce

-   James Watt -- parní stroj

-   vyšší nároky na suroviny

-   textilní průmysl

-   rozvoj kolejové dopravy

-   Anglie 1825 -- George Stephenson

-   vynález telegrafu

#### Kolejová doprava u nás

-   koňská dráha

    -   1832 Č. Budějovice--Linz (sůl)

    -   1833 Praha--Lány (dřevo)

-   parní železnice

    -   Vídeň--Halič

    -   1839 Vídeň--Brno

    -   1845 Olomouc

    -   1851 Praha--Drážďany

#### Význam zavádění strojů

-   anglické nebo jejich modely (Belgie, Francie, Porýní, Sasko, české
    > země \[Brno !!!!!!\])

-   pol. 19. st. → růst koupěschopnosti obyvatel

-   růst spotřeby textilního průmyslu (Brno, Liberec)

-   Brno -- *moravský Manchester*

-   do r. 1846 -- 62 hutí

-   bouře dělníků (mysleli si, že jim stroje berou práci) → rozbíjení
    > strojů

-   podnikatel--dělník; svobodný trh pracovní síly

-   *zakladatelské období*

#### Kritika sociálních křivd

-   kdo kritizoval:

    -   stát (chtěl zákonodárství omezující vykořisťování)

    -   křesťanská sdružení

    -   dělnická sdružení

    -   konzervativci (kritizovali liberály)

    -   tradiční malovýrobci

    -   radikální demokraté, socialisté

#### Protirobotní povstání

-   1821 jižní Morava

    -   potlačeno, tresty mírné

-   1831 východní Slovensko

-   hospodářské změny základem pro změny politické (1848)

**České národní obrození**
--------------------------

-   todle si prý máme zopakovat z
    > [[češtiny]{.underline}](http://zapisky.dpavlik.cz/f-druhak/literatura#nadpis-narodni-obrozeni)
    > a pak udělat zápis z Moodlu

-   definice nár. obrození: *proces vzniku novodobých národů, podstatou
    > jsou společenské a hospodářské změny*

-   u nás národní obrození splývá s obnovou jazyka

-   národ je *společenství lidí, co mají společný jazyk, historii,
    > kulturu, území*

-   Rakouské císařství

    -   mnohonárodnostní stát

    -   37 milionu obyvatel

    -   17 mil. -- Slované (větve národa - Poláci, Češi)

    -   8 mil. -- Němci

    -   5 mil. -- Maďaři

    -   5 mil. -- Rumuni + Italové

#### Struktura české společnosti

-   chybí měšťanstvo, inteligence

-   šlechta - národně odcizená

-   oslaveno státoprávní postavení Království českého - vláda sídlila ve
    > Vídni,

-   germanizace nižších vrstev - snaha sjednotit jazyk monarchie

#### Výhody:

-   na triviálních školách čeština

-   česká literatura z předbělohorské doby

u nás: národní obrození splývá s obnovou jazyka

### 1. fáze

-   60\. léta 18. st. --- počátek 19. st.

-   osvícenství

    -   vědecký zájem o studium jazyka, zvyků, kultury, dějin

-   panslavismus - myšlenka slovanské zájemnosti

-   zemský patriotismus -- boj za práva ne českého národa, ale českých
    > zemí obecně (populární u německý rodů v Čechách)

-   česká inteligence z lidových vrstev -- kněží (prestižní povolání,
    > byla mu přidělena fara) a učitelé

#### Obrany jazyka

-   B. Balbín: Rozprava na obranu jazyka slovanského, zvláště pak
    > českého (60.léta 17.století, vydáno až 1775 F. M .Pelclem)

-   K. Ignác Thám: Obrana jazyka českého

#### Historiografie

-   spisy o historii

-   **Gelasius Dobner**

    -   zakladatel historiografie

    -   kritizoval Hájkovu kroniku českou - historicky nepřesná

-   **František Martin Pelcl**

    -   1793 katedra českého jazyka a literatury Karlo-Ferdinandově
        > univerzitě

    -   čeština předmětem studia

-   **Jan Petr Cerroni**

    -   **Morava**

    -   uspořádal \\/

    -   sekretář moravsko-slezského gubernia

#### **Josef Dobrovský (1753--1829)**

-   **Dějiny českého jazyka a literatury (1791)**

-   osvícenský kriticismus - (Jan Nepomucký, evangelium sv. Marka)

-   1793 veřejně formuloval český národní program

#### Žurnalistika

-   **Václav Matěj Kramerius**

    -   *Česká expedice* (1790)

    -   Krameriovy c. k.(císařsko královské) pražské poštovské noviny

    -   Krameriovy c. k. vlastenecké noviny

#### České divadlo

-   Nosticovo → Stavovské divadlo (1798)

    -   od 1785 česky

-   **Václav Thám:** Břetislav a Jitka - první česká hra

-   Vlastenecké divadlo (1786)

-   U Hybernů - původně klášter Irů

### 2. fáze

-   počátek 19. st. --- 30. léta 19. st.

-   vliv politických poměrů v Evropě

-   romantismus

-   větší aktivita středních vrstev proti germanizaci

    -   kdo je Čech kupuje české výrobky

-   **1. novodobý český národně-kulturní program**

####  

#### Josef Jungmann (1777--1847)

-   **čeština**

    -   nejdůležitější znak národa

    -   rovnocenný jiným (němčině)

-   ***Slovník česko-německý***

-   ***Historie literatury české***

-   ***Slovesnost***

#### Přírodní a humanitní vědy

-   české názvosloví

-   A. Marek -- filosofická terminologie

-   Jan Sv. Presl -- chemická terminologie

-   Karel Bořivoj Presl -- botanická terminologie

-   Josef V. Sedláček -- mat. fyz. terminologie

-   Jan Evangelista Purkyně -

#### Rukopisy

-   Rukopis královedvorský (Josef Linda, Václav Hanka)

-   Rukopis zelenohorský (1818)

-   (?kritizoval Antonín Vašek (studoval na Jarošce)

-   Jan Herber?)

#### Muzea

-   **Vlastenecké museum v Čechách** (1818)

    -   hrabě František Antonín Kolovrat

    -   František Palacký -- Časopis českého muzea

    -   P. J. Šafařík

    -   hrabě Kašpar Šternberk -- stanovy (společenské spolky
        > vlesteneckého musea)

-   **Národní museum** v Brně (1818)

-   František Palacký

    -   Staří letopisové čeští

    -   Idea státu rakouského

### 3. fáze

-   30\. léta --- poč. 50. let 19. st.

-   vliv polského povstání (ve Varšavě)

-   ne tak moc panslavismus - myslí politicky

-   nová generace

    -   K. H. Borovský, K. H. Mácha, K. Sabina, J. J. Langer, F. L.
        > Rieger, F. Palacký

    -   Morava: F. M. Klácel, F. C. Kampelík - zakládal *Kampeličky -
        > spořitelny pro nižší vrstvu*, J. Ohéral - *zakladatel moravské
        > žurnalistiky*, J. Helcelet

    -   ↑ tadlecta jména prý budou v testu

#### František Palacký (1798--1876)

-   *Dějiny národu českého v Čechách a v Moravě* (původně německy; pak
    > česky; divá se jinak na historii Čech oproti ostatním)

-   příprava 1. českého politického programy: **austroslavismus**
    > (mírová spolupráce menších slovanských národů Střední Evropy
    > žijících na území Habsburské monarchie; má byt federace slovanů)

-   K. H. Borovský

#### Národní instituce

-   ***Matice česká*** (1831) - nadační font, šlenové přispívali; vydání
    > Jugmannova slovníku, atd

-   ***Matice moravská*** (1853)

-   ***Jednota pro povzbuzení průmysly v Čechách*** (1833) - F.L Rieger,
    > Al.P.Trojan, J. Perner; uvědomovali si že musí vzdělévat mládež
    > aby se uchytili v oboru techniky

-   ***Měšťanská beseda*** (1845) - Rieger, Trojan; přednášky

#### Divadlo

-   přibylo kočovných společností - (?rod Hrušínských?)

-   ochotnické divadlo - amaterští herci

-   **J. K. Tyl** (1808--1856) - Švanda dudák, nacionalismus

> Fidlovačka (Fr. Škroup; 1834)

#### Žurnalistika

-   Pražské noviny (Čelakovský → Borovský) → Česká včela

    -   příloha Česká včela

-   Čechoslav (Langer)

    -   ***Sláv a Čech*** -- veřejně pospáno, co je *austoslavisumus*

-   Jindy a nyní → Květy české (Tyl)

#### Radikálové

-   chtěli nezávislost českého státu

-   spolek **Repeal** (název od Irů)

    -   E. Arnold

    -   K. Sabina

    -   F. C. Kampelík

Slovenské obrození (1780 - 2.pol. 19.stol.)
-------------------------------------------

*(Neplati: ~~To v testu nebude, mám to v téhle kapitole jen kvůli
souvislosti.)~~*

### 1. období (1780--1820)

#### **Katolíci**

-   pokus o spisovnou slovenštinu

-   Generální seminář v Prešpurku (škola pro výchovu kněžstva)

-   Michal Kratochvíl, **Anton Bernolák** (tvůrce pokusu o spisovnou
    > slovenštinu, pro většinu Slováků nesrozumitelné - Východní
    > slovenština)

-   *bernoláčitna*

-   Ján Hollý, Ján Chalupka

#### Evangelíci

-   na co spisovná slovenština když máme češtinu

-   bibličtina (jazyk Bible Kralické)

-   lyceum v Prešpurku

-   Juraj Ribey, Juraj Palkovič

-   **Ján Kollár** (1793 - 1952) - ***Slávy dcera***

-   **Pavel Josef Šafařík** (1795 - 1861)

###  

### 2. období (1820 -- pol. 19. st.)

-   nová generace na bratislavském lyceu: **L. Štúr** (stal se
    > politikem) **, M. M. Hodža , J. M. Hurban**

-   studentské spolky - tam kde jezdili (Kežmarok, Levoča,)

-   1843 -- Štúr -- spisovný jazyk (základem nářečí středoslovenské) -
    > úspěšný pokus

-   1844 spolek **Tatrín** v Liptovském Mikuláši

    -   sjednocení katolíků i evangelíků

    -   samospráva

-   od 1847 = Štúr poslancem uherského zemského

#### Radikální demokraté

-   Jan Francisci

-   Janko KráŁ (poeta)

-   **program**: zrušení poddanství

-   umění: (malíři)

    -   Peter Bohúň

    -   Jozef Božetěch Klemens
