---
title: Mezinárodní vztahy před 1
---
**Mezinárodní vztahy před 1. sv. válkou**
-----------------------------------------

#### **Konec systému rovnováhy**

-   1814--1856 --- systém rovnováhy

-   krymská válka (1853--1856) -- VB a FR proti Rusku

-   1870--1871 --- změny v mezinárodních vztazích

    -   Francie poražena

    -   Německo velmocí

-   → hledají spojence

-   Bismarck -- snaha o izolaci Francie

-   → usmíření s Habsbursky, zlepšení vztahů s Ruskem

-   → 1873 Spolek tří císařů (× rozpory Ruska, RU a Německem)

#### **Balkán**

-   1875 protiturecké povstání v Bosně a Hercegovině

-   1876 v Bulharsku → turecký teror

-   1876 Srbsko + Černá Hora -- válka s Tureckem; turecké represe

-   postoj velmocí

    -   VB chce zachovat Turecko

    -   Rusko na straně slovanských národů

#### **Rusko-turecká válka**

-   1877 ruské vojsko + dobrovolníci z Rumunska a Bulharska

-   Turci ustupují

-   1878 přechod průsmykem Šipka

-   1878 mír v San Stefanu → nezávislost

    -   velké Bulharsko

    -   Srbsko

    -   Rumunsko

    -   Černá Hora

    -   Turecko získalo část Besarábie, Kars

#### **Berlínský kongres (léto 1878)**

-   potvrzena nezávislost

    -   Srbska

    -   Černé Hory

    -   Rumunska

-   Bulharsko rozděleno

    -   jih vrácen Turecku

    -   sever nezávislý

-   Makedonie vrácena Turecku

-   Bosna a Hercegoviny -- správa RU

-   Rusko izolováno -- zisky zmenšeny

#### **Vyhrocení vztahů mezi velmocemi**

-   1879 RU + N -- Dvojspolek (× R)

-   1882 Dvojspolek + Itálie (× Fr)→ Trojspolek

-   smlouvy se Srbskem, Rumunskem

-   1887 Ferdinand Koburský bulharským králem

-   → Německo vytlačovalo ruský vliv na Balkáně

#### **1887 -- nová krize v mezinárodních vztazích**

-   neobratná politika Bismarcka v Rusku

-   1893 tajná dohoda Ruska a Francie → Dvojdohoda

-   VB -- *splendid isolation*, nevýhodná

    -   1904 Srdečná dohoda (VB + Fr)

    -   1904--1905 -- rusko-japonská válka

-   1907 Dvojdohoda + VB → Trojdohoda

-   1908 RU anektovalo Bosnu a Hercegovinu

-   Marocká krize

-   1912 Itálie okupovala Libyi

-   1912--1913 --- 1. balkánská válka

    -   za nezávislost

    -   Bulharsko, Černá Hora, Řecko, Srbsko × Turecko

    -   osvobozena Makedonie, Thrákie, Albánie

-   spory o území

-   1913 -- 2. balkánská válka

    -   Bulharsko × Srbsko, Č. Hora, Řecko, Rumunsko

    -   ztráta území → sblížení s Německem

#### **Růst nacionalismu**

-   výbojný nacionalismus

-   nacionalismus malých národů

-   užití násilí

-   položit život za národ
