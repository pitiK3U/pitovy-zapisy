---
title: Mezinárodní vztahy v 2. pol. 19. století
---
**Mezinárodní vztahy v 2. pol. 19. století**
--------------------------------------------

### **Sjednocení Itálie**

-   po roce 1849 -- 8 států

-   **Sardinské království** (Sardinie + Piemont)

-   **Viktor Emanuel II.**, savojská dynastie

-   1852 hrabě **Cavour **

-   spojenectví s Francií (proti Rakousku) za Savojsko a Nizzu

#### **1859**

-   sardinsko-rakouská válka

-   červen -- bitva u Magenty, Solferina (úspěšné → nadšení)

-   **srpen -- připojeno Toskánsko, Modena, Romagna**

-   **Giuseppe Garibaldi** -- chtěl sjednotit Itálii, vůdce vlastenců
    > proti Rakušákům a Francouzům

-   listopad **1859** -- Napoleon III. uzavřel mír s Rakouskem ⇒
    > postoupilo je **Lombardií**

-   → Rakousko zabralo jen Miláno

#### **1860**

-   **jaro** -- povstání na **Sicílii**

-   květen -- Garibaldi -- výprava tisíce (dobrovolníci) - vítězství nad
    > králem

-   plebiscit (lidové referendum)

    -   výsledkem velký souhlas s připojením **Sicílie k Itálii**

    -   takže se tak stalo

#### **1861 - Italské království**

-   jednání o novém státu

-   březen -- všeitalský parlament v Turíně

-   *bude to **království***

-   králem Viktor Emanuel II. (ale stal se z něj **Viktor Emanuel I.**)

-   hlavní město = Florencie

#### **Další vývoj**

-   1866 prusko-rakouská válka

    -   drtivá porážka Rakušanů

    -   Itálie spojencem Pruska

-   (Custozza = porážka)

-   **1866 -** pražský mír -- **připojeno Benátsko k Itálii**

-   1870 Francie poražena Pruskem (Francie poslala armádu do papežského
    > státu)

-   září **1870** připojen **papežský stát** (bez boje)

-   1871 hlavním městem Řím

###  

### **Sjednocení Německa (1871)**

-   prudký rozvoj průmyslu

-   rozdrobenost = překážkou podnikání (cla, \...)

-   Prusko = nejsilnější

-   **Vilém I. Hohenzoller**

-   1862 **Otto von Bismarck** -- 1. ministrem

    -   chtěl Německo sjednotit ***,,krví a železem"***

    -   velkostatkář

-   1863--1864 --- válka s Dánskem

    -   Dánsko poraženo

    -   Šlesvicko připadlo Prusku

    -   Holštýnsko připadlo Rakousku (mezi nimi Prusko)

    -   1866 Bismarck prohlásil Holštýnsko za pruské

-   **1866 prusko-rakouská válka**

    -   3\. 7. 1866 -- bitva u Hradce Králové (Sadové)

    -   masakr

    -   generál Benedeck - nevěřil že vyhrají, neznal území

    -   Rakušané bojovali starším způsobem - pěšáci a bajonety, Prusko
        > střílelo prvně

    -   Rakousko ztratilo vliv v Německu

-   **1866 Severoněmecký spolek**

    -   Prusko + 21 států

    -   duben 1867 -- ústava (mezi liberáli a konzervativcům)

        -   Říšský sněm se odpovídal pouze králi- konzervativci

        -   všeobecné právo pro muže - liberálové

    -   proti sjednocení Německa byla Francie

    -   záminkou pro válku = španělský trůn získal Hohenzoller

-   **1870--71 --- prusko-francouzská válka**

    -   19\. 7. 1870 -- válku vyhlásil Napoleon III. (udělal chybu, nevydržel a
        > vyhlásil válku)

    -   1\. 9. 1870 -- Francouzi poraženi u Sedanu - Napoleon III. zajat →
        > vyhlášena republika

    -   27\. 10. 1870 -- porážka u Metz, vláda pokračovala ve válce

    -   → postoupili až do Paříže

-   **18. 1. 1871 -- Německé císařství**

    -   vyhlášeno ve Versailles v zrcadlovém sále

#### **Německé císařství**

-   spolkový stát

    -   22 monarchií

    -   3 svobodná město

    -   Alsasko-Lotrinsko

-   císařem Vilém I.

-   kancléřem Bismarck

-   10\. 5. 1871 -- mír ve Frankfurtu (5 miliard zlatých franků +
    > Alsasko-Lotrinsko)

-   jaro 1871 -- volby do Říšského sněmu

-   **nová ústava**

    -   Říšský sněm má moc jen zákonodárnou

    -   nejvýznamnější je kancléř, který se zodpovídá jen císaři, velké
        > pravomoci

    -   císař má velkou moc

    -   vznikla Spolková rada s omezenou mocí

-   Německo = velmocí

#### **60. a 70. léta**

-   konjuktura (průmyslový cyklus) + francouzská reparace

    -   konjuktura - hodně se prodává, hodně se vyrábí, malá
        > nezaměstnanost

    -   → krize z nadvýroby

    -   → deprese krize

    -   → oživení výroby

    -   → konjuktura

-   průmysl - chemický, peníze z válečních reparátů

-   Porůří

-   růst počtu dělníků

-   vliv Marxe - vytvoření společnosti beztřídní

-   **1869 Sociálně demokratická dělnická strana Německa** (Eisenach)

#### **Bismarckova politika**

-   proti katolické církvi

    -   Kulturkampf (kulturní boj; cíl omezit zasahování kněžstva do
        > politiky)

    -   1871 zákaz zasahování do politiky

-   proti socialistickému hnutí

    -   1871 zákon o socialistech, který zakazoval sociálně
        > demokratickou­ stranu

    -   politika cukru a biče

#### **Sociální zákonodárství**

-   bál se aby kvůli sociálním podmínkám nebyla revoluce

<!-- -->

-   zabezpečení ve stáří, při úrazu

-   zlepšení pracovních podmínek

-   vliv sociálních demokratů narůstal

-   1889 stávka dělníků v Porůří

    -   organizovali sociální demokraté

    -   chtěli pracovní dobu zkrátit na 10h

    -   využil Vilém II. - neměl ho rád

-   1890 Bismarck odvolán Vilémem II.

    -   ,,nesplnil, co slíbil"

#### **Bismarckova zahraniční politika**

-   tradiční spojení (německy mluvících zemí)

    -   Rakousko-Uhersko

    -   Rusko X 70. léta -- balkánská krize

-   80\. léta -- buduje se námořnictvo (až do 1. sv. války)

-   Afrika -- první kolonie

-   růst napětí s Anglií a Francií

-   Přední východ -- Rusko (němci chtěli proniknout do Turecka a
    > získat T. jako spojence - Rusko mělo podobné zájmy)

-   železnice Berlín -- Bagdád (Drang nach Osten -- *pronikání na
    > Východ*)

#### **Sociální demokracie**

-   Vilém II. jim povolil činnost

-   1891 volby -- 20 % hlasů - nejsilnější politickou stranou

-   růst vlivu

-   diferenciace názorů

    -   revizionisté (E. Bernstein - přítel Engelsův)

        -   Marxovo učení je už trošičku staré

        -   je potřeba poopravit = musí se **revidovat**

    -   radikálové (Spartakovci; Rosa Luxemburková)

        -   Tak jak to řekl Marx a Engels se nesmí měnit

        -   (→ později němečtí komunisté)

-   katolická strana Centrum

-   1905 vlna stávek

    -   úspěšné sjednocení → posílení nacionalismu

### **Francie - 2. císařství**

-   1851 převrat **Ludvíka Bonaparta** (- převzal na sebe pravomoce)

-   **1852 císařství**

-   nová ústava

    -   omezena občanská i politická práva

    -   Chapelierův zákon

    -   povolil zakládání spolků, ale nesmí být politické

-   60\. léta -- liberalizace režimu ⇒

    -   povolena opozice: republikáni, monarchisté

    -   agitace socialistů a anarchisté (nelegální)

-   hospodářský růst - Napoleon přecenil

-   1860 změna celních předpisů ⇒ potíže (více zahraničního zboží)

-   Napoleon III. povolil zakládat nepolitické dělnické spolky

-   přestavba Paříže - nezaměstnaní dostali práci

#### **Koloniální výboje**

-   *všude samé výdaje, ale žádné zisky*

-   1853 Krymská válka

    -   Francouzi vyhráli, Anglie, Turecko X Rusko

    -   moc do ní investovali, ale **nic nezískali**

-   Suezský průplav - snaha ovládnout Egypt nevyšla (vybudován
    > Ferdinandem Lesseps; 1869)

-   1859 podpora sjednocení Itálie - království Sardinské

-   snaha proniknout do Alžíru, Indočíny - nevraceli se mu zisky -
    > získali až po Napoleonově smrti

-   Mexiko

    -   nezvládali platit dluhy → Fr. chtěla si podmanit Mexiko

    -   1861 císařství

    -   arcivévoda Maxmilián Habsburský (bratr F. J. I.)

    -   Mexičané cizí vládu nechtěli

    -   1867 popraven

#### **Vztah k Prusku**

-   Bismarck vyprovokoval Napoleona III. (Šp.= Hohenzoller, Fr. se
    > cítila obklopena)

-   19\. 7. 1870 -- vyhlášena válka

-   2\. 9. -- Francie poražena u Sedanu

    -   Napoleon zajat

-   4\. 9. -- revoluce v Paříži

    -   císařství skončené→ 3. republika

-   prozatímní vláda (vláda národní obrany) - nechtěli ztratit Porýní,
    > válka pokračovala

-   Národní gardy - chránili Paříž

-   partyzáni

-   27\. 10. 1870 -- porážka u Metz - u pevnosti Méty

-   pruská vojska obklíčila Paříž

-   18\. 1. 1871 -- nová vláda Thiers \[Tiers\]

    -   tvrdý člověk, tajný monarchista, nenáviděl chudší vrstvy

-   28\. 1. 1871 -- příměří, proti vůli většiny Francouzů

-   vláda chce odzbrojit národní gardu

#### **Pařížská komuna**

-   vláda chce odzbrojit národní gardy - vládní armáda měla → nepodařilo
    > se

-   17\. a **18. 3. 1871** -- povstání

    -   vznikla **Pařížská komuna** = stát ve státě, volby pro ženy,
        > volby

-   26\. 3. volby do Rady komuny

    -   blanquisté, jakobíni, proudonisté

-   dekrety - mají platnost zákona

    -   národní garda nahrazovala policii a armádu

    -   nové orgány

    -   zavedena odluka státu a církve

    -   zavedení volených a sesaditelných úředníků

    -   zavedení sociálních opatření ve prospěch pracujících (zdravotní
        > péče)

    -   řešení bytové politiky

    -   spoluúčast dělníků při vedení podniků

    -   sociální opatření

        -   bezplatné vyučování pro děti

    -   rovnoprávnost žen ve všem

-   obrana Paříže

    -   pruské vojsko - příměří, vládní neholo útočit ze S

    -   vládní vojsko - z J

-   10\. 5. 1871 -- mír ve Frankfurtu n. Mohanem

    -   odstoupení Francie od Alsaska-Lotrinska

    -   reparace 5 mld. zlatých franků

    -   aby vojsko mohlo obklopil Paříž ze všech stran

-   útok vládního vojska na Paříž - asi 500 barikád

-   **májový týden (21.--28. 5. 1871)** - v Paříži se bojovalo o domy,
    > ulice, \...

-   represálie -- 30 000 mrtvých - děti, ženy, \...

-   vojenské soudy - byli před nimi Komunardi = civilisté, z Paříže
    > ubylo asi 100 tisíc ob

-   ohlas v Evropě - negativní = vládci se báli, ale nedošlo k něčemu
    > podobného → politický život ochromen, pozitivní = lze aby vláda
    > byla tvořena nejen bohatými

#### **Stabilizace republiky**

-   snahy monarchistů -- prezident Mac-Mahon (po vzniku ústavy
    > odstoupil)

-   při práci na ústavě převažují republikáni

-   **1875 -- nová ústava 3.republiky:**

    -   republika

    -   všeobecné hlasovací právo

    -   silná prezidentská moc

    -   obnoveny občanské svobody a občanská právě před nástupen
        > Napoleona III.

    -   bezplatné vzdělání

#### **Politické strany**

-   Radikální strana (název)

    -   odkaz na VFR

    -   odluka státu od církve - od Komuny

    -   omezení pravomocí prezidenta

    -   **Georges Clémenceau** - podílel se na konferenci, které vděčíme
        > za vznik ČSR

-   Socialistická strana

    -   roztříštění (blanquisté, marxisté, proudonisté, ...)

    -   1905 sjednocení

    -   měli propagovat že není rozdíl sociálních podmínek mezi
        > Francouzi, Němci, atd. - růst nacionalismu

    -   Jean Jaurés zastřelen - /\\ propagoval

#### **Politické a korupční aféry**

-   Dreyfusova aféra

    -   90\. léta 19 st.

    -   anitsemutismus

    -   žid nespravedlivě obviněn z německé špionáže

    -   odsouzen na galeje

    -   zapojil se Emil Zola - napsal článek proti francouzské
        > společnosti

-   aféra s akciemi Panamského průplavu - firma neměla peníze → akcie →
    > zkrachovala → Američané vybudovali → stát Panama

#### **Volby (1902)**

-   koalice Levý blok (umírnění socialisté + Radikální strana)

-   Radikální strana

    -   nejvlivnější až do r. 1914

    -   antiklerikalismus = proti církevní

    -   nacionalismus → protiněmecká orientace

    -   [[sblížení]{.underline}](https://goo.gl/ck3MOT) s VB a Itálií
        > (rozdělení Afriky, \... )
