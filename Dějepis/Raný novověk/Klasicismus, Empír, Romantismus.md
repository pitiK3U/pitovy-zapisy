---
title: Klasicismus, Empír, Romantismus
---
Klasicismus\
• vyrovnanost, souměrnost\
• uctívání antiky a renesance\
• rozumem lze změnit společnost (osvícenské myšlenky)\
• odklon od víry\
• láska ke svobodě\
• ideál občanských ctností\
• Akademismus - netvůrčí napodobování antických předloh - sochařství a
malířství\
Malířství\
• Jacques Louis David\
• Antion-Jean Gros \[Antoán žán gro\]\
• Jean Dominique Ingrese \[žán dominik engr\]\
• Antonín Mánes - obraz Pražského hradu\
• František Tkadlík - vynikající portrétista\
Sochařství\
• vyumělkované\
• Václav Prachner\
• Bertel Thorvaldsen\
Architektura\
• trojúhelníkový štít\
• antické sloupy -- portikon (podhloubíčko)\
• Kongres\
Hudba\
• vídeňský klasicismus\
• Joseph Haydn\
• W. A. Mozart - měl rád Prahu, bydlel i v Brně\
• Ludwig van Beethoven\
Literatura\
• J. W. Goethe\
Empír\
• sloh Napoleona císařství - často nábytek, kobry jako ucha, lvy,tygři
jako nohy nábytku\
\
Romantismus\
• různá datace (od r. 1790, největší rozvoj 20.--30. léta 19. st.)\
• nový životní pocit -- proti rozumu lidské duše\
• dobrodružství, nepředvídatelné situace, příroda, lidová kultura\
• národní hnutí -- národní tradice\
• mystické představy (,,duch doby, národa\")\
• uctíván středověk - období křížových výprav\
• přeceňována úloha jedince -- hrdiny\
• typické životní osudy: Byron, Puškin, Lermontov, Mácha\
• ovlivnil symbolismus, secesi, impresionismus a silou výrazu
expresionismus\
Malířství\
• Theodore Gericault - vor medůzy, důstojník kardinála dává povel k
ůtoku\
• Eugénie Delacriox - nejvýznamnější froncouzský malíř, vraždění na
ostrově Chílos, Svoboda vedoucí lid na barikády\
• Francisco de Goya - Nahá Maja (pohoršovalo to Francouze) -\> oblečená
Maja\
• John Constable -\
• William Turner - předchůdce impresionistů, Déšť, pára a rychlost\
• Caspar David Friedrich - německý malíř, Kříž v horách\
• Antonín Mánes - náš malíř\
• Josef Mánes - syn /\\,\
• Adolf Kosárek - český malíř, zemřel v 29 letech, krajiny\
• Josef Matěj Navrátil - uznávaný romantický malíř, maloval skalnaté
hory\
\
Architektura\
• napodobování středověkých hradů a slohů (přestavování zámků v
novgotickém stylu)\
• anglický park se zříceninou (vapadá jak přiroda)\
• Altány\
• Lidové stavby\
• minarety\
• Lednice - skleník + zámek + kolonáda na Raistně\
Hudba\
• pozdní Beethoven\
• Robert Schumman\
• Franz Schubert\
• Felix Mendelson-Bartholdy\
Novoromantici\
• Franz Liszt\
• Hektor Berlioz - francouz\
• Richard Wagner - německá hudba, Tristan\
• Petr Iljič Čajkovský - Labutí jezero, Louskáček\
• Bedřich Smetana - Má vlast\
• Antonín Dvořák - Rusalka, Novosvětská symfonie\
\
Romantismus ovlivnil symbolismus, secesi, impresionismus a sílou výrazu
expresionismus
