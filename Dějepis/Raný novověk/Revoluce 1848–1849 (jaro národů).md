---
title: Revoluce 1848–1849 (jaro národů)
---
Revoluce 1848--1849 *(jaro národů)*
===================================

####  **Charakteristika**

-   vznik občanské společnosti

-   programy národní emancipace

-   série revolucí vzájemně se ovlivňujících

#### **Itálie**

-   *risorgimento* - znovuzrození

-   Itálie rozdrobená na řadu států

-   12\. 1. 1848 bouře na Sicílii a v Neapoli

    -   **chtěli nezávislost na Bourbonech, sjednocení Itálie**

    -   Ferdinand II. Bourbonský zaskočen

    -   → navrhl ústavu

    -   vlna nadšení 🌊

-   vzrůst napětí v Lombardii a v Benátsku

-   17\. 3. povstání v Milánu a Benátkách (Miláno osamostatnilo se)

-   sardinský král Karel Albert (savojská dynastie) viděl šanci v tom,
    > že by se mohl stát panovníkem sjednocené Itálie

-   → 25. 3. 1848 vypověděl válku Rakousku

    -   25\. 7. porážka Custozzy \[-ci\]

-   léto 1848 -- vládli demokraté → republika v Toskánsku a Benátkách

-   leden 1849 -- Římská republika

-   nová válka sardinského krále

    -   střet březen 1849 u Novarry

    -   poraženi maršálem Václavem Radeckým z Radče (z Čech, Strous -
        > pochod z ??)

-   rakouská intervence v Toskánsku

-   neapolská vojska → Sicílie

-   červen -- francouzský expediční sbor → Římskou republiku

-   srpen 1849 -- poražena republika sv. Marka

-   takže vlastně všechno potlačeno

-   ale alespoň ukázali, jak by to mělo vypadat

-   významná jména

    -   **Giuseppe Mazzini** - nedočkal se sjednocení

    -   **Giuseppe Garibaldi -** \[Džuzepe Garibaldi\] velká zásluha na
        > sjednocení

-   význam:

    -   **poprvé požadavek sjednocení vyšel ze středních vrstev**

###  

### **Francie**

-   40\. léta -- opozice proti Ludvíkovi Filipovi a vládě F. P. Guizota

    -   liberálové - legální opozice

    -   demokraté

    -   socialisté

    -   rozšíření společného práva

-   cíle

    -   chtěli svrhnout vládu

    -   širší občanská i politická práva

    -   podnikání ve větší míře

#### **Únorová revoluce**

-   rozšíření volebního práva

-   banket - scházení se lidu

-   22\. 2. 1848 bouře - byl zakázán banket → rozzlobilo lid

-   24\. 2. barikády proti vládnímu vojsku

    -   vojsko moc nezasáhlo

    -   ⇒ Ludvík Filip abdikoval

-   prozatímní vláda (liberálové + demokraté + socialisté)

-   25\. 2. 1848 vyhlášena 2. republika

#### **Dekrety**

-   v moderních dějinách než je ústava mají dekrety ,,váhu" ústavy

-   všeobecné volební právo

-   právo sdružovací

-   právo na práci (když není práce stát musí zařídit)→ národní
    > dílny(zaměstávání dosud nezaměstaných dělníků; nepodstatné výrobky
    > → malé mzdy → stát musel dotovat) → zvýšeny daně rolníkům

#### **Další vývoj**

-   duben 1848 volby -- vítězství liberálních republikánů (chtěli
    > omezení politických práv)

-   květen -- socialisté se pokusili o převrat

    -   nepodařilo se jim to

-   23\. 6. 1848 zrušeny národní dílny

-   → živelné povstání dělníků (do 26. 6.) - účel pro ty co sestavovali
    > ústavu

    -   velmi tvrdě potlačeno

#### **Cesta k 2. císařství**

-   4\. 11. 1848 nová ústava (**úřad prezidenta** s mnoho pravomocemi)

-   10\. 12. prezidentem Ludvík Napoleon Bonaparte (Bonaparte si říkal po
    > strýci)

-   jaro 1849 -- parlamentní volby

    -   vítězí liberálové (strana pořádku - sdružení lib.)→omezeno
        > hlasovací právo

-   2\. 12. 1851 -- státní převrat

-   2\. 12. 1852 -- se Ludvík nechal prohlásit císařem - Napoleonem III.

### **Německo**

-   napětí od r. 1847

-   vliv úspěšných revolucí

-   nejsilnější stát Prusko

-   březen 1848 -- bouře v Porýní, cíl = **sjednocení**, osvobození

-   18\. 3. povstání v Berlíně (Prusko)

-   19\. 3. Fridrich Vilém IV. slíbil ústavu (tušil, že potlačení by nebylo
    > úspěšné)

-   vytvořil novou vládu -- liberálové (sjednocení je důležité kvůli
    > obchodu a přelévání pracovních sil)

#### **Snaha sjednotit Německo**

-   březen 1848 -- předfrankfurtský sněm

-   18\. 5. 1848 **Pangermánský sněm ve Frankfurtu**:

    -   velkoněmecká koncepce (všechny německy mluvící země - habsburská
        > monarchie - Čechy,\...)

    -   maloněmecká koncepce

    -   liberálové × demokraté

-   březen 1849 nová ústava:

    -   titul císaře nabídnut Fridrichu Vilémovi IV. (nechtěl být
        > omezován ústavou)

    -   zrušeno poddanství

    -   vyhlásila všeobecné volební právo pro muže

    -   stále monarchie s císařem v čele

#### **Výsledky a význam**

-   jaro 1849 demokraté -- povstání v Porýní, Drážďanech - nepodařilo
    > se, potlačeno

-   18\. 6. 1849 pruské vojsko rozehnalo frankfurtský sněm

-   Prusko -- **oktrojovaná ústava** (= ústava, která nevznikla v
    > parlamentu, vytvořena na zakázku panovníka politikem,
    > ,,**nevznikla demokraticky**")

    -   **zrušení roboty** za výkup (drahá; šlechtici, velkostatkáři)

-   v některých státech zrušena cenzura

-   zostření vztahů s Rakouskem

###  

### **Habsburská monarchie (Čechy)**

-   vliv Itálie a Francie

-   důvody:

    -   ?Metternich?

    -   Češi, Chorvati, Maďaři - bouřili se za svoji státnost

-   11\. 3. 1848 -- veřejné lidové shromáždění Svatováclavské láz­ně

    -   František August Brauner - liberální politik

-   petice císaři:

    -   země Koruny české = samostatnost, celek, společný sněm,
        > centrální úřad v Praze (austroslavismus) - moravané
        > nesouhlasili

    -   zrovnoprávnění češtiny a němčiny ve školách a úřadech

    -   svoboda tisku, shromažďování, náboženského vyznání

    -   zrušení roboty

    -   zřizování národních gard, které budou dbát na pořádek (místo
        > policie)

    -   **svatováclavský výbor** - měli komunikovat císaře a sdělit
        > požadavky

-   odpověděl až na třetí petici Kabinentním listem (8. 4.)

    -   slib, že budou vytvořeny úřady, český zemský sněm, jazyková
        > rovnoprávnost

-   13\. 3. -- velmi bouřlivé povstání ve Vídni

-   kancléř Metternich odstoupil

-   sliby císaře Ferdinanda I. -- ústavu, zrušení cenzury, rovnost před
    > zákonem, národní gardy, nová vláda,...

-   rakouští liberálové -- přípravy na frankfurtský sněm

-   11\. 4. -- Palackého Psaní do Frankfurtu - odmítl, zachovalo se, = Český
    > národ nikdy nepatřil k německému národu

-   **10. 4.** -- Svatováclavský výbor → **Národní výbor** --
    > liberálové + konzervativci (reprezentantem českých názoru, žádné
    > demonstrace ani bouře)

-   pol. května -- kníže Alfred Windischgrätz -- hledal cestu, jak
    > vyprovokovat Čechy, aby proti nim mohl vojensky zasáhnout

#### **Slovanský sjezd**

-   2\. 6. 1848 začal v Praze

-   myšlenka slovanské vzájemnosti

-   společný postup

-   Šafařík, Bakunin, Zach, Palacký, K. Havlíček Borovský

-   diferenciace názorů - nechápali austroslavismus Českých delegátů

-   *Manifest k Národům evropským*

    -   austroslavismus

    -   Palacký

    -   prohlášení jaké mají mít slované práva

####  

#### **Červnové povstání (Svatodušní bouře)**

-   12\. 6. 1848 -- mše sbratření -- Koňský trh (Václavské náměstí)

-   srážky na Celetné (úzká uličná, Windischgrätz vyprovokoval povstání;
    > staroměstké náměstí → )

-   začalo povstání - barikády postavěli Pražané

-   16\. 6. dělostřelecký útok na Prahu

-   17\. 6. kapitulace

-   Praha -- stav obležení (Windischgräzt) - studenti měli zakázáno se
    > učit → naverbování do armády

-   10\. 7. zasedá ústavodárný říšský sněm ve Vídni - scházení se liberálů

-   7\. 9. 1848 -- zrušení roboty na náhradu -- každý poddaný měl možnost si
    > tu půdu koupit, ten výkup byl vysoký, ale neměli na to našetřeno, ale
    > pro obě strany to bylo velmi pozitivní, byli naprosto svobodní, dostává
    > se jim do ruky velké množství kapitálu → 1. zemědělské stroje, umělá
    > hnojiva

-   říjen -- nové bouře v Uhrách a Vídni; nějaký úředník byl oběšen

-   **1. 11. konec revoluce v Předlitavsku** = České země, Rakousko,
    > Halič, Dalvácie (× Zalitavsko = Uhry, Chorvatsko, Semihradsko)

-   22\. 11. pokračuje sněm v Kroměříži -- Palacký se také zúčastnil

#### **František Josef I. (1848 -- 1916)**

-   Ferdinand, dobrotivý, ale málo schopný panovník → donucen abdikovat

-   nastoupil mladičký, 18 let

-   4\. 3. 1849 **oktrojovaná ústava** (= Stadionova) - není potřeba sněm

-   7\. 3. rozehnán kroměřížský sněm

-   tím končí snaha o demokratickou ústavu

-   májové spiknutí

    -   radikálové -- J. V. Frič, K. Sabina, E. Arnold

### **Uhersko**

-   od 1713 odpor proti centralizaci (chtěli víc = autonomii)

-   demokraté + liberálové (většinou příslušníci šlechty)

-   **Lajos Kossuth** \[lájoš košut\] - hlavní představitel

-   3\. 3. 1848 uherský sněm v Prešpurku (Bratislava)

-   17\. 3. císař uznal uherskou vládu

-   18\. 3. zrušeny feudální výsady, poddanství, obč. svobody; centralizace

    -   × Slováci, Chorvati, Rumuni, Srbové

    -   Uhry dělaly stejnou chybu jako Rakouská vláda - něchtěly uznat
        > ostatní národy

-   11\. 4. -- **uherská ústava** - chtěli Uherskou centralizaci,

-   národnostní konflikty

-   duben 1848 -- Nitranské žiadosti

    -   J. M. Hurban

    -   požadují aby uherský zemský sněm uznal slovenský národ

-   10\. 5. Žiadosti slovenského národa - zrovnoprávnit slovenštinu

-   → zatykač na Hurbana, Hodžu a Štúra

-   národnostních rozporů využívala vídeňská vláda:

    -   chorvatská armáda bán Jelačič - souhlas Rakouské vlády

    -   dobrovolníci na Slovensku: Ján Francisci (podporovala Víděnská
        > vláda)

#### **Konec revoluce**

-   14\. 4. 1849 uherský sněm v Debrecíně

    -   sesazeni Habsburkové

    -   vyhlášena samostatnost -- Franta požádal o pomoc Rusko (stále
        > platí Svatá aliance)

    -   13\. 8. 1849 porážka u Világose

        -   poslední velká bitva Rakousko, Rusko X Uhry

        -   Maďarsko poraženo

        -   Kossuth uprchl

    -   Sandor Petölfi

        -   zakladatel moderního maďarského básnictví

        -   byl zajat

#### **Výsledky a význam**

-   **odstraněny feudální výsady** - téměř ve všech zemí

-   **zrušeno poddanství** - ale za výkup, třeba na splátku a splácelo
    > se to 3 generace

-   **částečně obč. práva** - svoboda tisku a projevu

-   **politické programy** - poprvé vznikali

-   **získání zkušeností** - důležité pro mnoho revolucionářů

-   **změna struktury společnosti hospodářská, sociální** - poddaný se
    > stal svobodným vlastníkem půdy, šlechta a velkostatkáři začali
    > podnikat
