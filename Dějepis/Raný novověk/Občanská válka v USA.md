---
title: Občanská válka v USA
---
**Občanská válka v USA**
------------------------

#### **Rozsah USA od poč. 19. st.**

-   1803 Louisiana (koupili od Francie, od Napoleona - 15mil. franků)

-   1819 Florida (od Španělska - 5 mil. dolarů)

-   1846--1847 -- válka s Mexikem → Kalifornie, Texas, Nové Mexiko

-   1867 Aljaška (od Ruska 7 mil. 200 tis. dolarů. - nevědělo se o
    nerostném bohatství)

-   obyvatelstvo

    -   1800 -- 5,3 milionu

    -   1830 -- 13 milionů ← přistěhovatelství

#### **Hospodářství**

-   Jih -- plantáže - zadlužovali se, pracovali především otroci

-   Sever -- průmysl, železnice - ⅔ bavlnářství

-   zlatá horečka- 1845 v Pensylvánii,

-   od 1815 -- průmyslová revoluce

-   otrokářství = brzda rozvoje

    -   1820 zákaz šíření do nově ovládnutých států

-   Abolicionisté

    -   sdružení bojovali za zrušení otrokářství

    -   Louis Garrison - pomáhal otrokům utíkat do Kanady

    -   Underground Railway

        -   odvoz do Kanady

        -   40 tis.

    -   1859 John Brown -- povstání ve Virginii

        -   Harpers Ferry - skladiště zbraní

#### **Politické strany**

-   1828 Demokratická strana

    -   na Jihu

    -   vláda lidu, otroky do demokracie nepočítali

-   1854 Republikánská strana

    -   hospodářské jednota = smazání rozdílu mezi S a J

    -   zrušení otroctví - otroci si nemohli kupovat zboží ze severu

    -   bezplatné příděly půdy na západě, po malých celcích

-   **1860** zvolen prezidentem **Abraham Lincoln** (republikáni, 16.
    prezident; byl umírněný)

#### **Cesta k válce**

-   20\. 12. 1860 -- odvoláni z Washingtonu zástupci Jižní Karolíny

-   do 4. 2. 1861 -- 11 jižních států vystoupilo z Unie a

-   založily **Konfederované státy americké**

-   prezident Jefferson Davis

-   hl. město Richmond

#### **1861--1865 (fáze vojenská)**

-   12\. 4. 1861 -- útok Jižanů na Fort Sumter

-   **15. 4. -- počátek války**

-   úspěchy Jižanů - seveřané se na válku nepřipojovali; Sever byl
    závislí na bavlně → Jižané zbraně

-   generál R. E. Lee

-   od dubna 1861 -- blokáda jižního pobřeží - cesta z J na S, kvůli
    pašování

-   1862 Unionisté ovládli New Orleans

-   do léta 1863 -- střídavé úspěchy

#### **Změna ve vývoji války**

-   Lincoln vydal zákony

    -   **20. 5. 1862** -- **zákon o bezplatných přídělech půdy**

        -   65 ha, za nízký poplatek, pokud nehospodařil → stát si vzal
            > zpět půdu

    -   **1. 1. 1863** -- zákon o **zrušení otroctví**

        -   do armády nastoupilo množství propuštěných otroků = změnil
            > poměr sil

-   → 1.--3. 7. 1863 -- bitva u Gettysburgu

-   červenec 1863 -- gen. **Ulysses Grant** ovládl údolí Mississippi

-   1864 **Grant** vrchním velitelem Unionistů

-   jaro 1865 -- dobyta Jižní Karolína

-   3\. 4. 1865 -- dobyt Richmond

-   **9. 4. 1865 -- kapitulace generála Lee = konec vojenské fáze**

-   14\. 4. 1865 -- divadelní představení → A. Lincoln zastřelen

#### **1865--1877 (období rekonstrukce)**

-   vyrovnání hospodářských rozdílů

-   J okupován armádou

-   dodatek ústavy

    -   zrušeno otroctví bez náhrady ve všech státech

    -   rovnoprávnost černochů

    -   volební právo i pasivní

-   1866 Ku-klux-klan (Tenessee) - název od zvuku který vydávaly pušky,
    proti černochům

#### **Důsledky války**

-   proměna sociální struktury venkova

-   pronájem pozemků - pronájem pracovních sil

-   70\. léta -- hospodářská krize

    -   ceny obilí klesají

    -   ceny průmyslových výrobků stagnují (zůstavaly stejné)

-   celkově prosperita → nejvyspělejším státem světa

#### **Ekonomická situace**

-   průmyslový rozvoj

-   taylorismus

-   pásová výroba

-   monopoly -- koncerny

-   Henry Ford

#### **Sociální otázky**

-   izolované národnostní skupiny

-   Americká federace práce

    -   sdružovala kvalifikované dělníky

-   1886 -- Chicago

    -   1\. 5. -- generální stávka

        -   několik lidí zastřeleno

    -   4\. 5. -- protest proti zásahu

        -   další mrtví

-   Progresivisté

    -   demokraté i republikáni

    -   prezident Theodor Roosevelt

    -   prezident Woodrow Wilson

    -   založili minsterstvo pro obchod

#### **Expanze**

-   Tichý oceán

-   1898 anexe Havajských ostrovů

-   1898 Filipíny

-   část souostroví Samoa

-   sféra vlivu -- španělské kolonie -- Kuba

-   Čína

-   kanál v Panamské šíji (konflikt s Kolumbií)

    -   stát Panama

#### **Latinská Amerika**

-   1810 kreolové (míšenci) začali války za nezávislost

-   1826 Španělé vyhnáni téměř ze všech kolonií

-   1823 prezident James Monroe -- Monroeova doktrína „Amerika
    Američanům"

**Koloniální velmoci ...**
--------------------------

#### **Imperialismus**

-   označení z doby kolem r. 1900

-   snaha podrobit si území a jeho obyvatele

-   hospodářská prostata

-   vývoz kapitálu, odbytiště

-   cílem zisk

-   nacionalismus

#### **Misijní činnost**

-   šíření křesťanství

-   lékařská péče

-   vzdělání

-   odpor domorodců

####  

#### **Konec 19. století**

-   soupeření velmocí

-   pořadí hospodářské vyspělosti

    -   USA

    -   Německo

    -   Japonsko

    -   VB

    -   Francie

    -   Belgie

#### **Velká Británie**

-   Londýn -- City

-   1882 Egypt

-   1898 Súdán × Francie

    -   Fašoda

-   1898--1902 --- búrská válka

-   strategické body: Gibraltar, Malta, Suez, Kapsko, Aden, Cejlon,
    Singapur

-   dominia

    -   1867 Kanada

    -   1901 Austrálie

    -   1907 Nový Zéland

    -   1909 Jihoafrická unie

-   30 mil. km2

#### **Francie**

-   rozšíření kolonií

    -   1881 Alžírsko

    -   1881 Tunis

    -   1911 Maroko

    -   Kongo × Britové (Fašoda)

    -   1884 Tonkin, Annám

    -   1893 Laos

-   10 mil. km2

#### **Itálie**

-   Eritrea

-   část Somálska

-   1896 pokus ovládnout Habeš (Etiopii)

-   1912 Libye (po válce s Tureckem)

#### **Japonsko**

-   císař Mucuchito -- *tennó meidži* (1867--1912)

-   reformy *meidži* (1867--1868)

-   1877 poraženo poslední povstání samurajů

-   11\. 2. 1889 -- ústava

    -   císař

        -   armáda

        -   byrokracie

-   3\. místo na světě → výboje

-   1894--1895 --- válka s Čínou

    -   Tchajvan

    -   Pescadorské ostrovy

-   1904--1905 --- rusko-japonská válka

    -   → Mandžusko

    -   → Sachalin

#### **USA**

-   1\. místo na světě

-   viz občanská válka

### **Kolonie a polokolonie**

#### **Čína**

-   přístavy pronajímala Velká Británie, Francie, Německo, Rusko, USA,
    Belgie

-   1900 povstání boxerů

-   1911 jih republikou

-   1912 organizace Kuomintang

    -   boj proti velmocem

    -   Sun Jat-sen

-   1912 Pchu-ji se vzdal vlády

#### **Indie**

-   od 1818 britská kolonie

-   1857 povstání sipáhjů (domorodí vojáci v anglické armádě)

-   1858 sesazen poslední místokrál

-   → Indie součástí britského impéria

-   1875 císařství

-   1885 Indický národní kongres

    -   chtěli nezávislost

    -   Mahátma Gandhi

### **Motivy a přínos koloniálních výbojů**

-   prestiž

-   snaha zakrýt vnitřní problémy

-   strategický význam

-   hospodářské důvody

-   dovoz průmyslového zboží, stavba železnic, vzdělání → místní
    inteligence
