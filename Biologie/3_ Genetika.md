---
title: 3_ Genetika
---
Úvod
====

-   Z latinského genus - rod

-   Pojem zavedl r.1906 William Bateson

-   Počátky již ve středověku - křížení jedinců s nejvhodnějšími
    vlastnostmi

-   Johann Gregor Mendel (r. 1865)

-   Pokusy s křížením hrachu → zákonitosti pro přenos genetických znaků

-   Práce J. G. Mendela publikována r. 1866

-   Správnost Mendelových závěrů potvrzena až r. 1900 → genetika
    samostatným oborem

-   významný pokrok ve 2. polovině 20. století - popis nukleových
    kyselin

-   **Genetika = věda o dědičnosti a proměnlivosti živých organismů**

<!-- -->

-   **Dědičnost (heredita)**

    -   **=** schopnost organismů vytvářet potomky se stejnými nebo
        > podobnými znaky → zachování druhu

-   **Proměnlivost (variabilita)**

    -   = vzájemná odlišnost jedinců jednoho druhu, jejich rozdílná
        > schopnost reagovat na podmínky vnějšího prostředí

-   Proměnlivost je podmíněna

a)  faktory vnějšího prostředí

b)  genetickými vlivy

**Faktory prostředí a genetické vlivy se vzájemně doplňují**

-   **Moderní genetika**

    -   Ne pouze popis zákonitostí, ale cílený zásah → genové
        > inženýrství

-   Molekulární biologie (molekulární genetika)

-   Cytogenetika - buňky

-   Obecná (formální) genetika

Molekulární genetika
====================

Nukleové kyseliny
-----------------

-   R. 1994 - nositelkami genetické informace jsou nukleové kyseliny -
    DNA, RNA (ribóza)

### DNA

-   cukerná složka (deoxyribóza)

-   Dusíkaté báze

-   Fosfátová skupina (zbytek od kyseliny trihydrogenfosforečné)

#### Dusíkaté báze

-   Heterocyklické sloučeniny

-   Odvozené od purinu nebo pyrimidinu

-   Purinové báze

    -   **A** = adenin

    -   **G** = guanin

-   Pyrimidinové báze

    -   **C** = cytozin

    -   **T =** tymin

#### DNA

-   základem je polynukleotidový cukrfosfátový řetězec

-   **nukleotid** - cukr (pentóza / sacharid) + báze + fosfátová skupina

-   **nukleosid** - cukr + báze

-   nukleotidy v DNA v určitém pořadí - tzv. **sekvence**

-   Každý gen v DNA zapsán jako sekvence nukleotidů - zapisujeme
    zkratkami bází

5´ AACTGCGATGCCCTTAGC3´

-   Polynukleotidový řetězec - částečný popis struktury DNA

#### Kompletarita bází

-   Watson a Crick - DNA tvořena 2 polynukleotidovými řetězci navzájem
    spojenými H můstky mezi bázemi

-   A - T (2 můstky)

-   G - C (3 můstky)

-   Báze jsou navzájem komplementární

-   **Watsonovo-Crickovo párování bázi**

-   Komplementarita řetězců - obsah molekul purinových bází = obsah
    molekul pyrimidinových bází

-   **(A+G) / (T+C) = 1**

-   vzájemná orientace:

-   ![](media/image2.png){width="2.7343755468066493in"
    height="2.07001312335958in"}

#### Stabilita DNA

-   Vysoké teploty, chemické sloučeniny, specifické enzymy → denaturace
    DNA

-   Ochlazení, změna chemického složení roztoku → renaturace
    (reasociace) DNA

![](media/image4.png){width="3.3958333333333335in"
height="1.6145833333333333in"}

### RNA

-   Polynukleotidový řetězec

-   Cukr ribóza

-   Uracil (U)

-   A, C, G jako u DNA

-   Jednovláknová

-   3 typy - tRNA, rRNA, mRNA

#### Typy RNA

##### tRNA (transferová)

-   Vybírá správné aminokyseliny a umísťuje je do správného místa na
    ribosomu, aby mohly být začleněny do rostoucího aminokyselinového
    řetězce.

##### rRNA (ribosomální)

-   Tvoří jádro ribosomů, na kterých je mRNA překládána do proteinu

##### mRNA (informační, mediatorová)

-   Vzniká přepisem genů kódujících aminokyselinovou sekvencí proteinů a
    její základní funkcí je řídit vznik proteinů

rRNA a tRNA vznikají přepisem genů nekódujících aminokyselinovou
sekvenci proteinu. Jedná se o tzv. neinformační RNA.

Základní pojmy molekulární genetiky
-----------------------------------

-   **Gen** = informační a funkční jednotka obsahující genetickou
    informaci

-   **Genetická informace** = informace primárně obsažená v nukleotidové
    sekvenci DNA

-   **Genetický kód** = systém pravidel , podle kterých je určeno
    zařazení aminokyselin

-   **Genom** = všechny molekuly DNA živé soustavy, které se vyznačují
    replikací a dědí se na potomstvo

-   **Genotyp** = soubor všech genů jednotlivce

-   **Fenotyp** = soubor znaků a vlastností, kterými se v daném
    prostředí projevuje daný
    organismus![](media/image7.png){width="1.7552088801399826in"
    height="3.5104166666666665in"}

### Replikace (= syntéza)

-   tvorba replik molekul NK zajišťující přenos genetické informace z
    DNA do DNA nebo z RNA do RNA

-   eukaryota - DNA v jádře v chromozomech

-   prokaryota - kružnicová DNA

-   reparační procesy, velké množství dNTP

#### Předpoklady a požadavky pro replikaci

-   Templát = mateřská molekula

-   Primer = krátký oligoribonukleotid s volným 3´OH koncem

-   polymeráza = enzym, připojování
    > nukleotidů![](media/image8.png){width="1.953125546806649in"
    > height="1.965181539807524in"}

-   nukleotidy

<!-- -->

-   **semikonzervativní způsob =** v obou výsledných molekulách se
    zachová 1 řetězec výchozí, druhý je nový → dceřiné DNA mají stejnou
    informaci

-   Probíhá **semidiskontinuálně** = na mateřském vlákně
    3´-5´kontinuálně, na vlákně 5´-3´ diskontinuálně, po úsecích zv.
    **Okazakiho fragmenty** - spojeny DNA-ligázou

### Proteosyntéza

=syntéza proteinů

-   Převedení informace v DNA do konkrétního znaku

-   Nezbytná je RNA

-   2 fáze:

    -   transkripce (přepis)

    -   translace (překlad)

#### Transkripce 

-   Sekvence nukleotidů v daném genu se přepisuje do sekvence mRNA

##### mRNA

-   je lineární, syntetizuje se v jádře

-   RNA-polymeráza nasedne na templátové vlákno, k deoxyribonukleotidům
    přiřazovány koplementární ribonukletidy → mRNA

-   Řetězec RNA se prodlužuje ve směru 5´→ 3´

-   RNA-polymeráza dospěje na konec genu, vlákno mRNA se odpojí a putuje
    do cytoplazmy

![](media/image3.png){width="3.3281255468066493in"
height="2.4283705161854767in"}

#### Translace

-   V cytoplazmě se k mRNA připojují ribozomy (rRNA + specifické
    proteiny)

-   Ribozom nasedne na 5´konec mRNA → syntéza proteinu podle matrice
    mRNA

-   Gen. informace přepsaná z DNA do mRNA udává pořadí aminokyselin v
    proteinovém řetězci (AMK transportovány pomocí tRNA)

![](media/image5.png){width="4.253424103237095in"
height="3.5677088801399823in"}

##### tRNA

-   ,,jetelový trojlístek"

-   Akceptorové rameno

-   Antikodonové rameno

-   20 různých tRNA, každá nese 1 z 20 aminokyselin

-   označení - tRNA^Ala^, tRNA^Val^, tRNA^Leu^

![](media/image1.png){width="2.3593755468066493in"
height="2.870573053368329in"}

Genetický kód
-------------

-   sekvence mRNA čtena po 3 = tripletech nukleotidů

-   Každý triplet je kodon komplementární k antikodonu v tRNA

-   Triplety → zařazení

-   = **genetický kód**

<!-- -->

-   UAA, UAG, UGA - terminační (STOP) kodony

-   AUG - bifunkční (iniciační kodon, kóduje methionin)

-   2 znaky

    -   **univerzální** pro všechny organismy

    -   **degenerovaný** (1 aminokyselina kódována větším počtem kodonů)

Ústřední dogma molekulární biologie
-----------------------------------

-   Genetická informace se přenáší z DNA do RNA a odtud do molekuly
    proteinu

-   Zpětný přenos informace z proteinu do nukleové kyseline

DNA ⇔ DNA → RNA → protein

### Genové exprese

-   Gen = základní jednotka genetické informace

<!-- -->

-   2 základní skupiny

a)  **Strukturní geny** - informace o primární struktuře proteinů

b)  **Geny pro funkční RNA** - jejich transkripty nepodléhají translaci
    (geny kódující tRNA a rRNA)

#### Struktura genu

-   **Transkripční jednotka** - kóduje mRNA

-   **Promotor**, **terminátor**, regulační oblasti, začátek a konec
    transkripce

![](media/image6.png){width="6.270833333333333in"
height="3.1666666666666665in"}

Genové exprese u bakterií

-   Promotor, několik genů tv. **operon**

-   **Operátor** (= sekvence za promotorem) - regulace exprese genů,
    vazba **represoru**

-   obr.

<!-- -->

-   Molekuly měnící aktivitu represoru:

a)  Induktory

b)  Korepresory

obr z učebnice

Genová exprese u eukaryot

-   Operony se nevyskytují, mezi promotorem a terminátorem 1 (vlastní
    regulační oblasti)

<!-- -->

-   **transkripční faktory**

-   Umožňují nasednutí RNA-polymerázy → zahájení transkripce

-   Geny z **exonů** a **intronů**

-   Transkripcí vzniká hnRNA → sestřih, vyštěpení intronů → spojení
    zbylých exonů → funkční mRNA

obr,

-   Podle výskytu nekódujících sekvencí rozlišujeme 2 skupiny genů:

a)  **Jednoduché** (u bakterií)

> transkripce → mRNA→ translace→ primární struktura proteinu

b)  **Složené** (u eukaryí)

> transkripce → hnRNA → sestřih → mRNA→ translace→ primární struktura
> proteinu
