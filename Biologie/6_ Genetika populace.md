---
title: 6_ Genetika populace
---
Populace

soubor jedinců téhož druhu, kteří žijí na určitém území určitou dobu

jedinci dané populace jsou prostorově odděleny (nekříží se)

soubor všech alel v populaci se označuje jako genofond

četnost jednotlivých alel v genofondu je v % nebo desetinným číslem

autogamická (inbrední)

s omezeným výběrem partnerů (málo početné populace), dochází k
příbuzenskému křížení (inbreeding) - snižuje se počet heterozygotů a
zvyšuje se počet homozygotů

pokud se vyskytuje alela která nese

panmiktická populace

velká populace, náhodný výběr partnerů

v rovnováze počet hetero a homozygotů - rovnováhu lze vzjádřit pomocí
Hardyho - Weinbergova zákona

p^2^(AA) + 2pq(Aa) + q^2^(aa) = 1 p+q = 1

p - četnost dom. a

q - četnost reces. a

Rh- 16% - recesivní homozygoti - aa

Rh+ 84% - dominantní homozygoti a heterozygoti - AA, Aa

q^2^ = 0,16

q = 0,4

p+q = 1 ⇒ p = 0,6

p^2^ = 0.6^2^ = 0.36

AA 36%

Aa 2pq = 2 . 0.6 . 0.4 = 0.48 → 48%

Faktory které porušují panmiktické populaci

-   příbuzenské křížení

-   mutace - nízká četnost výskytu

-   selekce (výběr) - kladná (jedinci s výhodnými alelami) x nevýhodné
    (heterozygoti - skrytá), přírodní výběr - selekční tlak = způsobuje
    to že nevýhodná alela, která je dominantní vymizí rychle z populace
    a recesivní mizí pomalu - ovlivňuje rovnováhu

-   genetický posun = drift - menší počet potomků - nereplikují se
    všechny typy alel → četnost některých alel se může snížit / vymizet
    → mohou se vytvářet odlišné populace ve stejných podmínkách → vznik
    nových druhů

    -   3 kroky:

        -   1\. krok ekotyp - ekotyp šumavský a krkonošský

        -   2.

        -   plemena, variety

        -   poddruhy a druhy - fruh se může křížit pouze v druhu

-   migrace - imgrace (přínus nových alel do populace) a emigrace (odsun
    alel z populace)
