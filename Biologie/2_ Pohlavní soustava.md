---
title: 2_ Pohlavní soustava
---
-   Pohlavní rozmnožování - splynutí haploidních **gamet** (poloviční, 1
    > sada) → diploidní **zygota**

-   Dělení zygoty → zárodek (embryo)

-   Kombinace ♀ a ♂ genetické výbavy

<!-- -->

-   mužské gamety zv. **spermie**

Stavba spermie:![](media/image1.jpg){width="1.7447922134733158in"
height="2.4659722222222222in"}

-   **Hlavička** - jádro + akrozom

-   **Krček** - obsahuje mitochondrie (energetické centrum)

-   **Bičík** - umožňuje pohyb

<!-- -->

-   Ženy vytváří gamety zv. **oocyty**

-   Každé vajíčko obklopeno folikulárními buňkami → folikul

<!-- -->

-   Gamety vznikají **meiózou** - redukuje se počet chromozomů (2n → n)

a)  **Spermatogeneze**

-   Z primárního spermatocytu (2n) vzniknou 4 funkční spermie
    > (n)![](media/image2.jpg){width="2.0104166666666665in"
    > height="2.9461406386701663in"}

b)  **Oogeneze**

-   Z primárního oocytu vzniká 1 funkční vajíčko a 2 mal pólovitá
    > tělíska

-   Dokončení oogeneze až po vstupu spermie

<!-- -->

-   Pohlaví je určeno geneticky

-   Žena má dva pohlavní chromozomy X

-   Muž má jeden chromozom X a jeden Y

**Funkce pohlavní soustavy**

1)  Tvorba a uchování pohlavních buněk

2)  Produkce pohlavních hormonů

3)  Zprostředkování kontaktu spermie a vajíčka

4)  Zajištění vývinu nového jedince (zygota - porod) - pouze u žen

Pohlavní soustavou muže
=======================

-   K mužským pohlavním orgánům patří:

1.  **Varlata**

-   Párový orgán vejčitého tvaru

-   Uložena v šourko

-   Vznik spermií - v semenných kanálcích ze zárodečných buněk spermie

-   **Sertoliho buňky** - výživa spermií

-   **Leydigovy buňky** - produkce mužských pohlavních hormonů,
    > nejvýznamnější
    > testosteron![](media/image4.png){width="2.744792213473316in"
    > height="3.4933716097987753in"}

2.  **Nadvarle**

-   Párový kyjovitý orgán

-   nasedá na horní pól varlete

-   shromažďování a dokončení vývinu spermií

3.  **Chámovod**

-   Silnostěnná trubice z hladké svaloviny

-   vychází z nadvarlete do dutiny břišní

-   Vstupuje do prostaty - zde připojen k močové trubici

4.  **Měchýřkovité žlázky**

-   Párové žlázky na zadní straně moč. měchýře

-   Ústí do koncového úseku chámovodu

-   Vylučují tekutinu → výživa spermií

5.  **Prostata (předstojná žláza)**

-   tvořena hladkou svalovinou

-   **Prostatické žlázky** - ústí do močové trubice, produkce tekutiny →
    > lepší pohyblivost spermií

6.  **Pyj (penis)**

-   Topořivý kopulační orgán

-   Konec rozšířen v **žalud** krytý **předkožkou**

-   Prochází tudy močová trubice

-   Podél močové trubice **3 topořivá tělesa** - naplnění krví →
    > ztopoření penisu (erekce)

-   Spermie + výměšky z ostatních žláz → **ejakulát** (semeno, spermie)

 

Pohlavní soustava ženy
======================

-   K ženským pohlavním orgánům patří:

### Vaječníky

-   Párový orgán v dutině břišní

-   Oogeneze

-   Tvorba pohlavních hormonů (estrogeny, progesteron)

-   Činnost řízena folitropinem a luteotropinem z adenohypofýzy

### Vejcovody

-   Párová trubice k transportu oocytu z vaječníku do dělohy

-   Na začátku fimbrie(brvy) k zachycení oocytu

-   V dolní části oplození vajíčka a vznik zygoty

-   Ústí do dělohy

### Děloha

-   Dutý svalový orgán hruškovitého tvaru

-   vývoj oplozeného vajíčka až do porodu

-   Během těhotenství se zvětšuje

-   2 části:

1.  **Děložní tělo** - uvnitř dutina děložní, která vystlána sliznicí
    > zv. endometrium

2.  **Děložní hrdlo** - koncová část zv. děložní čípek

<!-- -->

4.  Pochva

-   Trubice tvořená hladkou svalovinou

-   Kopulační orgán, odvodná pohlavní cesta

-   Ústí poševním vchodem

5.  Předsíň poševní

-   Štěrbina mezi malými stydkými pysky

-   Obklopena dvěma páry stydkých pysků - malými a velkými

-   Ústí sem močová trubice a poševní vchod

-   Po stranách vestibulární žlázky - vylučují sekret

-   Topořivé útvary v klitorisu

![](media/image3.png){width="2.274821741032371in"
height="2.1159459755030623in"}

Menstruační cyklus

-   Soubor procesů provázejících dozrání vajíčka a přípravu ženy na
    > těhotenství

-   4 fáze

    -   **proliferační**

    -   **sekreční**

    -   **ischemická**

    -   **menstruační**

1.  **Proliferační fáze**

    -   Působení folitropinu → folikul - uvnitř prochází oocyt 1.
        > meiotický dělením

    -   Folikul se mění ve zralý Graafův folikul

    -   Graafův folikul produkuje estrogeny → růst děložní sliznice

    -   12\. - 14. den cyklu nastává **ovulace** - Graafův folikul praskne, oocyt
        > přechází do vejcovodů

2.  **Sekreční fáze**

    -   Prasklý folikul se mění na žluté tělísko - produkuje progesteron

    -   Děložní sliznice se připravuje na přijetí oplozeného vajíčka

<!-- -->

3.  **Ischemická fáze**

    -   **V případě, že nedojde k oplození**

    -   Oocyt zaniká, žluté tělísko → bílé tělísko

    -   Pokles produkce estrogenů a progesteronu

    -   Snížen přítok krve do děložní sliznice

4.  **Menstruační fáze**

    -   Odumírá děložní sliznice

    -   Krvácení (s krví odchází i zbytky sliznice)

    -   Za +-28 dní po začátku předchozího krvácení

Pozn.: Antikoncepce:

hormonální = zamezení uvolnění vajíčka do vejcovodů, umělé hormony

kondom - mechanická ochrana

nitroděložní tělísko - zavádí lékař, operativní; dává se dovnitř dělohy,
brání pohybu spermií a uchycení vajíčka, musí se po určité době měnit

výpočet neplodných dnů - výpočet dny dojde k uvolnění vajíčka (ovulační
testy)

přerušovaná soulož

Ontogeneze člověka
==================

-   Soubor změn od oplození do smrti

-   Změny kvalitativní a kvantitativní

1.  **Období prenatální (nitroděložní)**

    -   Embryonální fáze

    -   fetální fáze

2.  **Období postnatální**

Prenatální období - embryonální fáze
------------------------------------

-   Splynutí spermie a vajíčka → zygota

-   Rýhování zygoty → embryo, přesun do dělohy

-   Embryo → morula (mnohobuněčný tvar) → blastula (dutina)- tvořena
    > embryoblastem a trofoblastem (obaly)

-   Ve fázi blastuly vstup do dělohy - **nidace**

-   Počátek **těhotenství**

-   2\. - 3. týden těhotenství z blastuly **gastrula**

-   Embryoblast:

→ zárodečný terčík, vznik zárodečných listů - ektoderm, mozederm (cévy,
..), entoderm

→ amniový a žloutkový váček (výživa, ochrana) - amniová tekutina zv.
plodová voda

-   3\. týden lze odlišit hlavu a trup, základ nervové trubice

-   4\. týden - činnost srdce, základy končetin, oči, uší, žaberní oblouky

-   Krevní cirkulace mezi zárodkem a **placentou** (váží asi 500g,
    > zajišťuje výživu, ochranu, embryo je spojeno s placentou
    > pupečníkovou šnůrou)

-   5\. týden - končetiny + prsty, obličej

-   4\. - 8. týden je embryo nejcitlivější na teratogenní faktory !!!
    > (dochází nejčastěji potratům, alkohol, cigarety - poškození embrya)

**Prenatální vývoj** - fetální fáze (zárodek → plod)
----------------------------------------------------

-   začátek v 9. týdnu těhotenství

-   8\. - 9. týden se tvoří pohlavní orgány

-   12.týden - osifikace, tvorba moči

-   16\. - 20. týden ochlupení zv. **lanugo**

-   od 18. týdne aktivní pohyby

-   20\. týden: hnědá tuková tkáň (tepelná izolace + zdroj energii → vymizí;
    > obrázek)

-   21\. - 25. týden: základy nehtů

-   26.týden: začíná pozdní fetální období, dostatečně vyvinuté plíce,
    > krevní oběh, CNS - narozený plod je schopen přežít ve většině
    > případů

-   30\. - 40- týden: završení vývinu, dokončen vývin CNS, vytváří se
    > reflexy, zvyšuje se podíl bílého tuku, kůže je elastická

-   od 35. týdne mizí lanugo

-   Prenatální vývin končí **porodem**

Porod
-----

-   **1. doba porodní (otevírací doba) -** stahy děložního svalstva,
    > rozšiřování porodních cest, protržení amniového vaku → odtok
    > plodové vody (nejdůležitější; kontrakce)

-   **2. doba porodní(vypuzovací doba)**-úplné otevření děložního krčku
    > a vypuzení plodu(první vystupuje hlavička)(nejpomalejší a
    > nejbolestivější fází je vypuzení hlav.)

-   **3.doba porodní (lůžková doba)** - vypuzena placenta se zbytkem
    > pupečníku a plodových blan

-   **Šestinedělí** - období, kdy se organismus ženy vrací do stavu před
    > graviditou

-   **Laktace -** období produkce mléka, navozena prolaktinem a
    > oxytocinem

Postnatální vývin
-----------------

-   Začíná porodem, několik fází

### Novorozenecké období

-   Od narození do 28. dne života

-   Reflexy - dýchací, sací, polykací, uchopovací

-   Adaptační procesy - změna krevního oběhu, aktivace termogeneze,
    > bakteriální flóra ve střevě

### Kojenecké období

-   od 28.dne do 1 roku

-   Rychlý vývin a růst, zvyšování hmotnosti

-   od 6. měsíce - vhodné přikrmování kašovitou stravou, prořezává se
    > mléčný chrup

    -   Na konci období vzpřímená chůze, řeč

-   Pravidelné prohlídky u pediatra

### Batolecí období

-   Od konce 1. do konce 3. roku

-   Zdokonalují se motorické i duševní schopností

-   Rychlý rozvoj řeči

-   Základní hygienické návyky

-   Komunikace mezi vrstevníky

### Předškolní období

-   Od konce 3. roku do konce 5.-6. roku

-   Uvědomování si vlastního ,,já" (takový ty záchvaty)

-   Zlepšení motorických dovedností a řeči

-   Rozvoj abstraktního myšlení

### Školní období

-   od 6 do 15 let

-   Učení se společenským normám chování

-   Rozvoj duševních a fyzických schopností

### Puberta - individuální

-   U děvčat začíná mezi 9.-10. rokem, u chlapců od 11.-12. roku, končí
    > mezi 16.-18.rokem

-   Morfologické, fyziologické a psychické změny

-   Zrychlení růstu těla, změna aktivity endokrinních žláz

-   Druhotné pohlavní znaky, u dívek první menstruace, u chlapců hlasová
    > mutace

### Období adolescence

-   Od konce puberty do 21.-24. roku

-   Dokončuje se růst těla a vývin nervové soustavy

-   Příprava na budoucí povolání, nástup do zaměstnání

-   Konečný přechod do dospělosti

### Období dospělosti (adultní fáze)

-   začíná mezi 21.-24. rokem

-   Člověk je na vrcholu psychických i fyzických sil, realizuje se v
    > zaměstnání, zakládá rodinu, stará se o stárnoucí rodiče

-   U žen po 40. roce menopauza (konec menstruace)

-   Končí v 60. letech

### Období stáří (gerontická fáze)

-   Začíná po 60. roce

-   Zvýšení nemocnosti, úbytek fyzických sil

-   Ochabnutí kůže, zhoršení vidění i sluchu, vlasy ztrácí pigmentaci

-   Ztráta vody → snížení hmotnosti
