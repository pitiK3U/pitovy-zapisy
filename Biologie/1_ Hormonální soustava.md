---
title: 1_ Hormonální soustava
---
Žláza
=====

-   buňka, skupina buněk, orgán

-   Sekrece biologicky významných látek

1.  Exokrinní žlázy - vylučují produkty na povrch těla

2.  Endokrinní žlázy - vylučují produkty zv. Hormony do krve a tkání

Funkce endokrinního systému:
----------------------------

-   řízení všech procesů v těle

-   působení zprostředkováno **hormony** (různá chemická struktura)

-   Přenos hormonů tělesnými tekutinami - tzv. **hormonální regulace**

-   **Endokrinologie =** obor, který se zabývá funkcí a chorobami žláz s
    > vnitřní sekrecí a účinky hormonů

Hormony
-------

-   působení v nízkých koncentracích

-   rozpoznávány receptory v tkáních (orgánech)

-   vazba hormonu na receptor → reakce vedoucí k buněčné odpovědi =
    > **buněčná signalizace**

#### Hypotalamus

-   nejvyšší nadřazené centrum endokrinní sekrece

-   ovlivňuje činnost **hypofýzy** prostřednictvím:

    -   **liberinů -** podporují sekreci

    -   **statinů -** zastavují sekreci

**Hypofýza**

-   tvořena 2 laloky:

1.  **Přední lalok (adenohypofýza)**

2.  **Zadní lalok (neurohypofýza)**

##### Adenohypofýza

-   Produkuje tzv. **tropní hormony** (tropos = směr) - ovlivňují
    > činnost jiných endokrinní žláz

**Somatotropin (růstový hormon)**

-   podporuje růst těla

-   nadbytek → gigantismus, akromegalie

-   nedostatek → nanismus

**Prolaktin (laktogenní hormon)**

-   podporuje růst mléčné žlázy

-   po porodu vyvolávají laktaci

-   nadbytek u mužů → zvětšení prsou (gynekomastie)

**Adrenokortikotropní hormon (kortikotropin)**

-   stimuluje růst a funkci kůry nadledvin - vylučování zv. kortizol

-   Nadbytek → Cushingův syndrom (obezita, střídání nálad, deprese,
    > řídnutí kostí, u žen - vymizení menstruace, nepřirozený obličej)

**Tyreotropin (tyreotropní hormon)**

-   stimuluje syntézu hormonů štítné žlázy

**Folitropin**

-   význam pro funkci pohlavního ústrojí

-   U žen - růst folikulů, u mužů - tvorba spermií

**Luteotropin (luteinizační hormon)**

-   stimuluje růst folikulů, ovulaci, přeměnu folikulu na žluté tělísko,
    > průběh menstruačního cyklu

##### Neurohypofýza

**Antidiuretický hormon (ADH)**

-   omezuje vylučování vody močí → udržení stálého objemu tekutin v těle

**Oxytocin**

-   stimuluje stahy dělohy → urychluje porod

-   podporuje výdej mateřského mléka

##### Štítná žláza

-   tvořena 2 laloky po stranách chrupavky hrtanu

<!-- -->

-   vylučuje 2 hormony:

> **tyroxin (tetrajodtyronin)**
>
> **trijodtyronin**

-   stimulují syntézu bílkovin (zlepšení dusítaké bilance)

<!-- -->

-   Funkce štítné žlázy závisí na dostatku jódu

-   nedostatek jódu v období nitroděložního vývoje → **kretenismus**,
    > během života → **struma**

-   Některé buňky štítné žlázy tvoří kalcitonin - snižuje
    > **koncentraci** Ca^2+^ v krvi

##### Příštítná tělíska

-   útvary na zadní straně laloků štítné žlázy

> **Parathormon**

-   stimuluje odbourávání Ca z kostí → zvyšování koncentrace Ca v krvi

-   antagonista kalcitoninu

##### Nadledviny

-   Párové žlázy na horních pólech ledvin

-   Obaleny tukovým pouzdrem

-   Skládají se ze 2 částí - **kůra** a **dřeň**

a.  **Kůra nadlevin**

> **Kortikoidy**

-   2 základní skupiny:

> **Glukokortikoidy**

-   Nejvýznamnější **kortizol**

    -   reguluje hladinu glukózy

    -   zvyšuje metabolismus bílkovin a tuků

> **Mineralokortikoidy**

-   Hlavní zástupce **aldosteron** - řídí hospodaření organismu s
    > anorganickými ionty (Na^+^ a K^+^)

**dřeň nadledvin**

-   řízená vegetativním nervstvem

-   Tvorba 2 hormonů ze skupiny **katecholaminů**

**Adrenalin**

-   Tvoří se při stresové reakci

-   stimuluje srdeční činnost, rozšiřuje cévy, podporuje štěpení
    > glykogenu a rozpad tuků

**Noradrenalin**

-   Zvyšuje intenzitu srdečních kontrakcí **→** zvyšuje krevní tlak

##### Slinivka břišní (pankreas)

-   Žláza ve výši 1. a 2. bederního obratle

-   Z exokrinní a endokrinní části

-   Endokrinní část - **Langerhansovy ostrůvky**

    -   Obsahují několik typů buněk

<!-- -->

-   B-buňky produkují **inzulin**

    -   snižuje hladinu glukózy v krvi

    -   nedostatečná tvorba nebo neschopnost buněk na něj reagovat →
        > cukrovka

-   A-buňky produkují **glukagon**

    -   zvyšuje hladinu glukózy v krvi

    -   antagonista inzulinu

##### Vaječníky

-   Folikuly produkují **estrogeny** (estradiol)

    -   vývoj pohlavních orgánů a sekundárních pohlavních znaků

-   Žluté tělísko vylučuje **gestageny** (progesteron)

    -   průběh menstruačního cyklu

    -   rozvoj mléčné žlázy

##### Varlata

-   Leydigovy buňky produkují **testosteron**

    -   vývoj pohlavních orgánů a sekundárních znaků

    -   tvorba bílkovin
