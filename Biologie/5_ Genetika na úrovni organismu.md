---
title: 5_ Genetika na úrovni organismu
---
Genetika na úrovni organismu
============================

-   **nejstarší odvětví genetiky** - známá už před poznatky

-   zakladatel **Johann Gregor Mendel**

    -   narodil se v 22.7.1822 v Hynčicích součást obce blažně u nového
        > Jičína

    -   národnost Slezská - narodil se vlevo od potoku

    -   otec bojoval v pruské válce - postavil velký statek - zadlužil
        > se

    -   2 sestry

    -   nebyl na manuální práce → začal docházet do školy → Gymnázium →
        > Filosofický ústav - náročné na peníze

    -   1843 vstup do kláštera (přijímá jméno Gregor)

    -   studium **teologie**

    -   **→** začal učit → nedokončil zkoušky učitelské způsobilosti

    -   pokusy s hrachem → 1866 přednášky byly publikovány

    -   pokusy s ostatními rostlinami

    -   1868 opatem kláštera - zemědělská činnost - hospodářství a
        > ekonomika

    -   1881 Mendel byl ředitelem hypoteční banky v Brně

    -   pokusy se včely

    -   Medel se věnoval meteorologickým pozorováním

    -   zemřel 1884 - augustiniánský hrob

    -   1900 Huge de Vries, Carl Correns, Erich von Tschermak -
        > znovuobjevení Mendelových principů dědičnosti

Křížení - hybridizace
---------------------

-   jedinec = hybrid

-   pohlavní rozmnožování záměrné vybraných jedinců, kdy se sleduje
    > přenos znaků z rodičů na potomky, jedinec vzniká rýhování zygoty -
    > má 2 alely

-   alely písmeny abecedy A-Z, a-z (nepoužívá se X, Y - používá se u
    > pohlaví)

-   pokud 2 stejné alely → AA aa = homozygot

-   pokd 2 různé → Aa heterozygot

-   pokud alela i u homo i hetero → dominantní alela - velké písmeno

-   pokud pouze u homo → recesivní - malé písmeno

<!-- -->

-   autozomální dědičnost (chromatické )

    -   geny na pohlavních chromozomech - gonozomální dědičnost

nepohlavní - z 1 rodiče → stejná DNA = klon

-   kvalitativní znaky

    -   barva, tvar

    -   monogenní znaky:

monohybridismus

-   sledujeme jeden gen který kóduje jeden znak

-   generace rodičů - P parietální

-   generace dceřiná F - filiální

      A    A
  --- ---- ----
  A   AA   AA
  A   AA   AA

uniformní genotypicky i fenotypicky

P: AA x aa - gamety: A + a

F1

      A    A
  --- ---- ----
  a   Aa   Aa
  a   Aa   Aa

uniformní

F2

      A    a
  --- ---- ----
  A   AA   Aa
  a   Aa   aa

úplná dominance
---------------

3 : 1 fenotypový poměr

1 (AA) : 2(Aa) : 1(aa) genotypový poměr

neúplná dominance
-----------------

do fenotypu se recesivní alela částečně projeví (P: Č x B → F1: Růžová)

fenotypový poměr - 1 Č : 2 R : 1 B

genotypový poměr - 1(AA) : 2(Aa) : 1(aa)

intermediarita

Zadání

U skotu je bezrohost \> rohatost. Jaké potomstvo můžeme očekávat
zkřížením bezrohého býka s rohatými krávami, když jedna z nich již dříve
při stejném křížení porodila rohaté tele?

Bezrohost = AA, Aa

Rohatost = aa

→ tele musí mít aa

Máme 2 norky, standardní (čistý) norci hnědavá barva kožešiny a druzí
norci šedavou barvu. Víme, že hnědavá \> šedavou.

Jaké získáme potomstvo F1 po křížení těchto dvou ras norků?

Jaké bude složení F2 těchto potomků?

Jak bude křížení s šedavým samečkem s hybridní samičkou?

H: AA

Š: aa

P: AA x aa

F1

      A    A
  --- ---- ----
  a   Aa   Aa
  a   Aa   Aa

→ hnědé zbravení

F2

      A    a
  --- ---- ----
  A   AA   Aa
  a   Aa   aa

3 : 1

sameček: aa

samička: Aa

      A    a
  --- ---- ----
  a   Aa   aa
  a   Aa   aa

1 : 1

Zbarvení mourovaté srsti \> nad bílou srstí

mourovatý kocourek \| ~~AA /~~ **Aa**

s bílou kočkou Mickou → bílé koťátko \| aa → aa

s bílou Mindou → mourovaté koťátko \| aa → Aa

s mourovatou Líza → bílé koťátko \| Aa → aa

s mourovatou Číčou → mourovaté kotě \| AA / Aa → AA / Aa

Dlouhosrsté + dlouhosrsté → 18 dlouhosrsté + 5 hladkosrstí (fenotyp
skoro 3 : 1 )

Aa + Aa →

podíl homozygotů

genotyp 1 : 2 : 1

fenotyp 3 : 1

18/3 = 6 AA

12 Aa

      A    a
  --- ---- ----
  A   AA   Aa
  a   Aa   aa

U jehňat 3 typy ušních boltců

dlouhé - AA

bezuché - aa

a krátke Aa

dlouhé X dlouhé → vždy dlouhé

bezuché X bezuché → bezuché

dlouhé X bezuchý → krátké

Jaké potomstvo bude po vzájemném křížení mezi sebou?

Křížím krátko uchou ovce a bezuchého berana ?

Aa X Aa

      A    a
  --- ---- ----
  A   AA   Aa
  a   Aa   aa

fenotyp 1 (dlouhé) : 2 (krátké) : 1 (bezuché)

Aa X aa

      A    a
  --- ---- ----
  a   Aa   aa
  a   Aa   aa

kodominance - projeví se dvě dominantní alely zaráz

I^A^ I^B^ i

0: ii

A: I^A^I^A^, I^A^i

B: I^B^I^B^, I^B^i

AB: I^A^I^B^

Hi

matka 0, otec B, může mít některé z dětí skupinou shodnou s matkou? Ano,
pokud je otec hetero \\o/

V porodnici zaměnili 2 chlapce,

rodiče A (I^A^I^A^, I^A^i), 0 (ii) a → i 0 i A

A (I^A^I^A^, I^A^i ) , AB (I^A^I^B^) → pouze A

první z chlapců má 0 (ii)

druhý A

Zákon o uniformitě hybridů
--------------------------

Zákon o segregaci
-----------------

Zákon o volné kombinovatelnosti genů (vloh)
-------------------------------------------

-   generace f1

-   při vzájemném křížení vícenásobných hybridů vznikne mezi alelami
    tolik kombinací, kolik je teoreticky možných matematických kombinací
    mezi vzájemně nezávislých proměnných

-   neplatí, když jsou geny ve vazbě (leží na 1 chromozomu; )

-   ### Pravidlo štěpení:

    -   potomstvu hybridů není jednotnost (generace f2)

    -   objevují se dominantní a recesivní alely v určitém poměru
        > (genotypový poměr)

-   ### Pravidlo o samostatnosti genů

    -   každý dědičný znak organismů je určen alespoň jednou dvojicí
        > samostatných alel, od matky

-   genové interakce

    -   jev kdy jeden znak vzniká sloučením větším počtem genů
        > (**kvalitativní**; alely leží na různých lokusech)

    -   #### reciproká interakce

        -   je vzájemné působení dvou a více párů alel, které mezi sebou
            > mají vztah úplné dominance. Vzniká tak forem

Máme slepici, která má **růžicový** hřeben, homozygotní. **Hráškový**
hřeben.

když budeme křížit získáme 4 typy hřebenů → získáme **ořechový** a
**jednoduchý**

růžicový \| RRpp

hráškový \| rrPP

ořechový \| RP

jednoduchý \| rrpp

P: RRpp x rrPP

F1: RrPp (ořechový)

F2: 9 (ořech) : 3 (r) : 3 (h) : 1 (j)

-   #### recesivní epistáze

    -   recesivní homozygotní dvojice genu potlačí projev druhého genu,
        > který je dominantní

Alela B podmiňuje tvorbu černého barviva, alela b je nefunkční.

Alela C dovoluje tvorbu jakéhokoliv barviva, alela c je mutantní a brání
tvorbě jakéhokoli barviva, což znamená, že kombinace recesivních alel cc
způsobuje albinismus.

9 (černá) : 3 : 4 (albíni)

-   #### dominantní epistáze

    -   x

Alela C podmiňuje tvorbu bílého barviva a svým působením je dominantní
nad alelou G, která způsobuje žluté zbarvení. Nepřítomnost dominantních
alel ccgg se projeví zeleným zbarvením plodů

![](media/image1.png){width="5.177083333333333in"
height="3.8020833333333335in"}

-   Komplementarita (komplementace)

    -   je vzájemné doplňování alel

Příkladem je barva květu u bělokvětých hrachů. Barvu ovlivní geny C/c a
P/p, kde dominantní alela C vytvoří bezbarvý prekurzor květního barviva
a dominantní alela P tento bezbarvý prekurzor přemění na fialovou barvu

9 : 7

Dědičnost kvantitativních

-   znaků znamená že jeden gen určuje více genů malého účinku

-   většinou délka, výška

-   je jich více jak kvalitativních znaků, ovlivněny vnějším prostředím

-   polygenní systém daného znaku je soubor genů malého účinku, který
    podmiňuje kvantitativní znak

-   je jich hodně = složité

-   2 typy alel

    -   neutrální alela - alela genů, která musí být aby se znak vůbec
        > vznikl, ale neovlivňuje hodnotu znaku (že vůbec rostete)

    -   aktivní alela - (díky akt. alele jste vyrostli hodně nebo málo)

-   → výsledná hodnota je dána působením všech alel + vliv prostředí,
    platí pravidlo (Gausova křivka)

-   čistá linie se může odlišovat genotyp (g) a vnější prostředí (P)
    (rostlina která budeme mít málo živin neporoste tak jako ta která má
    živin hodně)

-   **heritabilita** - dědivost, **H^2^ = (g/P)^2\ ^** (když se H^2^
    blíží 0, znak hodně ovlivnitelný prostředím) (pokud se blíží 1 je
    dáno více genotypicky)

Gonozomální dědičnost

-   projevuje se v genu který leží na pohlavním chromozomu

-   shodná část = homologní

-   neshodná = heterologní

    -   geny, které jsou na X nejsou na Y

    -   jestliže alela na X je postižená, tak se projeví ve výsledku

-   typy

    -   savčí = Drosophila

        -   homogametní XX (samičí)

        -   heterogametní XY (samčí)

        -   u většině hmyzu, ryb a plazů, u savců

    -   ptačí = Abraxas

        -   heterogametní ZW (samičí XY)

        -   homogametní ZZ (samčí XX)

        -   motýli, ryby, obojživelníci, plazi a ptaáci

    -   protenor

        -   XX - samičí

        -   X0 - samčí

        -   ploštice a kobylky

    -   haplodiodie

        -   parteogeneze - trubáci

-   gonozomální aberace - odchylky

    -   možnosti

        -   Y0 - teoretická možnost, nebyla ještě popsána, asi končí
            > spontánním potratem už v raných fází vývoje

        -   Klinefelterův syndrom - 47 XXY, 48 XXXY → gynekomastie;
            > sterilita jedince

        -   X0 - (1:3000) - **Turnerův syndrom** - spermie nenese
            > pohalvní buňku, vajíčko vpořádku, ženu, menší, duševní
            > méně cennost

        -   XXXY - primární pohlavní znaky mužské, sekundární ženské

        -   XYY - supermuž - vyšší, nejsou sterilní, **mutace**

        -   XXX - superžena

-   Gonozomálné recesivní dědičnost:

    -   recesivní znak se i ženy neprojeví

> XX zdravá žena XY zdravý muž
>
> X'X přenašečka X'Y nemocný muž
>
> X'X' nemocná

-   např. hemofilie, daltonismus (barvoslepost), displasie

Otec a syn jsou barvoslepí a matka je zdravá. Je to pravda?

-   Není - syn má od otce Y a od matky X' → matka je přenašečka

       X'     Y
  ---- ------ -----
  X'   X'X'   X'Y
  X    XX'    XY

Hemofilik a daltoničku

muž X^H^Y

žena X^D^X^D^

         X^H^       Y
  ------ ---------- -------
  X^D^   X^D^X^H^   X^D^Y
  X^D^   X^D^X^H^   X^D^Y

červenooký sameček a bělooká samička

xx x XY

      X    Y
  --- ---- ----
  x   xX   xY
  x   xX   xY

Proměnlivost (variabilita)
--------------------------

-   tendence rozlišení potomstva

-   2 příčina

    -   genetická - proč nejsme kopie rodičů

        -   **crossing-over** ()

        -   **segregace chromozomů** do gamet

        -   nakombinování chromozomů v gametě

        -   **mutace** (změna genotypu, stavby DNA, ovl; vliv prostředí
            > → mutace)

### Mutace

-   Vznik

    -   **Spontánně**

        -   chybou při replikace DNA, nejsou vyvolány vnějšími vlivy

    -   **Indukovaně**

        -   vlivem mutagenů (biologické, chemické, fyzikální), např.
            > yperit, kolchicin, viry, UV

-   Rozsah (typy)

    -   Genové - zasáhnou jeden gen

    -   Chromozomové - stavbu chromozomu

    -   Genomové - počet chromozomů v jádře

#### Genové mutace

-   dochází ke změně jediného genu (pozměněnují pořadí .. →
    nesyntetizuje se bílkovina)

-   Delece (ztráta nukleotidů), inzerce(vložení nového nukleotidů),
    substituce (záměna nukleotidů), amplifikace (zmnožení jednoho
    nukleotidu/celého tripletu)

<!-- -->

-   nemění smysl (oba kodony kódují stejnou aminokyselinu) -

-   měnící smysl (změní kódovanou kyselinu) - Missense

-   beze smyslu (ukončení řetězce) - Nonsense mutations

-   Frameshift - posun čtecího rámce

př.

GAA valin

↓

GTA kys. glutanová

hemoglobin (barvivo s železem) - F

chlorofyl ()

S aberantní hemogl.

drepanocytoza - srpkovitost \-- (v oblasti malárie ?)

FF zdravý homozygot

FS aberantní hem. heterozygot

       F    F
  ---- ---- ----
  F    FF   FF
  FS   FS   FS

      F    S
  --- ---- ----
  F   FF   FS
  S   FS   SS

SS - nejsou schopni vázat kyslík, 25%

**Letální alela** - způsobí smrt jedince
