#!/bin/bash
IFS=','
dir=./public
orDir="index.html,math.html,Biologie/,Čeština/,Dějepis/,Zeměpis/,SV_test_Novoveka.md,Společenské vědy/"

if [[ ! -d $dir ]]; then
	mkdir $dir
fi

cp $orDir "$dir" -r

find "$dir" -name '*.md' -type f -print0 |
while IFS= read -r -d $'\0' filepath; do 
	FILE=$filepath 
	output="${FILE%.*}.html"
	$(pandoc "$FILE" --toc -s -o "$output")

	$(rm "$filepath")
done
