---
pagetitle: Math cheatsheet
---



# Kvadratické rovnice s komplexními koeficienty

$$
\boxed{
\begin{gathered}
	x_{1,2} = \frac{-b \pm \sqrt{|D|}(\cos \frac{\alpha}{2} + \imath \sin \frac{\alpha}{2})} {2a} ; a,b,c\in\mathbb{C} \\
	|\cos \frac{\alpha}{2}| = \sqrt{ \frac{1 + \cos \alpha}{2} } \\
	|\sin \frac{\alpha}{2}| = \sqrt{ \frac{1 - \cos \alpha}{2} }
\end{gathered}
}
$$

**! Protože $|\cos \frac{\alpha}{2}|$ a $|\sin \frac{\alpha}{2}|$ jsou v absolutní hodnotě, musíme ručně určit v jakém kvadrantu se nachází poloviční úhel !**

Příklad:

$$
\begin{gathered}
	x^2 + 3x + 10 \imath = 0 \\
	D = 9 - 4 \times 10 \imath = 41(\frac{9}{41} - \frac{9}{41} \imath) = 41(\cos \alpha + \imath \sin \alpha) \\
	x_{1,2} = \frac{-3 \pm \sqrt{41}(\cos \frac{\alpha}{2} + \imath \sin \frac{\alpha}{2}) }{2} \\
	|\cos \frac{\alpha}{2}| = \sqrt{ \frac{1 + \frac{9}{41} }{2} } = \sqrt{\frac{50}{82}} = \frac{5}{ \sqrt{41} } \\
	|\sin \frac{\alpha}{2}| = \sqrt{ \frac{1 - \frac{9}{41} }{2} } = \sqrt{\frac{32}{82}} = \frac{4}{ \sqrt{41} } \\ \\
	\text{ Protože úhel } \alpha \text{ se nachází ve 4. kvadrantu, jeho poloviční úhel bude v 2. kvadrantu } \rightarrow \cos \frac{\alpha}{2} \text{ bude záporný a } \sin \frac{\alpha}{2} \text{ kladný } \\ \\
	x_{1,2} = \frac{-3 \pm \sqrt{41}(-\frac{5}{ \sqrt{41} } + \imath \frac{4}{ \sqrt{41} } )}{2} = \frac{-3 \pm (-5 + 4 \imath)}{2} \\
	x_1 = 1 - 2 \imath \\
	x_2 = 4 + 2 \imath 
\end{gathered}
$$