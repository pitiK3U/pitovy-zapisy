---
title: Novověká filosofie - test
---

# Racionalismus

# Empirismus
* **empirie** = zkušenost
* poznání na základě smyslů

## Francis Bacon
* miloval Aristotela
* věřil na nesmrtelnou duši
* hmota = mnoho zákonů, ~~atomismus~~, základem je věda a poznání
* ***,,Scientia et potentia"*** - vědění je moc - poud člověk pozná aporozumí přírodě bude mu sloužit
* dílo:
* ***Velké obnovení věd*** - obnovění vědy na základě empirie a řízené indukci (od konkrétních věcí k celku), postaveno na smyslové zkušenosti
* ***Novum Organum*** - empirická a induktivní metoda, musíme se odprostit od **idolů**
	1. **idola lidského rodu**(tribus) - lidé jsou limitováni smysly
	2. **idola jeskyně**(spectus) - lidé jsou ovlivněni zájmy, výchovou, ...
	3. **idola tržiště**(fori) - lidé volí špatná slova --> nerozumí si --> hádají se jak na tržišti
	4. **idola divadla**(theatri) - lidé jsou ochotni věřit názorům, systémům které jsou vytvořeny, ale nevíme, jestli jsou pravdivé
* ***Nová atlantida*** - obraz ideální společnosti, řízená vědci na ostravě
* *O důstojnosti a pokroku věd* - vytičil roli vědy v budoucnu

## Thomas Hobbes
* jeden ze zakladatelů **materialismu**- celý svět je tovřen hmotou
* **mechanický materialismus** - svět tvořen jednou hmotou, které má pohyb
* **determinismus** - vše má příčinu i důsledek
* gnosoelogie - pomocí smyslů chápeme věci, vrcholem poznání je rozum
* *rozumné* = mravné; to, co je prospěšné pro stát

# Racionalismus
## René Descartes
* snažil se vytvořit systém jako matematika
* **deduktivní metoda** (nepoznávám konkrétní věci, ale vrozené idee)
* svět tvořen 2 substancemi (zvířata pouze tělěsa - nemyslí)
	* duchovní: **myšlení**
	* materiální: **prostorová tělesa**
	* **šišinka** - místo, kde se substance spojují
	* Bůh je nad oběma substancemi
* chtěl najít jednotný pricnip --> všeobecné pochybování
* ***,,Myslím, tedy jsem"*** - já jsem ten, kdo myslí a pochybuje
* *Svět* - nařčen z kacířství
* *Rozprava o metodě* - **deduktivní metoda**, jak správně uvažovat
* *Meditace* - výklad o existenci a nesmrtelnosti
* *Principy filozofie* - systematický výklad jeho myšlenek

## Jan Amos Komenský
* renesanční filosof
* poslední biskup Jednoty bratrské
* učitel, vzdělání nikdy nekončí
* pansofie = encyklopedické vzdělání
* *Všeobecná rozprava o nápravě věcí lidských*
* ***Labyrint světa a ráj srdce***
* *Didaktika magna*

Jansenismus
## Cornelius Jansen
* jansenismus - reaguje na přílišný racionalismus, rozum ožebračuje víru

## Blaise Pascal
* matematik, fyzik
* *Listy venkovanovy* - obhajoba jansenimsu

## Baruch Spinoza
* nizozemský racionalismus
* kritický k Bibli - přizpůsobena prostému myšlení
	* popíral božství Ježíše Krista
	* upíral Bohu lidské vlastnosti a rysy
* existuje pouze jediná substance: hmota
	* její vlastností je myšlení a pohyb
	* ztotožňuje přírodu s Bohem
* **determinismus**
* **deduktivní metoda**
* čím víc toho člověk zná, tím víc ho to vede ke vnitřní svobodě
* etika: pudy, vášně a instinkty musí být vedeny rozumem
* stát: má poskytovat svobodu myšlení a názorů


## Gottfried Wilhelm Leibnitz
* "univerzální génius"
* přestavidtel plurarismu
	* **monády**
		* nedělitelé substance, tvoří veškerý svět, žádná není stejná
		* jsou tvořeny Bohem, Bůh hodinářem --> nastavil monády aby ukazovali stejný čas
		* jsou oduševnělé
		* existují nezávisle na nás = **objektivní idealismus**
* Zlo:
	1. metafyzické: člověk není dokonalý jako Bůh
	2. fyzické: utrpení člověka
	3. morální: založeno na hříchu, člověk hřeší, protože je nedokonalá bytost

# Anglický empirismus
## John Lock
* idealogie: 
	* **nemůže být nic v naší mysli, co nebylo ve smyslech**
	* zdrojem poznatků = smyslová zkušenost
	* **tabula rasa** - naše mysl je prázdná tabule, do které se postupně ideemi zapusijí zkušenosti
	* idee získáváme pozorováním vnějšího světa
	* hmota má kvantitativní vlastnosti
	* jednotlivé věci - kvalitativní vlastnosti
	* --> subjektivní idealismus
* zakladatem **liberalismu**, propagatel přirozeného práva
* stát vzniká na zákaldě společenské smlouvy
	* stát nesmí porušovat **přirozené právo** (na život, na svobodu)
	* zákony má vytvářet instituce (parlament)
	* svrhnout panovníka pokud nedodržuje zákony
	* vychova nemá být řízena státem

## Isaac Newton
* mechanický výklad světa
* Odmítal složené ideje (trojjedinost boží)
* deista = Bůh je geniální mechanik - nezasahuje do vesmíru

## George Berkeley
* ***,,Být znamená být vnímán"*** - aby něco existovalo, musí být vnímáno jiným subjektem (hmota neexistuje)
	* Popisem a kombinací počitků vznikají jednotlivá jsoucna
	* **Solipsismus** - existuje jen subjekt, který vnímá - nic, kromě mého vědomí neexistuje, existuji jen já sám
* Ideje do nás vkládá Bůh = důkaz Boží existence
* Ontologie:
* 2 skupiny jsoucen
	* **Ideje** - barva, chuť, tvrdost, velikost - všichni dostávají stejné ideje od Boha
	* **Substance** - vnímá ideje; mysl, duše
* Mechanicky vnímá společnost - ovládána 2 lany - síly odstředivé (egoismus) i dostřediv (pud?)

## David Humme
* empirismus --> Navazuje na Locka
* zkušenost je duševní stav
	* 2 řady představ
		* **Imprese** - bezprostřední vjemy, dojmy
		* **Představy** - zprostředkované ideje - vznikají v rámci vzpomínek na imrese, člověk poznává různé řady představ, ztotožňuje přestavy o věcech s nimi samotnými
* gnoseologie: neřešitelné, nepoznatelné - **agnostik** (gnoseologický skeptik)
* Platí zákon, ale spíše jsme si vytvořili nějaké vztahy
	* ráno se probudíme --> vyjde Slunce
	* rozděláme oheň --> bude teplo
	* -- co když to nebude fugovat?
	* --> chce, aby to co je jasné bylo nahrazeno pravděpodobností (je pravděpodobné, že když rozděláme oheň bude teplo)
* Ateista, **kritika víry**
* Etika založená na citech a emocích
