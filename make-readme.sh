#!/bin/bash

if [[ -f README.md ]]; then
	$(rm README.md)
fi

$(touch README.md)

last_directory=$(dirname pwd)

find . -name '*.md' -type f -print0 |
while IFS= read -r -d $'\0' filepath; do 
    current_directory=$(dirname "$filepath")

    if [[ $current_directory != $last_directory ]]; then
        while [[ $current_directory != *"$last_directory"* ]] ; do
            last_directory=${last_directory%/*}
            $(echo "</details>" >> README.md)
        done
            #echo -e "CUR: $current_directory\nLAST: $last_directory\n\n"

        subdirectories=$(echo "$current_directory" | sed -e "s|$last_directory||" | sed -e "s|^\/||")
        #echo -e "CUR: $current_directory\nLAST: $last_directory\nNOW: $subdirectories\n\n"

        if [[ $subdirectories != "" ]]; then
            IFS='/'
            read -ra split <<< "$subdirectories"
            for subdir in "${split[@]}"; do
                last_directory="$last_directory/$subdir"
                $(echo "<details style=\"margin-left: 40px\"> <summary>$subdir</summary>" >> README.md)
            done

            # reseting IFS after modification
            IFS=' '
        fi
    fi

	file=$filepath
	file="${file%.*}.html"
    filename="${file##*/}"
    # Three spaces are for tricking pandoc to
    # not clear the newline      |||
    #                            VVV
	$(echo -e "["$filename"]("$file")   \n" >> README.md)
done

$(pandoc README.md -o index.html)
