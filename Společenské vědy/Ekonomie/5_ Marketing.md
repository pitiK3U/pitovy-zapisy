---
title: 5_ Marketing
---
Marketing
=========

-   Komunikace podniku se svým okolím (cena, produkt samotný, místo
    prodeje, propagace, reklama, ...)

-   Podnikatel vychází z potřeb zákazníka, snaží se je uspokojit lépe
    než konkurence

-   Jak se prezentuje podnik, podnikatel navenek

-   Moderní marketing, dlouhodobá investice

-   4P :

    -   Product (co)

    -   Price (za kolik)

    -   Place (kde)

    -   Promotion (jak upozornit)

Potřeby spotřebitele jsou ovlivněny :
-------------------------------------

A\) osobními charakteristikami (věk a fáze životního cyklu, příjmy,
životním stylem)

B\) psychologickými charakteristikami a sociokulturními vlivy (vztah
člověka k určité sociální skupině, emoce, postoje)

Individualizovaný marketing
---------------------------

-   = přizpůsobení se individuálním potřebám každého jednotlivého
    zákazníka

-   CRM = pochopení individ. potřeb zákazníka a jeho ovlivnění vhodnou
    formou komunikace (1.zisk individ.dat o zákazníkovi, abychom mohli
    odhadnout , jak na něj působit = online databáze zákazníků, 2.
    analýza potřeb ve vztahu k času, jejich změna, 3. ve vhodné chvíli
    učiníme individuálně uzpůsobenou nabídku)

-   Naslouchání zákazníkům (sledování sociálních sítí, reakce na to, co
    zákazníci píší, průběžné spravování vlastních stránek, ..)

-   „Budu naslouchat a rozumět tomu, co potřebujete."

Cílený marketing
----------------

-   Firma rozdělí kupující na trhu na tzv. segmenty (podobné potřeby a
    chování),kterým nabízí vhodný produkt,přizpůsobí cenu,distribuci a
    propagaci

-   1\. Segmentace = rozdělení zákazníků na segmenty dle kriterií
    geografických (stát,město,kraj,...),demografických (věk, pohlaví,
    povolání, vzdělání, národnost,...), dle příjmů a životního stylu

-   2\. Targeting = firma vytváří v rámci jedné společnosti několik značek,
    které jsou zaměřeny na různé skupiny zákazníků (značka cenově
    dostupná=lidová, jiná luxusní, prestižní)

-   3\. Positioning = cílem je jasně definovat produkt,značku nebo celou
    firmu

    -   **odlišit se od konkurence**

    -   **vytvořit image značky**

    -   **vymezit důvody,proč daný produkt koupit**

-   Pozn.:

-   konkurence cenová x necenová

4P Produkt
----------

-   Proč si produkt kupujeme = užitná hodnota produktu

-   Vlastnosti a kvalita produktu -- dotykový displej telefonu

-   Obal -- kvalita,vlastnosti produktu, informace o produktu

-   Rozšíření produktu (vylepšení, doplňky -- paměťová karta k telefonu)

-   Doplňkové služby -- doprava, poradenské služby, montáž

Price = Cena
------------

-   Stanovení ceny :

-   A)marketingový přístup :

-   \- pomocí průzkumu, lze zjistit, jakou cenu by zákazník akceptoval

-   \- dle průměru konkurenčních cen stanovíme cenu naši

-   B\) nákladový přístup

-   cena = náklady+zisk+DPH

Odlišné ceny téhož zboží
------------------------

-   Ceny téhož zboží se liší dle :

    -   místa prodeje (stát,velkoměsto,internet,jiná prodejna)

    -   zákazníků (věrnostní slevy,množstevní slevy,slevy pro důchodce a
        > děti)

    -   období (vyšší cena v sezoně -- Vánoce)

    -   způsobu placení (sleva na okamžité placení)

    -   doby, po kterou je výrobek na trhu (ceny starších se snižují,
        > nově uváděné výrobky jsou dražší)

Cenové praktiky
---------------

-   balíčky a „antibalíčky"

-   vyrovnávací ceny v prodejnách se širokým sortimentem (některé
    produkty výrazně levné, jiné vyšší, zákazník často koupí i ty drahé)

-   kotvení = člověk se zaměří (zakotví) na 1 informaci (u žádaného
    produktu je nízká cena, zákazník předpokládá, že i ostatní produkty
    mají výhodné ceny

-   u některých produktů je úmyslně vysoká cena s cílem později
    nabídnout slevu -- i po slevě je cena pro prodávajícího výhodná)

-   cena vypadá výhodněji („baťovská cena", cena uvedena bez DPH a malým
    písmem s DPH, během výprodejů přeškrtnuta původní cena, výrazné
    inzerování slevových akcí a výprodejů -- pestré nápisy ve výloze)

Placement = distribuce
----------------------

-   = způsob, kterým se produkt dostane k zákazníkovi

-   A\) **přímý** prodej -- internet, drobní obchodníci, řemeslníci
    (truhláři, kováři)

-   B\) prodej **pomocí mezičlánků** -- produkt je dodán do maloobchodu, kde
    nakupují zákazníci, nebo velkoobchodu, kde pořizují zboží maloobchodníci

Promotion= propagace
--------------------

-   **Reklama** (lat. clamare = křičet, volat):

    -   média

    -   poutače (okolí silnic, dopravní prostředky, domy, ...)

    -   filmy (product placement)

    -   významné osobnosti

-   **Reklamní triky** : klamavá reklama, neetická („Bóbika"),
    plagiátorství, skrytá (rozhovor se známou osobností o novém
    automobilu), podprahová, product placement (hrdina jede v automobilu
    Mercedes, kouří cigarety určité značky, pije Cinzano, ...),
    fotomontáž, ...

Osobní finance
==============

-   **Příjmy** domácností :

**A) pravidelné**

\- plat/mzda, příjem z podnikání

\- sociální dávky (přídavky na děti, ...)

\- starobní důchod

\- renty

\- výsluhy

\- úroky (z dlouhodobých vkladů)

**B) nepravidelné**

výhry, odměny a bonusy zaměstnanců, odstupné při propuštění, úroky z
vkladů, příjmy z cenných papírů, z autorských práv, ...

Výdaje
------

Spotřební koš
-------------

-   **Nezbytné** (pevné) výdaje :

    -   nájem,splátka půjčky, hypotéky,leasingu, energie, ...

-   **Kontrolovatelné** výdaje: (na dovolenou, \...)

    -   jídlo, telefon, oblečení, benzin, opravy,...

-   **Vytváření rezervy** = mimořádné výdaje :(, mateřství, dlouhodobá
    nemoc, ... (pokazí se pračka, myčka, upadne výfuk, \...)

Jak financovat jednorázové výdaje domácího rozpočtu
---------------------------------------------------

-   **Výdaje, které můžeme plánovat** :

    -   Dovolená, VŠ studium, koupě auta nebo vlastního bydlení, ...

    -   **Výdaje neočekávané** : pračka, lednička, havárie auta, děti
        > ...

-   = 4 možnosti :

    -   1\. spořit

    -   2\. krýt je z rezerv (rezerva by měla činit troj- až šestinásobek
        > průměrných měsíčních výdajů)

    -   3\. půjčka, nákup na splátky, leasing === vzniknou nové pevné výdaje

    -   4\. jednorázové zvýšení příjmů (vedlejší pracovní poměr, brigáda, ...)

Osobní aktiva a osobní pasiva
-----------------------------

-   **Osobní aktiva** (takový majetek,který díky tomu,že jsme si jej
    pořídili,může přinášet příjmy)

1.*Finanční aktiva* -- akcie, dluhopisy, podílové listy

2*.Majetek, který v čase stoupá na hodnotě* a je dobře prodejný = umění

3.*Nemovitosti, které vytvářejí příjem* (penzion, pronájem bytu)

4.*Autorská práva* -- knihy, hudba, patenty

5.*Podnikání*, které nevyžaduje osobní přítomnost (penzion, )

**Osobní pasiva** (majetek, který vytváří výdaje) :

1.*nemovitosti k osobní potřebě* (byt,dům,chata,...)

2.*automobil k osobní potřebě*

3.*osobní spotřební věci*

pozn. : majetek může být současně osobním aktivem i pasivem (dům na
bydlení x na pronajímání)

3 otázky osobního financování
-----------------------------

1.Jak se zajistit proti těmto rizikům :

*ztráta příjmů*: nemoc, nezaměstnanost, bankrot ,...

*ztráta majetku*: živelná událost (vichřice, požár,povodeň), krádež ,...

*vznik nečekaných výdajů*: náhrada škody při autohavárii, kterou jsme
způsobili,...

Pojištění
---------

-   = slouží ke zmírnění negativních dopadů rizikových událostí (mají ja
    jedince, firmu, stát negativní vliv)

-   = smluvní vztah mezi **pojistitelem** (pojišťovna) a **pojištěným =
    pojistník** (osoba)

-   **Pojistná nebezpečí** = rizika, na něž se pojištění vztahuje (úraz,
    dopravní nehoda, vyplavení domu, \...)

-   **pojistná událost** = skutečnost, s níž je spojena povinnost
    pojistitele (pojišťobvny) vyplatit **pojistné vyplnění** (náhradu
    škody)

-   Pojišťovny na základě **pojistné smlouvy** vybírají **pojistné** od
    **pojistníka** (osoba, jež uzavřela pojistnou smlouvu)

-   Po uzavření pojistné smlouvy dostaneme doklad = **pojistka**

-   Pojištění:

    -   dorovolné

    -   povinně/zákonné (zdravotní/sociální pojištění pojištění
        > odpovědnosti za škodu způsobenou provozem vozidla

### **1.Životní** (ochrana před událostí, které **určitě** nastane)**:**

A.  *Rizikové ŽP* = klient je pouze pojištěn (pro případ smrti,
    důchodové nebo dožití se určitého věku = uvedeno v pojistné
    smlouvě), pojistné plnění se vyplácí pojištěnému nebo v případě jeho
    smrti tzv. *obmyšlené osobě* (pozůstalí)

B.  *Investiční ŽP* = kombinace pojištění a spoření (investování),část z
    účtu se investuje do podílových fondů (klient si může vybrat)

C.  *Kapitálové ŽP* = kombinace pojištění a investování (pojišťovna
    garantuje min. vyplacenou částku, klient neovlivní investici)

D.  *Úrazové připojištění* = samostatně nebo k ŽP

### **2.Neživotní** (ochrana před důsledky událostí, které nastat mohou, ale nemusí)**:**

1.  **úrazové pojištění** (mírní následky úrazu finanční kompezací -
    lečení, dlouhodobá nemoc)

2.  **pojištění za škodu způsobenou provozem vozidla** (=povinné
    ručení) - mírní škody, jež pojištěný způsobí

3.  **pojištění odpovědnosti za škody v občanském životě** = způsobené
    běžnou činností neúmyslně

4.  **pojištění nemovitosti a pojištění domácnosti** = mírní důsledky
    přírodních katastrof, požárů, krádeži

pozn.: nemovitost nesmí být *podpojištěno*

5.  **havarijní pojištění** = mírní škody na vozidlech způsobené živly,
    havárií nebo krádeží

6.  **pojištění úvěru** (=schopnosti splácet) = pojišťovna hradí splátky
    úvěru za dlužníka (ztráta zaměstnání)

7.  **cestovní pojištění** = nejčastěji před cestou do zahraničí, hradí
    se léčebné výlohy, ztráta zavazadel

*Pojištění odpovědnosti :*

škody, které způsobí svým jednáním sám pojištěný někomu jinému (na
majetku nebo zdraví)

2.Jak naložit s případným přebytkem domácího rozpočtu

-   Pravidelné ukládání (**investování**) nebo jednorázové uložení
    (**investice**)

-   Je třeba zvážit, jaké mají naše investice :

1.  **výnosy** -- porovnáme procenta výnosu

2.  **riziko**

3.  **likviditu** = rychlost, se kterou můžeme prostředky použít k
    > placení (vysokou likviditu mají hotové peníze, nelikvidní jsou
    > prostředky vložené do nemovitosti-trvá, než ji prodáme a získáme
    > peníze)

pozn.: abychom omezili riziko,rozložíme investice do více možností =
**diverzifikujeme je**

Investování s nižším rizikem = spoření
--------------------------------------

-   **Bankovní vklady**-výnos je málokdy vyšší než míra inflace,proto
    jsou vhodné pro krátkodobé uložení peněz (1 rok), např. spoření na
    dovolenou, vklady jsou pojištěny na 100% hodnoty, max. na 100 000
    eur, likvidita je vysoká (x termínované vklady)

-   **Stavební spoření** -- státní příspěvek (čím větší peníze tím větší
    příspěvek), likvidita velmi nízká

-   **Penzijní pojištění** -- státní příspěvek, likvidita nízká

-   **Soukromé životní pojištění** -- pojištění
    (smrt,úraz)+spoření,nízká likvidita

Investování s vyšším rizikem
----------------------------

-   **Podílové fondy** -- třeba investovat delší dobu, aby se případné
    ztráty vyrovnaly s výnosy, informace u instituce, která fond
    zakládá, likvidita vysoká

-   **Nákup dluhopisů** -- vhodný pro investování větších částek,
    vyžaduje znalost (stát mi slíbí, že za několik let bude dluhopis
    splacen s úroky)

-   **Investování do akcií** -- výnosy+dividendy, investování nejméně 5
    let, likvidita závisí na poptávce po akcii

-   **Investování do majetku** -- nemovitosti, umění, starožitnosti,
    drahé kovy, diamanty, cizí měna (hyperinflace,válka,politická
    nestabilita), výnos i riziko může být vysoké, **vyžaduje znalosti**,
    likvidita nízká

-   **Investovat do podnikání**

**3.Jak řešit opakovaný deficit domácího rozpočtu**

A)  **snížením výdajů** :

-   krátkodobě, zejm. snížit kontrolovatelné výdaje

-   dlouhodobě, zejm. ovlivnit pevné výdaje a výdaje vyplývající z
    osobních pasiv

B)  **zvýšení příjmů** :

-   krátkodobě, např. brigáda,vedlejší pracovní úvazek, prodej
    majetku,...

-   dlouhodobě, např. lépe placené zaměstnání, zahájit podnikání,
    pronájem majetku,...

Dobré a špatné dluhy

-   **Špatný dluh** (úvěr) :

    -   bereme si jej na zbytné věci

    -   bereme si jej na věci, které po splacení dluhu zestárnou,
        > neslouží svému účelu (dovolená, dárky na splátky,...)

    -   pokud si nejsme jisti, že jsme schopni splácet celou dobu

    -   pokud si nezjistíme, že si půjčujeme za přiměřeně vhodných
        > podmínek (ne za vyšší než obvyklou úrokovou sazbu)

-   **Předlužení** = dlužník/domácnost nemůže splácet (x při sjednání
    úvěru si ponecháme rezervu, pojištění proti neschopnosti splácet
    úvěr = vyšší splátky)

-   **Při nesplácení úvěru** :

    -   **Upomínky** (sankční úroky, dohoda s bankou -- odložení nebo
        > snížení splátky, změna délky úvěru)

    -   **Vymáhání**

        -   A\) zajištěný dluh -- propadne majetek, který byl dán do zástavy, při
            > pojištění proti nesplácení uhradí pojišťovna

        -   B\) nezajištěný dluh -- 1.věřitel může prodat vymahací agentuře 2.vymáhá
            > soudně

    -   **Exekuce** -- pokud dluh není splacen ani po soudním nařízení
        > (obstavení účtů,zabavení majetku)

pozn.: osobní bankrot,soudní rozhodnutí,musí splatit alespoň 30% z
každého dluhu,5 let splácení,zbytek dluhu je prominut
