---
title: 6_ Banky
---
Banky
-----

-   Poskytují finanční služby

-   Soukromé nebo státní instituce

-   ČR - **dvouúrovňový bankovní systém :**

A)  ČNB (fce, nezávislost, ...; cíl není zisk)

B)  obchodní banky (Komerční banka, ČSOB, Wüstenrot, Air Bank,
    > Raiffeinsenbank, UniCreditBank, Fio banka, Equa bank, ...)

ČNB
---

-   Dohled nad celým peněžním systémem

-   Emituje peníze

-   Banka bank (nastavuje povinou výši rezerv komerčních bank,poskytuje
    jim úvěry za tzv. diskontní sazbu=úroková míra,drží část jejich
    peněz)

-   Reguluje množství peněz v oběhu (vydáváním nových peněz nebo nákupem
    státních dluhopisů)

-   Reguluje měnový kurz (skupování domácí nebo zahraniční měny)

-   Vede účet vlády

-   Zajišťuje příjmy a výdaje státního rozpočtu

-   Sedmičlenná bankovní rada

-   Sídlo v Praze Na Příkopech (bývalá prvorepubliková budova
    Živnostenské banky) + regionální pobočky

Komerční banky
--------------

-   Instituce,které shromažďují volné peněžní prostředky od drobných či
    velkých střadatelů (obvykle za to nabízejí úrok), většinu těchto
    prostředků nabízejí k zapůjčení za úrok (jednotlivcům,firmám,státním
    institucím)

-   Po roce 2008 jsou banky povinny držet určitou výši minimálních
    rezerv a určitý objem vlastního kapitálu

-   Banky jsou povinné řádně vysvětlit ústně i písemně za jakých
    podmínek poskytuje finanční službu

-   Spotřebitel má právo do 14 dnů odstoupit od smlouvy o finanční
    službě

Činnost komerčních bank
-----------------------

-   A\) Aktivní operace -- poskytování úvěrů za úrok, banka tak vytváří
    aktiva, to co jí přinese zisk v budoucnosti

-   B\) Pasivní operace -- přijímání vkladů, na kterých je v případě
    dlouhodobějšího uložení poskytován výhodnější úrok, bance tak vznikají
    pasiva === příjmy z aktiv by měly převážit nad ztrátami z pasiv

-   C\) Služby -- zprostředkování platebního styku, prodej a nákup valut,
    vedení účtů za poplatek, depozitní a poradenské služby

Bankovní účet
-------------

-   Vklady, úvěry, bezhotovostní platby

-   -   bezhotovostní platba :

-   číslo účtu, kód banky, variabilní symbol = odlišení jednotlivých
    plateb, konstantní symbol = druh platby, specifický symbol =
    nepovinný

-   -   platební karty (PIN, podpis, bezkontaktní, ....)

-   internetové bankovnictví

Bankovní vklady
---------------

-   **Vklady na běžných účtech** -- neslouží k ukládání peněz, ale k
    provádění platebního styku,výše vkladů kolísá,pro banku to není
    stabilní zdroj financí,banka poskytuje zákazníkům nízké úrokové
    sazby

-   **Vklady na spořících účtech** -- libovolné ukládání peněz za vyšší
    úrok,ale disponování s penězi bývá omezeno (výpovědní lhůta,nelze
    použít k běžným platbám,...) x sankce v podobě poplatku za předčasný
    výběr

-   **Termínované vklady** -- sjednané na pevně stanovenou částku a na
    určitou dobu, poté vklad končí a peníze musíme vybrat, ale lze vklad
    obnovit,předčasné ukončení vkladu = sankční poplatek

-   Pozn.:

-   vklady jsou ze zákona pojištěny, v případě krachu banky klient
    dostane náhradu ve výši 100% vkladu (od Fondu pojištění vkladů),
    max. 100 000 EUR

Ceny produktů
-------------

-   **Úrok** = částka, kterou buď dostaneme, nebo zaplatíme

-   **Úroková sazba** (míra) = vyjadřuje v procentech, jakou část z
    uložené nebo půjčené částky bude úrok činit

    -   Na celkovou výši úrokových sazeb má značný vliv inflace, při
        > růstu inflace banky zvyšují úrokové sazby úvěrů i vkladů, aby
        > se hodnota úspor nesnižovala a nedocházelo tak k vybírání
        > úspor

    -   Úrokové sazby mohou být :

        -   pevné = sazba z vkladu nebo úvěru je stále stejná

        -   -   pohyblivá = úrokové sazby platí jen určitou dobu a banka
            > si ponechává právo je měnit (vzroste inflace, zesílí
            > konkurence, ...)

-   **RPSN** = roční procentní sazba nákladů, zahrnuje všechny poplatky
    spojené s úvěrem

Další produkty k ukládání peněz
-------------------------------

### Stavební spoření

-   Stavební spoření

-   Spoření se státním příspěvkem, max. státní příspěvek 20 000,- ročně,
    min. doba spoření = 6 let

-   Při uzavírání smlouvy se stanoví cílová částka (naše úložky+výnosy z
    nich+nárok na úvěr)

-   Během spoření může spořitelna poskytnout úvěr na bydlení, stavební
    úpravy či jiné účely související s bydlením

-   Po uplynutí 6 let lze peníze vyzvednout a použít na cokoli x dříve =
    přijdeme o státní podporu

-   Úspory jsou pojištěny

-   Českomoravská stavební spořitelna (s liškou), stavební spořitelna
    České spořitelny (s buřinkou), Modrá pyramida, Raiffeisen, HYPO,
    Wüstenrot, ...

### Doplňkové penzijní pojištění

-   Penzijní fondy shromažďují finanční prostředky od účastníků a od
    státu, spravují je za účelem jejich zhodnocení

-   Pravidelné úložky, ke kterým stát poskytuje příspěvek, od 90-230 Kč
    při měsíční úložce 300-1 000 Kč

-   Při úložce vyšší než 12 000 Kč za rok lze část spoření odečíst od
    základu daně a snížit si tak daň

-   Na spoření může přispívat i zaměstnavatel

Cenné papíry
------------

-   = vyjadřují nárok jejich vlastníka na peněžní hodnotu (pohledávka)
    nebo na majetek či zboží za předem stanovených podmínek

-   **Emitent** = ten, kdo vydává cenný papír

-   **Emise** = uvedení cenného papíru na trh, jedná se o jednorázové a
    hromadné vydání cenného papíru (např. firma emituje XY milionů kusů
    svých akcií),

-   cílem emise je prodat cenné papíry investorům a získat tak finanční
    prostředky

-   3 základní typy cenných papírů :

-   a\) **zbožový cenný papír** = dokládá, komu patří určité zboží, kdo má
    právo s ním disponovat, nejsou předmětem investic, jsou svou podstatou
    majetkové (př. lodní listy, které vyjadřují část nákladu zboží na
    lodi,skladní listy, ...)

-   **b) Cenný papír peněžního trhu** -- představují krátkodobou
    pohledávku se splatností do 1 roku (př. směnky a šeky, dále
    pokladniční poukázky ČNB, státní pokladniční poukázky Ministerstva
    financí ČR, depozitní certifikáty emitované komerčními bankami)

-   c\) **Cenný papír kapitálového trhu** -- dlouhodobé cenné papíry s
    platností delší než 1 rok :

    -   majetkové = zajišťují jejich nositeli práva na část majetku
        > jejich vystavovatele (akcie, podílové listy)

    -   úvěrové = představují finanční dluh (dluhopisy, hypoteční listy)

-   -   Pozn.:

    -   prodejem a nákupem cenný papír mění svého majitele, obchodování
        > probíhá na trhu s cennými papíry (burza cenných papírů) ,
        > dohlíží ČNB

### A) Krátkodobé cenné papíry

-   **Směnka** = potvrzuje závazek dlužníka vůči věřiteli, lze ji napsat
    na jakýkoliv papír, převodem směnky na nového majitele lze uhradit
    existující dluh = slouží jako platební nástroj

-   **Šek** = platební nástroj (obliba v USA), výstavce šeku dává šekem
    své bance příkaz, aby vyplatila (hotovostně nebo na účet) z jeho
    účtu peníze osobě, která jej předloží, šek musí být krytý

-   **Státní pokladniční poukázky** = vydávána Ministerstvem financí
    ČR,stát získává prostředky na pokrytí krátkodobého schodku státního
    rozpočtu (př. povodně),vysoká nominální hodnota,za dluh ručí
    stát=nízké riziko,výnos jistý, ale nízký

-   **Depozitní certifikáty** = vydávají komerční banky ve snaze získat
    finanční prostředky,vyšší nominální hodnota,výhodou je vyšší úrok

### B) Dlouhodobé cenné papíry

-   **Akcie** = nárok na část majetku podniku,podniky emitují nejčastěji
    akcie na počátku své činnosti (zdroj kapitálu pro vybudování
    podniku) nebo během podnikání (=chtějí rozšířit výrobní kapacitu),

-   Akcie kmenové=podíl na řízení podniku x akcie prioritní=není
    hlasovací právo,vyšší dividenda,

-   Akcie podniku obchodovatelné nebo neobchodovatelné,

-   Nominální hodnota akcie=peněžní hodnota přiřazená jedné akcii

-   Emisní kurz=cena, za niž podnik prodává akcii

-   Emisní ážio=rozdíl mezi nominální hodnotou a emisním kurzem

-   Hodnota akcie se během obchodování na kapitálovém trhu může měnit
    (nabídka x poptávka = tržní cena = kurz akcie)

<!-- -->

-   **Dluhopisy** (=obligace) = úvěrové cenné papíry, emitovány
    nejrůznějšími subjekty,závazek dlužníka vůči věřiteli = slib, že
    dluh bude ve stanoveném termínu splacen včetně výnosu

-   -   Dle emitenta dluhopisy dělíme :

    -   **státní**=vydává Ministerstvo financí ČR,na trh je uvádí ČNB
        > formou aukce,jistá a stabilní investice s poměrně nízkým
        > úrokem

    -   **komunální**=se souhlasem Ministerstva financí ČR vydává
        > samosprávný celek (financování infrastruktury,kanalizace,...)

    -   **bankovní**=vydávají banky s cílem získat finanční prostředky
        > pro své klienty

    -   **podnikové**=vydávají firmy pro zajištění dlouhodobých investic

-   **Kupon**=určuje výši výnosu z dluhopisu

-   S dluhopisem se může obchodovat na kapitálovém trhu za jeho tržní
    hodnotu

-   ČNB dohlíží nad trhem s dluhopisy a povoluje jejich emise

-   Zejména drobní investoři mohou investovat do dluhopisů
    prostřednictvím **dluhopisových fondů**, které mají svá portfolia
    složena z různě rizikových dluhopisů

Burzy
-----

-   = organizace, které převzaly záštitu nad trhem s komoditami a
    cennými papíry za přesně vymezených podmínek

-   Burzy dělíme na:

    -   **komoditní** (zboží)= obchod s komoditami
        > (materiály,suroviny,potraviny,...),zboží se zde fyzicky
        > nevyskytuje,předmětem může být i komodita dosud neexistující
        > (zatím nevypěstovaná rýže), v ČR např. Komoditní burza Praha

    -   **devizová**=obchod s národními měnami (např. dolar)

    -   **cenných papírů**=obchod s akciemi,dluhopisy atd.,Burza cenných
        > papírů Praha

-   -   **Burzy** ovlivňují ekonomiku -- stanovují se zde ceny komodit,
    devizové kurzy a cena kapitálu

-   Nad burzou s cennými papíry dohlíží ČNB, nad komoditními burzami
    Ministerstvo zemědělství ČR a Ministerstvo obchodu a průmyslu ČR

-   Na burze se mohou pohybovat jen ty subjekty, které splňují náročné
    požadavky

-   Obyčejní lidé využívají služeb zprostředkovatelů (banka,makléřská
    firma,...), kterým svěří peníze a dají pokyn k nákupu

-   Zprostředkovatelé za nás obchodují prezenčně nebo elektronicky

-   **Burzovní makléř** informuje své klienty,doporučuje
    postup,realizuje dohodnuté obchody,vede o nich záznamy,podmínkou je
    odborná makléřská zkouška,ekonomické vzdělání SŠ nebo VŠ,etické a
    zodpovědné chování

Nedostatek finančních prostředků
--------------------------------

-   **Kreditní karta** = peníze navíc, úvěrové peníze, pro krátkou dobu,
    vysoký úrok (20%)

-   **Kontokorentní úvěr** = majitel běžného účtu čerpá „do mínusu"
    (více než má na účtu), za poplatek, za přečerpání pokuta

-   **Hypoteční úvěr** = zajištěn nemovitostí, až na 30 let

-   **Úvěr ze stavebního spoření** = financování nemovitosti,podmíněn
    výší úspory

-   **Spotřebitelský úvěr** v bance (též specializované úvěrové
    společnosti),na pořízení elektroniky,vybavení
    domácnosti,automobilu,...

-   **Splátková prodej**, splácíme zboží, které se stalo naším majetkem
    v okamžiku prodeje

-   **Leasing**, splácená věc (automobil) je až do okamžiku poslední
    splátky majetkem leasingové společnosti

-   **Účelové a neúčelové úvěry**

-   !!! **lichváři** !!!

-   -   -   
