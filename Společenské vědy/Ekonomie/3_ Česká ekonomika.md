---
title: 3_ Česká ekonomika
---
Česká ekonomika
===============

-   Po roce 1948 **centrálně řízený systém, pětiletky**

-   Orientace na **těžký průmysl** (diktováno SSSR) x první republika =
    spíše lehký

-   Těžba uhlí, ocelárny, zbrojařský průmysl

-   Zahraniční obchod orientován na **RVHP**

-   **Státní** (97%) **a družstevní vlastnictví** x soukromý sektor

-   Přebytkový rozpočet, zajištěno odbytiště, není nezaměstnanost, ...

-   **60. léta** = Ota Šik, pokus o ekonomickou reformu

-   1989 **transformace** ekonomiky

-   ekonomové : Valtr Komárek x Václav Klaus

-   **liberalizace cen** (uvolnění) = růst cen, inflace

-   **tržní** ekonomika, omez. zásahy státu

-   **liberalizace zahraničního obchodu** (x vysoká cla před 1989)

-   **soukromý sektor** (do 1989 97% státní vlastnictví)

-   devalvace České koruny

-   **1995 koruna plně konvertibilní** (= volně směnitelná)

-   **změna struktury ekonomiky** (těžký průmysl, zbrojařský průmysl x
    strojírenství, automobilový průmysl, služby)

-   **ztráta konkurence schopnosti** (doly, sklárny, textilní průmysl,
    ...)

-   **strukturální nezaměstnanost**, rekvalifikace

-   1995 ČR v OECD

Privatizace, restituce
----------------------

### **1.Malá privatizace** -- maloobchod, služby (aukce 1991-93)

zisk : 31 772 580 586 (zvláštní účet)

### **2.Velká privatizace** -- střední a větší podniky (aukce, veřejná soutěž, prodej předem vybranému kupci, převod na a.s.)

### **3.Restituce** (25.2.1948 -- 1.1.1990) vrácení podniků původním majitelům co komunisté sebrali

**Církevní restituce** (22.11.2012) -- navrácení majetku v hodnotě 75
mld. Kč, dalších 30 let vyplatit 59 mld. Kč za nevrácený majetek

### 4. Kuponová privatizace

-   **kuponová knížka** (1 000,-Kč) = 10 kuponů po 100 bodech

-   **DIK** = držitel investičních kuponů

-   *Tomáš Ježek , Václav Klaus*

-   kupony směnitelné za akcie

-   přímo **akcie nebo privatizační fond**

-   **2 vlny** (1992, 1994)

-   **tunelování** privatizačních fondů (Harvardské investiční
    fondy=Viktor Kožený=Bahamy, Motoinvest=Pavel Tykač, ... → využili
    toho) x **nebylo právně ošetřeno** !

Současná ekonomika ČR
---------------------

-   strojírenský a automobilový průmysl

-   nárůst % lidí v terciéru

-   ČR = menší otevřená ekonomika

-   ekonomika ČR závislá na větších (Německo)

-   Schengen (2007) : jednodušší vývoz i dovoz, 4 svobody, zaručena
    minimální kvalita

-   menší ochrana domácího trhu

-   2004 aktivní saldo

-   2006 ČR = velmi rozvinutou zemí

-   2013 intervence ČNB (27 CZK/EUR) - oslabení české koruny

-   přijetí eura = nejdříve 2020 (A.Babiš) ?

-   Geografické podmínky ČR :

-   \- vhodné pro **zemědělství**

-   \- zdroje **nerostných surovin** (uhlí na Ostravsku a v severních
    Čechách) podmiňují silný průmysl

-   Doprava :

-   -vývoz a dovoz mimo Evropu prostřednictvím **námořní dopravy**
    (obráběcí stroje do USA, kaučuk z Asie)

-   -doprava do přístavů a z přístavů a v republice v rámci **kamionové
    dopravy** == význam dobré dopravní infrastruktury

-   V ČR :

-   vysoký podíl **sekundárního sektoru** (roku 2011 cca 39% HDP), po
    Norsku v rámci Evropy nejvyšší podíl,firmy ze sekundéru patří ke
    klíčovým zaměstnavatelům, výrazně se podílejí na vývozu

-   **Primární sektor** cca 2% HDP, **terciární** (včetně kvartéru) 59%
    HDP

-   Rozhodujícím odvětvím sekundéru je **zpracovatelský** průmysl
    (výroba strojů, chemikálií) a výroba a rozvod **elektřiny**

<!-- -->

-   Největší firmy v ČR :

-   Škoda Auto, energetický podnik ČEZ a výrobce elektroniky Foxconn

-   Růst odvětví zaměřených na služby (terciární sektor) :

-   služby v domácnosti, ve vzdělání a dopravě (Student Agency)

-   Podpora vědy, výzkumu a technických inovací (kvartér) -- výroba
    nanovlákna

-   ČR = vyspělý stát, roku 2011 český HDP na osobu 25 900 dolarů v
    paritě kupní síly (cca 362 949 Kč), což představuje 80% průměru
    EU, 52. místo ve světě

-   ČR vyváží především stroje a dopravní prostředky, nejvíce exportu
    směřovalo do zemí EU (83%), do SRN 32,1%

-   V rámci EU = jednotný vnitřní trh, nejsou cla

-   Vůči třetím zemím (mimo EU) uplatňuje EU jednotnou celní politiku,
    závaznou i pro ČR (cla, množstevní kvóty, technické překážky, ...) x
    každý stát má vlastní proexportní politiku (nesmí porušit směrnice
    EU -- hospodářskou soutěž)

České ekonomické myšlení
------------------------

-   počátky z dob R-U, české země průmyslově vyspělé x zastaralé výrobní
    techniky

-   **František Cyril Kampelík**, lékař, spisovatel, buditel --
    **kampeličky**=vesnická svépomocná úvěrová družstva

### Albín Bráf

-   ekonom R-U, politik, novinář, profesor UK

-   člen panské sněmovny

-   ministr zemědělství

-   staročech

-   spoluzakladatel **Zemské banky**, České akademie věd a umění

-   **čeští spotřebitelé mají nakupovat české výrobky** (svůj k svému)

### JUDr. Karel Engliš

-   politik a ekonom

-   **prvorepublikový ministr financí**, první rektor MU, po 2.sv.v.
    rektor KU

-   podílel se na měnové reformě ČSR

-   **guvernér Národní banky Československé**

-   člen národně-demokratické strany

### JUDr. Josef Kaizl

-   ekonom, profesor, politik

-   stoupenec liberalismu

-   staročech, mladočech

-   poslanec ŘR, **ministr financí Předlitavska **

-   odpůrce marxismu

### JUDr. Jaroslav Preiss

-   ekonom, bankéř, poslanec, profesor KU

-   **ředitel Živnostenské banky**

-   za 1. sv.v. amnestován Karlem I.

-   za protektorátu odmítl spolupracovat s nacisty = pokuta 5 mil.
    (uhradil)

### JUDr. Alois Rašín

-   český a československý politik a ekonom

-   jeden z Mužů 28. října, člen Čsl. národní demokracie

-   **první ministr financí ČSR**, **měnová reforma** - snaha
    stabilizace

-   atentát 1923 → zemřel

### Po 2. světová válka

-   **po roce 1948** -- komunisté, řízená ekonomika, ...

-   **60. léta: Ota Šik** -- pokus o „třetí cestu" = mezi kapitalismem a
    socialismem

-   ekonom a politik Pražského jara

-   emigrace do Švýcarska = profesor ekonomie v Sankt Gallenu

### Čeští ekonomové

-   Vladimír Dlouhý - ,

-   Tomáš Ježek,

-   **Václav Klaus**,

-   Valtr Komárek,

-   **Jiří Rusnok** - guvernér České národní banky,

-   Miroslav Singer,

-   Zdeněk Tůma,

-   Josef Tošovský,

-   Jan Švejnar,

-   Miloš Zeman
