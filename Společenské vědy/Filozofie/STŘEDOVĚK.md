---
title: STŘEDOVĚK
---
[[Antika]{.underline}](https://drive.google.com/open?id=1Is4r3bCpHEfy3GkhyGvVVO7YYIBzGqPMBC9fHIOCUfw)
Předchozí \| Další

STŘEDOVĚK

křesťanství: Ježíš Kristus, mesiáš ?? help me :( cuz dis is impossible
to take notes

-   rychle se šířilo díky jeho učňům

-   dobří budou odměněni a zlí potrestání v posmrtném životě

-   se smrtí život nekončí

-   blablablablabla

-   → církev a zákony

Nový zákon:

-   ze 27 knih v řečtině

-   ze 4 základních částí

    -   evangelia Matouš Marek Jan

    -   skutky apoštolů

    -   epištoly

    -   apokalypsa čili zjevení Janovo

Hieronymus

-   přeložil do latiny = **Vulgata**

<!-- -->

-   pronásledování římany

313 edikt milánský - zlom, bude tolerovat křesťanům svobodně víru

318-381 spor o dogmata - trojjedinost boha, JK společný s bohem

325 ekumenický koncil v Nikai - konstantin I. - Arius (JK není božské
povahy = prostředník mezi člověkem a bohem)× **Athanasius** (syn boží je
stejné podstaty jako bůh otec; vyhrála)

380 edikt Cunctos Populus, - Theodosius, křesŤanství = státní
náboženství

381 2. ekumenický koncil v Konstantinopolu - ustanovení svaté trojice

451 chalkedonský koncil 4. koncil - přiznal plné božství a lidství JK

-   při vzniku nebezpečné pro křesŤanství - **ariánství** a
    **gnosticismus** a **manicheismus** (zakladatel byl ukřižován)

    -   tvrdil že existují 2 světy - říše světla - otec světla a z této
        > říše sestoupil JK × říše temna - otec temna = Jahve (židovský
        > Bůh); odmítá starý zákon; člověk se má podle ?? vykoupit sám

 

Středověká filosofie
--------------------

1\. Patristika (počátek křesťanství -- 8. stol.):

a\) apologetika

b\) dogmatika = formování a zpřesňování křesťanského učení

c\) systematika = první křesťanské systémy, např. učení sv Augustina

2\. Scholastika (8.-14/15. stol.):

a\) raná (9.-12. stol.)

b\) vrcholná (13. stol.)

c\) pozdní (14.-15. stol.)

### Patristika

-   patres = otec, křesťanští otcové co se podílí na vznik

-   → přesun do Alexandrie - naráží na pohanskou

-   **Apologetové** = obránci křesťanství

-   **Apologetika** = literární **obrana** křesťanství, vyvraceli
    pomluvy, vysvětlovali, že nejsou nebezpeční pro řím atd.

-   ##### **Justin Mučedník** - křesťan v plášti filosofa, na základě udání zabit

    -   credo quia absurdum est - já věřím protože je to
        > absurdní/protismyslné/nevysvětlitelné

    -   ostrý, ironický

<!-- -->

-   Alexandrijská škola

-   Klemens Alexandrijský

-   Origenes

-   Dionysios Alexandrijský

-   pokouší se ???

#### Vrcholná patristika řecká

-   ##### **Škola kapadocká**:

-   Basileos z Kaisareie (330-379

-   Grégorios z Nazianzu (329-390) - konstantinický patriarcha

-   **Grégorios z Nyssy** (335-394) - Řehoř Nysejský

-   -   ##### **Škola antiochijská** :

-   Jan Zlatoústý-Chrýsostomos (345-407) - významný kazatel, psal
    komentáře k biblickým textům, konstantinickým patriarchou

#### Vrcholná patristika latinská

-   ##### **Ambrosius** (+397) - učitel sv. Augustina, vystupoval proti ariánství

-   ##### Hieronymus (347-420) 

    -   lat. překlad bible = Vulgata

    -   dožívá v betlémě

-   ##### Lev I. Veliký (asi 400-461) - římský papež, posílil autoritu papežského státu

-   ##### **Aurelius Augustinus** (354-430)

    -   život: narodil se na území Alžírska, matka křesťanka

    -   Hippo Regius - dožil zde a stal se křesťanským biskupem

    -   kartáginský buřič

    -   seznámil se s Abroziem → stal se křesťanem

    -   dílo:

    -   ***Vyznání*** (Confessiones) = jediná dlouhá modlitba - 13 knih

    -   „Stvořil jsi nás pro sebe a neklidné je naše srdce, dokud
        > nespočne v Tobě"

    -   ***O Trojici*** (De Trinitate)

    -   jestli není trest za to že se vzdal svých bohů a přijal
        > křesťanství

    -   ***O obci Boží*** (De cevitate Dei)

    -   otázka lidské svobody - lidé odsouzeni k dědičnému hřešení kvůli
        > Adamovi, Bůh, ale vykupuje ty, co dopředu mají předurčeno

    -   **o původu světa** = vše vytvořil Bůh, hotové věci či zárodky,
        > současně se světem vznikl i čas

    -   o původu zla = zlo nepřítomností dobra

    -   2 světy

        -   obec pozemská

        -   obec boží

### Pozdní patristika (4.-5.století):

-   ##### Dyonýsios Areopagita

-   Učitelé patristiky:

-   Cassiodorus (5., 6.st.)

-   Isidor ze Sevilly (kolem 600)

-   Beda Venerabilis,

-   Jan z Damašku (kolem 700)

-   ##### Alcuin z Yorku (kolem 800)

Scholastika
-----------

-   patřící škole

-   učeným jazykem latina

-   **Alcuin z Yorku**

-   7 svob. umění základem

    -   trivium = gram., rét., dialekt

    -   quadrivium = aritmetika, astronomie, geometrie, musica

-   filosofie složkou teologie = vysvětluje, objasňuje, přibližuje
    náboženství

-   pro a proti, ano a ne

-   **spor o univerzálie**

### Raná Scholastika (8.-12. století)

-   **spor o univerzálie**:

    -   obecné pojmy / obceniny

a)  **Realisté**

-   obecné pojmy jsou před věcmi

-   **univerzálie** = platónovy ideje = **jediné reálně existují**

-   podle obecnin jsou tvořeny konkrétní věci

-   **v božské mysli jsou modely** - například koně, člověka

-   **nejprve obecniny něco jako modely → skutečné věci**

a)  **Nominalisté**

-   nejprve konkrétní věci → potom pojmy - lidé pojmenovali věci

-   obecniny jenom jména (nomina)

Realisté:

-   Jan Scotus Eriugena (810-877)

    -   prolbémy s církvi, zdůraznoval význam rozumu, × rozešel se s
        > církví

-   **Anselm** z Canterbury(1033-1109)

    -   ,,druhý otec scholastiky", pocházel ze vznešené rodiny, střední
        > věk v franz. klášterech, **požadoval aby veškeré myšlení bylo
        > podřadné víře**; ,,**Věřím abych pochopil**"; nejvýšší
        > autorita = bible, rozum nemůže proti této autoritě odporovat

-   Vilém z Champeaux 1070-1121

    -   radikalista; existuje bělost i kdyby neexistovala bílá,
        > člověčenstvo,

Nominalisté:

-   Jean Roscellinus z Compiegne

    -   svět s skládá z kontrétních věcí; pojmy jsou vymyšlené

    -   neexistuje bělost jen bílá, neexistuje člověčenstvo

    -   tvrdil že neexistuje trojjediný Bůh, ale 3 osoby → poškodil ten
        > směr

Kompromisní řešení:

-   **Pierre Abelard**

    -   tvůrce scholastické metody

    -   zamiloval se bezhlavě do neteře pař. panovníka (**Heloisa**)

    -   byl jejím učitelem → přivedl ji do jiného stavu → utekl s ní →
        > oženili se jinde → skrýval to aby se nemusel vzdát ? →
        > panovník najal někoho a zbavili ho mužství

    -   → stali se řeholníky

    -   ,,Snažím se pochopil, abych mohl věřit" - **rozum vede k víře**

    -   **úmysl co jsme zamýšleli \> skutek**

    -   nesouhlasí s obviňováním židů za zabití ježíše

    -   není možné aby existovaly pojmy nebo veci = **univerzálie jsou
        > ve věcech obsaženy**

    -   ***Historie mých nehod*** - autobiografická záležitost, líčí
        > život těchto dvou zamilovaných lidí

###  Vrcholná scholastika (13. stol.)

-   svět ovládl **Aristoteles**, mnoho překladů - zakázán nejprve kvůli
    novoplatosnkým spisům ← dostal se do Evropy, kvůli křížovým výpravam

-   **sumy** = pokus o shrnutí veškeré informace do knih

-   filosofie pěstována na univerzitách - Oxfordská, Boloňská

-   filosofové ze 2 řádů:

    -   františkáni = řád menších bratří

        -   František z Assisi (1182-1226)

            -   z apeninského poloostrova,

            -   četná vidění a sny v zajatí → služba Ježíší Krista

            -   viděl jak církev žije v bohatství → přijel dom

            -   **chudoba, společný život**, **láska** (člověka k
                > člověku; k přírodě) **život podle evangelia**

        -   Jan Fidanza-Bonaventura

            -   vrací se k sv. Augustinovi

    -   dominikáni

        -   sv. Dominik-Domingo de Guzmán

            -   řád bratří kazatelů - angažovali se v kontaku s věřícími

            -   žebravý řád, měli podporu papeže

            -   celoživotní studium → inkvizitoři

        -   Albert Veliký

            -   13\. stol., z německého prostředí, studoval na it. univerzitách

            -   byl oblíbený → povolán do paříže, přednášel pod širým
                > nebem

            -   napsal obrovské dílo 21 svazků

            -   všestranný

            -   komentáře k Aristotelovým dílům

            -   ***Suma teologická***

        -   **Tomáš Akvinský**

            -   narodil se v Itálii → výchova u Benediktinů

            -   klášter Monte Cassino

            -   → dominikánem → nelíbilo se bratrům

            -   utekl z věznění bratrů do Paříže

            -   → Kolín nad Rýnem

            -   → 2. pobyt v paříži 1262 - **vrcholné období** -
                > nejdůležitějším učitelem scholastiky

            -   jeho dílo

                -   ***Summa teologická*** - dokončil

                -   ***Summa proti pohanům***

                -   ***Quaestiones***

                -   ***Výklad písma svatého***

            -   jeho učení **tomismus**

            -   **papež ho prohlásil za ?? Učitele církve**

            -   **Fosáhova**

            -   gnoseologii

                -   objektivní a pravdivé poznání existuje

                -   mezi rozumem a vírou není rozpor - jedině Bůh má
                    > pravdu

                -   2\. způsoby nacházení pravdy

            -   ontologie

                -   inspiroval Aristotelem

                -   nejvyšší formou je Bůh

                -   forma a látka

            -   **důkazy boží existence**

                -   všechno se **pohybuje** = musí existovat 1. hybatel

                -   všechno má svojí **příčinu** = příčinou je bůh

                -   věci mohou **být** a nemusí → musí existovat 1.
                    > nutnost = Bůh

                -   existují stupně **dokonalosti** → nejvyšším je Bůh

                -   na světě má všechno svůj **smysl** → Bůh to řídí

            -   duše nesmrtelná, nezničitelná

                -   projevy duše = poznání a vůle

                -   poznání = smyslové poznání

                -   musí existovat svobodná vůle

            -   4 antické

                -   rozumnost

                -   statečnost

                -   spravedlnost

                -   uměřenost

            -   \+ 3 ctnosti křesťanské

                -   víra

                -   láska

                -   naděje

            -   rozum má velký význam

            -   ve státu musí být někdo kdo ji řídí

            -   lid. spol. stvořena Bohem = stát boží cosi?

            -   zákon je nezbytný

                -   nejvyšší je boží zákon

                -   musí být spravedlivé a proveditelné

            -   světská moc se musí podřídit

### Pozdní scholastika 14.-15.století

-   Roger Bacon 1214-1294

-   dílo

    -   ***Větší dílo***

    -   ***Menší dílo***

    -   ***Třetí dílo***

-   Jan Duns Scotus

    -   pochází ze skotska z obce Duns

    -   skotský mnich, františkán

    -   doctor marianus - teorie o neposkvrněném početí Marie

    -   našemu myšlení je nadřazena vůně

    -   poznání intuitivní (ne rozumové)

-   William Ockham 1300-1350

    -   doctor invincibilis - neporazitelný

    -   důraz na logiku

    -   řešil vztah filosofie a teologie

        -   oddělil je

    -   poslední scholastik

    -   angličan františkán

    -   zabývá se poměrem světské a papežské moci

    -   kritizoval mocenskou politiku
