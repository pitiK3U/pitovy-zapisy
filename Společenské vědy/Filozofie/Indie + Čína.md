---
title: Indie + Čína
---
Předchozí \| Další
[[Antika]{.underline}](https://drive.google.com/open?id=1Is4r3bCpHEfy3GkhyGvVVO7YYIBzGqPMBC9fHIOCUfw)

Praktická filozofie / dějiny
============================

### Logika

= správné uvažování, usuzování

### Estetika

= hodnocení krásy a ošklivosti, vynášení

Indická filosofie
-----------------

-   zánik civilizace způsobili **Árjové** - filosofie je spojená s
    jejich příchodem

-   **Védy** - náboženské texty, obrovské spisy, 1500-1000.př.n.l. -
    polyteismus

-   **Brahmány** - varny (kasty), náboženské texty, **brahma × átman**,
    vznik privilegovaných vrstev, rozdělení společnosti, nejvyšší
    vrstva - brahmani, bojovníci, výrobci, 1000 př.n.l.,

-   **Upanišady** - nábožensko-filosofické texty, pesimistické, celý
    život je strast, podstatou je, aby átman splynul s brahmou, kladou
    důraz na askezi - vzdání se

    -   **samsára** -reinkarnace, koloběh nových životů, cílem je aby se
        > koloběh zastavil

    -   **mokša** - vysvobození z věčného koloběhu

    -   **karman** - nástroj jak dosáhnout vysvobození, vše v našem
        > životě se sčítá a až naplníme určitou míru skutků

    -   **brahma** -celosvětový duchovní princip

    -   **átman** - duchovní pocit,

-   Purány - komentáře ke starým textům

-   Sútry - shrnutí

Neortodoxní systémy (nástika)
-----------------------------

-   ### džinismus

    -   zakladatelem **Vardhamána Mahávíra** - princ, z bohaté rodiny,
        > rodiče vstoupili do sekty, sebevražda = vysvobození,
        > vyhladověli,

    -   džinisté

    -   vykupitelé - měli se zjevovat džinové na zemi po určité době

    -   džaina = vítěz nad koloběhem

    -   asi 1000 let po jeho smrti bylo sepsáno jeho učení

    -   rozpadli se na

        -   švétambarové - bílá roucha

        -   digambarové - oděni vzduchem = chodí nazí

    -   **džíva** - nekonečný počet individuálních duší - duševní
        > princip

    -   **adžíva** - hmotný princip

    -   neporušenou duši??

    -   asketický způsob života - zříci se světské rozkoše

    -   nelhat, nebrat co není dáváno

    -   ahinsá - cesta nenásilí, vegetariáni, cedí vodou, rouška aby
        > nesnědli hmyz

-   ### Buddhismus

    -   **Siddharta Gautama** - nejsou o něm informace - příběhy,
        > legendy

        -   matka měla sen před narozením, že ji unesli 4 králové \--

        -   sen vyložili tak, že jestli bude vychováván v království tak
            > bude králem a když odejde tak sejme závoj nevědomosti

        -   král ho vychovával v přepychu → princ ale vyjížděl na
            > projížďky → 1. - stáří → 2. - nemoci a chudoba → 3. smrt

        -   ve 29 letech odešel měl manželku i syna

        -   chodil jako asketa, hledal jak se vysvobodit z

        -   sedl si pod fíkovník a tam uzřel lidské koloběh → přišel jak
            > zastavit lidský koloběh

        -   

    -   osvícený, probuzený

    -   4 pravdy

        -   všechno naše žití je strast

        -   tato příčina že prahneme po životě → když odstraníme touhu
            > po životě, tak by mělo přijít i

        -   utrpení je možné ukončit

        -   4\. pravda \-- 8 dílná stezka - jak se zbavit toho věčného

    -   sbírky posvátných textů - **t**(**r**)**ipitaka** - texty
        > budhova učení

    -   **ateistické náboženství**

    -   nesobecké jednání bude přinášet štěstí

    -   je potřeba přestat lpít na našem životě

    -   **Nirvana** - v tomto stavu se vyprostí, stav pozitivní
        > blaženosti

    -   Buddhova etika

        -   nezabíjej živé

        -   neber co ti nedávají

        -   nemluv nepravdu

        -   nepij opojné nápoje

        -   nebuď necudný

#### Směry buddhismu

1.  Hínajína (malý vůz, cesta) -

    -   neexistuje bůh, ani ,,já",

    -   mokša je moje individuální záležitost,

    -   théraváda, menšina

2.  Mahájána (velký vůz, cesta),

    -   Buddha - jako božská postava, bódhisattvové - dobrovolně se
        > zřekli Nirvany a zůstanou na zemi, pokračují v znovuzrození
        > aby pomohli ostatním, 60% buddhistů

3.  Tantrajána

    -   tantry - tajné nauky, lamaismus - (lama - mniši, Velcí lámové -
        > Dalai Lama - 14., nežije v Tibetu), Tibetská kniha mrtvých - o
        > převtělování

4.  Zen-buddhismus

    -   společné meditace

    -   mistr - Guru, nejsou vázáni buddhismem - když musí tak zabijí
        > živého tvora

    -   fyzická práce

Čínská filosofie
================

-   dějiny lidské a přírodní dějiny jsou propojeny

-   protikladné energie, vše je na nich postavené, nemohou být
    samostatné:

    -   **jin** - přerušovaná čára, ženský, smrt, noc, tma, země

> ×

-   **jang** - nepřerušená čára, symbol mužský, světlo, pohyb, den,
    > život, nebe

<!-- -->

-   ***I-ťing*** - kniha proměn, konfucius je znovu vydal a opatřil ,
    nejstarší dokument čínského filosofického myšlení.

    -   užívána na věštění = **orákulum**

-   vše nahodilé, vše se děje právě teď a nyní = **koincidence**

(pozn.: Periodizace:
--------------------

-   starověká čínská filozofie (6-2. století př.n.l.)

-   středověká čínská filozofie (2. století př.n.l. - 1000 n.l.)

-   neokonfucianismus (1000 -současnost)

)

Konfucius (551/2-479 př.n.l.)
-----------------------------

-   Kchung-fu-c´ - ,,mistr z rodu Kchung"

-   ze státečku Lu

-   škola - učili se zde chlapci

-   odmítal se stát úředníkem → nakonec se stal ministrem spravedlnosti

-   dobrovolně odešel do vyhnanství → později se vrátil

-   shromáždil všechny významné projevy kultury

-   **5 kanonických knih**:

    -   I-ťing (**Kniha proměn** - princip Jin a Jang)

    -   Š-ting (**Kniha písní** - lidové písně)

    -   Šu-ťing (**Kniha listin** - důležité dokumenty, zákony, právní
        > normy)

    -   Čchun-čchiou (**Letopisy jar a podzimů** - dějiny státu Lu)

    -   Li-ťing (**Kniha ritů** - kniha zvyků, vznikla po Konfuciově
        > smrti)

-   další 4 klasické knihy:

    -   **Hovory** (Lun-ju)

    -   Veliké učení

    -   učení středu

    -   Meng-c´

-   vytváří normy chování, snažil se vést ke sebe zdokonalení,

-   **agnosticismus** - co je za naším světem není poznatelné

-   člověk hledá správnou cestou :

    -   vzdělání

    -   úctu k tradicím, předkům

    -   poctivost, upřímnost

    -   sebevzdělávání se

    -   panovník má vést lid svým příkladem

    -   kladl důraz na výchovu

    -   **šu** - schopnost vcítit se do toho druhého - empatie

Mencius
-------

-   Nejvýznamnější učeň konfucia

-   pozitivně laděné myšlenky

-   povaha člověka je v zásadě dobrá

-   proti válce

-   Zdůrazňoval také, že nejdůležitější složkou každého státu je lid,
    nikoliv jeho vládce. Vládce, který neplní své povinnosti, je lid
    dokonce oprávněn sesadit a popravit.

Leo-c´(600 př.n.l.-?)
---------------------

-   starý mistr, zakladatel taoismu

-   *Tao-te-ťing* - nevíme jestli napsal, kniha o cestě a ctnosti

    -   Tao - prazáklad světa, zákon nebes, cesta, rozum

    -   čchi - energie, která vychází z Tao, díky čchi vznikají živé
        > věci

-   jsoucna pochází z Tao

-   vše se skládá z jin a jang

-   výsledek by měla být harmonie, rovnováha

-   etika: podobná křesťanské

    -   neoplácet zlobu zlému

    -   máme se vzdát pohanství

    -   žít v prostotě, v souladu s přírodou

    -   nemáme prahnout pro vzdělání a majetku

,,legisté", lex-zákon

-   objevují se v 3. st., při sjednocení číny

    -   
