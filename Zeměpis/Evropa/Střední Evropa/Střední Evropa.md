---
title: Střední Evropa
---
• střed Evropy -- 26 km severně od Vilniusu (Litva)

• dělení národností (jazykové):

• germánská (Německo, Švýcarsko, Rakousko, Lichtenštejnsko)

• slovanská (ČR, Slovensko, Polsko, Slovinsko)

• ugrofinské (Maďarsko)

• vliv Habsburků a císařského Německa na historický vývoj regionu

• změny na politické mapě střední Evropy po 1. sv. válce, 1945 a 1989
(rozpad Československa, )

1\. Vyhledej na mapě: Severoněmeckou nížinu, ostrov Rujanu,
Středoněmeckou vysočinu, Schwarzwald, Bavorský les, Zugspitze, Rýn,
Dunaj, Mohan, Labe, Odru, Meklenburskou jezerní plošinu, Východofríské a
Severofríské ostrovy, Durynský les, Harz

2\. Kde leží Erzgebirge, Fichtelgebirge, Böhmerwald a Bayerischer Wald?

Spolková republika Německo
==========================

Přírodní podmínky
-----------------

• povrch Německa se zvedá od severu k jihu

• na severu se rozkládá Severoněmecká nížina ovlivněná pleistocenním
zaledněním

• Střed zabírá Porýnská břidličná vrchovina a středoněmecká vysočina
ohraničená nižšími pohořími

• do jižního Německa zasahuje severní okraj Alp, nejvyšší částí jsou
**Bavorské Alpy** (**Zugpsitze** -- 2963 m)

• přímořské klima na severu přechází směrem k jihu ke kontinentálnímu

• hustá říční síť (Dunaj, Rýn, Odra, Labe), většina řek odtéká do
Severního moře a Baltu, význam říční dopravy, síť kanálů

• množství jezer ledovcového původu (Müritz \[Meklemburská jezerní
plošina\], Bodamské, Chiemské jezero)

Nížiny podél Severního moře
---------------------------

• geologicky staré podloží překryté mladými čtvrtohorními usazeninami
nanesenými ledovci a řekami

• pobřeží je při přílivu zaplavované vodou

○ watty -- jiný název pro poldry; občasně zaplavované pobřeží

○ marše -- pás rovin vzniklý ukládáním usazenin

• nížiny jsou na jihu ohraničeny pásem morén(obrovské množství hornin od
ledovce), které dosahují výšku od 30 do 300 m

• v západní části nížin jsou četná rašeliniště a vřesoviště

Nížiny podél Baltu
------------------

• pobřeží lemované písčitými plážemi a kosami má příkré břehy z
křídových usazenin

• v nížinách jsou rozsáhlé jezerní plošiny ledovcového původu
(Meklenburská, Pomořanská a Mazurská)

1\. Hustota zalidnění Evropy vyhledej informace o aglomeraci(konurbace)
Rhein - Main a Rhein-Ruhr

a\. Rhein-Main(Rýn-Mohan): aglomerace na soutoku řek Rýn a Mohan.
Největší města: Fankfurt a. M., Wiesbaden, Mainz, Darmstadt, Offenbach
a. M.

b\. Rhein-Ruhr(Porýní-Porúří): aglomerace na soutoku řek Rýn a Ruhr.
Největší města: Kolín n. Rýnem, Essen, Dortmund, Dusseldorf, Duisburg,
Bochum, Wuppertal

Sociální sféra
--------------

### Obyvatelstvo, sídla, administrativní členění

• nejlidnatější země Evropy (nepočítáme-li Rusko) 82 milionu

• nízký přirozený přírůstek

• národnostně homogenní (nepočítáme-li menšiny, např. Lužičtí Srbové)

• přistěhovalectví po 2. světové válce (Turci, Kurdové, Balkáňané) -
projevy xenofobie vůči imigrantům

• vysoká průměrná hustota zalidnění (Porýní-Porúří, Sasko, Rýn-Mohan)

• největší města: Berlín (3,5 milionu ob.), Hamburg, Mnichov, Kolín n.
Rýnem, Frankfurt n. M., Stuttgart, Drážďany, Lipsko

• rozdíly v životní úrovni mezi západem a východem země

• SRN je spolkový stát skládající se ze 16 spolkových zemí, které mají
vlastní ústavu, parlament a vládu (Federace)

Darmstadt

1\. Jaké byly důsledky 2. sv. války pro Německo? -- Snížení počtu
obyvatelstva, hospodářství, zničená města, posunutí hranic, placení
válečných reparací, Německo bylo rozdělené na 4 okupační zóny (později
spolkové země).

2\. Co víš o Berlínské zdi? -- Rozdělovala východní a západní Německo.

3\. Vysvětli zkratky SRN a NDR.

### Důsledky 2. světové války

• zničené a vyčerpané hospodářství

• válečné reparace

• odsun Němců z východoevropských států

• rozdělení Německa na okupační zóny - sovětská, americká, britská,
francouzská, rozdělení Berlína (východní, západní)

• 1949 vznik SRN a NDR - výsledek studené války

• 1961 Berlínská zeď

• 1989 začalo bourání

• 1990 znovusjed­nocení Německa se souhlasem vítězných mocností

1\. Co víš o německém hospodářském zázraku? SRN nastartování ekonomiky po
válce, **Volkswagen** - lidový vůz, **stabilizace Markyporýní**

2\. Zhodnoť postavení Německa v evropské ekonomice

3\. Která jsou nejvýznamnější hospodářská jádra Německa

### Ekonomika Německa

• velikostí HDP zaujímá Německo 1. místo v Evropě a přední místo ve
světě

• je zakládajícím členem EU a členem G7

• německý hospodářský zázrak: obnova západního Německa po 2. sv. válce

○ Zničené hospodářství na konci 2. svět. Války je rychle obnovováno, ale
po vzniku dvou německých států se ubírá různým směrem a narůstají
rozdíly ve vyspělosti, které nebyly doposud srovnány

○ podpora USA (kvůli rozšíření vlivu a kontroly)

○ symboly: marka, Volkswagen

• surovinové bohatství představují především ložiska uhlí v Porúří,
Porýní a Lužici a zásoby kamenné soli

• mezi nejvýznamnější odvětví patří automobilový průmysl (VW, Mercedes,
BMW, Opel), elektrotechnika (Siemens, Bosch, Grundig), chemický průmysl
(Ingolstadt), potravinářský průmysl

### Hospodářská jádra Německa

1.  **severní Porýní-Vestfálsko**

    -   průmyslový region evropského významu s centry Dortmund, Essen,
        > Duisburg, Düsseldorf, Kolín n. Rýnem (Porýní-Porúří)

    -   jedná se o nejlidnatější spolkovou zemi (17 mil.)

    -   ○ těžký průmysl prošel restrukturalizací = utlumení (např.
        > omezena těžba uhlí)

2.  **Porýní-Falc, Sársko, Hesensko, Bádensko-Württenbersko**

3.  **Bavorsko **

    -   největší spolková země (70 550 km2) s významem automobilovým a
        > potravinářským průmyslem

    -   úloha cestovního ruchu v Bavorských Alpách

    -   hlavní centra Mnichov(BMW) a Norimberk

4.  **Berlín**

5.  **Hamburk -- Brémy**

6.  **Sasko -- Drážďany, Lipsko**
