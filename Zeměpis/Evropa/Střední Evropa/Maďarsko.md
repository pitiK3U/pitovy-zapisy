---
title: Maďarsko
---
Vyhledej na mapě: Velkou a Malou uherskou nížinu, Bakoňský les, Mátru,
Bukové hory, Tiszu, Balaton, NP Hortobágy

Klima příznačné pro Maďarsko - Kontinentální klima, sucho, srážky se
nedostávají přes hory

kolomráz - sněží

Jaké půdy a jaký rostlinný pás převažují na území Maďarska

**Maďarsko**

### **Přírodní zajímavosti**

-   Dunaj ze severu, z Západu řeka Rába, Dráva na JZ hranicí s
    > Chorvatskem

-   Balaton - ,,Maďarské moře"

#### **Panonská pánev**

-   rozsáhlá deprese rozkládající se především na území Maďarska a
    > okrajově zasahující do sousedních států

-   ve třetihorách se zde rozkládalo moře

-   pánev je pokryta silnou vrstvou mořských, říčních a eolických
    > usazenin (usazeniny nanesené větrem)

-   v těchto nánosech jsou bohaté zásoby podzemních vod

-   původní stepi (tráva; *pusta*; černozem) byly většinou přeměněny na
    > zemědělskou krajinu

-   kontinentální klima (teplá suchá léta, chladná suchá zima)

-   zbytky pusty jsou zachovány v NP Hortobágy

#### **Balaton = Blatenské jezero**

-   tektonického původu největší jezero ve střední Evropě

-   rozloha 596 km^2^, délka 78km

-   Pobřeží přiléhající k Bakoňskému lesu je strmé, zbylé oblasti jsou
    > ploché a často bažinaté

-   Jezero je poměrně mělké (11m) a vysychá. V jížní části jsou významná
    > hnízdiště ptactva

-   jezero zanášeno sedimenty nanesenými větrem, proto musí být průbežně
    > bagrováno

-   Balaton je nejdůležitější lázeňskou oblastí a po Budapešti druhou
    > nejnavštěvovanější v Maďarsku

#### **NP Hortobágy**

-   území o rozloze 700 km^2^ ležící na SV Maďarska je tvořeno pusztou,
    > rybníky a močály

-   jsou zde rozsáhlé pastviny a vodní plochy s bažinami jsou domovem
    > mnoha ptáků (racek chechtavý, volavka červená a bílá \...)

-   na stepi se pasou stáda skotu, ovcí a polodivokých koní

-   v centru se nachází vesnice Hortobágy, kde lze navštívit Pastýřské
    > muzeum, Velkou csárdu (původně zájezdní hostinec) nebo nejdelší
    > kamenný most v Maďarsku (170 m)

-   NP je biosférickou rezervací na seznamu přírodních památek UNESCO

#### **Termální vody**

-   přírodní bohatství maďarska

-   vyskytují se na 60 % území a využívají se v lázeňství, k vytápění
    > bytu nebo skleníku

-   mezi vyhlášené lázně patří také Budapešť, kde byly termální vody
    > známy již ve starověku (římané využívali termální prameny)

Lázně Szechenyi

-   Budapěšť

-   Největší v Evrope

-   2 termální prameny 74 a 77°C

-   zdrcadlové - ženská a mužská část

-   artéské lázně

1.  Ve kterých zemích žijí početné maďarské menšiny - J Slovenska, S
    > Srbsko, Z Rumunsko

2.  O která území přišlo Maďarsko po r. 1918

3.  Největší maďarská města

Kluž

### **Historie, obyvatelstvo, sídla**

-   ve starověku římská provincie Panonie (hranice Dunaj-Rýn)

-   příchod Maďarů do střední Evropy v 9. st. z oblasti Uralu
    > (ugrofinské; přispěli k rozpadu VM)

-   od r. 1526 součást habsburské monarchie (bitva u Moháče, Ludvík se
    > utopil, kdo vymře dříve, druhý převezme)

-   rakousko-uherské vyrovnání v r. 1867 (Předlitavsko, Zalitavsko;
    > společný panovník, armáda, měna, zahraniční politika, jinak ne →
    > doplatilo na to Slovensko)

-   ztráta 2/3 území po rozpadu monarchie

-   role Maďarska za 2. sv. války - na straně němců, J Slovenska a
    > Podkarpatská rus

-   revoluce v r. 1956

    -   chtěli svrhnout komunismus

    -   sověti to krvavě potlačili

-   maďarština náleží k ugrofinským jazykem

-   národnostně výrazně homogenní stát - jazyk, bránili se přístupu
    > jiných etnik

-   maďarské menšiny v sousedních zemích

-   dominantní role Budapešti a posilování vlivu menších regionálních
    > center (Miskolc, Debrecen, Szeged, Pécs, Györ)

-   velké vesnice a množství samot

-   administrativně se Maďarsko člení na župy

#### **Budapešť**

-   2 miliony obyvatel

-   římský vojenský tábor a osada Aquincum (dobrá voda) na místě dnešní
    > Budapešti (kolem r. 89 AD)

-   po příchodu Maďarů dvě města -- Buda a Pešť (rozděleny Dunajem)

-   po r. 1526 města obsazena Turky (hlavním městem Uher se stává
    > Prešpurk -- 1536--1784)

-   2\. pol. 19. st. -- spojení obou měst -- Budapešť

-   Královský palác

-   Matyášův chrám

-   Rybářská bašta - UNESCO

-   Řetězový most

-   Budova Parlamentu

-   metro (1896) - nejstarší metro v kontinentální evropě

-   termální lázně

-   Budínský hrad

1.  Typické produkty - uzeniny - klobásy(čabajka, uherský), paprika,
    > masné výrobky, ovoce, zelenina

### **Ekonomika Maďarska**

-   intenzivní zemědělství zaměřené na produkci obilovin, ovoce,
    > zeleniny, vinné révy (Tokaj), chov prasat a skotu - výhodné
    > podmínky pro zemědělství

-   export potravinářských výrobků (vepřové maso, čabajka, uherák, víno,
    > mletá paprika \...)

-   z nerostných surovin má význam pouze bauxit (hliník,)

-   rozvinutý průmysl elektrotechnický, dopravní, potravinářský,
    > farmaceutický

-   hospodářským jádrem země je **budapešťská aglomerace**
