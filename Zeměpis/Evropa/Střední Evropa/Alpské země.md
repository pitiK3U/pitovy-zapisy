---
title: Alpské země
---
-   Vyhledej v atlase: Mont Blanc, Monte Rosu, Matterhorn(Švýcarsko),
    > Grossglockner (vysoké taury), Dachstein (Severní Alpy), Ženevské,
    > Naeuchatelské, Vierwaldstädttské, Bodamské jezero, Rýn, Rhonu,
    > Inn, Salzbach, Drávu

-   Vysvětli pojmy

    -   kar -- ledovcový kotel, ve kterém se tvoří ledovec

    -   moréna -- směs suti, kterou před sebou tlačí ledovec

    -   trog -- ledovcové údolí bez ledovce ve tvaru U

    -   föhn (fén) -- teplý a suchý vítr, který vane z jihu na Alpy

(Severní Alpy = zvrásněný vápenec a pískovec - na dně moří, melasové
vrstvy před severními Alpy;

centrální Alpy = hloubkové horniny;

jižní Alpy = plochý vápenec)

**Jeskyně Hunterbruhl**

Největší podzemní jeskyně v Evropě

6200 m2

1.  **Přírodní podmínky**

    -   Alpy

        -   nejvyšší evropské pohoří vzniklé **alpinsko-himalájským
            > vrásněním** (především třetihory)

        -   pohoří se táhne v délce 1200 km od Středozemního moře až do
            > Slovinska

        -   v pleistocénu rozsáhlé zalednění

        -   v Bernských Alpách se nachází nejdelší údolní ledovec --
            > Aletschský (cca 26 km)

        -   množství ledovcových jezer

        -   Alpy se člení na **západní** a **východní** (zhruba od
            > Bodamského jezera) nebo také na **centrální** (nejvyšší),
            > vápencové a flyšové pásmo (původní usazeniny na dně moře)

        -   význam Alp pro turistiku, sport a rekreaci

        -   nejvyšší vrchol -- **Mont Blanc**

        -   nejvyšší vrchol východních Alp -- **Grossglockner**

Vyhledej v atlase: Švýcarský Jura, Švýcarská plošina, Rakouská žulová
plošina, Alpské předpolí, Solná komora, Vídeňská pánev

-   Švýcarský Jura

    -   střídá se vrchol a údolí

    -   hranice Švýcarska a Francie

-   Švýcarská plošina

    -   od Dunaje na sever

    -   něco jako Českomoravská vrchovina

-   Alpské předpolí

    -   podhůří Alp

-   Solná komora

    -   místo plné přírodních a historických zajímavostí

    -   plno ledovcových jezer

    -   v minulosti se tam těžila sůl

-   Vídeňská pánev

    -   hodně vinic

    -   území široko kolem Vídně

    -   *weinviertel*

-   podnebí alpských zemí je ovlivněno nadměrnou výškou, orientací svahů
    > a fénem

-   *fén* je teplý suchý vítr z jihu a překonávající pohoří orientovaná
    > západovýchodním směrem; na jižní straně pohoří dojde ke srážkám a
    > na severní pak vane suchý vzduch, který se s klesající nadmořskou
    > výškou otepluje

-   *termický stupeň* -- na 100 výškových metrů klesá teplota o 0,6
    > stupňů

-   velký hydroenergetický potenciál řek, ale v zimě nedostatek vody

-   Vyhledej rakouské spolkové země a jejich hlavní města

-   Co víš zajímavého o Vídni?

-   Které turistické cíle v Rakousku znáš?

Rakousko
========

1.  Historie, obyvatelstvo, administrativní členění
    -----------------------------------------------

    -   významná role monarchie v evropské politice od 16. st. do r.
        > 1918

    -   1938 zabrání Rakouska nacistickým Německem (Einschluss)

    -   1945 rozdělení země na čtyři okupační zóny

    -   1955 znovunabytí státní suverenity a vyhlášení neutrality

    -   národnostně homogenní stát, česká menšina ve Vídni

    -   nejhustěji osídlena Vídeňská pánev a Podunají

    -   Rakousko se administrativně člení na 9 spolkových zemí (zemský
        > sněm, vláda, v čele hejtman), autonomie spolkových zemí je
        > poměrně malá

    -   mezi největší města patří Vídeň (1,6 mil.), Graz, Salzburg,
        > Linec, Innsbruck

Fohn (fén)

-   Místní vítr

-   Teplý vlhký vítr přichází od středozemního moře

-   Narazí na hory a snaží se překonat hory

-   Jak stoupá tak se ochladí a prší a jak překročí vrchol je vypršený a
    > směrem dolů se otepluje

-   Horská překážka

-\> vynucený výstup vzduchu

-\> hladina kondenzace

-\> vytvoření oblaku

-\>další výstup vzduchu

-\> vypadávání srážek

-\> překonat horské překážky

-   Příklady:

    -   Gruzie -povodí řeky Rioni

    -   **Chinook** - Sklanaté hory Kanada a USA

    -   Aply - Rakousko

    -   Apeniny - Itálie

    -   Pyrenej - Fr, Šp

    -   Krušné hory - Česká republika

    -   Nízké Tatry - Slovensko

Stefans Dom, Hofburg, Vídeňská opera, Schönbrunn, UNO-city

-   Jak je zaměřená rakouská energetika?

-   Co víš o alpském zemědělství?

-   Které jsou hlavní dálniční osy v Rakousku?

**Hospodářství**

-   ložiska magnezitu, solí (Salcbursko), rud kovů (Štýrsko) a zásoby
    > dřeva

-   rozvinuté hutnictví a strojírenství (speciální stavební stroje a
    > dopravní prostředky) a dřevařský průmysl

-   význam hydroenergetiky a dalších obnovitelných zdrojů (větrná a
    > sluneční energie) -- cca 75 % vyrobené el.

-   odmítají jadernou energetiku (ale berou z Dukovan)

-   alpské zemědělství zaměřené na chov mléčného skotu napojené na
    > potravinářství (mléko, sýry, čokoláda Milka), význam zemědělství
    > pro údržbu krajiny

-   velkým zdrojem příjmů je cestovní ruch (Alpy, Vídeň, Salzburg, Solná
    > komora)

-   Rakousko je tranzitní zemí, frekventované dálnice A1, A2 a průsmyky
    > Brennerský a Semmering

-   význam říční dopravy na Dunaji

Švýcarsko
=========

Historie, obyvatelstvo, administrativní členění
-----------------------------------------------

-   Švýcarsko je federace

-   Kříž Helvetů = keltské kmeny

-   **Luzern - Vierwaldstätter See**

-   r\. 1291 uzavírají kantony Schwyz, Uri a Unterwalden věčný spolek, který
    > bojuje proti Habsburkům a stává se základem švýcarského státu (,, 3
    > lesní kantony\")

-   r\. 1815 potvrzena neutralita (bitva u Waterloo)

-   4 úřední jazyky (\~ 60 % mluví německy; němčina, francouzština - V,
    > italsky, rétorománština - došlo k romanizaci keltského kmene
    > rétorů)

-   většina obyvatel je soustředěna ve Švýcarské plošině

-   Švýcarsko je spolkovou republikou skládající se z 26 kantonů
    > (samostatná autonomie ale jedna vláda, parlament, ústava, zákony
    > -\> jasná federace)

-   Otázka švýcarské konfederace

-   kladen důraz na demokracii -- častá referenda

-   Bern, Curych (největší město, bankovní jádro), Ženeva (sídlo OSN,
    > ČK), Basilej, Lausanne (sídlo mezinárodního olympijského výboru)

-   Švýcarská garda -- Švýcaři se nechali najímat jako žoldáci, která
    > chrání papeže ve Vatikánu

    -   Musí dostat doporučení od místního faráře

    -   Podstoupit různé testy

-   Jaké je zaměření švýcarské ekonomiky? -- Služby (cestovní ruch,
    > bankovnictví).

-   Které švýcarské produkty jsou známé ve světě? -- Nože, hodinky,
    > potraviny- sýry, čokolády.

Hospodářství
------------

-   člen ESVO, schengenský prostor, není členem EU

-   výší HDP na obyvatele zaujímá přední místo ve světě

-   ale zase je tam všechno drahé (i pro ně)

-   ekonomika státu se zaměřuje na kvalitní služby a průmyslová odvětví
    > nenáročná na suroviny (suroviny jim chybí)

-   tradice bankovnictví (Curych číslo 1)

-   rozvinutý cestovní ruch - další tahoun ekonomiky

-   přesné a jemné strojírenství (výroba hodinek, měřících a lékařských
    > přístrojů)

-   nože Victorinox (původně pro armádu)

-   výroba léčiv

-   vyspělé potravinářství (sýry Emmental (tal = údolí), mléko, čokoláda
    > Nestlé)

-   alpské zemědělství (státní dotace a ochrana trhu)

-   agroturistika

**Roger Feder** - tenista

Ženeva

-   významné bankovní a obchodní centrum světového

-   Ženevské jezero

-   Velké kongresy

-   Část OSN

-   Světová zdravotnická organizace

-   Světové ústředí červeného kříže

-   Světová meterologická organizace

Nože Victorinox

-   Nůž na objednávku švýcarské armády

    -   Měl šroubovák - údržba na víno

-   Původně Victoria -\> zavedena nerezová ocel (,,inox\")
