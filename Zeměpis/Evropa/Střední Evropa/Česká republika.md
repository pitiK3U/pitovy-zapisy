---
title: Česká republika
---
**Česká republika**
-------------------

-   vyhledej v atlase

    -   rozlohu a počet obyvatel ČR - 78 886km2

    -   okrajové obce státu a jejich souřadnice

    -   vzdálenost S--J a Z--V - 278km a 493km

    -   nejkratší vzdálenost od moře - 326km

    -   délku stání hranice se sousedními zeměmi - 2 290km

        -   s Německem: 810km

        -   s Polskem: 762km

        -   s Rakouskem: 466km

        -   se Slovenskem: 252 km

-   Velký státní znak České republiky

    -   tvar - husitská pavéza

### **Poloha ČR**

#### **Matematickogeografická**

-   obce:

    -   Lobendava 51°03' s.š. (Sever)

    -   Vyšší brod 48° 33' s.š. (Jih)

    -   Krásná 12° 05' v.z.d. (až za Aší)

    -   Bukovec 18° 51' v.z.d.

-   vzdálenost

    -   S-J 278km

    -   Z-V 493km

-   nejkratší vzdálenost od moře (Šluknov - Štětinský záliv) 326km

-   geografický střed státu leží u obce Číhošť (49°44's.š. a 15°20'
    > v.d.) - Ledeč nad Sázavou - Číhošťský zázrak - za totalitního
    > režimu umlátili

-   ČR leží na 50.rovnoběžce a 15. poledníku (středoevropské časové
    > pásmo)

**Barrandiern**

-   oblast mezi Prahou a plzní

-   pozůstatek starého horstva

-   usazené horniny které nebyly přeměněné

-   starohory, starší prvohory

-   Český kras,

-   bohatá naleziště trilobitů - pozůstatky moře

-   Jean Shaen Barrand

**Fyzickogeografická**

-   vnitrozemský stát ležící ve střední Evropě na rozhraní mezi
    > oceánským a kontinentálním klimatem

-   střední nadmořská výška činí 450 m (Evropa 350 m)

-   ČR leží na hlavním evropském rozvodí -- nedostatek velkých toků

-   poloha na styku Českého masivu a Karpat

-   nejvyšší bod Sněžka (1603 m), nejnižší Labe u Hřenska (115 m)

#### **Geopolitická**

-   ČR leží v oblasti, kde se střetávaly mocenské zájmy (součást
    > Rakouska-Uherska, zabrání Sudet a vytvoření protektorátu, součást
    > sovětského bloku, návrat do Evropy po r. 1989)

### **Hranice státu**

-   hranice ČR patří mezi nejstarší (asi tisíc let) a nejstabilnější v
    > Evropě

-   převážně přírodní tvořená pásmem pohraničních hor

-   celková délka hranice 2.290 km je značná vzhledem k rozloze státu,
    > je velmi členitá a s řadou výběžků (ašský, šluknovský,
    > frýdlantský, broumovským, rychlebský, osoblažský)

-   současná státní hranice byla stanovena na základě mírových smluv

    -   versailleská (1919; hranice s Německem)

    -   st. germainská (1919; s Rakouskem)

    -   s Polskem stabilizována po mezinárodní arbitráži v r. 1920
        > (problém s Těšínem)

    -   se Slovenskem po drobných úpravách potvrzena mezistátní smlouvou
        > v r. 1997

### **Územní vývoj státu**

-   7\. st. -- Sámova říše

-   9\. st. -- Velká Morava

    -   první státní útvar

    -   rozpad způsoben vnitřními rozpory a Maďary

-   10\. st. -- položeny základy přemyslovského státu v Čechách, Morava
    > připojena v 1. pol. 11. st.

    -   úděly, aby mladší sourozenci nezapichovali ty starší

-   1212 -- Zlatá bula sicilská

-   13\. st. -- rozšiřování území za vlády Přemysla Otakara II. a Václava II.

-   Přemyslovci vymřeli po meči

-   14\. st. -- Lucemburkové, vznik zemí Koruny české

-   1526 -- součást habsburské říše

-   1918 -- vznik samostatného Československa

-   1918--1938 -- první republika

-   1938 -- mnichovská dohoda

-   1938--1939 -- druhá republika

-   1939

    -   Slovenský štát

    -   Protektorát Čechy a Morava

-   1945 -- ztráta Podkarpatské Rusi

-   1993 -- rozdělení státu na Českou a Slovenskou republiku

### **Mapování našeho území**

-   1518 -- nejstarší tištěná mapa Čech od Mikuláše Klaudyána

-   1569 -- mapa Moravy od Pavla Fabricia

-   1627 -- mapa Moravy od J. A. Komenského

-   1760--1780 -- probíhá úřední mapování vojenské a civilní

-   1935 -- Atlas Republiky československé

-   1866 -- Atlas Československé socialistické republiky

### **Prezentace**

#### **Barrandier**

-   oblast mezi Prahou a Plzní

-   starší prvohory

-   mořské usazeniny, horniny

-   CHKO Český kras

-   jmenuje se po francouzském paleontologovi, Joachimovi Barrande

**Žofínský prales**

-   nejstarší přírodní rezervace ve střední Evropě

-   založena 1838

-   od18. století bez zásahu člověka

-   rezervace oplocena

#### **Jeskyně Kůlna**

-   sever Moravského krasu

-   součástí Sloupsko-šošůvských jeskyní

-   nálezy z období člověka neandrtálského (120 000 BC)

**Poštovna anežka**

-   nejvýše položená stavba v ČR

-   1600 m.n.m.

-   2007 otevřena pres. Klausem

-   prodejna občerstvení, jedinečné známky, posílání dopisů

-   poštovna od r1899

-   ,,stavebnice"

#### **Karlštejn**

-   14\. století

#### Jáchymov

-   18tis. → 2.ř tis obyvatel

-   těžba stříbra → smolinec

-   Marie Curie Sklodowská - získání uranu - težba

-   První radonové lázně na světě

-   radové prameny z hloubky 500,

#### Glaciální relikty v Krkonoších

-   všivec sudetský

-   sasanka narciskokvětá

-   Ostružiník moruška

-   Šídlo horské

-   ohrožení - kosodřeviny, klima, imise

#### Jeskyně Balcarka

-   1869 dr. Wankel-pralidé

-   objevena po částech

-   1948 spojeny jednotlivé části

#### Zlatá lyže

-   závod v lyžování v Novém městě na Moravě

-   Josef Jílek

-   cena = zlatá lyže

-   původní trať - muži 30km (18km, ženy 20km)

#### Koněpruské jeskyně

-   Beroun

-   nejdelší jeskynní systém v Čechách

-   délka 2050m

-   koněpružické růžice

### **Geomorfologické členění (vyšší geomorf, jednotky)**

-   geomorfologie je věda zabývající se tvary zemského povrchu

#### **Provincie (subprovincie)**

-   Česká vysočina

    -   Šumavská

    -   Krušnohorská

    -   Krkonošsko-jesenická

    -   Česko-moravská

    -   Česká tabule

    -   Poberounská

-   Středoevropská nížina

    -   Středopolské nížiny

-   Západní Karpaty

    -   Vněkarpatské sníženiny

    -   Vnější Západní Karpaty

-   Západopanonská pánev

    -   Vídeňská pánev

### **Geomorfologický vývoj České vysočiny**

-   hranice Česká vysočina -- Západní Karpaty --- zhruba Znojmo, Brno,
    > Ostrava

-   v České vysočině převládají předhercynské útvary -- hlavně
    > krystalické břidlice (ČMV, Šumava,...)

-   mezi nejstarší **nepřeměněné** útvary náleží oblast Barrandiernu -
    > usazeniny

-   největší mocnost zemské kůry je v okolí Sedlčan -- 42 km

-   tektonicky aktivní oblasti jsou na Chebsku, Náchodsku, Opavsku

-   v horninovém složení převažují žuly, pískovce, vápence a vulkanické
    > horniny

-   **prahory** -- vznik ČMV, Šumavy, jižních Čech

-   **starohory** -- nejstarší mořské usazeniny v Barrandienu

-   **prvohory** -- **mořská transgrese - někdy bylo moře někdy ne,**

    -   kaledonské vrásnění --

        -   část území zalita mořem,

        -   vznik devonských vápenců Moravského krasu,

    -   hercynského vrásnění --

        -   vysoká pohoří,

        -   ložiska černého uhlí (Moravskoslezský kraj, Ostravsko,
            > Kladensko),

        -   moře pouze na okrajích masivu,

-   **druhohory** --

    -   snižování horstev,

    -   mělké křídové moře,

    -   po jeho ústupu moře vznik České křídové tabule

-   **třetihory** --

    -   vliv alpinského vrásnění -- zdvih okrajových pohoří, zlomy,
        > poklesy, vulkanismus v Českém středohoří (třeba Říp a něco v
        > Jesenících), Doupovských horách a Nízkém Jeseníku,

    -   vývoj říční sítě,

    -   hnědouhelné pánve,

    -   Malý Roudný, Velký Roudný, Venušina sopka

-   **čtvrtohory** --

    -   doznívající vulkanismus, pleistocenní zalednění (u nás
        > minimální; menší horské ledovce v Krkonoších a na Šumavě),

    -   váté písky a spraše (Moravský kraj, mezi Bzencem a Hodonínem-
        > písečné duny - porostlé borovicí),

    -   dotvářené říční sítě,

    -   vliv člověka

### **Geologický vývoj Karpat**

-   **druhohory**

    -   oblast zalita mořem,

    -   vznik alpsko-karpatské předhlubně,

    -   vyvrásnění centrálních Karpat

-   **třetihory**

    -   vyvrásnění vnějších **flyšových** Karpat - pískovce, jílovité
        > břidlice

    -   ústup moře → vývoj říční sítě

-   **čtvrtohory**

    -   pevninský ledovec na Ostravsku a v Moravské bráně

    -   spraše a váté písky - Moravské úvaly Dolnomoravský, ... = naváté
        > semínka hlíny - písečné duny (mezi )

    -   ledovec - bludné balvany

    -   dotvoření říční sítě

    -   vliv člověka

### **Šumavská subprovincie**

-   Šumava (Velký Javor 1456 - Německo, Plechý 1378 - Česko),

-   Šumavské podhůří,

-   Český les (Čerchov 1042),

-   Novohradské hory

#### **Šumava**

-   poloha na hranicích s Německem a Rakouskem

-   jedna z geologicky nejstarších oblastí - prahory, starohory

-   vliv alpinského vrásnění - na Šumavu okrajový vliv = vyzdvižena

-   lokální zalednění v pleistocénu (ledovcová jezera - Černé jezero 39
    > m, kary, morény, ...), Čertovo j., jezero Laka, Plešné jezero,
    > Prášilské

-   biosférická rezervace NP - šumavské slatě a pláně (rašeliniště a
    > močály), Národní Přírodní Rezervace Boubín = prales (široké pláně
    > , po pleistocénu začali tát plochy ledovce, voda neodtekla nikam
    > pryč, zůstala na místě → rašeliniště, před 10 tis. let roztál
    > ledovec; 1 000 let 1 m rašeliny ⇒ 10m rašeliny; Kvilda = -30 až
    > -35°C i v létě )

-   společně s Bavorským lesem se jedná o nejzalesněnější část střední
    > Evropy

-   prameny Vltavy (pramení pod Černou horou) a Otavy (Vydra), Lipenská
    > nádrž - plošně největší nádrž

-   hřebeny Šumavy prochází hlavní evropské rozvodí (Černé X Severní)

-   Chalupská slať

-   Černé jezero - ledovcový kotel

#### Novohradské hory

-   poloha při hranicích s Rakouskem

-   NPR Žofínský prales (1838), prales Hojná Voda - nejstarší přírodní
    > rezervace ve střední Evropě

-   rekreační centra Dobrá Voda, Hojná voda

-   zdroje kvalitní pitné vody

### **Krušnohorská subprovincie**

-   Krušné hory (Klínovec 1244),

-   Smrčiny,

-   Karlovarská vrchovina,

-   Doupovské hory,

-   České středohoří (Milešovka 837),

-   Děčínská vrchovina,

-   Podkrušnohorské pánve

    -   Chebské, Sokolovská, Mostecká

#### **Krušné hory**

-   kerné pohoří na hranicích s Německem prvo a druhohorního stáří,
    > mírně se svažuje do Saska a příkře do Podkrušnohorských pánví

-   podél východního okraje největší zlom

-   v minulosti těžba rud, stříbra a uranu

-   Jáchymov - v 16.stol. s 18 000 lidmi druhé nejlidnatější město v
    > Čechách, těžba stříbra - tolary (dolar), 1. radioaktivní lázně na
    > světě (marie)

-   Krušné hory jsou významným klimatickým předělem

-   poškozené lesní porosty

**významná ložiska hnědého uhlí**

#### **Podkrušnohorské pánve**

-   **Mostecká** (největší těžba hnědého uhlí), Sokolovská, Chebská

-   pánvemi protéká řeka Ohře

-   povrchová těžba hnědého uhlí - devastace přírody, zánik řady sídel

-   89 = limity na těžbu uhlí - dnes chtějí společnosti co těží uhlí
    > navýšit - ústup sídla

-   → koncepce vychází že máme energii získávat z toho čeho máme nejvíc
    > = **hnědé uhlí**

-   soustředění průmyslu - energetika, chemie, ...

-   zdroje termálních pramenů a minerálních vod

#### **České středohoří**

-   třetihory, vznik tektonickou činností

-   pohoří sopečného původu rozdělené na dvě části údolím Labe

-   nejvyšší vrchol Milešovka patří k největrnějším místům v ČR,

-   je zde umístěna observatoř, která patří do světové sítě
    > meteorologických stanic severní polokouli

-   České středohoří je CHKO

#### **Labské pískovce**

-   pískovce jsou součástí Děčínské vrchoviny a přesahují na území
    > Německa

-   koňovité údolí řeky Labe

-   pískovce se ukládaly na dně křídového moře

-   zvětráváním vznikl členitý reliéf se skalním městem, jehož rozsah
    > nemá ve střední Evropě obdoby

-   skalní města

-   lokalita je součást NP České Švýcarsko a CHKO Labské pískovce

-   nejznámějším objektem Labských pískovců je Pravčická brána

### Krkonošsko-jesenická subprovincie

-   Lužické hory,

-   Ještědsko-kozákovský hřbet (Ještěd 1012),

-   Jizerské hory (Smrk 1124),

-   Krkonoše (Sněžka 1603),

-   Broumovská vrchovina,

-   Orlické hory (Velká Deštná 1115),

-   Kralický Sněžník (Kralický Sněžník 1423),

-   Rychlebské hory,

-   Hrubý Jeseník (Praděd 1491),

-   Nízký Jeseník

#### Krkonoše

-   pohoří na severu ČR příkře se svažující do Polska a mírněji do České
    > vysočiny, převažují horniny staro a prvohorního stáří

-   výrazná modelace reliéfu během pleistocénu - vznik karových a
    > údolních ledovců - kary, morény, jezera, trogy, na nezaledněných
    > vrcholech došlo k silnému mrazovému zvětrávání

-   na svazích Krkonoš se vytvořily čtyři vegetační výškové stupně:

    -   listnaté a smíšené lesy s kulturními loukami
        > (přetvořené)400-800m

    -   horské smrčiny 800-1200m

    -   severské louky, kleč, rašeliniště 1200-1450m

    -   lišejníková tundra, kamenité sutě 1450-1602m

-   pohoří má pestrou skladbu rostlin a živočichů s řadou endemitů
    > (unikátní živočichové pro určitou oblast) a glaciálních reliktů
    > (rostliny přetrvávající z doby ledové)

-   Krakonošovy zahrádky - sníh i přes červen -

-   příroda hřebenů Krkonoš s drsným klimatem se podobá přírodě severní
    > Evropy - severská tundra

-   KRNAP (1963), biosférická rezervace (1922)

-   hodně poškozeny - turisté + bývalé hospodaření

-   centra Špindlerův Mlýn, Pec pod Sněžkou, Harrachov, Janské Lázně

#### Broumovská vrchovina

-   součástí vrchoviny je CHKO Broumovsko s Adršpašskoteplickými skálami
    > a Broumovskmi stěnami

-   lokalita patří k největším skalním městům střední Evropy

-   členitý reliéf způsobuje klimatickou inverzi (převrácené teploty - v
    > nižší části chladněji)

-   regionem protéká řeka Metuje

#### Kralický sněžník

-   rulová klenba, ze které vybíhají směrem k jihu dva hřbety

-   pramenná oblast řeky Moravy

-   centrální bod hlavního evropského rozvodí - na vrcholu Klepý se
    > stýkají rozvodnice Baltu, Severního a Černého moře

#### Hrubý Jeseník

-   nejvyšší pohoří Moravy se zaoblenými hřbety a hluboce zaříznutými
    > údolími

-   hřebeny Jeseníků patří k nejchladnějším oblastem v republice
    > (Praděd - průměrná roční teplota vzduchu 0,9°C)

-   vyvinuté výškové vegetační stupně

-   horská sedla Ramzovské (nejvýše položená rychlíková zastávka v ČR) a
    > Červenohorské

-   CHKO Hrubý Jeseník

-   NPR Praděd - vrchol Praděd, Petrovy kameny, Velká a Malá kotlina,
    > Bílá Opava - kaňon, pramen Opavy

-   NPR Rejvíz - horské rašeliniště s Velkým a Malým Mechovým jezírkem

-   NPR Šerák-Keprník - zbytky pralesních porostů horských smrčin

-   lázně Jeseník (Priznicovy lázně), Ramzová, Červenohorské sedlo, Malá
    > Morávka, Karlov, Karlova studánka (nejčistší ovzduší)

-   Ještěd

-   Luční bouda, rašeliniště Úpy, Sněžka

-   Pramen Labe

-   Adršpašské skály

-   Švýcárna - původně ??? pro lidi co hlídali lesy, pozvali švýcary

### Českomoravská subprovincie

-   Brněnská vrchovina,

    -   **Drahanská vrchovina**

    -   **Bobravská vrchovina** (JZ Brna)

    -   **Boskovická brázda** (Česká Třebová - Moravský Krumlov)

-   Českomoravská vrchovina (Javořice 837),

-   Středočeská pahorkatina,

-   Jihočeské pánve

#### Drahanská vrchovina (Skalky 735)

-   útvar prvohorního stáří ležící severně od Brna ohraničený
    > Hornomoravským úvalem, Vyškovskou bránou, Bobravskou vrchovinu a
    > Boskovickou brázdou

-   součástí Drahanské vrchoviny je **Moravský kras**

    -   nejrozsáhlejší krasové území v ČR o rozloze cca 100km2 tvořené
        > devoskými vápenci

    -   v r. 1956 vyhlášena CHKO se sídlem v Blansku

    -   množství povrchových a podpovrchových krasových jevů kaňony,
        > údolí, závrty, propasti, jeskyně, ponory, vývěry, ...

    -   **severní část**

        -   Sloupsko-šošůvské jeskyně (j. Kůlna - nálezy z období
            > člověka neandrtálského z doby před 120 000 lety),

        -   Punkevní jeskyně (prof. Absolon, Masarykův dóm)

        -   Rudické propadání - 90m

        -   propast Macocha - 138m

        -   Amatérská jeskyně (41km, Punkva)

        -   Kateřinská jeskyně

        -   Balcarka

    -   **střední část** - Rudické propadání, Býčí skála

    -   **jižní část** - Ochozská jeskyně, j. Pekárna - rytiny koní a
        > bizonů na kostech zvířat (lovci koní a sobů před 13 000 lety)

#### Bobravská vrchovina

-   leží na JZ okraji Brna je zbytkem tektonicky rozlámané paroviny

#### Boskovická brázda

-   příkopová propadlina táhnoucí se od Moravské Třebové k moravskému
    > Krumlovu vyplněná permokarbonskými sedimenty

#### Českomoravská vrchovina

-   nejrozsáhlejší geomorfologický útvar o rozloze 13 500 km2

-   stará oblast skládající se především z krystalických břidlic
    > (přeměněné horniny) a hlubinných vyvřelin (**žula**)

-   nejvyšší části jsou Jihlavské vrchy-Javořice a CHKO Žďárské
    > vrchy-Devět skal 836 m

-   pramenná oblast Svratky (pramení ve Žďárských vršcích), Sázavy,
    > Jihlavy, Oslavy

-   nesouvislé lesní porosty střídající se s rybníky, loukami a
    > pastvinami (brambory)

-   centra

    -   Nové Město na Moravě

    -   Tři Studně - sýkovec

    -   Devět skal

    -   Velké Dářko - rekreační oblast,

#### Jihočeské pánve

-   sníženiny vyplněné druho a třetihorními sedimenty dělící se na
    > Třeboňskou a Českobudějovickou pánev

-   Třeboňská pánev je naší největší rybníkářskou oblastí, je významným
    > hnízdištěm a migrační zastávkou ptactva

-   jsou zde četná rašeliniště, umělé kanály (Zlatá stoka), území bylo
    > vyhlášeno biosférickou rezervací

-   rybníky Rožmberk 489 ha - plošně největší (duby), Horusický, Velký
    > Tisý, Dvořiště, ...

-   Štěpánek Natolický, Jakub Krčín z Jelčan 16.století (budování
    > rybníků, Třeboňský regent)

-   pánví protéká Lužnice a Ležárka

-   Třeboň - lázně, renesační budovy

1.  vyhledej: Říp, Ralsko, Trosky, Bezděz a Kunětickou horu + původ

### Česká tabule

-   oblast tvořená především křídovými sedimenty-pískovce, slínovce

-   skalní města CHKO Český ráj - Prachovské skály, Hrubá Skála, Malá
    > Skála

-   třetihorní vulkanické tvary - Říp, Bezděz, Trosky, Ralsko, Kunětická
    > hora

-   nejnižší částí tabule je Polabská nížina, dolní Poohří a dolní
    > Povltaví - intenzivní zemědělství

-   řeky Labe, Jizera, Ohře, Vltava

-   bohaté zásoby spodních vod (zdroj déšť a tající sníh)

### Poberounská subprovincie

-   Brdy (Tok 865), Plzeňská pahorkatina, Křivoklátská vrchovina,
    > Pražská plošina

-   součástí subprovincie je Barrandien

-   CHKO Křivoklátská vrchovina je biosférickou rezervací s rozsáhlými
    > společenstvy lesů v zachovanlém stavu

-   CHKO

-   Berounka vzniká soutokem 4 řek: Úhlava, Radbuza, Úslava,

### Karpaty

### Vnější Západní Karpaty

-   Pavlovské vrchy (Děvín 550),

-   Žďárnický les,

-   Chřiby Hostýnsko - vsetínská hornatina,

-   Kyjovská pahorkatina,

-   Vizovická vrchovina,

-   Bílé Karpaty (Velká Javořina 970),

-   Javorníky (Velký Javorník 1071),

-   Moravsko-slezské Beskydy (Lysá hora 1323)

#### Pavlovské vrchy (Pálava)

-   CHKO, biosférická rezervace (Dolní Morava - Valticko-Lednický areál)

-   slabé zkrasovělé jurské vápence táhnoucí se až do Rakouska

-   jedna z nejteplejších a nejsušších oblastí v ČR

-   stepní a lesostepní společenstva v NPR Děvín (kosení / pastva -
    > stádo koz a ovcí)

-   lovci mamutů - Věstonická venuše

-   pěstování vinné révy

-   nádrže Nové Mlýny na řece Dyji (museli se čistit, Lužní les, řada
    > živočichů vázána na vlhké prostředí) → značný zásah člověka do
    > přírody - bez záplav → umělé záplavy → vodu z nádrži upuštěna →
    > vytvoření bio ?COSI? → ostrovy uprostřed nádrže - místo pro ptáky

#### Bílé Karpaty

-   CHKO, biosférická rezervace - **horské louky** - nejrůznější skladba
    > rostlin (mnoho druhů orchidejí)

#### Moravskoslezské Beskydy

-   samostatná geologická jednotka ve flyšovém pásmu tvořená pískovci a
    > břidlicemi přavážně křídového stáří

-   plošně největší CHKO v ČR 1160 km2 se zbytky původních pralesovitých
    > lesů a s druhově pestrými loukami a pastvinami

-   řeky Bečva, Ostravice, Olše

-   centra Rožnov pod Radhoštěm, Frenštát, Valašské Meziříčí, Pustevny,
    > Radhošť

-   Radegast - pohanský bůh slavností, originál ve Valašském Meziříčí

-   Pustevny - národní kulturní památka, Jurkovič, vyhořela

-   Valašské muzeum v přírodě, nejstarší skanzen Rožnov pod Radhoštěm

#### Vněkarpatské sníženiny

-   Dyjsko-svratecký úval,

-   Vyškovská brána,

-   Hornomoravský úval,

-   Moravská brána,

-   Ostravská pánev

1.  Které faktory ovlivňují klima na území na území našeho státu?

2.  V atlase vyhodnoť mapy průměrných ročních teplot vzduchu a úhrnů
    srážek

3.  Které jsou hlavní zdroje znečištění ovzduší u nás?

Charakter klimatu ČR
--------------------

-   poloha státu na rozhraní mezi oceánským a kontinentálním klimatem

-   na utváření klimatu má vliv:

    -   **převládající západní proudění**

    -   **reliéf** (nadm. v. 100m = 0,6°C, srážkový stín, expozice
        > svahů - orientace svahů ke světoým stranám)

    -   **lesní plochy** - zadržuje vlhkost

    -   **vodní plochy** - mikroklima → větší vlhkost

    -   **zastavěné plochy** - zvýšení teploty - beton, asfalt, sklo

    -   **rozložení vzduchových hmot**

-   počasí u nás ovlivňuje rozložení cyklon a anticyklon nad Azorskými
    ostrovy, Islandem, Britskými ostrovy, Skandinávii a Ruskem

-   aprílové počasí, ledoví muži (12.-14.5. zmrznutí ovocné úrody),
    Medard (půlka června - ), babí léto (slunečno, hezky)

### Rozložení teplot

-   vliv nadmořské výšky (100 m / 0.6°C)

-   z hlediska teplot vymezujeme klimatické oblasti:

    -   **teplé** (nížiny)

    -   **mírně teplé** (vrchoviny pod 700m)

    -   **chladné** (vrchoviny nad 700m, pohoří)

### Rozložení srážek

-   ovlivněno nadm. v. a polohou vůči převládajícím větrům

-   nejsušší oblasti leží ve srážkovém stínu Krušných hor a ČMV

klimadiagram

Brno - dlouhodobá průměrná teplota 8.7°C, málo srážek

1.  Lokalizuj naše řeky v rozsahu mapy v atlase na s. 11

2.  Z učebnice zpracuj téma Zásahy člověka do hydrosféry

(sucho - 95%, snížila se hladina vody, problémy v zemědělství, **pokles
zásob podpovrchových vod** (neobnovitelné - velký problém))

Hydrosféra
----------

### Řeky

-   nevýhodná poloha státu na hlavním **evropském rozvodí** (Klepý 1143
    m; většina řek pramení na našem území -\> nestihnou se rozvodnit -
    vodní doprava větší odtok)

-   **sněhovo-dešťový** (středoevropský) režim odtoku (málo dešťů a málo
    sněžení → málo zásob vody)

-   řeky jsou napájeny převážně dešťovými a sněhovými srážkami, méně
    podpovrchovými zdroji

-   rozdíly v povrchovém a specifickém odtoku

-   voda z našeho území odtéká do Severního, Černého a Baltského moře

-   **labe je hlavní říční osou** (i přesto že není nejdelší; pramení v
    Krkonoších na Labské louce → jih do české tabule)

-   Morava

    -   Kralický sněžník

    -   →

    -   → ústí na Rakousko-slovenské hranici do dunaje

-   Odra

    -   Oderských vrších

    -   →

    -   na S Lužická ... → vlévá se do Odry

### Jezera

-   **ledovcová** ( Černé j. 18 ha, 40 m, nejhlubší a největší j)

-   **krasová** (na dně Macochy)

-   **rašelinová** (Šumava, Hrubý Jeseník)

-   **říční** - slepá ramena řek

### Rybníky

-   jižní Čechy (**Rožmberk** 489 ha)

-   ČMV (Velké Dářko)

-   jižní Morava (Pohořelicko, Lednické rybníky - Nesyt; Šlechta chtěla
    peníze - rybníky - ryby - způsob podnikání)

### Přehradní nádrže

-   vltavská kaskáda

Prosté podzemní vody

Minerální vody

Vliv člověka na hydrosféru

V atlase na str.12 zpracuj informace o rozmístění a charakteristice
hlavních půdních typů a druhů v ČR, využij rovněž text a nákresy půdních
profilů v učbnici na str. 18-19

Půdy ČR
-------

-   vymezení půdních typů a druhů (jiné vlastnosti, jiné zastoupení
    horizontů; vymezujeme na základě zrnitosti)

-   půdotvorné činitelé:

    -   **geologický podklad** (vliv na chemismus půdy a množství
        > minerálních živin v půdě)

    -   **reliéf** (nadm. výška, expozice svahů, eroze)

    -   **klima** (srážky, výpar a teploty ovlivňují vzestupný nebo
        > sestupný pohyb podzemních vod)

    -   **vegetace** (zdroj humusu)

    -   **edafon** (řasy, bakterie, plísně, houby... )

    -   **člověk** (odlesňování-eroze, zástavba, kyselé deště, umělá
        > hnojiva...)

Živá příroda
------------

-   vývoj rostlinstva a živočišstva na území ČR byl ovlivněn polohou na
    rozhraní několika geografických zón

-   **boreální, středoevropská lesní, alpské a karpatské** a také
    polohou na rozhraní zaledněné Evropy v glaciálech - značená
    biodiverzita

-   vegetační stupně údolních niv, dubový a bukový byly výrazně
    přeměněny člověkem - zemědělské plochy, regulace řek, likvidace
    lužních lesů

-   **význam lesních ekosystémů**

    -   z rozlohy státu zabírají asi 33% (poslední dobou úbytek - sucho,
        > kůrovec → vykáceny lesy - Beskydy, Jeseníky)

    -   druhové skladbě dominuje smrk 55%

    -   člověk výrazně ovlivnil skladbu (vykácel listnáče → smrk roste
        > rychleji)

    -   význam lesa pro ekologickou stabilitu (mráz, škůdci)

    -   význam lesa pro vodohospodářství (les zadržuje vodu)

    -   význam lesa pro rekreaci

    -   zabraňování erozi

1.  pomocí učebnice a atlasu vyhledej naše NP, CHKO a biosférická
    rezervace

2.  Která z těchto chráněných území leží na Moravě?

Ochrana přírody
---------------

1838 - Žofínský prales a prales Hojná voda

1858 - Boubínský prales

-   po r. 1989 zřízeno MŽP, jsou vydávány nově právní normy pro ochranu
    přírody, zavádí se ekologická politika státu

-   1991 - Státní program péče o životní prostředí

-   postupně dochází ke zkvalitňování životního prostředí, ale značná
    zátěž z minulosti a rostoucí problém s nárůstem automobilové
    dopravy, prašností atd. (velké znečištění v zimě topením v
    domácnostech)

### Zvláště chráněná území (ZCHÚ) v ČR:

#### Velkoplošná CHÚ 

-   **NP** ( vyhlašuje Parlament ČR;

> zonace - NP rozdělené do zón,
>
> zóna č.1 = nejvíce chráněná,
>
> správa = stará se o NP, zaměstnanci )

-   **CHKO** (vyhlašuje Vláda ČR)

<!-- -->

-   Orgánem ochrany přírody v NP a CHKO jsou jejich správy, návštěvníci
    jsou povinni řídit se řádem těchto CHÚ

-   U NP jsou vytvořeny ochranné zóny

**Krkonošský NP 1963**

**NP Šumava 1991**

**NP Podyjí 1991**

**NP České Švýcarsko 2000**

#### Maloplošná CHÚ

-   národní přírodní rezervace (NPR)

-   přírodní rezervace (PR; louka,)

-   národní přírodní památka (NPP; strom)

-   přírodní památka (PP)

#### Biosférické rezervace

-   vyhlašuje **UNESCO**

-   v ČR 6 br

-   **Šumava** + Bayerischer Wald

-   **Třeboňsko**

-   **Křivoklátsko**

-   **Bílé Karpaty**

-   **Krkonoše**

-   **Pálava**

#### Mokřadní ekosystémy

-   jedná se o lokalitu chráněné na základě Ramsarské úmluvy, v ČR mezi
    ně patří např. Třeboňsko, Šumavské slatě nebo Lednické rybníky

NATURA 2000

-   soustava chráněných území evropského významu

-   vytvářejí členské státy EU

-   cílem ochrana živočichů rostlin, a typů přírodních stanovišť

-   pro každé území jsou stanovena jasná pravidla jak se o to starat -
    je kontrolováno

1.  Vyhledej v atlase kraje ČR a jejich hlavní města

2.  Z učebnice zjisti rozlohou a počet obytel Jm kraje a porovnej s
    ostatními kraji

3.  Kdo je současným hejtmanem JM kraje a kde je sídlo kraje

Regiony České republiky
-----------------------

-   ČR se skládá ze tří historických zemí: Čech, Moravy, Slezska ( pro
    současné administrativní uspořádání státu se již toto členění
    nehodí)

-   v r. 2000 byly zavedeny nové ***vyšší územní samosprávné celky -
    kraje***

-   celkem 14 krajů, v čele stojí hejtman, rozlohou největší je
    Středočeský, nejlidnatější je Moravskoslezský

-   **okresy** - 76 okresů, které fungují již jako statické jednotky
    (okresní úřady už nefungují, musíme znát okresy jihomoravského
    kraje)

-   v roce 2003 zanikají okresní úřady, jejich pravomocí byly přeneseny
    zčásti na kraje, většinou ale na **obce s rozšířenou působností**
    (pověřené obce III.stupně), kterých je **205** ( okresy rozděleny na
    více částí kvůli lidem, aby měli k úřadům blízko; museli ale být
    vytipovány obce kde ??)

-   nejmenší administrativní jednotkou v ČR je **obec** (cca 6250)

-   **unitární** - řízený z jednoho centra (x federace)

-   v rámci EU byla v ČR vytvořena **soustava územních statistických
    jednotek - *NUTS II*** *(*statistické jednotky, rozdělení peněz z
    EU, podle vlastních pravidel sloučili několik krajů, aby je
    splňovaly)

-   jedná se o 8 oblastí, které slouží Eurostatu (statistický úřad EU)
    pro porovnání ekonomických ukazatelů členských zemí

-   jedná se o oblasti, které počtem obyvatel překračují 1 milion, kromě
    Prahy, Středočeského a Moravskoslezkého kraje byly ostatní spojeny
    po dvou až třech

-   na základě tohoto členění jsou přijímány finanční prostředky ze
    strukturálních fondů EU, které jsou určeny na rozvoj zaostávajících
    regionů - tzn., že HDP zde nedosahuje 75% průměru na 1 obyvatele
    EU - chudé regiony pod 75%

-   bohaté nad 120%

NUTS 1 - ČR

NUTS 2 - 8 regionů vymezených EU

NUTS 3 - 14 krajů

NUTS 4 - 76 okresů

NUTS 5 - 6249 obcí

1.  Z učebnice na str. 30 získej informace ? kraje, okresy JM kraje

Vývoj osídlení a zalidnění
--------------------------

-   osidlování našeho území bylo ovlivněno přírodními,
    sociálně-ekonomickými a geopolitickými faktory (vazba na průmyslové
    aglomerace; odsun němců z pohraničí - dodnes se nepodařilo
    plnohodnotně osídlit)

-   Z nejstarších etnických skupin to byli Keltové (5.-4. stol.
    př.n.l.), Markomani a Kvádové (počátek n.l.) a Slované (5.-6.stol.)

-   nejdříve osidlovány nížiny, do vyšších poloh přichází domácí obyv. a
    od 13.stol. němečtí přistěhovalci

-   přirozený přírůstek byl i přes vysokou porodnost do pol. 18. stol.
    nízký - války, nemoci, neúrody

-   od pol. 18.stol. se zvyšuje zemědělská produkce, klesá úmrtnost
    v 19. stol. se rozvíjí průmysl - růst počtu obyv.

-   v 1. pol. 20. stol. klesá porodnost i úmrtnost a populační vývoj je
    ovlivněn dvěma světovými válkami a odsunem Němců po 2.sv. válce
    (více jak 2 mil)

-   1948, 1968 - politická emigrace

-   ve 2.pol. 20.stol. porodnost kolísá, výrazný vzestup nastává v
    70.letech díky populačním opatřením vlády (prodloužení mateřské
    dovolené, zvýšení na děti) → baby boom

Demografická struktura obyvatelstvo
-----------------------------------

-   změny v demografickém chování populace po r. 1989 (chceme dohnat
    západní evropu → uzavíráním sňatků v pozdějším věku, budování
    kariéry)

**Přirozený přírůstek**

-   rozdíl mezi počtem narozených a zemřelých osob v přepočtu na 1000
    obyv. Rozlišujeme kladný a záporný PP.

-   PP je ovlivněn:

    -   úhrnnou plodností - průměrný počet živě narozených dětí na jednu
        > ženu; měl by být 2, v čr je 1,2 nebo 1,3

    -   ekonomickou situací

    -   kariérou, studiem, cestováním

    -   pozdějším uzavřením sňatku

    -   první dítě kolem 28-30 let

**Celkový přirozený přírůstek**

-   součet přirozeného a migračního salda = rozdíl počtu přistěhovalých
    a vystěhovalých osob

### Střední délka života (naděje dožití) 

\- průměrná předpokládaná délka života dnes narozeného dítěte

-   muž 76 let (2015)

-   ženy 82 let

-   ovlivněno:

    -   úrovní lékařské péče

    -   stavem ŽP

    -   životním stylem

### Hustota zalidnění (počet obyvatel : rozlohou)

-   průměrná hustota zalidnění = 131 obyv./km2

-   územní rozdíly jsou způsobeny přírodními, socioek. a historickými
    faktory

-   určitý územní pohyb obyvatel může být ovlivňován strukturálními
    změnami v ekonomice

Slezsko - těžba uhlí

Morava - rozvoj průmysl

### Věková struktura obyvatelstva

-   předproduktivní věk 0-14 let 14,5% (2011)

-   produktivní věk 15 - 59 let 62.7% (15-64 = 69.6%)

-   poproduktivní věk 60 - a více 22,7% (65 a více = 15,8%)

zvyšování důchodů, zvyšování nákladů an seniory a v dlouhodobém měřítku
klesá

asi 6 milionu neaktivních (studenti, nezaměstnaní, postižení, ženy na
mateřském), 5 milionů ekonomicky aktivních

→ pracovní síly ze zahraničí

Romové

-   nejpočetnější menšinu v čr v 2016 246 000 obyv.

-   nejvyznamnější skupinou jsou slovenští romové

-   romové patří k sociálně vyloučitelným

-   Ústecký kraj - Most

### Národnostní složení

-   národnostní složení ČR je značné homogenní

-   národnost lze volit libovolně a měnit

<!-- -->

-   k českému obyvatelstvu (Češi, Moravané, Slezané) se hlásí 94%

-   česká národnost 90,4%

-   moravská 3,7% (nejvíce Brněnsko)

-   slezská 0,1%

-   slovenská 1,9%

-   polská 0,5%

-   německá 0,4%

-   romská 0,1%

<!-- -->

-   z cizinců jsou nejpočetnější Slováci, Ukrajinci (přichází po
    roce 89) a Vietnamci

-   projevy nacionalismu, rasismu a xenofobie

Počet obyv v Brně

-   cca 380 000 v 2017

-   muži 183 000

-   ženy 196 227

-   57 600 lidí v předproduktivním věku

-   půměr 42.5 let

### Sídelní struktura ČR

-   Jaké populace se v daném sídle nacházejí (Majetní X nemajetní a kde
    bydlí; národností menšiny; podle věku; podle pohlaví)

-   v období agrární společnosti převládají venkovská sídla

-   feudální města jsou zakládána především ve 13. a 14.stol.
    (křižovatky obchodních cest, těžba surovin, správní centra)

-   proces industrializace nastoupený v 19.stol. znamená růst měst v
    průmyslových oblastech a naopak stagnaci nebo úpadek měst
    ovlivněných tímto procesem

-   dochází k odlivu obyvatel z venkova

-   proces urbanizace

-   průměrná vzdálenost sídel v ČR je cca 3 km

-   v 60. a 70. letech 20.stol. dochází k připojování menších obcí k
    větším městům, po roce 1989 proces osamostatnění

-   v současné době počet obyvatel v našich městech stagnuje

-   míra urbanizace (cca 70%)

-   nové urbanistické zóny na okrajích sídel (které?)

-   proces suburbanizace (střed města → okraj města)

<!-- -->

-   vysoký stupeň rozdrobenosti venkovských sídel (a)

-   relativně nízké zastoupení velkoměst (b)

-   významná role malých a středních ( c)

(a) Současná sídelní struktura České republiky se vyznačuje poměrně
    značnou roztříštěnosti, která se odráží i prostřednictvím existence
    velkého počtu relativně malých obcí. K 1.1. 2011 existovalo na území
    ČR celkem 4 856 obcí s počtem obyvatel menším než 1 000 (77,7% z
    celkového počtu všech obcí) a k tomuto datu žilo v malých obcích 1
    789 091 obyvatel (pouze 17% obyvatelstva ČR(. Počet venkovských
    sídel v České republice, tj. vesnic, rozptýlených sídel, vísek samot
    se odhaduje na cca 40 tisíc, při průměrné vzdálenosti síde okolo 1,5
    km.

Pramen ČSÚ 2010

(Ovšem význam malých obcí nelze čistě omezit na pouhé populační
hledisko,ale je třeba brát v potaz též významný faktor)

**obce menší 1 000 obyvatel fakticky ,,spravují" více než polovinu
rozlohy státu**, důležité je rovněž jejich plošné, v podstatě
**rovnoměrné rozložení** (byť existují regionální rozdíly \...)

### Městská sídla

-   vymezení pojmu město:

    -   **historicko-právní hledisko** (v historii obdrželo statut města
        > → dnes již nesplňuje podmínky města)

    -   **kvantitativní hledisko** (počet obyvatel,, bývalo \> 10 000)

    -   **kvalitativní hledisko** (vybavenost sídla)

-   městys - sídlo s velikostí mezi venkovským a městským sídlem

zástavby - ve městech spíše vysokopodlažní budovy

-   velkoměsta - největší města u nás, musí mít více jak 100 000 obyv.
    (**Praha, Brno, Ostrava, Plzeň, Liberec, Olomouc**)

-   statutární město - město, které má právo se členit na městské části,
    úřad, který řídí statutární město = magistrát(dominikánské náměstí),
    v čele magistrátu primátor (**JUDr. Markéta Vaňková**), městské
    části určitou samosprávu, v čele městské části je starosta (29
    městských částí Brna)

-   funkce měst - nejrůznější (učebnice )

    -   průmysl

    -   obchod

    -   služby

    -   kultura

    -   územní správa

    -   obytná funkce

    -   speciální (lázně, sportovní střediska apod.)

-   problémy našich měst - velký

-   kulturní památky UNESCO - (atlas cestovní ruch)

    -   Praha

    -   Brno

    -   Olomouc

    -   Litomyšl

    -   Kutná Hora

    -   Lednicko-valtický areál

    -   Třebíč

    -   Telč

-   vývoj urbanistických zón - každá část města vzniká v určité době,
    těmto částem říkáme urbanistické zón (nejstarší = historické
    centrum - Poříčí (Staré brno) → nám. Svobody - Petrov, Špilberk →
    vznik průmyslové zóny, nová obytná zóna (nové dopravní tepny,
    služby) → po 2. sv. v. panelová sídliště)

#### Velké Brno

-   16.4.1919 - připojení 2 sousedních měst Králova Pole a Husovice a 21
    dalších obcí

-   125 000 ob → 264 000 ob (1930)

### Venkovská sídla

-   značný počet malých venkovských sídel (cca 3800 obcí s méně než 500
    obyv.)

-   velký počet těchto sídel v jižních Čechách a na ČMV

-   vesnice okrouhlého, návesního a řadového typu

-   úpadek venkovských sídel v pohraničí po 2.sv.v.

-   na vývoj venkovských sídel:

    -   zemědělská

    -   **obytná**

    -   udržování lidových tradic

    -   **rekreační** (druhé bydlení - chata, chalupa)

    -   údržba krajiny

    -   **revitalizace** - obnovování / oživování přírody, života na
        > venkově

    -   **suburbanizace** - ovlivňuje dnešní venkov, vznik satelitních
        > městeček, nové ob na vesnici, potřeba nové infrastruktury

-   problémy venkovských sídel

kotovice - voda, seno

došková střecha(), šindelová střecha, roubenky (dřeviny, dřevěné trámy s
mezery)

### Vývoj hospodářství

-   postavení českých zemí v rámci Rakouska-Uherka = nejvyspělejší země
    v RU (ložiska černého a hnědého uhlí - Chebská pánev, Mostecká,
    Sokolovská)

-   prvorepublikové Československo patřilo mezi průmyslové země světa a
    prosazovalo se na světových trzích - strojírenství, zbrojní,
    obuvnictví, pivo, sklářský

-   rozdíly v úrovni českých zemí a Slovenska

-   po r. 1948 dochází ke znárodnění průmyslu a kolektivizaci
    zemědělství, je zrušen soukromý sektor, ekonomika je centrálně
    plánována, preferování těžkého průmyslu, orientace na východní trhy,
    zaostávání za vyspělými státy, vysoká nezaměstnanost žen, zhoršování
    ekologické situace

-   po r. 1989 dochází k přechodu na tržní hospodářství a celkové
    transformaci ekonomiky

### Transformace ekonomiky

-   návrat k tržní ekonomice, omezení role státu

-   rozpad Československa a východních trhů

-   hledání nových odbytišť - orientace na dominantní EU

-   snížení dotací - hospodářské subjekty musí dobře fungovat a
    orientovat se na sebe

-   velká a malá privatizace, kuponová privatizace

-   restituce v zemědělství - návrat majetku původním majitelům

-   příliv zahraničního kapitálu - otevření hranic, zahraniční kapital u
    nás začal investovat - ne vše dopadlo dobře

-   od r. 2004 vývoz \> dovoz (dnes výrazně proexportní ekonomika)

-   útlum těžkého průmyslu a zemědělské výroby a pokles zaměstnanosti v
    těchto odvětvích (problémy Moravskoslezského kraje,), nezaměstnanost
    (kolem 3%; nedostatek sil)

-   posílení sektoru služeb - obrovské změny, opomíjený za komoušů

-   rozšíření dopravního napojení na Rakousko a Německo - pokusy

-   budování dálniční sítě a železničních koridorů - modernizace
    železnic a dopravy

-   rozvoj cestovního ruchu ← otevření hranic

-   větší otevření trhu vstupu do EU přináší své výhody i nevýhody

-   ekologická politika státu - zvýšení

#### Kupónová privatizace

-   2 vlny (začátek v 90. letech)

-   Václav Klaus, Tomáš Ježek

-   cílem privatizace odnárodnění majetku státu

-   Kuponové knížky - 10 kupónů

-   podniky - možnost odkoupit akcie, akcionáři očekávali výdělek (ze
    zisku)

-   levná koupě podniku a prodej do zahraničí → úmyslné zkrachování
    podniku v ČR

1.  vyhodnoť přiložené grafy a mapy z str. 46

### Vývoj a rozmístění průmyslu

-   základy průmyslu položeny již za RU, další rozvoj za 1. rep.

-   hlavní průmyslové oblasti se formují během 19. a 1.pol.20.stol. v
    severní polovině : ložiska uhlí, kontakty na Německo

-   dalšími centry průmyslu se stávají Plzeň, Brno a Zlín

-   hlavními odvětvími jsou strojírenství, textilní a potravinářský
    průmysl, obecně dominuje

-   výrazná změna odvětvové struktury po r. 1948, orientace na východní
    trhy, zaostávají za vývojem, devastace ŽP

-   industrializace Slovenska, lokalizace průmyslu do J Čech, ČMV a
    východní Moravy

-   k hlavním průmyslovým oblastem patří:

    -   Podkrušnohoří,

    -   pražská aglomerace

    -   Plzeň

    -   Jablonec-Liberec

    -   Pardubice-Hradec

    -   Ostravsko

    -   Olomouc

    -   Zlín

    -   Brněnsko

-   změny po r. 1989 -

    -   privatizace

    -   změny v odvětvové a územní struktuře (útlum těžkého průmyslu)

    -   vstup zahraničního kapitálu, modernizace

    -   využití levné, ale kvalifikované pracovní síly

    -   budování nových průmyslových zón (úplně jinak jak fabriky z
        > 19.stol.; Černovické terasy)

    -   značné investice jdou do elektrotechniky a automobilového
        > průmyslu (po r. 89 hlavním tahounem naší ekonomiky)

Těžba uhlí

-   od 70. let. 20.století se zvýšila težba uhlí

-   znečištění životního prostředí

-   zbořeno mnoho vesnic

-   územní **limity** → tendence útlumu těžby

-   současný stav - pokusy o prolomení limitů → silné snahy navýšit
    těžbu

-   energetická koncepce státu - největší zisk energie z uhlí

 Odvětví průmyslu
----------------

### Těžební průmysl, energetika

-   **pokles** podílu **těžby** **surovin** na objemu průmyslové výroby
    > (uhlí a stavební materiály)

-   černé uhlí - Ostravsko-karvinský revír (utlumuje se (těžba se
    > nevyplácí) → nárůst nezaměstnanosti)

-   hnědé uhlí - Severočeský a Sokolovský revír (tlačí na navýšení
    > těžby)

-   zastavena těžba uranu (Rožná - na Vysočině)

-   těžba stavebních surovin (žula, vápenec, sklářské písky, kaolin)

-   ropa a zemní plyn (Hodoninsko, Břeclávsko; musíme dovážet z Ruska;
    > zásobníky)

-   výroba elektrické energie:

    -   odsíření tepelných el.

    -   dominantní postavení ČEZ

    -   rozhodující podíl pevných paliv (uhlí)

    -   do r. 2020 13% podíl obnovitelných zdrojů na výrobě el. en.

-   tepelné elektrárny (53%)

-   jaderné elektrárny (33 %)

-   výroba energie z obnovitelných zdrojů (12 %)

    -   hydroelektrárny (3,5 %)

    -   větrné elektrárny

    -   boom solárních elektráren (patříme na špičku v Evropě)

    -   spalování biomasy (spalování dřeva, slámy)

-   naše energetika bude stát na tepelných elektrárnách

### Hutnictví

-   odvětví prošlo výrazným poklesem objemu výroby a zaměstnanosti

-   závislost na dovozu rud a kovů

-   nezdařená privatizace Poldi Kladno

    -   bylo to jedno z hlavních center u nás

    -   byla vytunelována

    -   dneska stojí

-   modernizace stávajících komplexů, opatření v oblasti ŽP

-   hlavním jádrem hutnictví je Ostravsko

    -   Arcelor Mittal (Nová Huť), Evraz Vítkovice Steel (v provozu už
        > jen Nové Vítkovice, obrovská továrna)

        -   část provozu dnes zastavena

        -   daly by se tam natáčet horrory

        -   snaží se, aby se to alespoň nějak využívalo (výstavy,
            > Colours aj.)

    -   Třinecké železárny

    -   Válcovny plechu Frýdek Místek (Arcelor Mittal)

    -   Bohumín

### Strojírenský průmysl

-   nejrozšířenější odvětví s mnohaletou tradicí

    -   od špendlíků až po bagry, všechno

    -   za režimu jsme zásobovali celý blok RVHP, ale záměrně nám
        > snižovali úroveň, abychom nebyli moc konkurence

-   kvalifikovaná pracovní síla

-   přímé zahraniční investice

-   změny ve výrobním programu, útlum nebo zánik výroby (např. problémy
    > velkých brněnských podniků)

    -   Brno: Královopolská strojírna (už jen skladiště), Zbrojovka
        > (přesunuta do Uherského Brodu), Zetor (museli utlumit výrobu),
        > První brněnská strojírna (udržela se, funguje)

-   dominuje výroba dopravních prostředků elektronika, elektrotechnika

-   konkurence je velká výroba dopravních prostředků

-   **automobily** (tahoun naší ekonomiky od roku 1989

    -   Škoda Mladá Boleslav (Volkswagen)

    -   TPCA Kolín (Toyota, Peugeot, Citroen)

    -   Hyundai Nošovice (u Frýdku-Místku)

    -   Tatra Kopřivnice (u Nového Jičína) (náklaďáky)

        -   dříve osobáky (velká spotřeba)

        -   po 1989 už jen náklaďáky

<!-- -->

-   **tramvaje**, **trolejbusy**, **lokomotivy** - Škoda Plzeň

-   **autobusy** - Iveco Vysoké mýto (Karosa), irisbus Libchavy

-   **letadla**

    -   Evektor Kunovice - ultralehká a sportovní letadla

    -   Zlín Aircraft (Otrokovice) - sportovní letadla Zlín

Hlavní centra chemického průmyslu v ČR - Kralupy nad Vltavou, Pardubice,
Napajedla, Česká rafinérská Litvínov(Petrochemie)

###  

### Chemický průmysl

-   odvětví, které prošlo rozsáhlou restrukturalizací vedoucí ke
    > zkvalitnění výrobků a ochraně ŽP

-   výroba je soustředěna do míst s vodními zdroji, uhlím a ropovody
    > (Litvínov, Kralupy nad Vltavou)

-   hlavními centry jsou severní Čechy, střední Čechy a některá centra
    > na moravě

-   řada odvětví jako je petrochemie, výroba hnojiv, barviv, plastů,
    > léků, gumárenský průmysl (težká - petrochemie, hnojiva; lehká
    > -léky, kosmetika)

-   k největším výrobcům patří např.:

    -   Chemopetrol Litvínov

    -   Setuza Ústí n. Labem

    -   Synthesia Pardubice

    -   Barum Otrokovice (Continental - německý koncern, který zastínil
        > Barum)

    -   Zentiva Praha

    -   Komárov u Opavy

učebnice spotřební průmysl

[[text z
https://www.odmaturuj.cz/zemepis/spotrebni-prumysl/]{.underline}](https://www.odmaturuj.cz/zemepis/spotrebni-prumysl/)

### **Spotřební průmysl** 

-   Spotřební průmysl představuje skupinu odvětví vyrábějících převážně
    předměty určené ke krátkodobé či dlouhodobé spotřebě.

-   Nejrozvinutějším odvětvím je textilní průmysl.Současný textilní
    průmysl využívá ve velké míře i chemické vlákna.Největšími středisky
    jsou Liberec(vlnařství), Náchod,Dvůr Králové a
    Krnov(bavlnářství).Mimo tuto oblast má bohatou tradici Vlnařské
    výroby Brno.

#### PRŮMYSL ODĚVNÍ

-   Na textilní průmysl navazuje průmysl oděvní.Jako mladší odvětví je
    koncentrován do několika velkých středisek. Tradičním centrem je
    Prostějov.

-   #### PRŮMYSL KOŽEDĚLNÝ A OBUVNICKÝ

-   Většinu surovin musíme dovážet.Největším střediskem obuvnického
    průmyslu je Zlín, Zruč nad Sázavou a Třebíč. Středisky kožené
    galanterie jsou Klatovy a Přerov.

-   #### DŘEVOZPRACUJÍCÍ PRŮMYSL

-   Dřevozpracující průmysl navazuje na hlavní oblasti těžby dřeva v
    horských a podhorských územích a na větší lesní celky. Zde nacházíme
    především závody na prvotní zpracování dřeva-pily. Na střední
    Moravě,ve středních a jižních Čechách je soustředěna výroba nábytku.
    Bohatou tradici i světové jméno má výroba tužek, zápalek, dřevěných
    hudebních nástrojů.

-   #### PRŮMYSL PAPÍRU A CELULOZY

-   Výroba papíru a celulozy vyžaduje velké množství vody, a proto tyto
    závody nacházíme vždy v blízkosti vodních toků. K největším závodům
    patří kombinát v Paskově u Ostravy, ve Větřní na horním toku Vltavy
    a ve Štětí na středním Labi. Odpadky vznikající při výrobě značně
    znehodnocují kvalitu vody.

-   #### SKLÁŘSKÝ PRŮMYSL

-   Hlavní oblastí sklářské výroby jsou stále severní a severozápadní
    Čechy.Světoznámým střediskem výroby bižuterie je Jablonec nad Nisou.

-   #### PRŮMYSL PORCELÁNU A KERAMIKY

-   Výroba porcelánu je soustředěna do okolí vysoce kvalitních kaolinů v
    západních Čechách. Většina užitkového porcelánu se vyrábí v okolí
    Karlových Varů.

-   #### Keramický průmysl

-   Vyrábí z jílu, lupků a dalších surovin. Je rozvinut především v
    okolí Plzně a Rakovníka, na Moravě jsou největším střediskem Velké
    Opatovice.

Zemědělství
-----------

### Zemědělská výroba do r. 1989

-   násilná kolektivizace po r. 1948 (JZD, státní statky - společné
    hospodářství)

    -   sebrali i těm co nechtěli - vymysleli si obvinění - popravení
        > lidé, zatčeni

    -   Jednotná zemědělská družstva

-   velkoplošné hospodaření s cílem vyprodukovat co nejvíce potravin a
    zajistit soběstačnost

-   pěstování plodin v oblastech s nevhodnými přírodními podmínkami -
    nerentabilní výroba

-   nadměrné užívání průmyslových hnojiv - dopad na ŽP

-   dotování neprosperujících podniků (podniky nesměli zkrachovat)

-   přezaměstnanost (cca 600 tis.; dnes pod 200 tis.; → začala mizet
    polní zvěř)

### Transformace zemědělství

-   změny ve vlastnických poměrech (soukromníci, družstva, s.r.o, a.s.;
    většina půdy v čr vlastní podnikatelé - nezajímají se o půdu, chtějí
    zisk)

-   pokles zaměstnanosti (cca 100 tis., vývoj věkové struktury - ubývá
    mladých lidí)

-   snížení dotací

-   změny v prostorovém rozmístění zemědělské výroby (rostlinná výroba
    tam kde jsou pro to podmínky)

-   změny ve struktuře půdního fondu (snížení rozlohy orné půdy, podpora
    zatravňování a zalesňování - tam kde nejsou vhodné podmínky pro
    pěstování)

-   ekozemědělství, agroturistika (zemědelství bez chemie, šetrný
    přístup ke krajině a chovu domácích zvířat → biopotraviny; napojení
    se na cestovní ruch, výhodné v oblastech hojně navštěvované turisty)

-   celkový pokles zemědělské produkce, zahraniční konkurence, problém s
    odbytem (problém s údržbou krajiny)

-   cesta do budoucna - rodinné firmy (motivace lidí hospodařit)

-   přírodní zdroje 12-13

-   půdní fond 47

### Zemědělská výroba

-   celkové snížení objemu zemědělské výroby, zvláště živočišné

-   vliv přírodních podmínek na zaměření výroby (poslední roky je sucho;
    > voda **20-30cm pod povrchem** je skoro vyčerpána - kořeny
    > zemědělských plodin → nárůst cen potravin)

-   změny v podhorských a horských oblastech

-   problémem jsou nízké výkupní ceny mléka a obilí

-   #### Rostlinná výroba

-   zajišťuje potraviny pro obyvatelstvo a krmivo pro dobytek

-   cca polovina osevních ploch připadá na obiloviny, z toho především
    > na pšenici ječmen

-   nárůst produkce olejnin, především řepky (výroba bionafty)

-   pokles produkce brambor, cukrové řepy, ovoce a zeleniny

-   vinařské oblasti na Znojemsku, Břeclavsku, Hodonínsku, Mělnickou a
    > Litoměřicku (nejlepší přírodní podmínky)

<!-- -->

-   chmelařské oblasti na Lounsku a Žatecku

-   sucho → zdražení potravin

####  Živočišná výroba

-   výrazný pokles chovu skotu (rostoucí ceny masa, nemoc šílených
    > krav), v nížinách se zaměřuje hlavně na mléko, v podhorských
    > oblastech na maso (extenzivní chov)

-   chov prasat soustředěn hlavně do oblastí s dostatkem krmiva a větším
    > odbytem, vepřové je v Česku nejoblíbenější, přesto stavy prasat u
    > nás stále klesají - konkurence evnějšího masa z dovozu

-   určitý nárůst chovu drůbeže, ovcí a koní

-   tradici má chov ryb a včelařství (vykrádání rybníků a úlů)

### Zemědělské výrobní oblasti

-   **Kukuřičná**

    -   Dyjskosvratecký a Dolnomoravský úval

    -   úrodné černozemě, tepelné, ale suché klima

    -   pšenice, kukuřice, teplomilné druhy ovoce, zeleniny a vinné révy

-   **Řepařská**

    -   Hornomoravský úval, Polabí, dolní Povltaví a dolní Poohří

    -   černozemě, hnědozemě, teplé mírně vlhké klima

    -   pšenice, sladovnický ječmen, cukrovka, zelenina

-   **Obilnářská-bramborářská**

    -   vrchoviny, především ČMV

    -   hnědé půdy, podzoly, chladnější vlhčí klima

    -   brambory, krmná pšenice a ječmen, oves, píciny

-   **Pícninářská**

    -   nejvyšší části vrchovin a horské oblasti

    -   hnědé půdy, podzoly, gleje, chladné vlhké klima

    -   převaha luk a pastvin pro živočišnou výrobu

? - broskve, meruňky; okurky, papriky, rajčata, melouny

text z
[[http://www.vitejtenazemi.cz/cenia/index.php?p=pudni\_fond\_cr&site=puda]{.underline}](http://www.vitejtenazemi.cz/cenia/index.php?p=pudni_fond_cr&site=puda)

### Půdní fond ČR

-   Celková výměra ČR je 7, 9 mil. ha. Největší díl -- více než polovinu
    (54 %) - využíváme k zemědělské činnosti, tedy k [[produkci potravin
    a
    krmiv]{.underline}](http://www.vitejtenazemi.cz/cenia/index.php?p=vyuzivani_pudy_v_zemedelstvi_a_zemedelska_produkce&site=puda).
    Jedná se o činnost provozovanou na orné půdě, v sadech, vinicích a
    chmelnicích a trvalých travních porostech (jako jsou louky a
    pastviny, které zaujímají cca 1 mil. ha). Tyto plochy tvoří tzv.
    zemědělský půdní fond (cca 4,3 mil. ha). Orná půda pak zaujímá cca
    38 % zemědělského půdního fondu. Asi třetinu půdy (33 %) ČR
    pokrývají lesy, které využíváme k rekreaci nebo jako zdroj surovin a
    léčiv (dřevo, byliny). Přibližně 15 % půdy zaujímají [[chráněná
    území]{.underline}](http://vitejtenazemi.cenia.cz/krajina/index.php?article=63).
    Dále půdy využíváme jako pozemky pro stavbu obydlí a různých
    zařízení, které slouží k výrobním účelům (haly) nebo ke sportovní a
    rekreační činnosti (hřiště, sportovní a relaxační centra).

-   Zemědělská půda se u nás nachází převážně v méně příznivých půdně
    klimatických podmínkách. Nadprůměrně úrodných orných půd je u nás
    přibližně 40 %, průměrně a podprůměrně úrodných půd je 54 % a pro
    zemědělskou činnost zcela nevhodných ploch je cca 6 %.

-   Více než 20 % zemědělské půdy se v ČR rozkládá v nadmořské výšce nad
    500 m n. m., z celoevropského hlediska české zemědělství náleží k
    typu podhorskému až horskému. Oblasti s vyšší nadmořskou výškou lze
    považovat za méně příznivé pro zemědělskou činnost. Díky poměrně
    vysoké hustotě zalidnění ČR má však zemědělská činnost tradici i v
    těchto oblastech.

![](media/image1.png){width="2.7083333333333335in"
height="1.8541666666666667in"}

kolektivizace

-   dobrovolný návrh - neujal se

-   → násilý, vyhodí z práce, děti nebudou moc studovat

-   dávky, nedalo se přežít bez JZD

1.  Jaké jsou současné trendy v dopravě ČR?

2.  V atlase vyhledej hlavní dálniční tahy a železniční koridory v ČR

Doprava a spoje
---------------

### Vývoj a změny dopravní sítě na území ČR

-   dominantní postavení silniční a železniční dopravy

-   základy dopravní sítě položeny za Rakouska-Uherska

-   změny v orientaci přepravních proudů (po 2.sv. orientace na sovětský
    > svaz; po 89 zlepšování propojení na Německo a Rakousko)

-   hustotou dopravní sítě patříme na přední místa v Evropě

-   před r. 1989 značně dotována nákladní doprava (podporován těžký
    > průmysl)

-   po r. 89 je nutno:

    -   modernizovat dopravu

    -   rozšířit napojená na okolní státy

    -   především na Rakousko a Německo

    -   prioritou jsou dálnice, rychlostní silnice a železniční koridory

-   tranzitní poloha ČR - významné evropské silnice a železnice (E)

-   omezení dotací

-   částečná privatizace

### 2. Železniční doprava

-   nerovnoměrné rozmístění železniční sítě

-   značná hustota (celková délka asi 9 600 km), ale málo
    elektrifikovaných a vícekolejných tratí (cca třetina)

-   malá propustnost a nízké rychlosti (max 160km/h, pendolino - Praha -
    Ostrava; žádné vysokorychlostní železnici)

-   nerentabilní provoz na vedlejších tratích

-   nízká úroveň kultury cestování

-   pokles objemu nákladů (útlum těžkého průmyslu; po r. 89 **pokles
    přepravy osob**; poslední dobou pokles ceny jízdného - dotace)

-   ekologičtější, ale dražší provoz železnic

-   rychlostní koridory: (jsou nejvíce vytížené a největší význam; staré
    ale průběžně modernizované - mapř nádraží; nemáme vysokorychlostní
    žel.)

1.  Děčín - Praha - Pardubice - Brno - Břeclav

2.  Petrovice - Ostrava - Přerov - Břeclav

3.  Ostrava - přerov - Praha - Plzeň - Cheb

4.  Děčín - Praha - Č. Budějovice

### 3. Silniční doprava

-   hustá, ale zastaralá silniční síť

-   nutno budovat ucelený systém dálnic a rychlostních silnic
    (potřebujeme 2000km, máme pouze 1300)

-   změny 1.1.2016 (zrušeny rychlostní silnice - část převedena na
    dálnice a část na silnice 1. třídy)

-   cca 1200 km dálnic

-   chybí obchvaty kolem měst (změna po 89 ale silnice na to nebyli
    stavěné)

-   problém s kamionovou dopravou

-   zavedení mýtného

-   znečištění ovzduší

### 4. letecká doprava

-   nárůst přepravy osob po r. 1989

-   rozdíl mezi vnitrostátní a mezistátní přepravou (vnitrostátní malý
    význam; nejvýznamnější u nás ČSA )

-   letiště v Praze, Brně (prodělečná - velké dotace), Ostravě a
    Karlových Varech

-   charterové lety - objednávají cestovní kanceláře

-   letiště Brno-Tuřany

### 4. Říční doprava

-   podmínky pro provozování říční dopravy v ČR

-   využití řek Labe, Vltavy (Praha - Mělník, menší nákladní lodě;
    parníčky i Praha - ?) a Moravy (baťův kanál - v 1935-38 vybudován
    podél řeky, Otrokovice - Rohatec, 50 km; na hodonínsku se těžil
    ?lignit? - hnědé uhlí, Baťa potřeboval jako polivo pro své továrny,
    po r 48 podniky znárodněny, doprava zchátrala)

-   velký problém - málo vody- suché období

1.  jaké znáš předpoklady (podmínky) cestovního ruchu? -

2.  Do kterých zemí cestujeme nejčastěji? - Chorvatsko, Slovensko,
    Rakousko

3.  Ze kterých zemí k nám přijíždí nejvíce návštěvníků? - Slovensko,
    Německo, Rusko

Cestovní ruch a rekreace
------------------------

-   změny v CR po r. 1989 (nové destinace, CK, agentury)

    -   Čedok, sport turist, - 3 cestovky

-   předpoklady (podmínky) CR

    -   přírodní - národní parky a biosférické rezervace

    -   kulturní - spousta, za komunistů byly památky poničeny

    -   politické - politicky stabilní země (bezpečnost)

    -   ekonomické - mám na to / nemám na to; ekonomický stav dané země

    -   dopravní infrastruktura - jak se tam dopravím, jaké je na daném
        > místě

    -   materiálně-technické - sport, vybavení hotelů,

    -   stav životního prostředí

CR - DCR

ZCR - ACR

\- PCR

### DCR - 

-   tradice má horská turistika, lyžování, návštěva historických památek

-   rozšířená forma druhého bydlení (chaty, chalupy) - posíleno za
    minulého režimu, nemohli cestovat; po 89 cestování do zahraničí

-   otázka kvality nabízených služeb

### ACR

-   nejnavštěvovanější destinací je Praha, přírodní a kulturní památky
    UNESCO a lázně (Karlovy Vary, Františkovy lázně, Luhačovice)

-   nejvíce návštěvníků přijíždí z Německa, Británie, Ruska, Itálie,
    Nizozemska

-   narůstá podíl turistů z Asie (Čína, jižní Korea) a z Latinské
    Ameriky (Brazílie, Mexiko)

-   ČR navštíví ročně kolem xx turistů

-   nejnavštěvovanější kraje jsou Praha, Karlovarský a Jihomoravský

-   nutno zlepšit nabídku a kvalitu služeb a propagaci

### PCR

-   nejčastěji navštěvované země jsou Chorvatsko, Slovensko, itálie,
    Řecko, Španělsko

-   turisté cestují hlavně k moři, za poznávacími zajezdy a lyžování do
    Alp

-   narůstá zájem o cesty do exotických zemí

-   zvyšují se nároky klientů na kvalitu poskytovaných služeb

Zahraniční obchod
-----------------

-   patří k nejvýznamnějším vnějším vztahům státu

-   proexportní ekonomika - rizikové

-   po r. 1989 značné otevření trhu a závislost na vývozu - silná
    zahraniční konkurence

-   vývoj bilance ZO - aktivní = vývoz \> dovoz

-   export ovlivňován stabilitou koruny

-   struktura ZO - to co vyvážíme a dovážíme, pestrá, potraviny,
    strojírenské produkty

    -   rozpad východních trhů

    -   hledání odbytišť

    -   orientace na EU - 86% exportu

    -   stanovení prioritních zemí
