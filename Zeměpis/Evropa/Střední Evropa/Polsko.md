---
title: Polsko
---
**Polsko**
----------

1.  Vyhledejte na mapě: Pomořanskou a Mazurskou jezerní plošinu,
    > Velkopolskou, Slezskou a Mazovskou nížinu, Lublinskou vrchovinu,
    > Malopolskou vrchovinu, pohraniční pohoří s ČR a Slovenskem,
    > Bělověžský prales, Vislu, Odru, Wartu, jezero Sniardwy

2.  Nastudujte si územní změny Polska

<!-- -->

1.  ### **Přírodní podmínky**

    -   převážnou část Polska tvoří nížiny a jezerní plošiny modelované
        > ledovcem, v jižní části nastupují vrchoviny a na hranici s ČR
        > a Slovenskem pohoří

    -   [[viz
        > Balt]{.underline}](https://drive.google.com/open?id=1JMEGzz5TWDXPmEXreQ0vRnv0Qu9vzyY5Rd9BEt6LReg)

    -   sever a střed vyplňují nížiny, k jihu nastupují plošiny, na jihu
        > pohoří

    -   klima přechází od severu z oceánského ke kontinentálnímu

    -   většina řek odvádí vodu do Baltu, nejdelší je Visla, dopravní
        > tepnou je Odra, největším jezerem je Sniardwy v Mazurské
        > plošině (domácí cestovní ruch)

### **Přírodní zajímavosti**

#### **Bělověžský národní park**

-   UNESCO

-   chráněné území při polsko-běloruské hranici

-   jedná se o nížinný typ pralesa s převahou listnatých dřevin,
    > pozoruhodné jsou staleté duby

-   v parku žije populace zubra evropského nebo zpětně vyšlechtění
    > tarpani (divoký kůň)

#### **Sloviňský národní park**

-   UNESCO

-   národní park rozkládající se v severním Polsku na pobřeží Baltu

-   součást parku jsou pohyblivé písečné duny, jezera, občasně sycená
    > mořskou vodou při bouřích, rašeliniště, louky a lesy

-   z rostlin jsou zajímavé halofytní druhy (na zasolené půdě), z
    > živočichů los, bobr evropský, tuleň nebo sviňucha

1.  na mapách v atlase 52,53 a pomocí textu v učebnici na str. 32
    > vyhodnoť územní změny v polsku

<!-- -->

2.  ### **Historie, obyvatelstvo, administrativní členění**

    -   polsko-litevské knížectví

    -   německá kolonizace

        -   začala už ve středověku

        -   Němci se usazovali v západní a severní části Polska

        -   později Němci odsunuti

    -   [[trojí dělení Polska]{.underline}](#trojí-dělení-polska)
        > (nastudovat)

    -   změna hranic po skončení 2. světové války

    -   hnutí Solidarita

        -   vedl Lech Walesa

        -   strana, která vedla boj proti komunismu

    -   husté osídlení na jihu země v pásu od Wroclavi po Katovice

    -   k hlavním centrům patří města Varšava, Poznaň, Lódž, Wroclaw,
        > Katowice, Krakow, Gdaňsk, Štětín

    -   katovická aglomerace -- kvůli uhlí

    -   rozdíly v životní úrovni ve městech a na venkově a v západní a
        > východní části státu (západní vyspělejší)

    -   administrativně se Polsko člení na 16 vojvodství

    -   Polsko se s Ruskem moc nemá rádo

    -   zápis o hospodářství Polska

    -   Gdaňsk

        -   největší přístav

        -   na severu Polska

        -   v minulosti fakt cool

        -   dneska už nic moc

    -   Malbork

        -   křižácká pevnost

        -   na severu Polska

        -   červené cihly

    -   Krakov

        -   hrad Wawel (sídlo králů)

    -   Lešno

        -   Ámos (po bitvě na Bíle hoře)

    -   Toruň

        -   město na severu Polska

        -   narodil se tam Mikuláš Koperník

    -   Osvětim-Březinka

    -   solné doly Wieliczka

        -   už se tam sůl netěží :((

        -   ale jsou tam lázně :))

    -   Zakopane

        -   hlavní středisko zimních sportů

        -   skoky na lyžích

        -   Karpaty

### **Ekonomika**

-   surovinové zásoby na jihu Polska -- uhlí, síra, zinek, měď

-   soustředění těžkého průmyslu v oblasti Horno- a Dolno-slezské pánve

-   **Katovická aglomerace** -- největší ve východní části střední
    > Evropy, problémy s útlumem těžkého průmyslu

-   příliv zahraničních investic

-   značná zemědělská produkce, ale celková zaostalost, vysoký podíl
    > pracujících v zemědělství (cca 17 %)

-   velký vývozce brambor a masa

-   ne všechny produkty úplně okej

### Trojí dělení polska

-   šlechta moc moci

-   král žádnou

-   malá zahraniční politika

1.  Dělení

> 1772

-   Prusko: S Polska

-   Rakousku: část jihu polska

-   Rusku: Východ Polska

2.  Dělení

1793

-   Prusko:

-   Rusko:

-   povstání T. Kosciuszka

3.  Dělení

1795

-   definitivně obnoveno po 1.sv. válce

-   celkové dělení:

    -   Prusko: S Polska

    -   Rakousko: J Polska

    -   Rusko: V Polska
